var user_id;
var city_id;
var group_id;

$(document).on('submit' , '.add_user_form' , function(e){
    e.preventDefault();
    add_user_form();
});


$(document).on('submit' , '.edit_user_form' , function(e){
    e.preventDefault();
    edit_user_form();
});

$(document).on('submit' , '.add_city_form' , function(e){
    e.preventDefault();
    add_city_form();
});

$(document).on('submit' , '.edit_city_form' , function(e){
    e.preventDefault();
    edit_city_form();
});

$(document).on('submit' , '.edit_group_form' , function(e){
    e.preventDefault();
    edit_group_form();
});

$(document).on('submit' , '.add_group_form' , function(e){
    e.preventDefault();
    add_group_form();
});

// submit new user
function add_user_form()
{    
    var modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'><img class=\'uk-margin-top\' src=\'assets/img/spinners/spinner.gif\' alt=\'\'>');
    var formdata = new FormData($('.add_user_form')[0]);
    $.ajax({
        type : 'POST',
        url  : base_url + '/add-user',
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        data : formdata,
        success: function(data)
        {
            modal.hide();
            if (data['state'] == 'fail') {
                $('#notification_area').html(data['message']);
                $('#notification_area').fadeIn();
                setTimeout( function() { $('#notification_area').fadeOut(); } , 2000);
            }else{
                $('#users_table').html(data['users']);
                UIkit.modal("#modal_add").hide();
                $('.add_user_form')[0].reset();
                $('#role')[0].selectize.clear();
                $(".md-input-success").each(function(){
                    $(this).removeClass('md-input-success');
                });
                $(".md-input-filled").each(function(){
                    $(this).removeClass('md-input-filled');
                });
            }
        }
    });
}

//preview Image
$(document).on('change' , '#user_image' , function(){
    if (this.files && this.files[0]) {
        var reader = new FileReader();


        reader.onload = function (e) {
            $('#image_preview').attr('src', e.target.result);
        }

        reader.readAsDataURL(this.files[0]);
    }
});

// get data of user and put it in the edit form
$(document).on('click' , '.get_edit_modal' , function(){
    user_id = $(this).prop('name');
    $.ajax({
        type : 'GET',
        url  : base_url + '/edit-user/' + user_id,
        success: function(data)
        {
            $('#modal_edit').html(data['modal']);
            altair_forms.init();
        }
    });
});

// submit changes to a user
function edit_user_form()
{
    var modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'><img class=\'uk-margin-top\' src=\'assets/img/spinners/spinner.gif\' alt=\'\'>');
    var formdata = new FormData($('.edit_user_form')[0]);
    $.ajax({
        type : 'POST',
        url  : base_url + '/edit-user/' + user_id,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        data : formdata,
        success: function(data)
        {
            modal.hide();
            $('#users_table').html(data['users']);
            UIkit.modal("#modal_edit").hide();
        }
    });
}

//preview image in edit
$(document).on('change' , '#user_image_edit' , function(){
    if (this.files && this.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image_preview_edit').attr('src', e.target.result);
        }

        reader.readAsDataURL(this.files[0]);
    }
});


$(function() {
    // makes instance from component
    var unit_type_selectize = $("#unit_type")[0].selectize;
    var floor_selectize = $("#floor")[0].selectize;
    var finishing_selectize = $("#finishing")[0].selectize;
    var payment_type_selectize = $("#payment_type")[0].selectize;
    var objective_selectize = $("#objective")[0].selectize;
    var feature_selectize = $("#feature")[0].selectize;

    // unit type
    unit_type_selectize.on("item_add" , function(value, item){
        $.ajax({
            type : 'POST',
            url  : base_url + '/add-setting',
            data :
            {
                table : $('.unit_type').prop('id'),
                value : value
            }
        });
    });

    unit_type_selectize.on("item_remove" , function(value, item){
        $.ajax({
            type : 'POST',
            url  : base_url + '/delete-setting',
            data :
            {
                table : $('.unit_type').prop('id'),
                value : value,
                col   : 'unit_type'
            },
            success : function(data){
                if(data=='1'){
                    unit_type_selectize.addOption({value: value,title:value});
                    unit_type_selectize.addItem(value , 1);
                    UIkit.notify({message: 'This Value is already in use. Cannot Delete!' , pos: 'top-center'});
                }
            }
        });
    });
    // unit type

    // floor
    floor_selectize.on("item_add" , function(value, item){
        $.ajax({
            type : 'POST',
            url  : base_url + '/add-setting',
            data :
            {
                table : $('.floor').prop('id'),
                value : value
            }
        });
    });

    floor_selectize.on("item_remove" , function(value, item){
        $.ajax({
            type : 'POST',
            url  : base_url + '/delete-setting',
            data :
            {
                table : $('.floor').prop('id'),
                value : value,
                col   : 'floor'
            },
            success : function(data){
                if(data=='1'){
                    floor_selectize.addOption({value: value,title:value});
                    floor_selectize.addItem(value , 1);
                    UIkit.notify({message: 'This Value is already in use. Cannot Delete!' , pos: 'top-center'});
                }
            }
        });
    });
    // floor

    // finishing
    finishing_selectize.on("item_add" , function(value, item){
        $.ajax({
            type : 'POST',
            url  : base_url + '/add-setting',
            data :
            {
                table : $('.finishing').prop('id'),
                value : value
            }
        });
    });

    finishing_selectize.on("item_remove" , function(value, item){
        $.ajax({
            type : 'POST',
            url  : base_url + '/delete-setting',
            data :
            {
                table : $('.finishing').prop('id'),
                value : value,
                col   : 'finishing'
            },
            success : function(data){
                if(data=='1'){
                    finishing_selectize.addOption({value: value,title:value});
                    finishing_selectize.addItem(value , 1);
                    UIkit.notify({message: 'This Value is already in use. Cannot Delete!' , pos: 'top-center'});
                }
            }
        });
    });
    // finishing

    // payment_type
    payment_type_selectize.on("item_add" , function(value, item){
        $.ajax({
            type : 'POST',
            url  : base_url + '/add-setting',
            data :
            {
                table : $('.payment_type').prop('id'),
                value : value
            }
        });
    });

    payment_type_selectize.on("item_remove" , function(value, item){
        $.ajax({
            type : 'POST',
            url  : base_url + '/delete-setting',
            data :
            {
                table : $('.payment_type').prop('id'),
                value : value,
                col   : 'payment_style'
            },
            success : function(data){
                if(data=='1'){
                    payment_type_selectize.addOption({value: value,title:value});
                    payment_type_selectize.addItem(value , 1);
                    UIkit.notify({message: 'This Value is already in use. Cannot Delete!' , pos: 'top-center'});
                }
            }
        });
    });
    // payment_type


    // objective
    objective_selectize.on("item_add" , function(value, item){
        $.ajax({
            type : 'POST',
            url  : base_url + '/add-setting',
            data :
            {
                table : $('.objective').prop('id'),
                value : value
            }
        });
    });

    objective_selectize.on("item_remove" , function(value, item){
        $.ajax({
            type : 'POST',
            url  : base_url + '/delete-setting',
            data :
            {
                table : $('.objective').prop('id'),
                value : value,
                col   : 'objective'
            },
            success : function(data){
                if(data=='1'){
                    objective_selectize.addOption({value: value,title:value});
                    objective_selectize.addItem(value , 1);
                    UIkit.notify({message: 'This Value is already in use. Cannot Delete!' , pos: 'top-center'});
                }
            }
        });
    });
    // objective


    // feature
    feature_selectize.on("item_add" , function(value, item){
        $.ajax({
            type : 'POST',
            url  : base_url + '/add-setting',
            data :
            {
                table : $('.feature').prop('id'),
                value : value
            }
        });
    });

    feature_selectize.on("item_remove" , function(value, item){
        $.ajax({
            type : 'POST',
            url  : base_url + '/delete-setting',
            data :
            {
                table : $('.feature').prop('id'),
                value : value,
                col   : 'feature'
            },
            success : function(data){
                if(data=='1'){
                    feature_selectize.addOption({value: value,title:value});
                    feature_selectize.addItem(value , 1);
                    UIkit.notify({message: 'This Value is already in use. Cannot Delete!' , pos: 'top-center'});
                }
            }
        });
    });
    // objective

});




$(document).on('keyup' , '.floor' , function(e){
    var code = e.keyCode || e.which;
    if(code == 13) {
        $.ajax({
            type : 'POST',
            url  : base_url + '/add-setting',
            data :
            {
                table : $('.floor').prop('id'),
                value : $('.floor').val()
            },
            success: function(data){}
        });
    }
});

$(document).on('keyup' , '.finishing' , function(e){
    var code = e.keyCode || e.which;
    if(code == 13) {
        $.ajax({
            type : 'POST',
            url  : base_url + '/add-setting',
            data :
            {
                table : $('.finishing').prop('id'),
                value : $('.finishing').val()
            },
            success: function(data){}
        });
    }
});

$(document).on('keyup' , '.payment_type' , function(e){
    var code = e.keyCode || e.which;
    if(code == 13) {
        $.ajax({
            type : 'POST',
            url  : base_url + '/add-setting',
            data :
            {
                table : $('.payment_type').prop('id'),
                value : $('.payment_type').val()
            },
            success: function(data){}
        });
    }
});

$(document).on('keyup' , '.objective' , function(e){
    var code = e.keyCode || e.which;
    if(code == 13) {
        $.ajax({
            type : 'POST',
            url  : base_url + '/add-setting',
            data :
            {
                table : $('.objective').prop('id'),
                value : $('.objective').val()
            },
            success: function(data){}
        });
    }
});

$(document).on('keyup' , '.feature' , function(e){
    var code = e.keyCode || e.which;
    if(code == 13) {
        $.ajax({
            type : 'POST',
            url  : base_url + '/add-setting',
            data :
            {
                table : $('.feature').prop('id'),
                value : $('.feature').val()
            },
            success: function(data){}
        });
    }
});

// submit new city with its districts
function add_city_form()
{
    var modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'><img class=\'uk-margin-top\' src=\'assets/img/spinners/spinner.gif\' alt=\'\'>');
    $.ajax({
        type : 'POST',
        url  : base_url + '/add-city',
        data : $('.add_city_form').serialize(),
        success: function(data)
        {
            modal.hide();
            if(data['state'] == 'fail'){
                $('#notification_area').html(data['message']);
                $('#notification_area').fadeIn();
                setTimeout( function() { $('#notification_area').fadeOut(); } , 2000);
            }else{
                $('#cities_table').html(data['cities_table']);
                UIkit.modal("#modal_city_add").hide();
                $('.add_city_form')[0].reset();
                $('#districts_add')[0].selectize.clear();
                $(".md-input-success").each(function(){
                    $(this).removeClass('md-input-success');
                });
                $(".md-input-filled").each(function(){
                    $(this).removeClass('md-input-filled');
                });
            }
        }
    });
}

var districts_selectize;
// get data of city and put it in the edit form
$(document).on('click' , '.get_edit_city_modal' , function(){
    city_id = $(this).prop('name');
    $.ajax({
        type : 'GET',
        url  : base_url + '/edit-city/' + city_id,
        success: function(data)
        {
            $('#modal_city_edit').html(data['modal']);
            altair_forms.init();
            altair_product_edit.init();
            districts_selectize = $("#districts_edit")[0].selectize;
            // districts edit
            districts_selectize.on("item_add" , function(value, item){
                var modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'><img class=\'uk-margin-top\' src=\'assets/img/spinners/spinner.gif\' alt=\'\'>');
                $.ajax({
                    type : 'POST',
                    url  : base_url + '/edit-city/' + city_id,
                    data :
                    {
                        value : value,
                        name  : 'district_add'
                    },
                    success: function(data)
                    {
                        modal.hide();
                        $('#cities_table').html(data['cities_table']);
                    }
                });
            });

            districts_selectize.on("item_remove" , function(value, item){
                var modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'><img class=\'uk-margin-top\' src=\'assets/img/spinners/spinner.gif\' alt=\'\'>');
                $.ajax({
                    type : 'POST',
                    url  : base_url + '/edit-city/' + city_id,
                    data :
                    {
                        value : value,
                        name  : 'district_remove'
                    },
                    success: function(data)
                    {
                        modal.hide();
                        $('#cities_table').html(data['cities_table']);
                        if(data['success']=='2'){
                            districts_selectize.addOption({value: value,title:value});
                            districts_selectize.addItem(value , 1);
                            UIkit.notify({message: 'This Value is already in use. Cannot Delete!' , pos: 'top-center'});
                        }
                    }
                });
            });
            // districts edit
        }
    });
});

$(document).on('click' , '.get_edit_group_modal' , function(){
    group_id = $(this).prop('name');
    $.ajax({
        type : 'GET',
        url  : base_url + '/edit-group/' + group_id,
        success: function(data)
        {
            $('#modal_group_edit').html(data['modal']);
            altair_forms.init();
            altair_product_edit.init();
        }
    });
});

$(document).on('change' , '.city_name_edit' , function(){
    $.ajax({
        type : 'POST',
        url  : base_url + '/edit-city/' + city_id,
        data : {
            value : $('.city_name_edit').val(),
            name  : 'city_name'
        },
        success: function(data)
        {
            $('#cities_table').html(data['cities_table']);
        }
    });
});



function edit_group_form()
{
    var modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'><img class=\'uk-margin-top\' src=\'assets/img/spinners/spinner.gif\' alt=\'\'>');
    $.ajax({
        type : 'POST',
        url  : base_url + '/edit-group/' + group_id,
        data : $('.edit_group_form').serialize(),
        success: function(data)
        {
            modal.hide();
            $('#groups_table').html(data['groups_table']);
            UIkit.modal("#modal_group_edit").hide();
        }
    });
}


$(document).on('click' , '.get_delete_modal' , function(){
    user_id = $(this).prop('name');
    $.ajax({
        type : 'GET',
        url  : base_url + '/delete-user/' + user_id,
        success: function(data)
        {
            $('#modal_delete').html(data['modal']);
        }
    });
});

$(document).on('click' , '.get_delete_city_modal' , function(){
    city_id = $(this).prop('name');
    $.ajax({
        type : 'GET',
        url  : base_url + '/delete-city/' + city_id,
        success: function(data)
        {
            $('#modal_delete').html(data['modal']);
        }
    });
});

$(document).on('click' , '.get_delete_group_modal' , function(){
    group_id = $(this).prop('name');
    var modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'><img class=\'uk-margin-top\' src=\'assets/img/spinners/spinner.gif\' alt=\'\'>');
    $.ajax({
        type : 'GET',
        url  : base_url + '/delete-group/' + group_id,
        success: function(data)
        {
            modal.hide();
            $('#modal_delete').html(data['modal']);
        }
    });
});

$(document).on('submit' , '.delete_user_form' , function(e){
    var modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'><img class=\'uk-margin-top\' src=\'assets/img/spinners/spinner.gif\' alt=\'\'>');
    e.preventDefault();
    $.ajax({
        type : 'POST',
        url  : base_url + '/delete-user/' + user_id,
        success: function(data)
        {
            modal.hide();
            $('#users_table').html(data['users']);
            UIkit.modal("#modal_delete").hide();
        }
    });
});

$(document).on('submit' , '.delete_city_form' , function(e){
    var modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'><img class=\'uk-margin-top\' src=\'assets/img/spinners/spinner.gif\' alt=\'\'>');
    e.preventDefault();
    $.ajax({
        type : 'POST',
        url  : base_url + '/delete-city/' + city_id,
        success: function(data)
        {
            $('#cities_table').html(data['cities_table']);
            modal.hide();
            UIkit.modal("#modal_delete").hide();
            if(data['success'] == "2"){
                UIkit.notify({message: 'This City is already in use. Cannot Delete!' , pos: 'top-center'});
            }
        }
    });
});

$(document).on('submit' , '.delete_group_form' , function(e){
    e.preventDefault();
    $.ajax({
        type : 'POST',
        url  : base_url + '/delete-group/' + group_id,
        success: function(data)
        {
            $('#groups_table').html(data['groups_table']);
            UIkit.modal("#modal_delete").hide();
        }
    });
});


function add_group_form()
{
    var modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'><img class=\'uk-margin-top\' src=\'assets/img/spinners/spinner.gif\' alt=\'\'>');
    $.ajax({
        type : 'POST',
        url  : base_url + '/add-group',
        data : $('.add_group_form').serialize(),
        success: function(data)
        {
            modal.hide();
            $('#groups_table').html(data['groups']);
            UIkit.modal("#modal_group_add").hide();
            $('.add_group_form')[0].reset();
            $('#group_type')[0].selectize.clear();
            $('#group_add')[0].selectize.clear();
            $(".md-input-success").each(function(){
                $(this).removeClass('md-input-success');
            });
            $(".md-input-filled").each(function(){
                $(this).removeClass('md-input-filled');
            });
        }
    });
}

$(document).on('change' , '.group_type' , function(){
    $.ajax({
        type : 'GET',
        url  : base_url + '/attributes',
        data :
        {
            type  : $(this).val()
        },
        success: function(data)
        {
            $('#attributes_list , #edit_attributes_list').html(data['attrs']);
            altair_forms.init();
            altair_product_edit.init();
        }
    });
});

$(document).on('keyup' , '.margins' , function(e){
    var type  = $(this).prop('name'); // 0 = exact - 1 = similar
    var field = $(this).prop('title');
    var value = $(this).val();
    $.ajax({
        type : 'POST',
        url  : base_url + '/add-margin',
        data :
        {
            type  : type,
            field : field,
            value : value
        },
        success: function(data)
        {
        }
    });
});

$(document).on('change' , '#language' , function(e){
    $.ajax({
        type : 'POST',
        url  : base_url + '/language',
        data : 
        {
            lang : $('#language').val()
        },
        success: function(data)
        {
            if (data == 1) {
                location.reload();
            }
        }
    });
});


// system login function
$(document).on('click', '#sign_in', function() {
    sign_in(this);
});

function sign_in(elem) {
    elem.innerHTML = "loging...";
    $("#login_form").slideUp();
    $.ajax({
        type: 'POST',
        url: base_url + '/',
        data: $('#login_form').serialize(),
        success: function(data) {
            $('#notification_area').html(data['message']);
            $('#notification_area').fadeIn();
            if (data['state'] == 'success') {
                $("#greetingUser").slideDown();
                setTimeout(function() {
                    window.location.replace(data['url']);
                }, 1000);
            } else {
                $("#login_form").slideDown();
                elem.innerHTML = "log in";
                setTimeout(function() {
                    $('#notification_area').fadeOut();
                }, 2000);
            }
        },
        complete: function(data) {
            $(elem).attr('disabled', false);
        }
    });
};


// customer login functions
$(document).on('click', '#sign_in_customer', function() {
    sign_in_customer();
});

function sign_in_customer() {
    $.ajax({
        type: 'POST',
        url: base_url,
        data: $('#login_form').serialize(),
        success: function(data) {
            $('#notification_area').html(data['message']);
            $('#notification_area').fadeIn();
            if (data['state'] == 'success') {
                setTimeout(function() {
                    window.location.replace(data['url']);
                }, 1000);
            } else {
                setTimeout(function() {
                    $('#notification_area').fadeOut();
                }, 2000);
            }
        }
    });
};

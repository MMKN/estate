var globalTimeout = null;  
$('#contact_list_search').keyup(function(){
  if(globalTimeout != null) 
    clearTimeout(globalTimeout);  
  globalTimeout = setTimeout(SearchFunc,500);  
});  
function SearchFunc(){ 
  $('#customers_filter').slideUp();
  globalTimeout = null;  
  //ajax code
  var string = $('#contact_list_search').val();
  if (string == "") {
      getPage(null, 1);
  }
  altair_helpers.content_preloader_show();
  target = "search";
  if (context == "owners") {
      target = "search-owners"
  }
  $.ajax({
      type: 'POST',
      url: base_url + "/customers/" + target,
      data: {
          string: string
      },
      success: function(data) {
          altair_helpers.content_preloader_hide();
          if (data == 0) {
              return;
          }
          $("#contact_list").html(data);
          $("#paginator").fadeOut();
          altair_helpers.content_preloader_hide();
      }
  });
};

function resetFilter(){
    $("#customer_filter_city")[0].selectize.clear();
    $("#customer_filter_type")[0].selectize.clear();
    $("#customer_filter_customers")[0].selectize.setValue(0);
    $("#neighbours_search")[0].selectize.clear();
    $("#customer_filter_purpose")[0].selectize.clear();
    if ($("#is_compound").is(":checked")) {
        $("#is_compound").click();
    }
    $('#contact_list_search').val('');
    getPage(null, 1);
};

function customersFilter(){
  $('#contact_list_search').val('');
  altair_helpers.content_preloader_show();
  $("#paginator").hide();
  $.ajax({
      type: 'POST',
      url: base_url + '/customers/filter',
      data: $('#customer_filter').serialize(),
      success: function(data) {
          $('#contact_list').html(data['grid']);
          altair_helpers.content_preloader_hide();
          $('#main').slideDown();
      }
  });
};



$(document).on('change', '#customer_filter_city', function() {
    var city_id = $('#customer_filter_city').val();
    // $('#customer_filter_neighbour')[0].selectize.disable();
    altair_helpers.content_preloader_show();

    $.ajax({
        type: 'GET',
        url: base_url + '/customers/neighbour/' + city_id,
        data: {},
        success: function(data) {
            // $('#customer_filter_neighbour')[0].selectize.enable();
            altair_helpers.content_preloader_hide();
            $('#customer_filter_neighbour').html(data['select']);
            altair_forms.init();
        }
    });
});
// customers advanced filter functions
function toggleAdvancedFilter(){
    if ($('#customers_filter').css('display') == 'none') {
        $('#customers_filter').slideDown();
        if ($("#customer_filter_customers").val() != 0) {
          $("#customer_filter_customers")[0].selectize.setValue(0)
        }
    }
    else{
        $('#customers_filter').slideUp();
    }
};

$('#customer_filter_customers').on('change', function(event) {
  $('#contact_list_search').val('');
    // event.preventDefault();
    altair_helpers.content_preloader_show();

    if ($(this).val() == "0") {
        context = "customers";
        // $("#customers_filter").slideDown();
    } else if ($(this).val() == "1") {
        context = "publicUsers";
        // $("#customers_filter").slideDown();
    } else {
        context = "owners";
        $("#customers_filter").slideUp();
    }
    $.ajax({
        async: true,
        type: 'GET',
        url: base_url + '/customers/switch',
        data: {
            target: $(this).val()
        },
        success: function(data) {
            $(".pager").html(data['links']);
            // $(".pager").hide();
            // $('#contact_list').html(data);
            altair_helpers.content_preloader_hide();
            $('#contact_list').html(data['list']);
            // $('#contact_list').fadeIn(500);
        }
    });
});

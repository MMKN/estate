// add submit funciton
function customerFormSubmit() {
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'POST',
        url: $('#add_form').prop('action'),
        data: $('#add_form').serialize(),
        success: function(data) {
            altair_helpers.content_preloader_hide();
            $("#contact_list").html(data);
            goHome();
            $("#second_icon").click();
            $('#wizard_advanced').steps('previous');
            // altair_wizard.advanced_wizard();
            resetForm();
        }
    });
};

// system user get profile
function getProfile(id) {
    current_id = id;
    document.location.hash = "people/customers/" + id;
    $('#user_profile').siblings().slideUp()
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'GET',
        url: base_url + '/profile/' + id,
        data: {},
        success: function(data) {
            $("#user_profile").html(data);
            if(deleted){
                deletedProfileActive();
            }
            $('#user_profile').slideDown();
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
            initProfile(data);
        }
    });
};

function deletedProfileActive(){
    $("#edit_customer").hide()
};


// website user get profile
function getUserProfile(id) {
    current_id = id;
    document.location.hash = "people/customers/" + id;
    $('#main').slideUp();
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'GET',
        url: base_url + '/customers/user-profile/' + id,
        data: {},
        success: function(data) {
            $("#user_profile").html(data);
            $('#user_profile').slideDown();
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
            initProfile(data);
        }
    });
};

// archive customer
function deleteCustomer(c_id) {
    current_id = c_id;
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'POST',
        url: base_url + '/customers/delete/' + current_id,
        data: {},
        success: function(data) {
            goHomeAndRefresh();
        }
    });
};

// Restore customer
function unarchiveCustomer() {
    var c_id = current_id;
    $.ajax({
        type: 'POST',
        url: base_url + '/customers/unarchive/' + c_id,
        data: {},
        success: function(data) {
            goHomeAndRefresh();
        }
    });
};

// get customer edit wizard
function customerEditWizard() {
    document.location.hash = "people/customers/edit/" + current_id;
    $("#wizardView").siblings().hide();
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'GET',
        url: base_url + '/customers/edit/' + current_id,
        data: {},
        success: function(data) {
            $("#wizard").html(data);
            $("#wizardView").show().siblings().hide();
            altair_form_adv.masked_inputs();
            altair_md.inputs();
            altair_md.checkbox_radio();
            altair_forms.init();
            altair_wizard.advanced_wizard_edit();
            altair_helpers.content_preloader_hide();
        }
    });
    setTimeout(function() {
        $(window).trigger('resize');
    }, 2000);
};



function updateCustomer() {
    altair_helpers.content_preloader_show();
    var inputs = $('#edit_formm').serialize();
    $.ajax({
        type: 'POST',
        url: base_url + '/customers/edit/' + current_id,
        data: $('#edit_formm').serialize(),
        success: function(data) {
            altair_helpers.content_preloader_hide();
            $("#contact_list").html(data);
            getProfile(current_id);
        }
    });
};



// inside profile functions
function sendSms() {
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'POST',
        url: base_url + '/customers/sms/' + current_id,
        data: {},
        success: function(data) {
            altair_helpers.content_preloader_hide();
            $("#sms_button").fadeOut(function() {
                if (data == "1906") {
                    $("#sms_fail").fadeIn();
                } else {
                    $("#sms_success").fadeIn();
                }
            });

        }
    });
};

// add note
function addTimeline() {
    if (!($("#submit_message").val())) {
        return;
    }
    altair_helpers.content_preloader_show();
    var msg = $("#submit_message").val();
    $.ajax({
        type: 'POST',
        url: base_url + '/customers/add-message',
        data: {
            msg: msg,
            id: current_id
        },
        success: function(data) {
            altair_helpers.content_preloader_hide();
            $("#submit_message").val('');
            $(".timeline").append(data);
        }
    });
};

// 
function editMessage(elem) {
    if (!($("#edit_message").val())) {
        return;
    }
    altair_helpers.content_preloader_show();
    var modal = UIkit.modal("#modal-timeline");
    modal.hide();
    var msg = $("#edit_message").val();
    $.ajax({
        type: 'POST',
        url: base_url + '/customers/edit-message/' + msg_id,
        data: {
            msg: msg,
            id: current_id
        },
        success: function(data) {
            altair_helpers.content_preloader_hide();
            $("#edit_message").val('');
            $("#msg_" + msg_id).text(msg);
        }
    });
}
function deleteMessage(elem) {
    altair_helpers.content_preloader_show();
    var modal = UIkit.modal("#modal-timeline");
    modal.hide();
    $.ajax({
        type: 'POST',
        url: base_url + '/customers/delete-message/' + msg_id,
        data: {},
        success: function(data) {
            altair_helpers.content_preloader_hide();
            
            $("#entry_" + msg_id).remove();
        }
    });
};

// add new task
function saveNotice(elem) {
    var task_title = $("#task_title").val();
    var task_description = $("#task_description").val();
    var assignee = $("#assignee").val();
    var priority = $("#priority").val();
    if (task_description == ''  ||  assignee == null) {
        return
    }

    // ux stuff
    event.preventDefault();
    UIkit.modal(".uk-modal").hide();
    altair_helpers.content_preloader_show();

    $.ajax({
        async: true,
        type: 'POST',
        url: base_url + '/customers/add-notice',
        data: {
            title: task_title,
            body: task_description,
            assignee: assignee,
            current_id: current_id,
            priority: priority
        },
        success: function(data) {
            if (data['code'] == 1) {
                altair_helpers.content_preloader_hide();
                UIkit.notify({
                    message: 'Task Has Been Added Successfuly.',
                    pos: 'top-center',
                    status: 'success'
                });
                $("#res_table").html(data['table']);
                resetAddTask();
            } else {
                altair_helpers.content_preloader_hide();
                UIkit.notify({
                    message: "Error",
                    pos: 'top-center',
                    status: 'danger'
                });
            }
        }
    });
}
// reset Add task modal
function resetAddTask() {
    $("#task_description").val('');
    $("#assignee")[0].selectize.clear();
};

function toggleActive() {
    $.ajax({
        type: 'POST',
        url: base_url + '/customers/toggle-active/' + current_id,
        data: {},
        success: function(data) {

        }
    });
};


// change city
function changeCity() {
    altair_helpers.content_preloader_show();
    var city_id = $('#city_select_edit').val();
    $.ajax({
        type: 'GET',
        url: base_url + '/customers/fetch-neighbour/' + city_id,
        data: {},
        success: function(data) {
            $('#neighbours_select_edit').html(data['select']);
            altair_helpers.content_preloader_hide();
            altair_forms.init();
        }
    });
};

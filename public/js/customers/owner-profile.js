// ---- Get Owner profile
function getOwnerProfile(id) {
    current_id = id;
    document.location.hash = "people/owners/add";
    $('#main').slideUp();
    altair_helpers.content_preloader_show();
    current_id = id;
    $.ajax({
        type: 'GET',
        url: base_url + '/customers/owner-profile/' + id,
        data: {},
        success: function(data) {
            $("#user_profile").html(data);
            $('#user_profile').slideDown();
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
            initProfile(data);
        }
    });
};


function deleteOwner(c_id) {
    current_id = c_id;
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'POST',
        url: base_url + '/customers/delete-owner/' + c_id,
        data: {},
        success: function(data) {
            $("#contact_list").html(data);
            altair_helpers.content_preloader_hide();
            $("#main").show().siblings().hide();
        }
    });
}


function unarchiveOwner() {
    var c_id = current_id;
    $.ajax({
        type: 'POST',
        url: base_url + '/customers/unarchive-owner/' + c_id,
        data: {},
        success: function(data) {
            $("#contact_list").html(data);
            goHomeAndRefresh();
        }
    });
}


// to add new property from owner profile
// 1- get add property wizard
function addProperty() {
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'GET',
        url: base_url + '/customers/add-property/' + current_id,
        data: {},
        success: function(data) {
            $("#wizard").html(data);
            altair_wizard.add_owner_property();
            altair_form_adv.masked_inputs();
            altair_md.inputs();
            altair_md.checkbox_radio();
            altair_forms.init();
            initMap();
            altair_helpers.content_preloader_hide();
            $("#wizard").show().siblings().hide();
        }
    });
}
// 2- Submit add property
function add_new_property() {
    var inputs = $('#add_property_form').serialize();
    var url = $('#add_property_form').prop('action');
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'POST',
        url: $('#add_property_form').prop('action'),
        data: inputs,
        success: function(data) {
            $('#view_box').html(data['grid']);
            altair_helpers.content_preloader_hide();
            $("#main").show().siblings().hide();
            resetForm();
        }
    });
}
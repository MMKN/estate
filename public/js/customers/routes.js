$(document).ready(function() {
    // reset URL
    document.location.hash = '';
});

var state = 'customers.main';
var context = "customers";
var current_id = 1;
var current_page = 1;
var deleted = 0;
var base_url = "{{ URL::to('/') }}";
var msg_id = 0;
var property_id;

//// 2- get main view

function getPage(elem, page) {
    document.location.hash = 'people/' + context;
    altair_helpers.content_preloader_show();
    deleted = 0;
    current_page = page;
    var target;
    if (context == "owners") {
        target = "paginated-owner?page=";
    } else {
        target = "paginated?page=";
    }
    $.ajax({
        async: true,
        type: 'GET',
        url: base_url + '/customers/' + target + page,
        data: {},
        success: function(data) {
            $("#contact_list").html(data['customers']);
            $('#main').slideDown().siblings().hide();
            $(".pager").html(data['links']);
            $("#paginator").show();
            altair_helpers.content_preloader_hide();
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
        }
    });
};

// To initialize profile
function initProfile() {
    // reinitialize uikit margin
    altair_uikit.reinitialize_grid_margin();
    // reinitialize textarea autosize
    altair_forms.textarea_autosize();
    // reinitialize switches
    altair_forms.switches();
    altair_md.inputs();
    altair_helpers.hierarchical_show();
    altair_md.fab_toolbar();
    altair_form_validation.init();
    altair_helpers.content_preloader_hide();
};

// back from profile
function backFromProfile() {
    $("#user_profile").slideUp();
    $("#user_profile").children().remove();
    if (deleted) {
        document.location.hash = "people/customers/archived"
        $("#archivedView").slideDown("280");
    } else {
        $("#main").show().siblings().hide();
        document.location.hash = "customers"
        deleted = 0;
    };
};

// Go back to home
function goHome() {
    document.location.hash = "people";
    event.preventDefault();
    $("#main").show().siblings().hide();
    state = 'customers.main';
};
// Refresh home grid
function refreshHome() {
    getPage(null, 1);
}

function goHomeAndRefresh() {
    refreshHome();
}

/////////////////////////////////////////////////////
//////////////// Wizard /////////////////////////////

// add wizard for customers and owners
function addWizard() {
    if (context == "customers") {
        addCustomerWizard();
        resetForm();
    }
    if (context == "owners") {
        addOwnerWizard();
    }
}
// reset add customer wizard
function resetForm() {
    var select = $("#unit_type")[0].selectize.clear();
    var select = $("#finishing_select")[0].selectize.clear();
    var select = $("#payment_select")[0].selectize.clear();
    var select = $("#floor_select")[0].selectize.clear();
    // var select = $("#city_select")[0].selectize.clear();
    var select = $("#neighbours_select select")[0].selectize.clear();


    if ($("#compound_check").is(":checked")) {
        $("#compound_check").click();
    }

    $('.ichk').iCheck('uncheck');
    $("#add_form")[0].reset();
    $(".md-input-success").each(function() {
        $(this).removeClass('md-input-success');
    });
    $(".md-input-filled").each(function() {
        $(this).removeClass('md-input-filled');
    });
    $(".rel-date").parent().addClass('md-input-filled');
    var parsley = $("#add_form").parsley();
    parsley.reset();
};

// add new customer
function addCustomerWizard() {
    document.location.hash = "people/customers/add";
    $("#addCustomer").show().siblings().hide();
    setTimeout(function() {
        $(window).trigger('resize');
    }, 1000);

};


// A - customers Crud
//// 1- getArchived
function getArchived() {
    altair_helpers.content_preloader_show();
    $("#main").slideUp();
    var target;
    if (context == "customers") {
        target = "deleted-customers";
    } else if (context == "owners") {
        target = "deleted-owners";
    } else {
        altair_helpers.content_preloader_hide();
        return;
    }
    $.ajax({
        type: 'GET',
        url: base_url + "/customers/" + target,
        data: {},
        success: function(data) {
            $("#archived").html(data);
            $("#archivedView").slideDown();
            altair_helpers.content_preloader_hide();
            deleted = 1;
        }
    });
};



// This code gets excuted when city changes
$(document).on('change', '#city_select', function() {
    altair_helpers.content_preloader_show();
    var city_id = $('#city_select').val();
    $.ajax({
        type: 'GET',
        url: base_url + '/customers/fetch-neighbour/' + city_id,
        data: {},
        success: function(data) {
            $('#neighbours_select').html(data['select']);
            altair_forms.init();
            altair_helpers.content_preloader_hide();
        }
    });
});
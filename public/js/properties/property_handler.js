///////////////////////////////////
// A- Properties CRUD functions
//// 1- get all properties by page
function getPage(elem, url) {
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'GET',
        url: base_url + '/paginated?page=' + url,
        data: {},
        success: function(data) {
            $("#view_box").html(data['properties']);
            $(".pager").html(data['links']);
            $(".pager").show();
            $("#main").slideDown();
            $("html, body").animate({
                scrollTop: 0
            }, "fast");
            altair_helpers.content_preloader_hide();
        }
    });
};
////  2- Get property by ID
function getProperty(id){
    property_id = id;
    if (state == "properties.archived") {
        $("#archivedView").slideUp();
        document.location.hash = 'archived/'+ property_id;
    }
    else{
        $("#main").slideUp();
        document.location.hash = property_id;
    }
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'GET',
        url: base_url + '/property',
        data: {
            id: property_id
        },
        success: function(data) {
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
            $('#profile_box').html(data['property']);
            $("#profile_box").slideDown();
            altair_helpers.content_preloader_hide();
            // FAB transitions
            altair_md.fab_toolbar();
            altair_form_validation.init();
        }
    });
};
//// 3- add property
function add_new_property() {
    altair_helpers.content_preloader_show();
    $('#wizardView').slideUp();
    var inputs = $('#add_property_form').serialize();
    var url = $('#add_property_form').prop('action');
    $.ajax({
        type: 'POST',
        url: base_url + "/add",
        data: inputs,
        success: function(data) {
            $('#wizard').children().remove();
            getPage(null,1);
            altair_helpers.content_preloader_hide();
        }
    });
};
function clone_property() {
    add_new_property()
};


//// 4- edit property

function edit_property() {
    var inputs = $('#edit_property_form').serialize();
    altair_helpers.content_preloader_show();
    $("#wizardView").slideUp();
    // var url    = $('#wizard_advanced_form').prop('action');
    $.ajax({
        type: 'POST',
        url: base_url + '/edit/' + property_id,
        data: inputs,
        success: function(data) {
            $("#wizard").children().remove();
            getProperty(property_id);
            altair_helpers.content_preloader_hide();
        }
    });
}

//// 3- Delete Property
function deleteProperty(id){
    altair_helpers.content_preloader_show();
    $('#profile_box').slideUp();
    $('#profile_box').children().remove(); 
    $.ajax({
        type: 'POST',
        url: base_url + '/delete/' + id,
        data: {},
        success: function(data) {
            getPage(null, 1);
            altair_helpers.content_preloader_hide();
        }
    });
};
//// 4- Restore Property
function unarchiveProperty() {
    altair_helpers.content_preloader_show();
    $('#profile_box').slideUp();
    $('#profile_box').children().remove(); 
    $.ajax({
        type: 'POST',
        url: base_url + '/unarchive/' + property_id,
        data: {},
        success: function(data) {
            altair_helpers.content_preloader_hide();
            $("#main").slideDown();
            refreshHome();
        }
    });
};
//// 5- get archived properties
function getArchivedProperties(){
    document.location.hash = 'archived';
    state = 'properties.archived';
    $("#archivedView").siblings().slideUp();
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'GET',
        url: base_url + "/deleted-properties",
        data: {},
        success: function(data) {
            $("#archived").html(data);
            $("#archivedView").slideDown();
            state = 'properties.archived';
            altair_helpers.content_preloader_hide();
        }
    });
};
//// 6- customer Wizard from home
function getCustomerWizard(){
    state = 'properties.addcustomer';
    document.location.hash = 'add-customer';
    altair_helpers.content_preloader_show();
    var inputs = $('#search_form').serialize();
    $.ajax({
        type: 'GET',
        url: base_url + '/customer-add',
        data: inputs,
        success: function(data) {
            $('#wizard').html(data['customer_add']);
            altair_helpers.content_preloader_hide();
            $("#wizardView").show().siblings().hide();
            altair_wizard.advanced_wizard();
            altair_forms.init();
            altair_md.inputs();
            altair_md.checkbox_radio();
            setTimeout(function() {
                $(window).trigger('resize');
            }, 1000);
        }
    });
};
//// 7- property wizard get and some helper functions :))
function addPropertyWizard(){
    state = 'properties.add';
    document.location.hash = 'add-property';
    $("#main").slideUp();
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'GET',
        url: base_url + '/add',
        success: function(data) {
            $('#wizard').html(data);
            $("#wizardView").fadeIn();
            altair_wizard.add_property_wizard();
            altair_form_adv.masked_inputs();
            altair_md.inputs();
            altair_md.checkbox_radio();
            altair_forms.init();
            altair_helpers.content_preloader_hide();
            setTimeout(function() {
                initMap();
                google.maps.event.trigger(map, "resize");
                $(window).trigger('resize');
                $('.check-feature').iCheck('uncheck');
            }, 1000);
        }
    });
};
$(document).on('change', '#city_add', function() {
    var city_id = $('#city_add').val();
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'GET',
        url: base_url + '/neighbour/' + city_id,
        data: {
            target: 'add'
        },
        success: function(data) {
            altair_helpers.content_preloader_hide();
            $('#neighbour_add').html(data['select']);
            altair_forms.init();
        }
    });
});
function remove_image(elem) {
    $(elem).closest('.image_container').remove();
};
function toggleOwner(elem) {
    if ($("#newOwner").length) {
        $(elem).text("New Owner?");
        var mForm = $("#newOwner")[0].outerHTML;
        $("#newOwner").remove();
        $("#pickOwner").fadeIn();
        $("#exist_owner").attr("required", true);
        $("#exist_owner").attr("disabled", false);
    } else {
        $(elem).text("Existing Owner?");
        $("#pickOwner").fadeOut();
        $("#pickOwner").after(mForm);
        $("#exist_owner").attr("required", false);
        $("#exist_owner").attr("disabled", true);
    }
};
//// 8- edit property wizard
function propertyEditWizard(property_id){
    state = 'properties.edit';
    document.location.hash = 'edit-property/' + property_id;
    altair_helpers.content_preloader_show();
    $('#profile_box').slideUp();
    $('#profile_box').children().remove();
    // get the id from button name to get its value
    $.ajax({
        type: 'GET',
        url: base_url + '/edit/' + property_id,
        data: {},
        success: function(data) {
            $('#wizard').html(data['property']);
            $("#wizardView").slideDown();
            altair_wizard.edit_property_wizard();
            altair_form_adv.masked_inputs();
            altair_md.inputs();
            altair_md.checkbox_radio();
            altair_forms.init();
            altair_helpers.content_preloader_hide();
            setTimeout(function() {
                google.maps.event.trigger(map_edit, "resize");
                $(window).trigger('resize');
                initMapEdit();
            }, 1000);
        }
    });
};
//// 9- clone property wizard
function propertyCloneWizard(property_id){
    state = 'properties.clone';
    document.location.hash = 'clone-property/' + property_id;
    altair_helpers.content_preloader_show();
    $('#profile_box').slideUp();
    $('#profile_box').children().remove();
    $.ajax({
        type: 'GET',
        url: base_url + '/edit/' + property_id,
        data: {},
        success: function(data) {
            $('#profile_box').html('');
            $('#wizard').html(data['property']);
            $("#wizardView").show().siblings().hide();
            altair_wizard.clone_property_wizard();
            altair_form_adv.masked_inputs();
            altair_md.inputs();
            altair_md.checkbox_radio();
            altair_forms.init();
            altair_helpers.content_preloader_hide();
            setTimeout(function() {
                google.maps.event.trigger(map_edit, "resize");
                $(window).trigger('resize');
                initMapEdit();
            }, 1000);
        }
    });
};

//////////////////////////////////
// B- Search and filter functions
//// 1- Search functions
function filter() {
    $("#view_box").fadeOut("280");
    altair_helpers.content_preloader_show();
    var inputs = $('#search_form').serialize();
    var url = $('#search_form').prop('action');
    $(".pager").hide();
    $.ajax({
        async: true,
        type: 'POST',
        url: $('#search_form').prop('action'),
        data: inputs,
        success: function(data) {
            console.log(data);
            $('#view_box').html(data['filter_result']);
            altair_helpers.content_preloader_hide();
            $('#view_box').fadeIn(280);
        }
    });
};
$(document).on('change', '#city_search', function() {
    $('#neighbours_search')[0].selectize.disable();
    altair_helpers.content_preloader_show();
    var city_id = $('#city_search').val();
    $.ajax({
        type: 'GET',
        url: base_url + '/neighbour/' + city_id,
        data: {
            target: 'search'
        },
        success: function(data) {
            $('#neighbour_search').html(data['select']);
            altair_forms.init();
            altair_helpers.content_preloader_hide();
            $('#neighbours_search')[0].selectize.enable();
        }
    });
});
//// 2- reset filter n search function
function resetSearch(){
    $('#neighbours_search')[0].selectize.disable();
    $('#unit_type_search')[0].selectize.clear();
    $('#city_search')[0].selectize.clear();
    $('#neighbours_search')[0].selectize.clear();
    $('#floor_search')[0].selectize.clear();
    $('#finishing_search')[0].selectize.clear();
    $('#payment_search')[0].selectize.clear();
    $('#objective_search')[0].selectize.clear();
    $('#sort_by')[0].selectize.clear();
    $("#sale").iCheck('check');
    if ($("#is_compound").is(":checked")) {
        $("#is_compound").click();
    }
    $('#search_form')[0].reset();
    $(".md-input-success").each(function() {
        $(this).removeClass('md-input-success');
    });
    $(".md-input-filled").each(function() {
        $(this).removeClass('md-input-filled');
    });
    $(".pager").show();
    getPage(null, 1);
};
//// 3- advanced filter toggle
function advancedFilterToggle(){
    $(window).resize();
    if ($('#advanced_seach').css('display') == 'none') {
        $('#advanced_seach').slideDown();
    } else {
        $('#advanced_seach').slideUp();
    }
};
//// 4- add customer 
function customerFormSubmit() {
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'POST',
        url: $('#add_form').prop('action'),
        data: $('#add_form').serialize(),
        success: function(data) {
            altair_helpers.content_preloader_hide();
            $("#main").show().siblings().hide();
            setTimeout(function() {
                $(window).trigger('resize');
            }, 1000);
            $("#second_icon").fadeOut('280');
            $("#first_icon").fadeIn('280');
            $('#reset_button').click();
        }
    });
};


////////////////////////////////
// C- Property Notes Crud
//// 1- add note
function addTimeline() {
    if (!($("#submit_message").val())) {
        return;
    }
    altair_helpers.content_preloader_show();
    var msg = $("#submit_message").val();
    $.ajax({
        type: 'POST',
        url: base_url + '/add-message',
        data: {
            msg: msg,
            id: property_id
        },
        success: function(data) {
            altair_helpers.content_preloader_hide();
            $("#submit_message").val('');
            $(".timeline").append(data);
        }
    });
};
//// 2- edit note
function editMessage() {
    if (!($("#edit_message").val())) {
        return;
    }
    altair_helpers.content_preloader_show();
    var modal = UIkit.modal("#modal-timeline");
    modal.hide();
    var msg = $("#edit_message").val();
    $.ajax({
        type: 'POST',
        url: base_url + '/edit-message/' + msg_id,
        data: {
            msg: msg
        },
        success: function(data) {
            altair_helpers.content_preloader_hide();
            $("#edit_message").val('');

            $("#msg_" + msg_id).text(msg);
        }
    });
};
//// 3- delete Note
function deleteMessage() {
    altair_helpers.content_preloader_show();
    var modal = UIkit.modal("#modal-timeline");
    modal.hide();
    $.ajax({
        type: 'POST',
        url: base_url + '/delete-message/' + msg_id,
        data: {},
        success: function(data) {
            altair_helpers.content_preloader_hide();

            $("#entry_" + msg_id).remove();
        }
    });
};





/////////////////////////////////
// D- Tasks
//// 1- add task
function saveNotice() {
    var task_title = $("#task_title").val();
    var task_description = $("#task_description").val();
    var assignee = $("#assignee").val();
    var priority = $("#priority").val();
    if (task_description == '' || assignee == null) {
        return
    }
    // ux stuff
    event.preventDefault();
    UIkit.modal(".uk-modal").hide();
    altair_helpers.content_preloader_show();
    $.ajax({
        async: true,
        type: 'POST',
        url: base_url + '/add-notice',
        data: {
            title: task_title,
            body: task_description,
            assignee: assignee,
            property_id: property_id,
            priority: priority
        },
        success: function(data) {
            console.log(data);
            if (data['code'] == 1) {
                altair_helpers.content_preloader_hide();
                UIkit.notify({
                    message: 'Task Has Been Added Successfuly.',
                    pos: 'top-center',
                    status: 'success'
                });
                $("#res_table").html(data['table']);
                resetAddTask();
            } else {
                altair_helpers.content_preloader_hide();
                UIkit.notify({
                    message: data['code'],
                    pos: 'top-center',
                    status: 'danger'
                });
            }
        }
    });
};




//////////////////////////////////////////////////////////
// UI stuff
/////////////////////////////////////////////////////////

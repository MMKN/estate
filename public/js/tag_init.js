$(function() {
    // product edit
    altair_product_edit.init();
});

altair_product_edit = {
    init: function() {
        // product tags
        altair_product_edit.product_tags();
    },


    product_tags: function() {


        // property type input tags field initialization
        $('#unit_type').selectize({
            plugins: {
                'remove_button': {
                    label: ''
                }
            },
            placeholder: 'Add Property Type and Press Enter',

            render: {
                option: function(data, escape) {
                    return '<div class="option">' +
                        '<span>' + escape(data.title) + '</span>' +
                        '</div>';
                },
                item: function(data, escape) {
                    return '<div class="item">' + escape(data.title) + '</div>';
                }
            },
            maxItems: null,
            valueField: 'value',
            labelField: 'title',
            searchField: 'title',
            create: true,
            onDropdownOpen: function($dropdown) {
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function() {
                            $dropdown.css({
                                'margin-top': '0'
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },
            onDropdownClose: function($dropdown) {
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function() {
                            $dropdown.css({
                                'margin-top': ''
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            }
        });

        //floors iput tags filed initialization
        $('#floor').selectize({
            plugins: {
                'remove_button': {
                    label: ''
                }
            },
            placeholder: 'Add floor and then press enter',

            render: {
                option: function(data, escape) {
                    return '<div class="option">' +
                        '<span>' + escape(data.title) + '</span>' +
                        '</div>';
                },
                item: function(data, escape) {
                    return '<div class="item">' + escape(data.title) + '</div>';
                }
            },
            maxItems: null,
            valueField: 'value',
            labelField: 'title',
            searchField: 'title',
            create: true,
            onDropdownOpen: function($dropdown) {
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function() {
                            $dropdown.css({
                                'margin-top': '0'
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },
            onDropdownClose: function($dropdown) {
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function() {
                            $dropdown.css({
                                'margin-top': ''
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            }
        });

        //finishing iput tags filed initialization
        $('#finishing').selectize({
            plugins: {
                'remove_button': {
                    label: ''
                }
            },
            placeholder: 'Add value and then press enter',

            render: {
                option: function(data, escape) {
                    return '<div class="option">' +
                        '<span>' + escape(data.title) + '</span>' +
                        '</div>';
                },
                item: function(data, escape) {
                    return '<div class="item">' + escape(data.title) + '</div>';
                }
            },
            maxItems: null,
            valueField: 'value',
            labelField: 'title',
            searchField: 'title',
            create: true,
            onDropdownOpen: function($dropdown) {
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function() {
                            $dropdown.css({
                                'margin-top': '0'
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },
            onDropdownClose: function($dropdown) {
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function() {
                            $dropdown.css({
                                'margin-top': ''
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            }
        });


        //payment style iput tags filed initialization
        $('#payment_type').selectize({
            plugins: {
                'remove_button': {
                    label: ''
                }
            },
            placeholder: 'Add value and then press enter',

            render: {
                option: function(data, escape) {
                    return '<div class="option">' +
                        '<span>' + escape(data.title) + '</span>' +
                        '</div>';
                },
                item: function(data, escape) {
                    return '<div class="item">' + escape(data.title) + '</div>';
                }
            },
            maxItems: null,
            valueField: 'value',
            labelField: 'title',
            searchField: 'title',
            create: true,
            onDropdownOpen: function($dropdown) {
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function() {
                            $dropdown.css({
                                'margin-top': '0'
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },
            onDropdownClose: function($dropdown) {
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function() {
                            $dropdown.css({
                                'margin-top': ''
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            }
        });


        //objective iput tags filed initialization
        $('#objective').selectize({
            plugins: {
                'remove_button': {
                    label: ''
                }
            },
            placeholder: 'Add value and then press enter',

            render: {
                option: function(data, escape) {
                    return '<div class="option">' +
                        '<span>' + escape(data.title) + '</span>' +
                        '</div>';
                },
                item: function(data, escape) {
                    return '<div class="item">' + escape(data.title) + '</div>';
                }
            },
            maxItems: null,
            valueField: 'value',
            labelField: 'title',
            searchField: 'title',
            create: true,
            onDropdownOpen: function($dropdown) {
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function() {
                            $dropdown.css({
                                'margin-top': '0'
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },
            onDropdownClose: function($dropdown) {
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function() {
                            $dropdown.css({
                                'margin-top': ''
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            }
        });


        //features iput tags filed initialization
        $('#feature').selectize({
            plugins: {
                'remove_button': {
                    label: ''
                }
            },
            placeholder: 'Add value and then press enter',

            render: {
                option: function(data, escape) {
                    return '<div class="option">' +
                        '<span>' + escape(data.title) + '</span>' +
                        '</div>';
                },
                item: function(data, escape) {
                    return '<div class="item">' + escape(data.title) + '</div>';
                }
            },
            maxItems: null,
            valueField: 'value',
            labelField: 'title',
            searchField: 'title',
            create: true,
            onDropdownOpen: function($dropdown) {
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function() {
                            $dropdown.css({
                                'margin-top': '0'
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },
            onDropdownClose: function($dropdown) {
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function() {
                            $dropdown.css({
                                'margin-top': ''
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            }
        });


        //districts iput tags filed initialization
        $('#districts_add').selectize({
            plugins: {
                'remove_button': {
                    label: ''
                }
            },
            placeholder: 'Add value and then press enter',

            render: {
                option: function(data, escape) {
                    return '<div class="option">' +
                        '<span>' + escape(data.title) + '</span>' +
                        '</div>';
                },
                item: function(data, escape) {
                    return '<div class="item">' + escape(data.title) + '</div>';
                }
            },
            maxItems: null,
            valueField: 'value',
            labelField: 'title',
            searchField: 'title',
            create: true,
            onDropdownOpen: function($dropdown) {
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function() {
                            $dropdown.css({
                                'margin-top': '0'
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },
            onDropdownClose: function($dropdown) {
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function() {
                            $dropdown.css({
                                'margin-top': ''
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            }
        });

        //districts iput tags filed initialization
        $('#districts_edit').selectize({
            plugins: {
                'remove_button': {
                    label: ''
                }
            },
            placeholder: 'Add value and then press enter',

            render: {
                option: function(data, escape) {
                    return '<div class="option">' +
                        '<span>' + escape(data.title) + '</span>' +
                        '</div>';
                },
                item: function(data, escape) {
                    return '<div class="item">' + escape(data.title) + '</div>';
                }
            },
            maxItems: null,
            valueField: 'value',
            labelField: 'title',
            searchField: 'title',
            create: true,
            onDropdownOpen: function($dropdown) {
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function() {
                            $dropdown.css({
                                'margin-top': '0'
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },
            onDropdownClose: function($dropdown) {
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function() {
                            $dropdown.css({
                                'margin-top': ''
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            }
        });


        //groups iput tags filed initialization
        $('#group_add , .group_districts_floors').selectize({
            plugins: {
                'remove_button': {
                    label: ''
                }
            },
            placeholder: 'Add value and then press enter',

            render: {
                option: function(data, escape) {
                    return '<div class="option">' +
                        '<span>' + escape(data.title) + '</span>' +
                        '</div>';
                },
                item: function(data, escape) {
                    return '<div class="item">' + escape(data.title) + '</div>';
                }
            },
            maxItems: null,
            valueField: 'value',
            labelField: 'title',
            searchField: 'title',
            create: true,
            onDropdownOpen: function($dropdown) {
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function() {
                            $dropdown.css({
                                'margin-top': '0'
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },
            onDropdownClose: function($dropdown) {
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function() {
                            $dropdown.css({
                                'margin-top': ''
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            }
        });


        //groups iput tags filed initialization
        $('#group_edit').selectize({
            plugins: {
                'remove_button': {
                    label: ''
                }
            },
            placeholder: 'Add value and then press enter',

            render: {
                option: function(data, escape) {
                    return '<div class="option">' +
                        '<span>' + escape(data.title) + '</span>' +
                        '</div>';
                },
                item: function(data, escape) {
                    return '<div class="item">' + escape(data.title) + '</div>';
                }
            },
            maxItems: null,
            valueField: 'value',
            labelField: 'title',
            searchField: 'title',
            create: true,
            onDropdownOpen: function($dropdown) {
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function() {
                            $dropdown.css({
                                'margin-top': '0'
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },
            onDropdownClose: function($dropdown) {
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function() {
                            $dropdown.css({
                                'margin-top': ''
                            })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            }
        });

        // $('#group_edit').selectize({
        //     plugins: {
        //         'remove_button': {
        //             label: ''
        //         }
        //     },
        //     placeholder: 'Add value and then press enter',

        //     render: {
        //         option: function(data, escape) {
        //             return  '<div class="option">' +
        //                 '<span>' + escape(data.title) + '</span>' +
        //                 '</div>';
        //         },
        //         item: function(data, escape) {
        //             return '<div class="item">' + escape(data.title) + '</div>';
        //         }
        //     },
        //     maxItems: null,
        //     valueField: 'value',
        //     labelField: 'title',
        //     searchField: 'title',
        //     create: true,
        //     onDropdownOpen: function($dropdown) {
        //         $dropdown
        //             .hide()
        //             .velocity('slideDown', {
        //                 begin: function() {
        //                     $dropdown.css({'margin-top':'0'})
        //                 },
        //                 duration: 200,
        //                 easing: easing_swiftOut
        //             })
        //     },
        //     onDropdownClose: function($dropdown) {
        //         $dropdown
        //             .show()
        //             .velocity('slideUp', {
        //                 complete: function() {
        //                     $dropdown.css({'margin-top':''})
        //                 },
        //                 duration: 200,
        //                 easing: easing_swiftOut
        //             })
        //     }
        // });




    }




};
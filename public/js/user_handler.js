var user_id;
var current_page = 1;
$(function() {
    var i = function() {
            main_content.show().siblings().hide()
        },
        o = function() {
            form.show().siblings().hide();
        },
        p = function() {
            profileView.show().siblings().hide();
        };

    $("#addNew").on("click", function(k) {
        k.preventDefault();
        $("#first_icon").fadeOut("280");
        $("#second_icon").fadeIn("280");
        altair_md.card_show_hide(body, void 0, o, void 0);
        setTimeout(function() {
            $(window).trigger('resize');
        }, 1000);
    });
    $("#second_icon").on("click", function(o) {
        o.preventDefault();
        $("#second_icon").fadeOut("280");
        $("#first_icon").fadeIn("280");
        altair_md.card_show_hide(body, void 0, i, void 0);
        $(".pager").show();
        resetForm();
    });

    $("#archieve_customer").on('click', function() {


        $(".md-fab-action-open-customers").css('display', 'block');
        $(".md-fab-action-close").css('display', 'none');
    });

});
$("#third_icon").on('click', function() {
    $("#third_icon").fadeOut();

    // $("#employees_list").fadeIn();
    getPage(null, current_page);
    $(".pager").show();

    $("#page_heading").slideDown();
    $("#archived_title").slideUp();
});
$(document).on('submit', '.edit_user_form', function(e) {
    e.preventDefault();
    submitEdit();
});

function myBinder() {
    $(".user-card").click(function() {
        $(this).children(".children").toggle();
    });
    $(".user-card a").click(function(e) {
        e.stopPropagation();
    });
}
$(document).ready(function() {
    $(".user-card").click(function() {
        $(this).children(".children").toggle();
    });
    $(".user-card a").click(function(e) {
        e.stopPropagation();
    });
});

var main_content = $("#main"),
    form = $("#add_box"),
    body = $("#main_index"),
    profileView = $("#profile_view");


$(document).on('submit', '.add_user_form', function(e) {
    UIkit.modal("#modal_user_add").hide();
    e.preventDefault();
    altair_helpers.content_preloader_show();
    var formdata = new FormData($('.add_user_form')[0]);
    $.ajax({
        async: true,
        type: 'POST',
        url: base_url + '/users/add',
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        data: formdata,
        success: function(data) {
            altair_helpers.content_preloader_hide();
            if (data['state'] == 'fail') {
                UIkit.notify({
                    message: 'something went wrong',
                    pos: 'bottom-center',
                    status: 'danger'
                });
                $('#notification_area').html(data['message']);
                $('#notification_area').fadeIn();
                setTimeout(function() {
                    $('#notification_area').fadeOut();
                }, 2000);
            } else {
                UIkit.notify({
                    message: "user added successfully :))",
                    pos: 'top-center',
                    status: 'success'
                });
                // $('#employees_list').html(data['users']);
                $('.add_user_form')[0].reset();
                $('#role')[0].selectize.clear();
                $('#manager')[0].selectize.clear();
                $(".md-input-success").each(function() {
                    $(this).removeClass('md-input-success');
                });
                $(".md-input-filled").each(function() {
                    $(this).removeClass('md-input-filled');
                });
                myBinder();
            getPage(null, current_page);
                
            }
        }
    });
});

var timer;
$("#user_list_search").on('keyup', function() {
    clearTimeout(timer);
    timer = setTimeout(function() {
        filter_users();
    }, 1500);
});

function getPage(elem, url) {
    altair_helpers.content_preloader_show();

    var target;

    target = "paginated?page=";
    current_page = url;
    console.log(target + url);
    $.ajax({
        type: 'GET',
        url: base_url + '/users/' + target + url,
        data: {},
        success: function(data) {
            $("#employees_list").html(data['users']);
            $('#employees_list').fadeIn();
            $(".pager").html(data['links']);
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
            altair_helpers.content_preloader_hide();
            myBinder();
        }
    });
}

function filter_users() {
    altair_helpers.content_preloader_show();
    $(".pager").hide();
    var query = $("#user_list_search").val();
    $.ajax({
        type: 'GET',
        url: base_url + '/users/filter',
        data: {
            query: query
        },
        success: function(data) {
            $('#filter_grid').html(data['grid']);

            $('#employees_list').fadeOut(500);
            $('#filter_grid').fadeIn(500);
            myBinder();
        },
        complete: function() {
            altair_helpers.content_preloader_hide();
        }
    });
}

function getProfile(id) {
    altair_helpers.content_preloader_show();
    user_id = id;
    $.ajax({
        type: 'GET',
        url: root_url + '/settings/edit-user/' + user_id,
        success: function(data) {
            altair_helpers.content_preloader_hide();
            $('#modal_edit').html(data['modal']);
            UIkit.modal("#modal_edit").show();
            altair_forms.init();
        },
        complete: function(data) {
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
        }
    });
}

function submitEdit() {
    altair_helpers.content_preloader_show();
    var formdata = new FormData($('.edit_user_form')[0]);
    UIkit.modal("#modal_edit").hide();
    $.ajax({
        type: 'POST',
        url: root_url + '/settings/edit-user/' + user_id,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        data: formdata,
        success: function(data) {
            altair_helpers.content_preloader_hide();
            $('#users_table').html(data['users']);
            getPage(null, current_page);

        }
    });
}



function deleteUser(id) {
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'POST',
        url: root_url + '/users/delete-user',
        enctype: 'multipart/form-data',
        data: {
            id: id
        },
        success: function(data) {
            $('#employees_list').html(data);
            myBinder();
        },
        complete: function(data) {
            altair_helpers.content_preloader_hide();
        }
    });
}

$("#get_archived_users").on('click', function() {
    var modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'><img class=\'uk-margin-top\' src=\'assets/img/spinners/spinner.gif\' alt=\'\'>');
    $(".pager").hide();

    $.ajax({
        type: 'GET',
        url: base_url + "/users/deleted-users",
        data: {},
        success: function(data) {
            $("#filter").fadeOut();
            $("#employees_list").fadeIn();
            $("#third_icon").fadeIn();
            $("#employees_list").html(data);
            modal.hide();
            $("#page_heading").slideUp();
            $("#archived_title").slideDown();
            myBinder();
        }
    });
});

function unarchive_user(id) {
    var c_id = id;
    $.ajax({
        type: 'POST',
        url: base_url + '/users/unarchive/' + c_id,
        data: {},
        success: function(data) {
            $("#employees_list").html(data);
            $("#third_icon").click();
            // $("#first_icon").fadeIn();
            // $(".md-fab-action-open-customers").css('display', 'block');
            // $(".md-fab-action-close").css('display', 'none');
            // $("#call_to_action_customers").fadeOut("280");
        }
    });
}

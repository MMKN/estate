// header auto hide/show on scroll

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
 // some code..
}else
{
    $(function header_auto_hide() {
    var last_scroll = 0;
    $(window).scroll(function() {
        var current_scroll = $(this).scrollTop();
        if (current_scroll < last_scroll) {
            // if(current_scroll < 48)
            $("#header_main").slideDown("280");
        } else if (current_scroll > 100) {
            $("#header_main").slideUp("280");
        }
        last_scroll = current_scroll;
    });
});
}

// style switcher scripts
$(function() {
    var $switcher = $('#style_switcher'),
        $switcher_toggle = $('#style_switcher_toggle'),
        $theme_switcher = $('#theme_switcher'),
        $mini_sidebar_toggle = $('#style_sidebar_mini'),
        $slim_sidebar_toggle = $('#style_sidebar_slim'),
        $boxed_layout_toggle = $('#style_layout_boxed'),
        $accordion_mode_toggle = $('#accordion_mode_main_menu'),
        $html = $('html'),
        $body = $('body');
    $switcher_toggle.click(function(e) {
        e.preventDefault();
        $switcher.toggleClass('switcher_active');
    });
    $theme_switcher.children('li').click(function(e) {
        e.preventDefault();
        var $this = $(this),
            this_theme = $this.attr('data-app-theme');
        $theme_switcher.children('li').removeClass('active_theme');
        $(this).addClass('active_theme');
        $html
            .removeClass('app_theme_a app_theme_b app_theme_c app_theme_d app_theme_e app_theme_f app_theme_g app_theme_h app_theme_i app_theme_dark')
            .addClass(this_theme);
        if (this_theme == '') {
            localStorage.removeItem('altair_theme');
        } else {
            localStorage.setItem("altair_theme", this_theme);
            if (this_theme == 'app_theme_dark') {
                $('#kendoCSS').attr('href', 'bower_components/kendo-ui/styles/kendo.materialblack.min.css')
            }
        }
    });
    // hide style switcher
    $document.on('click keyup', function(e) {
        if ($switcher.hasClass('switcher_active')) {
            if (
                (!$(e.target).closest($switcher).length) || (e.keyCode == 27)
            ) {
                $switcher.removeClass('switcher_active');
            }
        }
    });
    // get theme from local storage
    if (localStorage.getItem("altair_theme") !== null) {
        $theme_switcher.children('li[data-app-theme=' + localStorage.getItem("altair_theme") + ']').click();
    }
});
$(function googleFontsInit() {
    WebFontConfig = {
        google: {
            families: [
                'Source+Code+Pro:400,700:latin',
                'Roboto:400,300,500,700,400italic:latin'
            ]
        }
    };
    (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();
});

// Google map stuff
var marker;
function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 7,
        center: {
            lat: 30.21126899159907,
            lng: 31.192671386718757
        }
    });
    marker = new google.maps.Marker({
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        position: {
            lat: 30.21126899159907,
            lng: 31.192671386718757
        }
    });
    marker.addListener('dragend', toggleBounce);
}

function toggleBounce() {
    $('input[name="latitude"]').parent().addClass('md-input-wrapper md-input-focus');
    $('input[name="longitude"]').parent().addClass('md-input-wrapper md-input-focus');
    $('input[name="latitude"]').val(this.position.lat());
    $('input[name="longitude"]').val(this.position.lng());
};

function routingAlert(){
    UIkit.notify({
    message : 'Please use inner navigation system :))',
    status  : 'warning',
    timeout : 2000,
    pos     : 'top-center'
});
}
// routing alert
if (window.history && history.pushState) { // check for history api support
    window.addEventListener('load', function() {
        // create history states
        history.pushState(-1, null); // back state
        history.pushState(0, null); // main state
        history.pushState(1, null); // forward state
        history.go(-1); // start in main state

        this.addEventListener('popstate', function(event, state) {
            // check history state and fire custom events
            if (state = event.state) {

                event = document.createEvent('Event');
                event.initEvent(state > 0 ? 'next' : 'previous', true, true);
                this.dispatchEvent(event);

                // reset state
                history.go(-state);
            }
        }, false);
    }, false);
}
window.addEventListener('next', function() {
    routingAlert();
}, false);

window.addEventListener('previous', function() {
    routingAlert();    
}, false);
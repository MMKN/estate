var notification_id;
var offset = 1;
var curren_page = 1;

function loadMore() {
    $("#loadButton").attr('disabled', true);
    $.ajax({
        async: true,
        type: 'GET',
        url: root_url + '/notifications/more-notifications',
        data: {
            offset: offset,
        },
        success: function(data) {
            if (data != 0) {
                $("#notification_timeline").append(data);
            } else {
                $("#loadButton").hide();
            }
        }
    });
    offset += 1;
    $("#loadButton").attr('disabled', false);
}

function resetAdd() {
    $("#task_description").val('');
    $("#assignee")[0].selectize.clear();
}


function addNote(elem) {
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'POST',
        url: base_url + '/notes',
        data: {
            id: notification_id,
            body: $(elem).parent().find($('.note_area')).val()
        },
        success: function(data) {
            altair_helpers.content_preloader_hide();
            if (data['result'] == 1) {
                $('#comments_list').prepend(data['data']);
                $(elem).parent().find($('.note_area')).val('');
            } else {
                UIkit.notify({
                    message: data,
                    pos: 'top-center',
                    status: 'danger'
                });
            }
        }
    });
}

$("#filter_form").change(function(e) {
    e.preventDefault();
    var formdata = $("#filter_form").serialize();
    $.ajax({
        type: 'GET',
        url: base_url + '/notification-filter',
        data: formdata,
        success: function(data) {
            $("#filter_results").html(data);
            altair_md.inputs();
            altair_forms.init();
            altair_md.checkbox_radio();
        }
    });
});

function deleteIssue() {
    UIkit.modal("#modal-delete-notice").show();
}

function confirmDelete() {
    altair_helpers.content_preloader_show();
    UIkit.modal("#modal-delete-notice").hide();
    var id = notification_id;
    $.ajax({
        async: true,
        type: 'POST',
        url: base_url + '/delete-notification',
        data: {
            id: id
        },
        success: function(data) {
            altair_helpers.content_preloader_hide();
            back_from_details();
            getPage(null, curren_page);
            UIkit.modal("#modal-delete-notice").hide();
        }
    });
}

function searchKeyword(elem) {
    if ($(elem).val() == "") {
        reloadTable();
    } else {
        $("#filter_form").change();
    }
}

function reloadTable() {
    $.ajax({
        type: 'GET',
        url: base_url + '/notification-filter',
        success: function(data) {
            $("#filter_results").html(data);
            altair_md.inputs();
            altair_forms.init();
            altair_md.checkbox_radio();
        }
    });
}

function done(user_id, property_id, elem) {
    $.ajax({
        type: 'POST',
        url: base_url + '/done',
        data: {
            user_id: user_id,
            property_id: property_id
        },
        success: function(data) {
            $(elem).parent().parent().find('span.uk-badge').removeClass('uk-badge-danger').addClass('uk-badge-success').text('Done');
            $(elem).parent().html('<a onclick="undone(' + user_id + ', ' + property_id + ' , this)" class="md-icon material-icons">clear</a>');
            var count = parseInt($('#notifications_count').text());
            $('#notifications_count').html(count - 1);
        }
    });
}

function undone(user_id, property_id, elem) {
    $.ajax({
        type: 'POST',
        url: base_url + '/undone',
        data: {
            user_id: user_id,
            property_id: property_id
        },
        success: function(data) {
            $(elem).parent().parent().find('span.uk-badge').removeClass('uk-badge-success').addClass('uk-badge-danger').text('Open');
            $(elem).parent().html('<a onclick="done(' + user_id + ', ' + property_id + '  , this)" class="md-icon material-icons">done</a>');
            var count = parseInt($('#notifications_count').text());
            $('#notifications_count').html(count + 1);
        }
    });
}

function resetForm() {
    $.ajax({
        type: 'POST',
        url: base_url + '/undone',
        data: {
            user_id: user_id,
            property_id: property_id
        },
        success: function(data) {
            $(elem).parent().parent().find('span.uk-badge').removeClass('uk-badge-success').addClass('uk-badge-danger').text('Open');
            $(elem).parent().html('<a onclick="done(' + user_id + ', ' + property_id + '  , this)" class="md-icon material-icons">done</a>');
            var count = parseInt($('#notifications_count').text());
            $('#notifications_count').html(count + 1);
        }
    });
}


function saveNotice(elem) {
    var task_title = $("#task_title").val();
    var task_description = $("#task_description").val();
    var assignee = $("#assignee").val();
    var priority = $("#priority").val();
    if (task_description == ''  ||  assignee == null) {
        return
    }

    // ux stuff
    event.preventDefault();
    UIkit.modal(".uk-modal").hide();
    altair_helpers.content_preloader_show();


    $.ajax({
        async: true,
        type: 'POST',
        url: base_url + '/add-notice',
        data: {
            title: task_title,
            body: task_description,
            assignee: assignee,
            priority: priority
        },
        success: function(data) {
            if (data == 1) {
                altair_helpers.content_preloader_hide();
                UIkit.notify({
                    message: 'Notice Has Been Added Successfuly.',
                    pos: 'top-center',
                    status: 'success'
                });
                resetAdd();
                getPage(null, 1);
                // reloadTable();
            } else {
                altair_helpers.content_preloader_hide();
                UIkit.notify({
                    message: data,
                    pos: 'top-center',
                    status: 'danger'
                });
            }
        }
    });
}


function notification_details(id) {
    notification_id = id;
    altair_helpers.content_preloader_show();
    $('.page_heading').slideUp();
    $('#notification_content').slideUp();
    $.ajax({
        type: 'GET',
        url: base_url + '/details',
        data: {
            id: id
        },
        success: function(data) {
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
            $('#notification_details').html(data);
            $('#notification_details').slideDown();
            $("#back_btn").fadeIn();
            altair_helpers.content_preloader_hide();
            altair_md.inputs();
            altair_forms.init();
            altair_md.checkbox_radio();
            // property type input tags field initialization
            $('#edit_assignee').selectize({
                plugins: {
                    'remove_button': {
                        label: ''
                    }
                },
                placeholder: 'Add Property Type and Press Enter',

                render: {
                    option: function(data, escape) {
                        return '<div class="option">' +
                            '<span>' + escape(data.title) + '</span>' +
                            '</div>';
                    },
                    item: function(data, escape) {
                        return '<div class="item">' + escape(data.title) + '</div>';
                    }
                },
                maxItems: null,
                valueField: 'value',
                labelField: 'title',
                searchField: 'title',
                create: true,
                onDropdownOpen: function($dropdown) {
                    $dropdown
                        .hide()
                        .velocity('slideDown', {
                            begin: function() {
                                $dropdown.css({
                                    'margin-top': '0'
                                })
                            },
                            duration: 200,
                            easing: easing_swiftOut
                        })
                },
                onDropdownClose: function($dropdown) {
                    $dropdown
                        .show()
                        .velocity('slideUp', {
                            complete: function() {
                                $dropdown.css({
                                    'margin-top': ''
                                })
                            },
                            duration: 200,
                            easing: easing_swiftOut
                        })
                }
            });
        },
        error: function(data) {
            console.log("error");
            $('.page_heading').slideDown();
            $('#notification_content').slideDown();
            getPage(null, curren_page);
        }
    });
}


function back_from_details() {
    $("#back_btn").fadeOut();
    console.log("back ");
    altair_helpers.content_preloader_show();
    console.log("back2 ");
    setTimeout(function() {
        $('#notification_details').slideUp();
        $('.page_heading').slideDown();
        $('#notification_content').slideDown();
        // getPage(null , 1);
        altair_helpers.content_preloader_hide();
    }, 1000);

}

function editNotification() {
    // ux stuff
    event.preventDefault();
    UIkit.modal("#edit_issue").hide();
    altair_helpers.content_preloader_show();

    $.ajax({
        type: 'POST',
        url: base_url + '/edit-notification/' + notification_id,
        data: $('#edit_notification_form').serialize(),
        success: function(data) {
            altair_helpers.content_preloader_show();

            // $('#notification_details').html(data);
            altair_md.inputs();
            altair_forms.init();
            altair_md.checkbox_radio();
            notification_details(notification_id);
        }
    });
}

function getPage(elem, url) {
    curren_page = url;
    altair_helpers.content_preloader_show();
    console.log("getting page: " + url);
    $.ajax({
        async: true,
        type: 'GET',
        url: base_url + '/paginated?page=' + url,
        data: {},
        success: function(data) {
            $("#filter_results").html(data['notifications']);
            $(".pager").html(data['links']);
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
            altair_helpers.content_preloader_hide();
        }
    });
}

function toggleIssue(id, elem) {
    $(elem).attr('disabled', true);
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'POST',
        url: base_url + '/toggle-issue',
        data: {
            id: id
        },
        success: function(data) {
            $("#status-label").html(data['element']);
            if (data['status'] == 1) {
                $(elem).text(data['text']);
            } else {
                $(elem).text(data['text']);
            }

        }
    }).done(function() {
        $(elem).attr('disabled', false);
        altair_helpers.content_preloader_hide();
    });
}

function updateNotificationState(elem) {
    $(elem).parent().removeClass('md-bg-light-blue-50');
    // var count = parseInt($("#notifications_count").text());
    console.log("fetching");
    // count--;
    // if (count <= 0) {
    //     $("#notifications_count").text("");
    // } else {
    //     $("#notifications_count").text(count);
    // }
    $.ajax({
        type: 'GET',
        url: root_url + '/notifications/notification-count',
        data: {},
        success: function(data) {
            if (data <= 0) {
                $("#notifications_count").text("");
            } else {
                $("#notifications_count").text(data);
            }
        }
    });
}

// toggle advanced filter
// customers advanced filter functions
$('#advanced_filter').on('click', function() {
    if ($('#more_filters').css('display') == 'none') {
        $('#more_filters').slideDown();
    }
    else{
            $('#more_filters').slideUp();
    }
});
// Validation
//  require altair_forms.parsley_validation_config(); ( altair_admin_common.js )

$(function() {
    // validation (parsley)
    altair_form_validation.init();
});

// validation (parsley)
altair_form_validation = {
    init: function() {
        var $formValidate = $('#form_validation');

        $formValidate
            .parsley()
            .on('form:validated',function() {
                altair_md.update_input($formValidate.find('.md-input-danger'));
            })
            .on('field:validated',function(parsleyField) {
                if($(parsleyField.$element).hasClass('md-input')) {
                    altair_md.update_input( $(parsleyField.$element) );
                }
            });

        window.Parsley.on('field:validate', function() {
            var $server_side_error = $(this.$element).closest('.md-input-wrapper').siblings('.error_server_side');
            if($server_side_error) {
                $server_side_error.hide();
            }
        });

    }
};
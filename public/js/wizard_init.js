$(function() {
    // wizard
});

// wizard
altair_wizard = {
    content_height: function(this_wizard, step) {
        var this_height = $(this_wizard).find('.step-' + step).actual('outerHeight');
        $(this_wizard).children('.content').animate({
            height: this_height
        }, 280, bez_easing_swiftOut);
    },
    advanced_wizard: function() {
        var $wizard_advanced = $('#wizard_advanced'),
            $add_form = $('#add_form');
        if ($wizard_advanced.length) {
            $wizard_advanced.steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                trigger: 'change',
                onInit: function(event, currentIndex) {
                    altair_wizard.content_height($wizard_advanced,currentIndex);
                    // reinitialize textareas autosize
                    altair_forms.textarea_autosize();
                    // reinitialize checkboxes
                    altair_md.checkbox_radio($(".wizard-icheck"));
                    $(".wizard-icheck").on('ifChecked', function(event){
                        console.log(event.currentTarget.value);
                    });
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                    // reinitialize selects
                    altair_forms.select_elements($wizard_advanced);
                    // reinitialize switches
                    $wizard_advanced.find('span.switchery').remove();
                    altair_forms.switches();
                    // resize content when accordion is toggled
                    $('.uk-accordion').on('toggle.uk.accordion',function() {
                        $window.resize();
                    });

                    setTimeout(function() {
                        $window.resize();
                    },100);
                },
                onStepChanged: function (event, currentIndex) {
                    altair_wizard.content_height($wizard_advanced,currentIndex);
                    setTimeout(function() {
                        $window.resize();
                    },100)
                },
                onStepChanging: function (event, currentIndex, newIndex) {
                    var step = $wizard_advanced.find('.body.current').attr('data-step'),
                        $current_step = $('.body[data-step=\"'+ step +'\"]');

                    // check input fields for errors
                    $current_step.find('.parsley-row').each(function() {
                        $(this).find('input,textarea,select').each(function() {
                            $(this).parsley().validate();
                        });
                    });

                    // adjust content height
                    $window.resize();

                    return $current_step.find('.md-input-danger').length ? false : true;
                },
                onFinished: function() {
                    var step = $wizard_advanced.find('.body.current').attr('data-step'),
                        $current_step = $('.body[data-step=\"'+ step +'\"]');


                    // check input fields for errors
                    $current_step.find('.parsley-row').each(function() {
                        $(this).find('input,textarea,select').each(function() {
                            $(this).parsley().validate();
                        });
                    });


                    if ($current_step.find('.md-input-danger').length) {
                        return false;
                    }
                    customerFormSubmit();
                }
            }) /*.steps("setStep", 2)*/ ;

            $window.on('debouncedresize', function() {
                var current_step = $wizard_advanced.find('.body.current').attr('data-step');
                altair_wizard.content_height($wizard_advanced, current_step);
            });

            // wizard
            $add_form
                .parsley()
                .on('form:validated', function() {
                    setTimeout(function() {
                        altair_md.update_input($add_form.find('.md-input'));
                        // adjust content height
                        $window.resize();
                    }, 100)
                })
                .on('field:validated', function(parsleyField) {

                    var $this = $(parsleyField.$element);
                    setTimeout(function() {
                        altair_md.update_input($this);
                        // adjust content height
                        var currentIndex = $wizard_advanced.find('.body.current').attr('data-step');
                        altair_wizard.content_height($wizard_advanced, currentIndex);
                    }, 100);

                });

        }
    },
    
    advanced_wizard_edit: function() {
        var $wizard_advanced = $('#wizard_advanced_edit'),
            $add_form = $('#edit_formm');
        if ($wizard_advanced.length) {
            $wizard_advanced.steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                trigger: 'change',
                onInit: function(event, currentIndex) {
                    altair_wizard.content_height($wizard_advanced, currentIndex);
                    // initialize checkboxes
                    altair_md.checkbox_radio($(".wizard-icheck"));
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                    // reinitialize selects
                    altair_forms.select_elements($wizard_advanced);
                    // reinitialize textarea autosize
                    altair_forms.textarea_autosize();
                    // reinitialize switches
                    $wizard_advanced.find('span.switchery').remove();
                    altair_forms.switches();




                    setTimeout(function() {
                        $window.resize();
                    }, 100)
                },
                onStepChanged: function(event, currentIndex) {
                    altair_wizard.content_height($wizard_advanced, currentIndex);
                    setTimeout(function() {
                        $window.resize();
                    }, 100)
                },
                onStepChanging: function(event, currentIndex, newIndex) {
                    var step = $wizard_advanced.find('.body.current').attr('data-step'),
                        $current_step = $wizard_advanced.find('.body[data-step=\"' + step + '\"]');

                    // check input fields for errors
                    if (currentIndex > newIndex) {
                        return true;
                    }
                    $current_step.find('[data-parsley-id]').each(function() {
                        $(this).parsley().validate();
                    });

                    // adjust content height
                    $window.resize();

                    return $current_step.find('.md-input-danger').length ? false : true;
                },
                onFinished: function() {
                    var step = $wizard_advanced.find('.body.current').attr('data-step'),
                        $current_step = $wizard_advanced.find('.body[data-step=\"' + step + '\"]');


                    // check input fields for errors
                    $current_step.find('[data-parsley-id]').each(function() {
                        $(this).parsley().validate();
                    });

                    if ($current_step.find('.md-input-danger').length) {
                        return false;
                    }
                    updateCustomer();
                }
            }) /*.steps("setStep", 2)*/ ;

            $window.on('debouncedresize', function() {
                var current_step = $wizard_advanced.find('.body.current').attr('data-step');
                altair_wizard.content_height($wizard_advanced, current_step);
            });

            // wizard
            $add_form
                .parsley()
                .on('form:validated', function() {
                    setTimeout(function() {
                        altair_md.update_input($add_form.find('.md-input'));
                        // adjust content height
                        $window.resize();
                    }, 100)
                })
                .on('field:validated', function(parsleyField) {

                    var $this = $(parsleyField.$element);
                    setTimeout(function() {
                        altair_md.update_input($this);
                        // adjust content height
                        var currentIndex = $wizard_advanced.find('.body.current').attr('data-step');
                        altair_wizard.content_height($wizard_advanced, currentIndex);
                    }, 100);

                });

        }
    },
    add_property_wizard: function() {
        var $wizard_advanced = $('#wizard_advanced'),
            $add_form = $('#add_property_form');
        if ($wizard_advanced.length) {
            $wizard_advanced.steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                trigger: 'change',
                onInit: function(event, currentIndex) {
                    altair_wizard.content_height($wizard_advanced,currentIndex);
                    // reinitialize textareas autosize
                    altair_forms.textarea_autosize();
                    // reinitialize checkboxes
                    altair_md.checkbox_radio($(".wizard-icheck"));
                    $(".wizard-icheck").on('ifChecked', function(event){
                        console.log(event.currentTarget.value);
                    });
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                    // reinitialize selects
                    altair_forms.select_elements($wizard_advanced);
                    // reinitialize switches
                    $wizard_advanced.find('span.switchery').remove();
                    altair_forms.switches();
                    // resize content when accordion is toggled
                    $('.uk-accordion').on('toggle.uk.accordion',function() {
                        $window.resize();
                    });

                    setTimeout(function() {
                        $window.resize();
                    },100);
                },
                onStepChanged: function (event, currentIndex) {
                    altair_wizard.content_height($wizard_advanced,currentIndex);
                    setTimeout(function() {
                        $window.resize();
                    },100)
                },
                onStepChanging: function (event, currentIndex, newIndex) {
                    var step = $wizard_advanced.find('.body.current').attr('data-step'),
                        $current_step = $('.body[data-step=\"'+ step +'\"]');

                    // check input fields for errors
                    $current_step.find('.parsley-row').each(function() {
                        $(this).find('input,textarea,select').each(function() {
                            $(this).parsley().validate();
                        });
                    });

                    // adjust content height
                    $window.resize();

                    return $current_step.find('.md-input-danger').length ? false : true;
                },
                onFinished: function() {
                    
                    var step = $wizard_advanced.find('.body.current').attr('data-step'),
                        $current_step = $('.body[data-step=\"'+ step +'\"]');


                    // check input fields for errors
                    $current_step.find('.parsley-row').each(function() {
                        $(this).find('input,textarea,select').each(function() {
                            $(this).parsley().validate();
                        });
                    });


                    if ($current_step.find('.md-input-danger').length) {
                        return false;
                    }

                    add_new_property();
                }
            }) /*.steps("setStep", 2)*/ ;

            $window.on('debouncedresize', function() {
                var current_step = $wizard_advanced.find('.body.current').attr('data-step');
                altair_wizard.content_height($wizard_advanced, current_step);
            });

            // wizard
            $add_form
            .parsley()
                .on('form:validated',function() {
                    setTimeout(function() {
                        altair_md.update_input($add_form.find('.md-input'));
                        // adjust content height
                        $window.resize();
                    },100)
                })
                .on('field:validated',function(parsleyField) {

                    var $this = $(parsleyField.$element);
                    setTimeout(function() {
                        altair_md.update_input($this);
                        // adjust content height
                        var currentIndex = $wizard_advanced.find('.body.current').attr('data-step');
                        altair_wizard.content_height($wizard_advanced,currentIndex);
                    },100);

                });

        }
    },
    edit_property_wizard: function() {
        var $wizard_advanced = $('#wizard_advanced_edit'),
            $edit_form = $('#edit_property_form');
        if ($wizard_advanced.length) {
            $wizard_advanced.steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                trigger: 'change',
                onInit: function(event, currentIndex) {
                    altair_wizard.content_height($wizard_advanced,currentIndex);
                    // reinitialize textareas autosize
                    altair_forms.textarea_autosize();
                    // reinitialize checkboxes
                    altair_md.checkbox_radio($(".wizard-icheck"));
                    $(".wizard-icheck").on('ifChecked', function(event){
                        console.log(event.currentTarget.value);
                    });
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                    // reinitialize selects
                    altair_forms.select_elements($wizard_advanced);
                    // reinitialize switches
                    $wizard_advanced.find('span.switchery').remove();
                    altair_forms.switches();
                    // resize content when accordion is toggled
                    $('.uk-accordion').on('toggle.uk.accordion',function() {
                        $window.resize();
                    });

                    setTimeout(function() {
                        $window.resize();
                    },100);
                },
                onStepChanged: function (event, currentIndex) {
                    altair_wizard.content_height($wizard_advanced,currentIndex);
                    setTimeout(function() {
                        $window.resize();
                    },100)
                },
                onStepChanging: function (event, currentIndex, newIndex) {
                    var step = $wizard_advanced.find('.body.current').attr('data-step'),
                        $current_step = $('.body[data-step=\"'+ step +'\"]');

                    // check input fields for errors
                    $current_step.find('.parsley-row').each(function() {
                        $(this).find('input,textarea,select').each(function() {
                            $(this).parsley().validate();
                        });
                    });

                    // adjust content height
                    $window.resize();

                    return $current_step.find('.md-input-danger').length ? false : true;
                },

                onFinished: function() {

                    var step = $wizard_advanced.find('.body.current').attr('data-step'),
                        $current_step = $('.body[data-step=\"'+ step +'\"]');


                    // check input fields for errors
                    $current_step.find('.parsley-row').each(function() {
                        $(this).find('input,textarea,select').each(function() {
                            $(this).parsley().validate();
                        });
                    });


                    if ($current_step.find('.md-input-danger').length) {
                        return false;
                    }


                    edit_property();
                }
            }) /*.steps("setStep", 2)*/ ;

            $window.on('debouncedresize', function() {
                var current_step = $wizard_advanced.find('.body.current').attr('data-step');
                altair_wizard.content_height($wizard_advanced, current_step);
            });

            // wizard
            $edit_form
            .parsley()
            .on('form:validated',function() {
                setTimeout(function() {
                    altair_md.update_input($wizard_advanced_form.find('.md-input'));
                    // adjust content height
                    $window.resize();
                },100)
            })
            .on('field:validated',function(parsleyField) {

                var $this = $(parsleyField.$element);
                setTimeout(function() {
                    altair_md.update_input($this);
                    // adjust content height
                    var currentIndex = $wizard_advanced.find('.body.current').attr('data-step');
                    altair_wizard.content_height($wizard_advanced,currentIndex);
                },100);

            });

        }
    },

    clone_property_wizard: function() {
        var $wizard_advanced = $('#wizard_advanced_edit'),
            $edit_form = $('#edit_property_form');
        if ($wizard_advanced.length) {
            $wizard_advanced.steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                trigger: 'change',
                onInit: function(event, currentIndex) {
                    altair_wizard.content_height($wizard_advanced,currentIndex);
                    // reinitialize textareas autosize
                    altair_forms.textarea_autosize();
                    // reinitialize checkboxes
                    altair_md.checkbox_radio($(".wizard-icheck"));
                    $(".wizard-icheck").on('ifChecked', function(event){
                        console.log(event.currentTarget.value);
                    });
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                    // reinitialize selects
                    altair_forms.select_elements($wizard_advanced);
                    // reinitialize switches
                    $wizard_advanced.find('span.switchery').remove();
                    altair_forms.switches();
                    // resize content when accordion is toggled
                    $('.uk-accordion').on('toggle.uk.accordion',function() {
                        $window.resize();
                    });

                    setTimeout(function() {
                        $window.resize();
                    },100);
                },
                onStepChanged: function (event, currentIndex) {
                    altair_wizard.content_height($wizard_advanced,currentIndex);
                    setTimeout(function() {
                        $window.resize();
                    },100)
                },
                onStepChanging: function (event, currentIndex, newIndex) {
                    var step = $wizard_advanced.find('.body.current').attr('data-step'),
                        $current_step = $('.body[data-step=\"'+ step +'\"]');

                    // check input fields for errors
                    $current_step.find('.parsley-row').each(function() {
                        $(this).find('input,textarea,select').each(function() {
                            $(this).parsley().validate();
                        });
                    });

                    // adjust content height
                    $window.resize();

                    return $current_step.find('.md-input-danger').length ? false : true;
                },
                onFinished: function() {
                        

                    var step = $wizard_advanced.find('.body.current').attr('data-step'),
                        $current_step = $('.body[data-step=\"'+ step +'\"]');


                    // check input fields for errors
                    $current_step.find('.parsley-row').each(function() {
                        $(this).find('input,textarea,select').each(function() {
                            $(this).parsley().validate();
                        });
                    });


                    if ($current_step.find('.md-input-danger').length) {
                        return false;
                    }


                    clone_property();
                }
            }) /*.steps("setStep", 2)*/ ;

            $window.on('debouncedresize', function() {
                var current_step = $wizard_advanced.find('.body.current').attr('data-step');
                altair_wizard.content_height($wizard_advanced, current_step);
            });

            // wizard
            $edit_form
                .parsley()
                .on('form:validated', function() {
                    setTimeout(function() {
                        altair_md.update_input($add_form.find('.md-input'));
                        // adjust content height
                        $window.resize();
                    }, 100)
                })
                .on('field:validated', function(parsleyField) {

                    var $this = $(parsleyField.$element);
                    setTimeout(function() {
                        altair_md.update_input($this);
                        // adjust content height
                        var currentIndex = $wizard_advanced.find('.body.current').attr('data-step');
                        altair_wizard.content_height($wizard_advanced, currentIndex);
                    }, 100);

                });

        }
    },
    add_owner_property: function() {
        var $wizard_advanced = $('#wizard_advanced2'),
            $add_form = $('#add_property_form');
        if ($wizard_advanced.length) {
            $wizard_advanced.steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                trigger: 'change',
                onInit: function(event, currentIndex) {
                    altair_wizard.content_height($wizard_advanced,currentIndex);
                    // reinitialize textareas autosize
                    altair_forms.textarea_autosize();
                    // reinitialize checkboxes
                    altair_md.checkbox_radio($(".wizard-icheck"));
                    $(".wizard-icheck").on('ifChecked', function(event){
                        console.log(event.currentTarget.value);
                    });
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                    // reinitialize selects
                    altair_forms.select_elements($wizard_advanced);
                    // reinitialize switches
                    $wizard_advanced.find('span.switchery').remove();
                    altair_forms.switches();
                    // resize content when accordion is toggled
                    $('.uk-accordion').on('toggle.uk.accordion',function() {
                        $window.resize();
                    });

                    setTimeout(function() {
                        $window.resize();
                    },100);
                },
                onStepChanged: function (event, currentIndex) {
                    altair_wizard.content_height($wizard_advanced,currentIndex);
                    setTimeout(function() {
                        $window.resize();
                    },100)
                },
                onStepChanging: function (event, currentIndex, newIndex) {
                    var step = $wizard_advanced.find('.body.current').attr('data-step'),
                        $current_step = $('.body[data-step=\"'+ step +'\"]');

                    // check input fields for errors
                    $current_step.find('.parsley-row').each(function() {
                        $(this).find('input,textarea,select').each(function() {
                            $(this).parsley().validate();
                        });
                    });

                    // adjust content height
                    $window.resize();

                    return $current_step.find('.md-input-danger').length ? false : true;
                },
                onFinished: function() {
                    
                    var step = $wizard_advanced.find('.body.current').attr('data-step'),
                        $current_step = $('.body[data-step=\"'+ step +'\"]');


                    // check input fields for errors
                    $current_step.find('.parsley-row').each(function() {
                        $(this).find('input,textarea,select').each(function() {
                            $(this).parsley().validate();
                        });
                    });


                    if ($current_step.find('.md-input-danger').length) {
                        return false;
                    }
                    add_new_property();
                }
            }) /*.steps("setStep", 2)*/ ;

            $window.on('debouncedresize', function() {
                var current_step = $wizard_advanced.find('.body.current').attr('data-step');
                altair_wizard.content_height($wizard_advanced, current_step);
            });

            // wizard
            $add_form
                .parsley()
                .on('form:validated', function() {
                    setTimeout(function() {
                        altair_md.update_input($add_form.find('.md-input'));
                        // adjust content height
                        $window.resize();
                    }, 100)
                })
                .on('field:validated', function(parsleyField) {

                    var $this = $(parsleyField.$element);
                    setTimeout(function() {
                        altair_md.update_input($this);
                        // adjust content height
                        var currentIndex = $wizard_advanced.find('.body.current').attr('data-step');
                        altair_wizard.content_height($wizard_advanced, currentIndex);
                    }, 100);

                });

        }
    },

};
estatify
    .service('contactsService', ['contactsFactory', '$rootScope',
        function(contactsFactory, $rootScope) {
            // get companies
            this.getCompanies = function() {
                if ($rootScope.companies.length == 0) {
                    contactsFactory.getCompanies().success(function(respond) {
                        $rootScope.companies = respond.data;
                    }).error(function(error) {
                        UIkit.notify("Something went north, could not fetch companies data", 'danger');
                    });
                };
            };

            this.getIndividuals = function() {
                // get individuals
                if ($rootScope.individuals.length == 0) {
                    contactsFactory.getIndividuals().success(function(respond) {
                        $rootScope.individuals = respond.data;
                    }).error(function(error) {
                        UIkit.notify("Something went north, could not fetch individuals data", 'danger');
                    });
                };
            };
        }
    ])
    .service('leadsTagsService', ['$rootScope', 'leadsFactory',
        function($rootScope, leadsFactory) {
            this.getLeadsTags = function() {
                if ($rootScope.leadsTags.length == 0) {
                    this.leadsTagsLoaded = true;
                    leadsFactory.getTags().success(function(respond) {
                        $rootScope.leadsTags = respond.data;
                        this.leadsTagsLoaded = true;
                    }).error(function(error) {
                        UIkit.notify('failed to load some data', 'danger')
                    })
                }
            };
        }
    ])

.service('leadsService', ['leadsFactory', '$rootScope',
    function(leadsFactory, $rootScope) {
        this.getLeads = function() {
            if ($rootScope.leads.length == 0) {
                leadsFactory.getLeads().success(function(respond) {
                    $rootScope.leads = respond.data;
                }).error(function(error) {
                    UIkit.notify('failed to load the leads', 'danger')
                })
            }
        }
    }
])
.service('employeesService', ['employeesFactory', '$rootScope',
    function(employeesFactory, $rootScope) {
        this.getEmployees = function() {
            if ($rootScope.employees.length == 0) {
                employeesFactory.getEmployees().success(function(respond) {
                    $rootScope.employees = respond.data;
                }).error(function(error) {
                    UIkit.notify('failed to load the employees', 'danger')
                })
            }
        }
    }
])









.service('detectBrowser', [
    '$window',
    function($window) {
        // http://stackoverflow.com/questions/22947535/how-to-detect-browser-using-angular
        return function() {
            var userAgent = $window.navigator.userAgent,
                browsers = {
                    chrome: /chrome/i,
                    safari: /safari/i,
                    firefox: /firefox/i,
                    ie: /internet explorer/i
                };

            for (var key in browsers) {
                if (browsers[key].test(userAgent)) {
                    return key;
                }
            }
            return 'unknown';
        }
    }
])
    .service('preloaders', [
        '$rootScope',
        '$timeout',
        'utils',
        function($rootScope, $timeout, utils) {
            $rootScope.content_preloader_show = function(style, container) {
                var $body = $('body');
                if (!$body.find('.content-preloader').length) {
                    var image_density = utils.isHighDensity() ? '@2x' : '';

                    var preloader_content = (typeof style !== 'undefined' && style == 'regular') ? '<img src="front/assets/img/spinners/spinner' + image_density + '.gif" alt="" width="32" height="32">' : '<div class="md-preloader"><svg xmlns="http://www.w3.org/2000/svg" version="1.1" height="32" width="32" viewbox="0 0 75 75"><circle cx="37.5" cy="37.5" r="33.5" stroke-width="8"/></svg></div>';

                    var thisContainer = (typeof container !== 'undefined') ? container : $body;

                    thisContainer.append('<div class="content-preloader">' + preloader_content + '</div>');
                    $timeout(function() {
                        $('.content-preloader').addClass('preloader-active');
                    });
                }
            };
            $rootScope.content_preloader_hide = function() {
                var $body = $('body');
                if ($body.find('.content-preloader').length) {
                    // hide preloader
                    $('.content-preloader').removeClass('preloader-active');
                    // remove preloader
                    $timeout(function() {
                        $('.content-preloader').remove();
                    }, 500);
                }
            };

        }
    ]);
estatify
    .factory('windowDimensions', [
        '$window',
        function($window) {
            return {
                height: function() {
                    return window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
                },
                width: function() {
                    return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
                }
            }
        }
    ])
    .factory('utils', [

        function() {
            return {
                // Util for finding an object by its 'id' property among an array
                findByItemId: function findById(a, id) {
                    for (var i = 0; i < a.length; i++) {
                        if (a[i].item_id == id) return a[i];
                    }
                    return null;
                },
                // serialize form
                serializeObject: function(form) {
                    var o = {};
                    var a = form.serializeArray();
                    $.each(a, function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    return o;
                },
                // high density test
                isHighDensity: function() {
                    return ((window.matchMedia && (window.matchMedia('only screen and (min-resolution: 124dpi), only screen and (min-resolution: 1.3dppx), only screen and (min-resolution: 48.8dpcm)').matches || window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (min-device-pixel-ratio: 1.3)').matches)) || (window.devicePixelRatio && window.devicePixelRatio > 1.3));
                },
                // touch device test
                isTouchDevice: function() {
                    return !!('ontouchstart' in window);
                },
                // local storage test
                lsTest: function() {
                    var test = 'test';
                    try {
                        localStorage.setItem(test, test);
                        localStorage.removeItem(test);
                        return true;
                    } catch (e) {
                        return false;
                    }
                },
                // show/hide card
                card_show_hide: function(card, begin_callback, complete_callback, callback_element) {
                    $(card)
                        .velocity({
                            scale: 0,
                            opacity: 0.2
                        }, {
                            duration: 400,
                            easing: [0.4, 0, 0.2, 1],
                            // on begin callback
                            begin: function() {
                                if (typeof begin_callback !== 'undefined') {
                                    begin_callback(callback_element);
                                }
                            },
                            // on complete callback
                            complete: function() {
                                if (typeof complete_callback !== 'undefined') {
                                    complete_callback(callback_element);
                                }
                            }
                        })
                        .velocity('reverse');
                }
            };
        }
    ]);

angular
    .module("ConsoleLogger", [])
// router.ui debug
// app.js (run section "PrintToConsole")
.factory("PrintToConsole", [
    "$rootScope",
    function($rootScope) {
        var handler = {
            active: false
        };
        handler.toggle = function() {
            handler.active = !handler.active;
        };

        if (handler.active) {
            console.log($state + ' = ' + $state.current.name);
            console.log($stateParams + '=' + $stateParams);
            console.log($state_full_url + '=' + $state.$current.url.source);
            console.log(Card_fullscreen + '=' + card_fullscreen);

            $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                console.log("$stateChangeStart --- event, toState, toParams, fromState, fromParams");
                console.log(arguments);
            });
            $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
                console.log("$stateChangeError --- event, toState, toParams, fromState, fromParams, error");
                console.log(arguments);
            });
            $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
                console.log("$stateChangeSuccess --- event, toState, toParams, fromState, fromParams");
                console.log(arguments);
            });
            $rootScope.$on('$viewContentLoading', function(event, viewConfig) {
                console.log("$viewContentLoading --- event, viewConfig");
                console.log(arguments);
            });
            $rootScope.$on('$viewContentLoaded', function(event) {
                console.log("$viewContentLoaded --- event");
                console.log(arguments);
            });
            $rootScope.$on('$stateNotFound', function(event, unfoundState, fromState, fromParams) {
                console.log("$stateNotFound --- event, unfoundState, fromState, fromParams");
                console.log(arguments);
            });
        }

        return handler;
    }
]);
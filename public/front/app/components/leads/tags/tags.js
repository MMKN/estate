angular.module("estatify")
    .controller('tagsCtrl', ['$scope', '$rootScope', 'leadsFactory','$state','$timeout', 'leadsTagsService',
        function($scope, $rootScope , leadsFactory, $state , $timeout, leadsTagsService) {

            // init
            $timeout(function(){
                UIkit.modal("#leads_tags", {bgclose: false, keyboard:false,center:true}).show();
            }, 100);
            $('#leads_tags').on({
                'hide.uk.modal': function(){
                    $state.go('restricted.leads.index');
                }
            });

        	// get tags
            leadsTagsService.getLeadsTags();
        	

            // Add tag
        	$scope.resetTag = function(){
        		$scope.tag ={
        			name: '',
        			type:''
        		}
        	};
			$scope.resetTag();
        	$scope.addTag = function(){
        		UIkit.modal('#add_tag').hide();
        		$rootScope.content_preloader_show();
                console.log($scope.tag);
        		leadsFactory.addTag($scope.tag).success(function(respond){
        			$rootScope.leadsTags.unshift(respond.data);
                    $scope.resetTag();
        			UIkit.notify("We are all good (Y)" , 'success');
                    $rootScope.content_preloader_hide();
        		}).error(function(error){
                    $rootScope.content_preloader_hide();
        			UIkit.notify("Something went south :D" , 'danger')
        		});
        	};

            // delete Tag
            $scope.deleteTag = function($id , $index){
                UIkit.modal.confirm("Are you sure" , function(){
                    $rootScope.content_preloader_show();
                    leadsFactory.deleteTag($id).success(function(respond){
                        $rootScope.content_preloader_hide();
                        $rootScope.leadsTags.splice($index, 1);
                    }).error(function(error){
                        $rootScope.content_preloader_hide();
                        UIkit.notify("Something went south :D" , 'danger')
                    });
                })
            }

        }
    ])

    ;
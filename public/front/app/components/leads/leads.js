var leadsModule = angular.module("estatify", [angularDragula(angular)]);
leadsModule
    .controller('leadsCtrl', ['contactsFactory', 'employeesService', '$timeout', '$scope', '$rootScope', 'leadsFactory', 'variables', 'dragulaService', '$filter', 'leadsTagsService', 'contactsService', 'leadsService',
        function(contactsFactory,employeesService,$timeout ,$scope, $rootScope, leadsFactory, variables, dragulaService, $filter, leadsTagsService, contactsService ,leadsService) {
            // $rootScope.pageHeadingActive = true;

            // $scope.$on('$destroy', function() {
            //     $rootScope.pageHeadingActive = false;
            // });

            //  fetching some required data
            leadsService.getLeads();
            leadsTagsService.getLeadsTags();
            contactsService.getCompanies();
            contactsService.getIndividuals();
            employeesService.getEmployees();

            $scope.$watch('leadsTagsService.leadsTagsLoaded' , function(){
                if (leadsTagsService.leadsTagsLoaded) {
                    $scope.tempLeadsTags = angular.copy($rootScope.leadsTags);
                }
            });
            ////////////////////////////
            // Add lead
            $scope.resetLead = function() {
                $scope.lead = {
                    customer_type: '',
                    customer_id: null,
                    description: '',
                    start_date: $filter('date')(new Date(), 'dd.MM.yyyy'),
                    potential_value: null,
                    wining_probability: null,
                    tags: [],
                    members: [],
                    creator_id: 1,
                    status: '',
                }
            };
            $scope.resetLead();

            // open add new lead modal with custom modal configurations
            $scope.openAddLeadModal = function(){
                $timeout(function(){
                    UIkit.modal("#add_lead", {bgclose: false, keyboard:false , modal:false}).show();
                }, 100);
            };

            // add new lead
            $scope.addLead = function($lead){
                console.log($lead)
                UIkit.modal('#add_lead').hide();
                $rootScope.content_preloader_show();
                leadsFactory.addLead($lead).success(function(respond) {
                    $rootScope.leads.unshift(respond.data);
                    $scope.resetLead();
                    $rootScope.content_preloader_hide();
                }).error(function(error) {
                    UIkit.notify("Something went south :D", 'danger');
                    $rootScope.content_preloader_hide();
                    $scope.lead = $lead;
                    $scope.openAddLeadModal();
                });
            };




            // update portential customers according to the customer type(b2c or b2b)
            $scope.potential_customers = [];
            $scope.customers_selectize_config = {
                create:false,
                labelField: 'name',
                valueField: 'id',
                searchField: ['name' , 'phone','email'] ,
                placeholder: 'Potential Customer',
                maxItems: 1,
                load: function(query, callback) {
                    if (!query.length) return callback();
                    if ($scope.lead.customer_type == 'b2c') {
                        $rootScope.content_preloader_show();
                        contactsFactory.SearchIndividuals(query).success(function(respond){
                        $rootScope.content_preloader_hide();
                            callback(respond.data);
                        }).error(function(error){
                            callback();
                            $rootScope.content_preloader_hide();
                        });
                    }
                    else if($scope.lead.customer_type == 'b2b'){
                        $rootScope.content_preloader_show();

                        contactsFactory.searchCompanies(query).success(function(respond){
                            callback(respond.data);
                        }).error(function(error){
                            callback();
                            $rootScope.content_preloader_hide();
                        });
                    }
                    // $.ajax({
                    //     url: 'api/individuals/search',
                    //     data: {query:query},
                    //     type: 'GET',
                    //     error: function() {
                    //         alert('e')
                    //         callback();
                    //     },
                    //     success: function(res) {
                    //         alert('s')
                    //         console.log(res)
                    //         callback(res.data);
                    //     }
                    // });
                }

            };

            // while adding lead, we need to update customers array
            $scope.$watch('lead.customer_type', function() {
                // if ($scope.lead.customer_type == 'b2c') {
                //     $scope.potential_customers = $rootScope.individuals;
                // } else if($scope.lead.customer_type == 'b2b') {
                //     $scope.potential_customers = $rootScope.companies;
                // }else{
                //     $scope.potential_customers = '';
                // }
                $scope.potential_customers = [];

            });
            /////////////////////////////////////////////
            // $timeout(function(){
            //     $scope.leads.push({
            //         id: 173,
            //         winning_propability: 19,
            //         potential_value: 20000,
            //         description: 'CRM + potential website',
            //         tags: ['estatify', 'CRM', 'Website'],
            //         customer: {
            //             name: 'المثلث العقاري',
            //             type: 'company',
            //             id: 5
            //         },
            //         status: 'danger',
            //         creator_id: 8,
            //         contancts: [],
            //         members: [],
            //         tasks: []
            //     });
            // }, 2000);


            ////////////////////////////////////////////////////
            ///////////// external function ////////////////////
            // add contact stuff
             $scope.resetIndividual = function() {
                $scope.individual = {
                    name: '',
                    email: '',
                    phone: '',
                    address: '',
                    phone: '',
                    description: ''
                }
            };
            $scope.resetIndividual();
            $scope.resetCompany = function() {
                $scope.company = {
                    name: '',
                    email: '',
                    phone: '',
                    address: '',
                    phone: '',
                    description: ''
                }
            };
            $scope.resetCompany();
            $scope.addIndividual = function() {
                UIkit.modal('#add_contact').hide();
                $rootScope.content_preloader_show();
                contactsFactory.addIndividual($scope.individual).success(function(respond) {
                    $scope.lead.customer_id = respond.data.id;
                    $rootScope.individuals.unshift(respond.data);
                    $scope.resetCompany();
                    $rootScope.content_preloader_hide();
                    $scope.lead.customer_type = 'b2c';
                }).error(function(error) {
                    UIkit.notify("Something went south :D", 'danger');
                    $rootScope.content_preloader_hide();
                });
            };
            $scope.addCompany = function() {
                UIkit.modal('#add_contact').hide();
                $rootScope.content_preloader_show();
                contactsFactory.addCompany($scope.company).success(function(respond) {
                    $scope.lead.customer_id = respond.data.id;
                    $rootScope.companies.unshift(respond.data);
                    $scope.resetCompany();
                    $scope.lead.customer_type = 'b2b';
                    $rootScope.content_preloader_hide();
                }).error(function(error) {
                    UIkit.notify("Something went south :D", 'danger');
                    $rootScope.content_preloader_hide();
                });
            };

            // UI stuff
            $scope.epc_orders = {
                barColor: '#009688',
                scaleColor: false,
                trackColor: '#f5f5f5',
                lineWidth: 5,
                size: 110,
                easing: variables.bez_easing_swiftOut
            };


        }
    ])

;   
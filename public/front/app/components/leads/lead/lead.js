leadsModule
    .controller('leadCtrl', ['$scope', '$rootScope', 'leadsFactory', 'variables', 'dragulaService',
        function($scope, $rootScope, leadsFactory, variables, dragulaService) {

            $scope.lead = {
                id: 173,
                winning_propability: 19,
                potential_value: 20000,
                description: 'CRM + potential website',
                tags: ['estatify', 'CRM', 'Website'],
                customer: {
                    name: 'المثلث العقاري',
                    type: 'company',
                    id: 5
                },
                status: 'danger',
                creator_id: 8,
                contancts: [],
                members: [],
                tasks: []
            };


            ///////////////////////////////////
            ////// Scrum board stuff /////////
            $scope.task_groups = [{
                id: 'todo',
                name: 'To Do'
            }, {
                id: 'inAnalysis',
                name: 'In analysis'
            }, {
                id: 'inProgress',
                name: 'In progress'
            }, {
                id: 'done',
                name: 'Done'
            }];

            $scope.tasks_list = [];

            // task info
            $scope.taskInfo = function(task) {
                $scope.info = {
                    name: task.name,
                    title: task.title,
                    status: task.status,
                    description: task.description,
                    assignee: task.assignee
                }
            };

            // new task
            $scope.newTask = {
                name: 'Altair-245',
                assignee: [{
                    id: 1,
                    title: 'Aleen Grant'
                }, {
                    id: 2,
                    title: 'Tyrese Koss'
                }, {
                    id: 3,
                    title: 'Chasity Green'
                }, {
                    id: 4,
                    title: 'Me'
                }],
                group: [{
                    name: 'todo',
                    title: 'To Do'
                }, {
                    name: 'inAnalysis',
                    title: 'In Analysis'
                }, {
                    name: 'inProgress',
                    title: 'In Progress'
                }, {
                    name: 'done',
                    title: 'Done'
                }]
            };

            $scope.newTask_assignee = $scope.newTask.assignee;
            $scope.newTask_assignee_config = {
                create: false,
                maxItems: 1,
                valueField: 'id',
                labelField: 'title',
                placeholder: 'Select Assignee...'
            };
            $scope.newTask_group = $scope.newTask.group;
            $scope.newTask_group_config = {
                create: false,
                maxItems: 1,
                valueField: 'name',
                labelField: 'title',
                placeholder: 'Select Group...'
            };

            $scope.$on('tasks.drop', function(e, el, target, source) {
                console.log(target[0].id);
            });
        }
    ])
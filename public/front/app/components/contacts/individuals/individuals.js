angular.module("estatify")
    .controller('individualesCtrl', ['$scope', '$rootScope','contactsFactory','$timeout',
        function($scope, $rootScope ,contactsFactory,$timeout) {


        	// show edit modal for a individual
        	$scope.editIndividual = function($individual) {
        	    $scope.individual = angular.copy($individual);
        	    UIkit.modal("#edit_individual", {
        	        bgclose: false,
        	        center: true,
        	        keyboard: false
        	    }).show();
        	};

        	// Save individual
        	$scope.updateIndividual = function(individual) {
        	    UIkit.modal("#edit_individual").hide();
        	    $rootScope.content_preloader_show();
        	    contactsFactory.updateIndividual(individual.id, individual).success(function(respond) {
                    console.log(respond);
        	        $rootScope.individuals[$rootScope.individuals.findIndex(x => x.id == individual.id)] = respond.data;
        	        $rootScope.content_preloader_hide();
        	        UIkit.notify('Nice', 'success');
        	    }).error(function(error) {
        	        UIkit.notify('That didn\'t work', 'danger');
        	        $rootScope.content_preloader_hide();
        	        $scope.individual = individual;
        	        $timeout(function() {
        	            UIkit.modal("#edit_individual").show();
        	        });
        	    })
        	};

        	// delete individual
        	$scope.deleteIndividual = function(id) {
        	    UIkit.modal.confirm('Are you sure', function() {
        	    	$rootScope.content_preloader_show();
        	        contactsFactory.deleteIndividual(id).success(function(respond) {
        	            $rootScope.content_preloader_hide();
                        if ($scope.individuals.length>0) {
                            $scope.individuals.splice($scope.individuals.findIndex(x => x.id == id),1); 
                        }else{
        	            $rootScope.individuals.splice($rootScope.individuals.findIndex(x => x.id == id),1); 
                            
                        }
        	            UIkit.notify('That was easy :)', 'success');
        	        }).error(function(error) {
        	            UIkit.notify('That didn\'t work', 'danger');
        	            $rootScope.content_preloader_hide();
        	        })
        	    });
        	};

            // restore individual
            $scope.restoreIndividual = function(id) {
                UIkit.modal.confirm('Are you sure', function() {
                    $rootScope.content_preloader_show();
                    contactsFactory.restoreIndividual(id).success(function(respond) {
                        $rootScope.content_preloader_hide();
                        $rootScope.individuals[$rootScope.individuals.findIndex(x => x.id == id)].deleted = false; 
                        UIkit.notify('That was easy :)', 'success');
                    }).error(function(error) {
                        UIkit.notify('That didn\'t work', 'danger');
                        $rootScope.content_preloader_hide();
                    })
                });
            };




        }
    ]);
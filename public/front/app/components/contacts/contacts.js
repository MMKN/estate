angular.module("estatify")
    .controller('contactsCtrl', ['$scope', '$rootScope', 'contactsFactory', 'contactsService',
        function($scope, $rootScope, contactsFactory, contactsService) {
            // $rootScope.pageHeadingActive = true;

            // $scope.$on('$destroy', function() {
            //     $rootScope.pageHeadingActive = false;
            // });

            // get companies
            contactsService.getCompanies();
            // get Individuales
            contactsService.getIndividuals();


            $scope.getArchived = function(){
                $rootScope.content_preloader_show();
                contactsFactory.getArchivedIndividuals().success(function(respond){
                    angular.forEach(respond.data , function(x){
                        x.deleted = true;
                    })
                    $scope.individuals = respond.data.concat($rootScope.individuals);
                    $rootScope.content_preloader_hide();
                }).error(function(respond){
                    UIkit.notify("Something went south :D", 'danger');
                    $rootScope.content_preloader_hide();
                });
                contactsFactory.getArchivedCompanies().success(function(respond){
                    angular.forEach(respond.data , function(x){
                        x.deleted = true;
                    })
                    $scope.companies = respond.data.concat($rootScope.companies);
                    $rootScope.content_preloader_hide();
                }).error(function(respond){
                    UIkit.notify("Something went south :D", 'danger');
                    $rootScope.content_preloader_hide();
                });
            };



            $scope.resetCompany = function() {
                $scope.company = {
                    name: '',
                    email: '',
                    phone: '',
                    address: '',
                    phone: '',
                    description: ''
                }
            };
            $scope.resetCompany();
            $scope.addCompany = function() {
                UIkit.modal('#add_contact').hide();
                $rootScope.content_preloader_show();
                contactsFactory.addCompany($scope.company).success(function(respond) {
                    console.log(respond);
                    $rootScope.companies.unshift(respond.data);
                    $scope.resetCompany();
                    UIkit.notify("We are all good (Y)", 'success');
                    $rootScope.content_preloader_hide();
                }).error(function(error) {
                    UIkit.notify("Something went south :D", 'danger');
                    $rootScope.content_preloader_hide();
                });
            };

            $scope.resetIndividual = function() {
                $scope.individual = {
                    name: '',
                    email: '',
                    phone: '',
                    address: '',
                    phone: '',
                    description: ''
                }
            };
            $scope.resetIndividual();
            $scope.addIndividual = function() {
                console.log($scope.individual)
                UIkit.modal('#add_contact').hide();
                $rootScope.content_preloader_show();
                contactsFactory.addIndividual($scope.individual).success(function(respond) {
                    console.log(respond);
                    $rootScope.individuals.unshift(respond.data);
                    $scope.resetCompany();
                    UIkit.notify("We are all good (Y)", 'success');
                    $rootScope.content_preloader_hide();
                }).error(function(error) {
                    UIkit.notify("Something went south :D", 'danger');
                    $rootScope.content_preloader_hide();
                });
            };

        }
    ]);
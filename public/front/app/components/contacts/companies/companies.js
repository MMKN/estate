angular.module("estatify")
    .controller('companiesCtrl', ['$timeout', '$scope', '$rootScope', 'contactsFactory',
        function($timeout, $scope, $rootScope, contactsFactory) {


            // show edit modal for a company
            $scope.editCompany = function($company) {
                $scope.company = angular.copy($company);
                UIkit.modal("#edit_company", {
                    bgclose: false,
                    center: true,
                    keyboard: false
                }).show();
            };

            // Save company
            $scope.updateCompany = function(company) {
                UIkit.modal("#edit_company").hide();
                $rootScope.content_preloader_show();
                contactsFactory.updateCompany(company.id, company).success(function(respond) {
                    $rootScope.companies[$rootScope.companies.findIndex(x => x.id == company.id)] = respond.data;
                    $rootScope.content_preloader_hide();
                    UIkit.notify('Nice', 'success');
                }).error(function(error) {
                    UIkit.notify('That didn\'t work', 'danger');
                    $rootScope.content_preloader_hide();
                    $scope.company = company;
                    $timeout(function() {
                        UIkit.modal("#edit_company").show();
                    });
                })
            };

            // delete Company
            $scope.deleteCompany = function(id) {
                UIkit.modal.confirm('Are you sure', function() {
                	$rootScope.content_preloader_show();
                    contactsFactory.deleteCompany(id).success(function(respond) {
                        $rootScope.content_preloader_hide();
                        if ($scope.companies.length>0) {
                            $scope.companies.splice($scope.companies.findIndex(x => x.id == id),1); 
                        }
                        else{
                            $rootScope.companies.splice($rootScope.companies.findIndex(x => x.id == id),1);
                        }
                        UIkit.notify('That was easy :)', 'success');
                    }).error(function(error) {
                        UIkit.notify('That didn\'t work', 'danger');
                        $rootScope.content_preloader_hide();
                    })
                });
            };

            // restore Company
            $scope.restoreCompany = function(id) {
                UIkit.modal.confirm('Are you sure', function() {
                    $rootScope.content_preloader_show();
                    contactsFactory.restoreCompany(id).success(function(respond) {
                        $rootScope.content_preloader_hide();
                        $rootScope.companies[$rootScope.companies.findIndex(x => x.id == id)].deleted = false; 
                        UIkit.notify('That was easy :)', 'success');
                    }).error(function(error) {
                        UIkit.notify('That didn\'t work', 'danger');
                        $rootScope.content_preloader_hide();
                    })
                });
            };
        }
    ]);
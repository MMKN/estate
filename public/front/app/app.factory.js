angular.module('estatify')
    .factory('leadsFactory', ['$http',
        function($http) {
            return {
                getLeads: function() {
                    return $http.get('api/leads');
                },
                addLead:function(data){
                    return $http.post('api/leads', data);
                },
                getTags:function(){
                    return $http.get('api/tags')
                },
                addTag:function(data){
                    return $http.post('api/tags', data);
                },
                updateTag:function(id, data){
                    return $http.put('api/tags/' + id, data);
                },
                deleteTag:function(id){
                    return $http.delete('api/tags/' + id);
                },
            };
        }
    ])
    .factory('employeesFactory', ['$http',
        function($http) {
            return {
                getEmployees: function() {
                    return $http.get('api/employees');
                },
                // addLead:function(data){
                //     return $http.post('api/leads', data);
                // },
                // getTags:function(){
                //     return $http.get('api/tags')
                // },
                // addTag:function(data){
                //     return $http.post('api/tags', data);
                // },
                // updateTag:function(id, data){
                //     return $http.put('api/tags/' + id, data);
                // },
                // deleteTag:function(id){
                //     return $http.delete('api/tags/' + id);
                // },
            };
        }
    ])
    .factory('contactsFactory', ['$http',
        function($http) {
            return {
                getCompanies: function() {
                    return $http.get('api/companies');
                },
                searchCompanies: function(data) {
                    return $http.get('api/companies/search',data);
                },
                addCompany:function(data){
                    return $http.post('api/companies', data);
                },
                updateCompany:function(id, data){
                    return $http.put('api/companies/' + id, data);
                },
                deleteCompany:function(id){
                    return $http.delete('api/companies/' + id);
                },
                getArchivedCompanies: function() {
                    return $http.get('api/companies/archived');
                },
                getIndividuals: function() {
                    return $http.get('api/individuals');
                },
                SearchIndividuals: function(data) {
                    return $http.get('api/individuals/search' , data);
                },
                addIndividual:function(data){
                    return $http.post('api/individuals', data);
                },
                updateIndividual:function(id, data){
                    return $http.put('api/individuals/' + id, data);
                },
                updateIndividual:function(id, data){
                    return $http.put('api/individuals/' + id, data);
                },
                deleteIndividual:function(id){
                    return $http.delete('api/individuals/' + id);
                },
                getArchivedIndividuals: function() {
                    return $http.get('api/individuals/archived');
                },
            };
        }
    ])

;
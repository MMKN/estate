/* ocLazyLoad config */

estatify
    .config([
        '$ocLazyLoadProvider',
        function($ocLazyLoadProvider) {
            $ocLazyLoadProvider.config({
                debug: false,
                events: false,
                modules: [
                    // ----------- UIKIT ------------------
                    {
                        name: 'lazy_uikit',
                        files: [
                            // uikit core
                            "front/bower_components/uikit/js/uikit.min.js",
                            // uikit components
                            "front/bower_components/uikit/js/components/accordion.min.js",
                            "front/bower_components/uikit/js/components/autocomplete.min.js",
                            "front/assets/js/custom/uikit_datepicker.min.js",
                            "front/bower_components/uikit/js/components/form-password.min.js",
                            "front/bower_components/uikit/js/components/form-select.min.js",
                            "front/bower_components/uikit/js/components/grid.min.js",
                            "front/bower_components/uikit/js/components/lightbox.min.js",
                            "front/bower_components/uikit/js/components/nestable.min.js",
                            "front/bower_components/uikit/js/components/notify.min.js",
                            "front/bower_components/uikit/js/components/slider.min.js",
                            "front/bower_components/uikit/js/components/slideshow.min.js",
                            "front/bower_components/uikit/js/components/sortable.min.js",
                            "front/bower_components/uikit/js/components/sticky.min.js",
                            "front/bower_components/uikit/js/components/tooltip.min.js",
                            "front/assets/js/custom/uikit_timepicker.min.js",
                            "front/bower_components/uikit/js/components/upload.min.js",
                            "front/assets/js/custom/uikit_beforeready.min.js",
                            // styles
                            "front/bower_components/uikit/css/uikit.almost-flat.min.css"
                        ],
                        insertBefore: '#main_stylesheet',
                        serie: true
                    },

                    // ----------- FORM ELEMENTS -----------
                    {
                        name: 'lazy_autosize',
                        files: [
                            'front/bower_components/autosize/dist/autosize.min.js',
                            'front/app/modules/angular-autosize.min.js'
                        ],
                        serie: true
                    }, {
                        name: 'lazy_iCheck',
                        files: [
                            "front/bower_components/iCheck/icheck.min.js",
                            'front/app/modules/angular-icheck.min.js'
                        ],
                        serie: true
                    }, {
                        name: 'lazy_selectizeJS',
                        files: [
                            'front/bower_components/selectize/dist/js/standalone/selectize.min.js',
                            'front/app/modules/angular-selectize.min.js'
                        ],
                        serie: true
                    }, {
                        name: 'lazy_switchery',
                        files: [
                            'front/bower_components/switchery/dist/switchery.min.js',
                            'front/app/modules/angular-switchery.min.js'
                        ],
                        serie: true
                    }, {
                        name: 'lazy_ionRangeSlider',
                        files: [
                            'front/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js',
                            'front/app/modules/angular-ionRangeSlider.min.js'
                        ],
                        serie: true
                    }, {
                        name: 'lazy_masked_inputs',
                        files: [
                            'front/bower_components/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js'
                        ]
                    }, {
                        name: 'lazy_character_counter',
                        files: [
                            'front/app/modules/angular-character-counter.min.js'
                        ]
                    }, {
                        name: 'lazy_parsleyjs',
                        files: [
                            'front/assets/js/custom/parsleyjs_config.min.js',
                            'front/bower_components/parsleyjs/dist/parsley.min.js'
                        ],
                        serie: true
                    },

                    // ----------- COMPONENTS -----------
                    {
                        name: 'lazy_prismJS',
                        files: [
                            "front/bower_components/prism/prism.js",
                            "front/bower_components/prism/components/prism-php.js",
                            "front/bower_components/prism/plugins/line-numbers/prism-line-numbers.js",
                            'front/app/modules/angular-prism.min.js'
                        ],
                        serie: true
                    },
                    {
                        name: 'lazy_charts_easypiechart',
                        files: [
                            'front/bower_components/jquery.easy-pie-chart/dist/angular.easypiechart.min.js'
                        ]
                    },
                    // Dragula
                    {
                        name: 'lazy_dragula',
                        files: [
                            'front/bower_components/angular-dragula/dist/angular-dragula.min.js'
                        ]
                    },

                    // ----------- KENDOUI COMPONENTS -----------
                    {
                        name: 'lazy_KendoUI',
                        files: [
                            'front/bower_components/kendo-ui/js/kendo.core.min.js',
                            'front/bower_components/kendo-ui/js/kendo.color.min.js',
                            'front/bower_components/kendo-ui/js/kendo.data.min.js',
                            'front/bower_components/kendo-ui/js/kendo.calendar.min.js',
                            'front/bower_components/kendo-ui/js/kendo.popup.min.js',
                            'front/bower_components/kendo-ui/js/kendo.datepicker.min.js',
                            'front/bower_components/kendo-ui/js/kendo.timepicker.min.js',
                            'front/bower_components/kendo-ui/js/kendo.datetimepicker.min.js',
                            'front/bower_components/kendo-ui/js/kendo.list.min.js',
                            'front/bower_components/kendo-ui/js/kendo.fx.min.js',
                            'front/bower_components/kendo-ui/js/kendo.userevents.min.js',
                            'front/bower_components/kendo-ui/js/kendo.menu.min.js',
                            'front/bower_components/kendo-ui/js/kendo.draganddrop.min.js',
                            'front/bower_components/kendo-ui/js/kendo.slider.min.js',
                            'front/bower_components/kendo-ui/js/kendo.mobile.scroller.min.js',
                            'front/bower_components/kendo-ui/js/kendo.autocomplete.min.js',
                            'front/bower_components/kendo-ui/js/kendo.combobox.min.js',
                            'front/bower_components/kendo-ui/js/kendo.dropdownlist.min.js',
                            'front/bower_components/kendo-ui/js/kendo.colorpicker.min.js',
                            'front/bower_components/kendo-ui/js/kendo.combobox.min.js',
                            'front/bower_components/kendo-ui/js/kendo.maskedtextbox.min.js',
                            'front/bower_components/kendo-ui/js/kendo.multiselect.min.js',
                            'front/bower_components/kendo-ui/js/kendo.numerictextbox.min.js',
                            'front/bower_components/kendo-ui/js/kendo.toolbar.min.js',
                            'front/bower_components/kendo-ui/js/kendo.panelbar.min.js',
                            'front/bower_components/kendo-ui/js/kendo.window.min.js',
                            'front/bower_components/kendo-ui/js/kendo.angular.min.js',
                            'front/bower_components/kendo-ui/styles/kendo.common-material.min.css',
                            'front/bower_components/kendo-ui/styles/kendo.material.min.css'
                        ],
                        insertBefore: '#main_stylesheet',
                        serie: true
                    },
                    // ----------- THEMES -------------------
                    {
                        name: 'lazy_themes',
                        files: [
                            "front/assets/css/themes/_theme_a.min.css",
                            "front/assets/css/themes/_theme_b.min.css",
                            "front/assets/css/themes/_theme_c.min.css",
                            "front/assets/css/themes/_theme_d.min.css",
                            "front/assets/css/themes/_theme_e.min.css",
                            "front/assets/css/themes/_theme_f.min.css",
                            "front/assets/css/themes/_theme_g.min.css",
                            "front/assets/css/themes/_theme_h.min.css",
                            "front/assets/css/themes/_theme_i.min.css",
                            "front/assets/css/themes/_theme_dark.min.css"
                        ]
                    },

                    // ----------- STYLE SWITCHER -----------
                    {
                        name: 'lazy_style_switcher',
                        files: [
                            "front/assets/css/my_style_switcher.css",
                        ]
                    }

                ]
            })
        }
    ]);
angular
    .module('estatify')
    .controller('mainCtrl', [
        function () {}
    ])
    
;
angular
    .module('estatify')
    .controller('main_headerCtrl', [
        '$timeout',
        '$scope',
        '$window',
        '$state',
        function ($timeout,$scope,$window,$state) {

            $('#menu_top').children('[data-uk-dropdown]').on('show.uk.dropdown', function(){
                $timeout(function() {
                    $($window).resize();
                },280)
            });

            // autocomplete
            $('.header_main_search_form').on('click','#autocomplete_results .item', function(e) {
                e.preventDefault();
                var $this = $(this);
                $state.go($this.attr('href'));
                $('.header_main_search_input').val('');
            })

        }
    ])
    .controller('themeCtrl', ['$scope', '$rootScope',
        function($scope, $rootScope) {
            $scope.themes = [
                {
                    name: "app_style_default",
                    class: "default_theme"
                },
                {
                    name: "switcher_theme_a",
                    class: "app_theme_a"
                },
                {
                    name: "switcher_theme_b",
                    class: "app_theme_b"
                },
                {
                    name: "switcher_theme_c",
                    class: "app_theme_c"
                },
                {
                    name: "switcher_theme_d",
                    class: "app_theme_d"
                },
                {
                    name: "switcher_theme_e",
                    class: "app_theme_e"
                },
                {
                    name: "switcher_theme_f",
                    class: "app_theme_f"
                },
                {
                    name: "switcher_theme_g",
                    class: "app_theme_g"
                },
                {
                    name: "switcher_theme_h",
                    class: "app_theme_h"
                },
                {
                    name: "switcher_theme_i",
                    class: "app_theme_i"
                },
                {
                    name: "switcher_theme_dark",
                    class: "app_theme_dark"
                }
            ];
            $scope.toggleStyleSwitcher = function($event) {
                $event.preventDefault();
                $rootScope.styleSwitcherActive = !$rootScope.styleSwitcherActive;
            };

            $scope.changeTheme = function($event, theme) {
                $event.preventDefault();
                var $this = $($event.currentTarget);

                $this
                    .addClass('active_theme')
                    .siblings().removeClass('active_theme');

                $rootScope.main_theme = theme.class;

                if (theme.class == '') {
                    localStorage.removeItem('minionz_theme');
                } else {
                    localStorage.setItem("minionz_theme", theme.class);
                }
            };


            // check which theme is active
            if (localStorage.getItem("minionz_theme") !== null) {
                $rootScope.main_theme = localStorage.getItem("minionz_theme");
            } else {
                $rootScope.main_theme = "default_theme"
            }
        }
    ])
;
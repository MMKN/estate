estatify
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {

            // Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
            $urlRouterProvider
                .otherwise('/leads/index');

            $stateProvider
            // -- ERROR PAGES --
                .state("error", {
                    url: "/error",
                    templateUrl: 'front/app/views/error.html',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'lazy_uikit'
                            ]);
                        }]
                    }
                })
                .state("error.404", {
                    url: "/404",
                    templateUrl: 'front/app/components/pages/error_404View.html'
                })
                .state("error.500", {
                    url: "/500",
                    templateUrl: 'front/app/components/pages/error_500View.html'
                })
            // -- LOGIN PAGE --
                .state("login", {
                    url: "/login",
                    templateUrl: 'front/app/components/login/loginView.html',
                    controller: 'loginCtrl',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'lazy_uikit',
                                'lazy_iCheck',
                                'front/app/components/login/loginController.js'
                            ]);
                        }]
                    }
                })
            // -- RESTRICTED --
                .state("restricted", {
                    abstract: true,
                    url: "",
                    views: {
                        'main_header': {
                            templateUrl: 'front/app/shared/header/headerView.html',
                            controller: 'main_headerCtrl'
                        },
                        '': {
                            templateUrl: 'front/app/views/restricted.html'
                        }
                    },
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'lazy_uikit',
                                'lazy_selectizeJS',
                                'lazy_switchery',
                                'lazy_prismJS',
                                'lazy_autosize',
                                'lazy_iCheck',
                                'lazy_themes',
                                'lazy_style_switcher',
                                'front/app/shared/header/header.js',
                            ]);
                        }]
                    }
                })
            // -- DASHBOARD --

                .state("restricted.leads", {
                    abstract: true,
                    url:'/leads',
                    template: '<div ui-view autoscroll="false"/>',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'lazy_charts_easypiechart',
                                'lazy_dragula',
                                'front/app/components/leads/leads.js',
                                'front/app/components/leads/lead/lead.js',
                            ],{serie: true});
                        }]
                    },
                })
                .state("restricted.leads.index", {
                    url: "/index",
                    templateUrl: 'front/app/components/leads/index.html',
                    controller: 'leadsCtrl',
                    data: {
                        pageTitle: 'Home'
                    },
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                            ],{serie: true});
                        }]
                    },

                    params: { hidePreloader: true }
                })
                    .state("restricted.leads.index.tags", {
                        url: "/tags",
                        templateUrl: 'front/app/components/leads/tags/list.html',
                        controller: 'tagsCtrl',
                        data: {
                            pageTitle: 'Tags'
                        },
                        resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'front/app/components/leads/tags/tags.js',
                                
                            ],{serie: true});
                        }]
                    },
                    params: { hidePreloader: true }
                    })
                .state("restricted.leads.lead", {
                    url: "/:lead_id",
                    templateUrl: 'front/app/components/leads/lead/lead.html',
                    controller: 'leadCtrl',
                    data: {
                        pageTitle: 'Lead'
                    },
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                // 'lazy_dragula',
                                // 'lazy_charts_easypiechart',
                                // 'front/app/components/leads/leads.js',
                            ],{serie: true});
                        }]
                    },
                    params: { hidePreloader: true }

                })

                
                .state("restricted.contacts", {
                    abstract: true,
                    url:'/contacts',
                    template: '<div ui-view autoscroll="false"/>',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'front/app/components/contacts/contacts.js',
                                'front/app/components/contacts/companies/companies.js',
                                'front/app/components/contacts/individuals/individuals.js',
                            ]);
                        }]
                    },
                })
                .state("restricted.contacts.index", {
                    url: "/index",
                    templateUrl: 'front/app/components/contacts/index.html',
                    controller: 'contactsCtrl',
                    data: {
                        pageTitle: 'Home'
                    },
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                            ],{serie: true});
                        }]
                    },
                    params: { hidePreloader: true }
                    
                })
                
        }
    ]);

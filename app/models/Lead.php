<?php

class Lead extends Eloquent{

	use SoftDeletingTrait;
	protected $table = 'leads';

	protected $hidden = ['deleted_at' , 'created_at' , 'updated_at'];

	public static $rules = [
		'customer_id' => 'required',
		'customer_type' => 'required',
		'type' => 'in:b2b,b2c'
	];

	public function tasks()
	{
		return $this->hasMany('Task');
	}

	public function users()
	{
		return $this->belongsToMany('User', 'leads_users');
	}

	public function tags()
	{
		return $this->belongsToMany('Tag' , 'leads_tags');
	}

}

<?php

class Tag extends Eloquent{

	use SoftDeletingTrait;
	protected $table = 'tags';

	protected $hidden = ['deleted_at' , 'created_at' , 'updated_at'];

	public function leads()
	{
		return $this->belongsToMany('Lead', 'leads_tags');
	}

	public function tasks()
	{
		return $this->belongsToMany('Task', 'tasks_tags');
	}

}

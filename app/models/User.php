<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
    use SoftDeletingTrait;


	protected $table = 'users';
	protected $hidden = array('password', 'remember_token');

	public static $rules = [
		'name'   => 'required|unique:users',
        'email'  => 'required|unique:users',
        'mobile' => 'required|unique:users'
	];

	public function parent()
    {
        return $this->hasOne('User');
    }

    public function children()
    {
        return $this->hasMany('User','manager');
    }
}

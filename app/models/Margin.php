<?php

class Margin extends Eloquent{

	protected $table = 'margins';

	public static function getValue($field , $type)
	{
		$margin = Margin::whereField_name($field)->get()->first();
		if(!$margin)
		{
			return '';
		}

		if ($type == 0) {
			return $margin->value_exact;
		}

		return $margin->value_suggest;
	}
}

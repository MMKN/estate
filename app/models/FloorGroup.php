<?php

class FloorGroup extends Eloquent{

	protected $table = 'floors_groups';

	public function getFloor($id){
		return Floor::whereId($id)->pluck('value');
	}
}

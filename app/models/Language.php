<?php

class Language extends Eloquent{

	protected $table = 'languages';

	public function getMainLang($val)
	{
		$lang = Language::whereValue($val)->get()->first();
		if ($lang->selected == 1) {
			return 'selected';
		}else{
			return '';
		}
	}
}

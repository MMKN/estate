<?php

class Individual extends Eloquent{

	use SoftDeletingTrait;
	protected $table = 'individuals';

	protected $hidden = ['deleted_at' , 'created_at' , 'updated_at'];

	public static $rules = [
		'name' => 'required',
		'phone' => 'required'
	];
}

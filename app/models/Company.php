<?php

class Company extends Eloquent{

	use SoftDeletingTrait;
	protected $table = 'companies';

	protected $hidden = ['deleted_at' , 'created_at' , 'updated_at'];

	public static $rules = [
		'name' => 'required',
		'phone' => 'required'
	];

}

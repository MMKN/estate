<?php

class RequestsToCall extends Eloquent{

	protected $table = 'requests_to_call';

	public function getStatus($status)
	{	
		if ($status == 0) {
			return '<span class="md-card-list-item-date uk-badge uk-badge-danger uk-margin-right" style="color:#fff">Open</span>';
		}

		return '<span class="md-card-list-item-date uk-badge uk-badge-success uk-margin-right" style="color:#fff">Done</span>';
	}

	public function getUserName($user_id)
	{
		if(!PublicUser::find($user_id)){return 0;}
		return PublicUser::find($user_id)->pluck('name');
	}

	public function getPhoneNumber($user_id)
	{
		if(!PublicUser::find($user_id)){return 0;}
		return PublicUser::find($user_id)->pluck('mobile');
	}
}

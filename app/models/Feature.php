<?php

class Feature extends Eloquent{

	use SoftDeletingTrait;
	protected $table = 'features';

	public function checkIfSelected($feature_id , $property_id)
	{
		$feature = PropertyFeature::whereProperty_id($property_id)->whereFeature_id($feature_id)->get();
		if(count($feature)) return 'Checked';
		return '';
	}

	public function getFeature($id){
		return $this->whereId($id)->pluck('value');
	}
}

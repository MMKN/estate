<?php

class Neighbour extends Eloquent{

	use SoftDeletingTrait;
	protected $table = 'neighbours';

	public function getNeighbour($id){
		return $this->whereId($id)->pluck('name');
	}

}

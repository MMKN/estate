<?php

class Favourite extends Eloquent{

	protected $table = 'favourites';

	// favourite belongs to property
	// every favourite has its corresponding property
	public function getProperty(){
		return $this->belongsTo('Property' , 'property_id');
	}

}

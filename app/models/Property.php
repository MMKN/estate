<?php

class Property extends Eloquent{

	protected $table = 'properties';

	use SoftDeletingTrait;

	public static $rules = [
		'title'              => 'required',
        'price'              => 'required|numeric',
        'purpose'            => 'required',
        'address'            => 'required',
        'plot_no'            => 'required',
        'apartment_no'       => 'required',
        'description'        => 'required',
        'unit_type'          => 'required',
        'city'               => 'required',
        'neighbour'          => 'required',
        'is_compound'        => 'required',
        'finishing'          => 'required',
        'payment_style'      => 'required',
        'floor'              => 'required',
        'area'               => 'required',
        'rooms_no'           => 'required',
        'toilets_no'         => 'required',
        'distance_facilites' => 'required',
        'latitude'           => 'required',
        'longitude'          => 'required',
        'heard'              => 'required',
	];

	public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

	public function getCity($city_id)
	{
		$city = City::whereId($city_id)->withTrashed()->pluck('name');
		return $city;
	}

	public function getNeighbour($neighbour_id)
	{
		$neighbour = Neighbour::whereId($neighbour_id)->withTrashed()->pluck('name');
		return $neighbour;
	}

	public function getOwner($owner_id)
	{
		$owner = Owner::withTrashed()->whereId($owner_id)->get()->first();
		return $owner;
	}

	public function getFeatures($property_id)
	{
		// get the IDs of the features of thar property
		$property_features = PropertyFeature::whereProperty_id($property_id)->get();
		$features = [];

		// get the features values by its id
		foreach ($property_features as $feature) {
			$features[] = $this->getFeature($feature->feature_id);
		}
		return $features;
	}

	public function getFeature($feature_id)
	{
		$feature = Feature::whereId($feature_id)->withTrashed()->pluck('value');
		return $feature;
	}

	public function getUnitType($unit_type)
	{
		$unit_type = UnitType::whereId($unit_type)->withTrashed()->pluck('value');
		return $unit_type;
	}

	public function getPurpose($purpose)
	{
		if ($purpose == 0) {
			return trans('main.Sale');
		}else{
			return trans('main.Rent');
		}
	}

	public function getPurposeState($selected_purpose , $real)
	{
		if ($selected_purpose == $real) {
			return 'Checked';
		}else{
			return '';
		}
	}

	public function isCompound($is_compound)
	{
		if ($is_compound == 0) {
			return '<span class="uk-badge uk-badge-danger"> No </span>';
		}else{
			return '<span class="uk-badge uk-badge-success"> Yes </span>';
		}
	}

	public function getIsCompoundState($is_compound)
	{
		if ($is_compound == 0) {
			return '';
		}else{
			return 'Checked';
		}
	}

	public function getIsPublishedState($is_compound)
	{
		if ($is_compound == 0) {
			return '';
		}else{
			return 'Checked';
		}
	}

	public function getFinishing($finishing)
	{
		$finishing = Finishing::whereId($finishing)->withTrashed()->pluck('value');
		return $finishing;
	}

	public function getObjective($objective)
	{
		$objective = Objective::whereId($objective)->withTrashed()->pluck('value');
		return $objective;
	}

	public function getFloor($floor_id)
	{
		$floor = Floor::whereId($floor_id)->withTrashed()->get()->first();
		return $floor;
	}

	public function getPaymentType($payment_type)
	{
		$payment_type = PaymentType::whereId($payment_type)->withTrashed()->pluck('value');
		return $payment_type;
	}

	public function getMainImage($property_id)
	{
		$image = Image::whereProperty_id($property_id)->whereMain(1)->get()->first();
		if(!$image){ return 'assets/img/gallery/Image17.jpg';}

		return 'https://drive.google.com/thumbnail?authuser=0&sz=w200&id=' . $image->url;
	}


	public function checkLong($property_id)
	{
		$property = Property::withTrashed()->find($property_id);
		if ($property->longitude == '') {
			return 31.253340332031257;
		}

		return $property->longitude;
	}

	public function checkLat($property_id)
	{
		$property = Property::withTrashed()->find($property_id);
		if ($property->latitude == '') {
			return 29.95869952693869;
		}

		return $property->latitude;
	}

	public function getPrice($property_id)
	{
		if(!Property::find($property_id)){return;}
		return Property::find($property_id)->price;
	}

	public function city($property_id)
	{
		if(!Property::find($property_id)){return;}
		$city_id = Property::find($property_id)->city;
		return $this->getCity($city_id);
	}

	public function neighbour($property_id)
	{
		if(!Property::find($property_id)){return;}
		$neighbour_id = Property::find($property_id)->neighbour;
		return $this->getNeighbour($neighbour_id);
	}
}

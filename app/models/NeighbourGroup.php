<?php

class NeighbourGroup extends Eloquent{

	protected $table = 'neighbours_groups';

	public function getDistrict($id){
		return Neighbour::whereId($id)->pluck('name');
	}

}

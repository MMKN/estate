<?php

class Owner extends Eloquent{

	protected $table = 'owners';

	use SoftDeletingTrait;

	public static $rules = [
		'name '    => 'required',
        'email'    => 'required|email',
        'mobile1'  => 'required',
        'landline' => 'required'
	];
}

<?php

class TaskComment extends Eloquent{

	use SoftDeletingTrait;
	protected $table = 'task_comments';

	protected $hidden = ['deleted_at' , 'created_at' , 'updated_at'];

	public function task()
	{
		return $this->belongsTo('Task');
	}

}

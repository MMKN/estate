<?php

class AdminNotification extends Eloquent{

	protected $table = 'admin_notes';	

	// generate random key for each notification
	public function generate_random_string($name_length = 6)
	{
	    $alpha_numeric = 'abcdefghijklmnopqrstuvwxyz0123456789';
	    return substr(str_shuffle($alpha_numeric), 0, $name_length);
	}

	
	// return the name of assignee for a specific task
	public function getAssignee($assignee = 0)
	{
		if(!$assignee) return 'NO ONE';
		return User::find($assignee)->name;
	}

	// return the type of a specifc task
	/**
	* Type 0 = General
	* Type 1 = Call Request
	* Type 2 = Property Notice
	* Type 3 = Customer Notice
	*/
	public function getType($type = 0)	
	{
		if($type == 0) return $this->success(trans('main.General'));
		if($type == 1) return $this->success(trans('main.Call_requests'));
		if($type == 2) return $this->success(trans('main.Properties_Notices'));
		if($type == 3) return $this->success(trans('main.Customers_Notices'));
	}

	// return the priority of a specifc task
	/**
	* Status 0 = Urgent
	* Status 1 = Normal
	* Status 2 = Low
	*/
	public function getPriority($priority = 0)	
	{
		if($priority == 0) return $this->danger('urgent'); 
		if($priority == 1) return $this->success('Normal'); 
		if($priority == 2) return $this->primary('Low'); 
	}

	// return the status of a specifc task
	/**
	* Status 0 = Open
	* Status 1 = Closed
	* Status 2 = Reopened
	*/
	public function getStatus($status = 0)
	{
		if($status == 0) return $this->danger(trans('main.Open'));
		if($status == 1) return $this->success(trans('main.Closed'));
		if($status == 2) return $this->danger(trans('main.Reopened'));
	}

	// return the html of danger span with the value sent
	public function danger($value = '')
	{
		return '<span class="uk-badge uk-badge-danger uk-text-upper status-label">' . $value . '</span>';
	}

	// return the html of success span with the value sent
	public function success($value = '')
	{
		return '<span class="uk-badge uk-badge-success uk-text-upper status-label">' . $value . '</span>';
	}

	// return the html of primary span with the value sent
	public function primary($value = '')
	{
		return '<span class="uk-badge uk-badge-primary uk-text-upper status-label">' . $value . '</span>';
	}


	public function getMessage($notification)
	{
		if($notification->type == 0) return 'General';
		if($notification->type == 1) return 'User Request';
		if($notification->type == 2) return 'Property Notice';
		if($notification->type == 3) return 'Customer Notice';
	}

}

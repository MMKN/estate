<?php

class Task extends Eloquent{

	use SoftDeletingTrait;
	protected $table = 'tasks';

	protected $hidden = ['deleted_at' , 'created_at' , 'updated_at'];

	public function lead()
	{
		return $this->belongsTo('Lead');
	}

	public function comments()
	{
		return $this->hasMany('TaskComment');
	}

	public function tags()
	{
		return $this->belongsToMany('Tag', 'leads_tags');
	}

}

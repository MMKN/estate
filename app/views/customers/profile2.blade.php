@extends('_layouts.core')
@section('content')

        @include('customers.profile')

        @include('general.add_notice')
        
@stop
@section('scripts')
<script src="{{ URL::asset('js/general_handler.js') }}"></script>
<script src="{{ URL::asset('js/customer_handler.js') }}"></script>
<script src="{{ URL::asset('js/customers/filter.js') }}"></script>
<script src="{{ URL::asset('js/customers/customer-profile.js') }}"></script>
<script src="{{ URL::asset('js/customers/owner-profile.js') }}"></script>
<script src="{{ URL::asset('js/customers/routes.js') }}"></script>
<script type="text/javascript">
var base_url = "{{ URL::to('/') }}";
var msg_id   = 0;
var property_id;
altair_wizard.advanced_wizard();
</script>
@stop

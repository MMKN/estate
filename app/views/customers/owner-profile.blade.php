<div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
    <!-- Main content -->
    <div class="uk-width-1-1">
        <div class="md-card">
            <!-- user heading -->
            <div class="user_heading">
                <div class="user_heading_content clearfix">
                    <h2 class="heading_b uk-float-left"> <i class="material-icons md-48 md-light uk-text-middle">&#xE853;</i> &nbsp;&nbsp;{{$owner->name}}</h2>
                    @if($owner->trashed())
                    <button class="md-btn md-btn-warning uk-float-right" onclick="unarchiveOwner()">{{trans('main.unarchieve')}}</button>

                    @endif
                    <ul class="user_stats uk-float-right">
                        <li>
                            <!-- <a href="#new_issue" data-uk-modal="{ center:true }" class="md-btn"> <i class="material-icons uk-text-primary uk-text-middle">add_alert</i>{{trans('main.add_task')}}</a> -->
                        </li>
                    </ul>
                </div>
                <div class="md-fab-wrapper">
                    <div class="md-fab md-fab-toolbar md-fab-small md-fab-accent md-fab-wave-light">
                        <i class="material-icons">more_horiz</i>
                        <div class="md-fab-toolbar-actions"  id="actions">
                            <button type="submit" onclick="addProperty()" class="" id="add_property" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" title="{{trans('main.Add')}}"><i class="material-icons md-color-white">add</i></button>
                            <button type="submit"  onclick="UIkit.modal.confirm('Are you sure?', function(){ deleteOwner(current_id); });" class="archieve_customer" id="archieve_owner" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" title="{{trans('main.Delete')}}"><i class="material-icons md-color-white">&#xE872;</i></button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- user Content -->
            <div class="user_content">
                <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'scale'}" data-uk-sticky="{top:48, media: 960, boundary: true,showup:true,animation: 'uk-animation-slide-top'}">
                    <li class="uk-active"><a href="#">{{trans('main.Suitable_properties')}}</a></li>
                    <!-- <li><a href="#">{{trans('main.Timeline')}}</a></li> -->
                    <li><a href="#">{{trans('main.Personal_information')}}</a></li>
                </ul>

                <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">


                    <!-- Suitable content -->
                    <li>
                        <!-- Properties Grid  -->
                        <h3 class="full_width_in_card heading_c">
                            {{trans('main.Owned_Properties')}}
                        </h3>

                        <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-large-1-5 uk-text-center uk-margin-small-top" data-uk-grid-match="{target:'.md-card-content'}">
                            @if(count($properties))
                                @foreach($properties as $property)
                                    @include('properties.property_card')
                                @endforeach
                            @else
                                <div class="uk-width-medium-1-1 uk-margin-medium-bottom">
                                    <i class="material-icons md-48 uk-text-warning">warning</i>
                                </div>
                            @endif
                        </div>

                    </li>


                    <!-- Tasks start -->
                    <!-- <li class="uk-margin-small-top">
                       
                        <div class="">
                        @if(count($notifications))
                            <table class="uk-table" id="res_table">
                                <thead>
                                    <tr>
                                        <th class="uk-text-center">{{trans('main.Key')}}</th>
                                        <th class="uk-text-center">{{trans('main.Title')}}</th>
                                        <th class="uk-text-center">{{trans('main.Assignee')}}</th>
                                        <th class="uk-text-center">{{trans('main.Priority')}}</th>
                                        <th class="uk-text-center">{{trans('main.Created')}}</th>
                                        <th class="uk-text-center">{{trans('main.Updated')}}</th>
                                        <th class="uk-text-center">{{trans('main.Status')}}</th>
                                    </tr>
                                </thead>
                                <tbody id="table_results">
                                        @foreach($notifications as $notification)
                                            <tr>
                                                <td class="uk-text-center">
                                                    <span class="uk-text-small uk-text-muted uk-text-nowrap"> 
                                                        {{ $notification->key }} 
                                                    </span>
                                                </td>
                                                <td class="uk-text-center">
                                                    <a target="_blank" href="{{URL::route('notifications.details-page').'/'.( $notification->id )}}">
                                                        {{ $notification->title }} 
                                                    </a>
                                                </td>
                                                <td class="uk-text-center"> 
                                                    {{ $notification->getAssignee($notification->to) }} 
                                                </td>
                                                <td class="uk-text-center"> 
                                                    {{ $notification->getPriority($notification->priority) }} 
                                                </td>
                                                <td class="uk-text-small uk-text-center">
                                                    {{ date("j M" , strtotime($notification->created_at)) }}
                                                </td>
                                                <td class="uk-text-small uk-text-center">
                                                    19/Jun/16
                                                </td>
                                                <td class="uk-text-center">
                                                    {{ $notification->getStatus($notification->status) }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    
                                </tbody>
                            </table>
                            @else
                    <h3 class="uk-text-muted"> {{trans('main.no_tasks_found')}} </h3>
                            
                            @endif
                        </div>


                    </li>
 -->


                    <!-- Customer Info -->
                    <li>
                        <div class="uk-grid uk-margin-small-top uk-margin-large-bottom" data-uk-grid-margin>

                            <!-- Property information -->
                            

                            <!-- Contact information -->
                            <div class="uk-width-1-1">
                                <h4 class="full_width_in_card heading_c">{{trans('main.Personal_information')}}</h4>
                                <ul class="md-list md-list-addon">
                                    <li>
                                        <div class="md-list-addon-element">
                                            <i class="md-list-addon-icon material-icons">&#xE158;</i>
                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading">{{$owner->email}}</span>
                                            <span class="uk-text-small uk-text-muted">{{trans('main.Email')}}</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-addon-element">
                                            <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading">{{$owner->mobile1}}</span>
                                            <span class="uk-text-small uk-text-muted">{{trans('main.Mobile_main')}}</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-addon-element">
                                            <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading">{{$owner->mobile2}}</span>
                                            <span class="uk-text-small uk-text-muted">{{trans('main.Other_phones')}}</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-addon-element">
                                            <i class="md-list-addon-icon uk-icon-commenting"></i>
                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading">{{$owner->heard_from}}</span>
                                            <span class="uk-text-small uk-text-muted">{{trans('main.Heard_us_by')}}</span>
                                        </div>
                                    </li>
                                </ul>
                                <br>
                                <br>

                                <!-- <div class="uk-width-medium-1-1">
                                <a class="md-btn md-btn-primary uk-text-middle uk-text-center " onclick="sendSms()"><i class="material-icons md-24 md-light">&#xE163;</i> Sms with URL</a>
                            </div> -->
                            <br>
                            <br>
                        </div>
                    </div>
                </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Customer profile end -->

<!-- Go back from peroperty to home view -->
<div onclick="backFromProfile()" id="call_to_action" class="md-fab-wrapper">
    <a class="md-fab  md-fab-accent md-fab-wave-light"><i class="material-icons">arrow_back</i></a>
</div>

<!-- delete form begin -->
<article id="archieve_box" class="uk-width-medium-2-5 uk-text-center  uk-container-center" style="display:none">
    <div class="md-card ">
        <div class="md-card-content">
            <div class="uk-grid">
                <h3 class="uk-width-medium-1-1- uk-margin">{{trans('main.customer_delete')}}</h3>

                <div class="uk-width-medium-1-2 uk-margin">
                    <a class="md-btn md-btn-danger" id="confirm_delete" href="#">{{trans('main.Delete')}}</a>
                </div>

                <div class="uk-width-medium-1-2 uk-margin">
                    <a class="md-btn md-btn-success" id="archieve_cancel" href="#">{{trans('main.Cancel')}}</a>
                </div>

            </div>

        </div>
    </div>
</article>
    <!-- delete form end -->


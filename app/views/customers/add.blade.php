<div class="md-card">
    <div class="md-card-content">
        <form class="uk-form-stacked" id="add_form" method="post" action="{{ URL::route('customers.add') }}">
            <div id="wizard_advanced">

               <!-- second section -->
                <h3>Property characteristics</h3>
                <section>
                    <div class="md-card-content ">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="parsley-row">
                                <div class="uk-input-group" style="margin-top:0px;">
                                    <span class="icheck-inline">
                                        <input type="radio" value="0" name="radio_purpose" id="val_radio_male"  class="wizard-icheck" required {{ (new Property())->getPurposeState($purpose_selected , 0) }}  />
                                        <label for="val_radio_male" class="inline-label">{{trans('main.Sale')}}</label>
                                    </span>
                                    <span class="icheck-inline">
                                        <input type="radio" value="1" name="radio_purpose" id="val_radio_female"  class="wizard-icheck" {{ (new Property())->getPurposeState($purpose_selected , 1) }}/>
                                        <label for="val_radio_female" class="inline-label">{{trans('main.Rent')}}</label>
                                    </span>
                                </div>
                            </div>

                            <div class="uk-width-medium-1-5 parsley-row">
                                <div class="uk-margin-small-top">
                                    <select id="product_search_status" required name="unit_type[]" multiple data-md-selectize-bottom>
                                        <option value="" disabled>{{trans('main.Property_type')}}</option>
                                        @if($unit_type_selected)
                                        @foreach($unit_type_selected as $unit_type)
                                            <option value="{{ $unit_type }}" selected>{{ (new UnitType())->getUnitType($unit_type) }}</option>
                                        @endforeach
                                        @endif
                                        @foreach($unit_types as $unit_type)
                                        <option value="{{ $unit_type->id }}">{{ $unit_type->value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-5 ">
                                <div class="parsley-row  uk-margin-small-top">
                                    <select onchange="updateDistricts()" name="city" required>
                                        <option value="" disabled>{{trans('main.City')}}</option>
                                        <option value="{{ $cities_selected }}" selected>{{ (new City())->getCity($cities_selected) }}</option>
                                        @foreach($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-5 ">
                                <div class="parsley-row  uk-margin-small-top" id="neighbours_select">
                                    <select id="neighbours" name="neighbour[]" required multiple data-md-selectize-bottom>
                                        <option value="" disabled> {{trans('main.District')}} </option>
                                        @if($neighbours_selected)
                                        @foreach($neighbours_selected as $neighbour)
                                            <option value="{{ $neighbour }}" selected>{{ (new Neighbour())->getNeighbour($neighbour) }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-5 ">
                                <div class="parsley-row  uk-margin-small-top">
                                    <select id="product_search_status" name="finishing[]" required multiple data-md-selectize-bottom>
                                        <option value="" disabled>{{trans('main.Finishing')}}</option>
                                        @if($finishing_selected)
                                        @foreach($finishing_selected as $finishing)
                                            <option value="{{ $finishing }}" selected>{{ (new Finishing())->getFinishing($finishing) }}</option>
                                        @endforeach
                                        @endif
                                        @foreach($finishings as $finishing)
                                        <option value="{{ $finishing->id }}">{{ $finishing->value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-5 parsley-row">
                                <label for="masked_currency">{{trans('main.Price_from')}}</label>
                                <input class="md-input masked_input" required value="{{ $price_from_selected }}" name="price_from" id="masked_currency" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                            </div>
                            <div class="uk-width-medium-1-5 parsley-row">
                                <label for="masked_currency">{{trans('main.Price_to')}}</label>
                                <input class="md-input masked_input" required value="{{ $price_to_selected }}" name="price_to" id="masked_currency" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                            </div>

                            <div class="uk-width-medium-1-5">
                                <div class="parsley-row  uk-margin-small-top">
                                    <select id="product_search_status" name="payment_style[]" required  multiple data-md-selectize-bottom>
                                        <option value="" disabled>{{trans('main.Payment_style')}}</option>
                                        @if($payment_style_selected)
                                        @foreach($payment_style_selected as $payment_style)
                                            <option value="{{ $payment_style }}" selected>{{ (new PaymentType())->getPaymentStyle($payment_style) }}</option>
                                        @endforeach
                                        @endif
                                        @foreach($payment_types as $payment_type)
                                        <option value="{{ $payment_type->id }}">{{ $payment_type->value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="uk-width-medium-1-5 ">
                                <div class="parsley-row  uk-margin-small-top">
                                    <select  id="product_search_status" name="floor[]"  required multiple data-md-selectize-bottom>
                                        <option value="" disabled>{{trans('main.Floor')}}</option>
                                        @if($floor_selected)
                                        @foreach($floor_selected as $floor)
                                            <option value="{{ $floor }}" selected>{{ (new Floor())->getFloor($floor) }}</option>
                                        @endforeach
                                        @endif
                                        @foreach($floors as $floor)
                                        <option value="{{ $floor->id }}">{{ $floor->value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-5">
                                <div class="parsley-row  uk-margin-small-top">
                                    <select id="val_select" name="objective" required >
                                        <option value="" disabled >{{trans('main.Objective')}}</option>
                                        <option value="{{ $objective_selected }}" selected>{{ (new Objective())->getObjective($objective_selected) }}</option>
                                        @foreach($objectives as $objective)
                                        <option value="{{ $objective->id }}">{{ $objective->value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-5 parsley-row">
                                <label for="masked_currency">{{trans('main.Rooms_No')}}</label>
                                <input class="md-input masked_input" id="masked_currency" required name="no_rooms" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                            </div>
                            <div class="uk-width-medium-1-5 parsley-row">
                                <label for="masked_currency">{{trans('main.Toilets_No')}}</label>
                                <input class="md-input masked_input" id="masked_currency" required name="no_bathrooms" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                            </div>
                            <div class="uk-width-medium-1-5 parsley-row">
                                <label for="masked_currency">{{trans('main.Area')}}</label>
                                <input class="md-input masked_input" value="{{ $area_selected }}" id="masked_currency" required name="area" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': '', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                            </div>


                            <div class="uk-width-medium-1-5 parsley-row">
                                <div class="uk-input-group " style="margin-top:13px;">
                                    <input type="checkbox" value="0" name="is_compound" data-switchery data-switchery-color="#ffa000" id="switch_demo_success" {{ (new Property())->getIsCompoundState($compound_selected) }}/>
                                    <label for="switch_demo_success" class="inline-label">{{trans('main.Compound')}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <!-- First section -->
                <h3>{{trans('main.Personal_information')}}</h3>
                <section>

                    <div class="md-card-content ">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <div class="parsley-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">

                                            <i class="md-list-addon-icon material-icons md-36">&#xE853;</i>
                                        </span>
                                        <label for="fullname">{{trans('main.Full_Name')}}<span class="req">*</span></label>
                                        <input type="text" name="fullname" required class="md-input" />
                                    </div>
                                </div>
                            </div>

                            <div class="uk-width-medium-1-2 parsley-row">
                                <div>
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="md-list-addon-icon material-icons md-36">&#xE0B0;</i>
                                        </span>
                                        <label>{{trans('main.Mobile_main')}}</label>
                                        <input type="tel" class="md-input masked_input" name="user_phone"  value="" data-inputmask="'mask': '999 - 999 99 999'" data-inputmask-showmaskonhover="false" required />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-grid" data-uk-grid-margin>

                            <div class="uk-width-medium-1-2">
                                <div class="parsley-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">

                                            <i class="md-list-addon-icon material-icons md-36">&#xE158;</i>
                                        </span>
                                        <label for="email">{{trans('main.Email')}}<span class="req">*</span></label>
                                        <input type="email" name="email" data-parsley-trigger="change"  class="md-input" />
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2 parsley-row">
                                <div>
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="md-list-addon-icon material-icons md-36">&#xE0B0;</i>
                                        </span>
                                        <label>{{trans('main.Other_phones')}}</label>
                                        <input type="text" class="md-input" name="user_phone2" value=""  />

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <div class="parsley-row uk-margin-top">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">

                                            <i class="md-list-addon-icon material-icons md-36">&#xE878;</i>
                                        </span>
                                        <label for="val_birth">{{trans('main.Date_added')}}<span class="req">*</span></label>
                                        <input type="text" value="{{ date('m.d.Y') }}" name="relation_start" id="val_birth" required class="md-input" data-parsley-americandate data-parsley-americandate-message="This value should be a valid date (MM.DD.YYYY)" data-uk-datepicker="{format:'MM.DD.YYYY'}" />
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2 parsley-row" style="margin-top:1em">
                                <!-- <label for="val_select">Heard us by*</label> -->
                                <div class="uk-input-group">
                                <span class="uk-input-group-addon">
                                            <i class="md-list-addon-icon material-icons md-36">hearing</i>
                                        </span>
                                <select id="val_select" name="heard_from" required >
                                    <option value="" disabled selected>{{trans('main.Heard_us_by')}}</option>
                                    <option value="press">{{trans('main.Olx')}}</option>
                                    <option value="net">{{trans('main.Facebook')}}</option>
                                    <option value="mouth">{{trans('main.word_of_mouth')}}</option>
                                    <option value="other">{{trans('main.Others')}}</option>
                                </select>
                                </div>
                            </div>

                            
                            
                            <div class="uk-width-medium-1-2 parsley-row" style="margin-top:1em">
                                <!-- <label for="val_select">Heard us by*</label> -->
                                <div class="uk-input-group">
                                    <span class="uk-input-group-addon">
                                        <i class="md-list-addon-icon material-icons md-36">hearing</i>
                                    </span>
                                    <select name="manager" id="manager" required data-md-selectize-inline>
                                        <option value="" disabled selected>Manager</option>
                                        <option value="{{Auth::user()->id()}}" selected>Me</option>
                                        @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            

                        </div>

                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-1">
                                <div class="parsley-row">
                                    <label for="message">{{trans('main.Description')}}</label>
                                    <textarea class="md-input" name="message" cols="10" rows="4" data-parsley-trigger="keyup" data-parsley-minlength="5" data-parsley-maxlength="500" data-parsley-validation-threshold="10" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.."></textarea>
                                </div>
                            </div>


                        </div>
                    </div>
                </section>
            </div>
        </form>
    </div>
</div>

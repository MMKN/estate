<div class="md-card">
    <div class="md-card-content">
        <form class="uk-form-stacked" id="edit_formm" method="post" action="{{ URL::route('customers.edit') }}">
            <div id="wizard_advanced_edit">
                <!-- First section -->
                <h3>Property characteristics</h3>
                <section>
                    <div class="md-card-content">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-5 parsley-row">
                                <div class="uk-input-group" style="margin-top:13px;">
                                    <span class="icheck-inline">
                                        <input type="radio" value="0" name="radio_purpose" id="val_radio_male" {{$request->purpose == "0"? "checked" : ""}} class="wizard-icheck" />
                                        <label for="val_radio_male" class="inline-label">{{trans('main.Sale')}}</label>
                                    </span>
                                    <span class="icheck-inline">
                                        <input type="radio" value="1" name="radio_purpose" id="val_radio_female" {{$request->purpose == "1"? "checked" : ""}} class="wizard-icheck"/>
                                        <label for="val_radio_female" class="inline-label">{{trans('main.Rent')}}</label>
                                    </span>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-5">
                                <div class="uk-margin-small-top">
                                    <select id="product_search_status" required name="unit_type[]"  multiple data-md-selectize-bottom>
                                        <option value="" disabled>{{trans('main.Property_type')}}</option>
                                        @foreach($unit_types as $unit_type)
                                        <option value="{{ $unit_type->id }}" {{in_array($unit_type->id , $request_unit_type)?"selected" : ""}}>{{ $unit_type->value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-5 ">
                                <div class="parsley-row  uk-margin-small-top">
                                    <select id="city_select_edit" onchange="changeCity()" name="city" required>
                                        <option value="" disabled selected>{{trans('main.City')}}</option>
                                        @foreach($cities as $city)
                                        <option value="{{ $city->id }}" {{$request->city == $city->id? "selected" : ""}}>{{ $city->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-5 ">
                                <div class="parsley-row  uk-margin-small-top" id="neighbours_select_edit">
                                    <select id="neighbours" class="neighbours_edit" name="neighbours[]" required multiple data-md-selectize-bottom>
                                        <option value="" disabled selected>{{trans('main.District')}}</option>
                                        @foreach($selected_neighbours as $neighbour)
                                        <option value="{{ $neighbour->id }}" {{in_array($neighbour->id , $request_neighbours)?"selected" : ""}}>{{ $neighbour->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-5 ">
                                <div class="parsley-row  uk-margin-small-top">
                                    <select id="product_search_status" name="finishing[]" multiple data-md-selectize-bottom required>
                                        <option value="" disabled selected>{{trans('main.Finishing')}}</option>
                                        @foreach($finishings as $finishing)
                                        <option value="{{ $finishing->id }}" {{in_array($finishing->id , $request_finishing)?"selected" : ""}}>{{ $finishing->value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-5">
                                <label for="masked_currency">{{trans('main.Price_from')}}</label>
                                <input class="md-input masked_input" name="price_from" value="{{$request->price_from}}" id="masked_currency" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                            </div>
                            <div class="uk-width-medium-1-5">
                                <label for="masked_currency">{{trans('main.Price_to')}}</label>
                                <input class="md-input masked_input" name="price_to" value="{{$request->price_to}}" id="masked_currency" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                            </div>
                            <div class="uk-width-medium-1-5 ">
                                <div class="parsley-row  uk-margin-small-top">
                                    <select id="product_search_status" name="payment_style[]"  multiple data-md-selectize-bottom required>
                                        <option value="" disabled selected>{{trans('main.Payment_style')}}</option>
                                        @foreach($payment_types as $payment_type)
                                        <option value="{{ $payment_type->id }}" {{in_array($payment_type->id , $request_payment)?"selected" : ""}}>{{ $payment_type->value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                            <div class="uk-width-medium-1-5 ">
                                <div class="parsley-row  uk-margin-small-top">
                                    <select  id="product_search_status" name="floor[]"  multiple data-md-selectize-bottom required>
                                        <option value="" disabled selected>{{trans('main.Floor')}}</option>
                                        @foreach($floors as $floor)
                                        <option value="{{ $floor->id }}" {{in_array($floor->id , $request_floors)?"selected" : ""}}>{{ $floor->value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-5 ">
                                <div class="parsley-row  uk-margin-small-top">
                                    <select id="val_select" name="objective" required >
                                        <option value="" disabled selected>{{trans('main.Objective')}}</option>
                                        @foreach($objectives as $objective)
                                        <option value="{{ $objective->id }}" {{$request->objective == $objective->id? "selected" : ""}}>{{ $objective->value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-5 parsley-row">
                                <label for="masked_currency">{{trans('main.Rooms_No')}}</label>
                                <input class="md-input masked_input" id="masked_currency" value="{{$request->rooms_no}}" name="no_rooms" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                            </div>
                            <div class="uk-width-medium-1-5 parsley-row">
                                <label for="masked_currency">{{trans('main.Toilets_No')}}</label>
                                <input class="md-input masked_input" id="masked_currency" value="{{$request->toilets_no}}" name="no_bathrooms" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                            </div>
                            <div class="uk-width-medium-1-5 parsley-row">
                                <label for="masked_currency">{{trans('main.Area')}}</label>
                                <input class="md-input masked_input" id="masked_currency" value="{{$request->area}}" name="area" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': '', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'placeholder': '0'" data-inputmask-showmaskonhover="false" required />
                            </div>
                            <div class="uk-width-medium-1-5 parsley-row">
                                <div class="uk-input-group " style="margin-top:13px;">
                                    <input type="checkbox" value="0" name="is_compound" class="compound-switch" data-switchery {{$request->is_compound == "1"? "selected" : ""}} data-switchery-color="#ffa000" id="switch_demo_success" />
                                    <label for="switch_demo_success" class="inline-label">{{trans('main.Compound')}}</label>
                                </div>
                            </div>
                            
                            <div class="uk-width-medium-1-1 parsley-row" style="margin: 25px 0px;">
                                <div class="uk-grid" id="features">
                                    <h3 class="uk-width-1-5">{{trans('main.Features')}}</h3>
                                    @foreach($features as $feature)
                                    <div class="">
                                        <input type="checkbox" name="features[]" data-md-icheck value="{{ $feature->id }}" id="customer_edit_{{ $feature->id }}" {{in_array($feature->id , explode("|" , $request->features))?"checked" : ""}}/>
                                        <label for="customer_edit_{{ $feature->id }}" class="inline-label"> {{ $feature->value }} </label>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- second section -->
                <h3>{{trans('main.Personal_information')}}</h3>
                <section>
                    <div class="md-card-content ">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <div class="parsley-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="md-list-addon-icon material-icons md-36">&#xE853;</i>
                                        </span>
                                        <label for="fullname">{{trans('main.Full_Name')}}<span class="req">*</span></label>
                                        <input type="text" name="fullname" required class="md-input" value="{{$customer->name}}" />
                                    </div>
                                </div>
                            </div>
                            
                            <div class="uk-width-medium-1-2">
                                <div>
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="md-list-addon-icon material-icons md-36">&#xE0B0;</i>
                                        </span>
                                        <label>{{trans('main.Mobile_main')}}</label>
                                        <input type="text" class="md-input masked_input" name="user_phone" value="{{$customer->mobile_1}}" data-inputmask="'mask': '999 - 999 99 999'" data-inputmask-showmaskonhover="false" required />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <div class="parsley-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="md-list-addon-icon material-icons md-36">&#xE158;</i>
                                        </span>
                                        <label for="email">{{trans('main.Email')}}<span class="req">*</span></label>
                                        <input type="email" name="email" data-parsley-trigger="change" value="{{$customer->email}}" class="md-input" />
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div>
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="md-list-addon-icon material-icons md-36">&#xE0B0;</i>
                                        </span>
                                        <label>{{trans('main.Other_phones')}}</label>
                                        <input type="text" class="md-input" name="user_phone2" value="{{$customer->mobile_2}}"  />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <div class="parsley-row uk-margin-top">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="md-list-addon-icon material-icons md-36">&#xE878;</i>
                                        </span>
                                        <label for="val_birth">{{trans('main.Date_added')}}<span class="req">*</span></label>
                                        <input type="text" value="{{ date('m.d.Y' , strtotime($customer->relation_start))}}" name="relation_start" id="val_birth" required class="md-input" data-parsley-americandate data-parsley-americandate-message="This value should be a valid date (MM.DD.YYYY)" data-uk-datepicker="{format:'MM.DD.YYYY'}" />
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2 parsley-row ">
                                <!-- <label for="val_select">Heard us by*</label> -->
                                <select id="val_select" name="heard_from" required >
                                    <option value="" disabled selected> {{trans('main.Heard_us_by')}} </option>
                                    <option value="press" {{$customer->heard_from == "press"?"selected" : ""}}>{{trans('main.Olx')}}</option>
                                    <option value="net" {{$customer->heard_from == "net"?"selected" : ""}}>{{trans('main.Facebook')}}</option>
                                    <option value="mouth" {{$customer->heard_from == "mouth"?"selected" : ""}}>{{trans('main.word_of_mouth')}}</option>
                                    <option value="other" {{$customer->heard_from == "other"?"selected" : ""}}>{{trans('main.Others')}}</option>
                                </select>
                            </div>
                            <div class="uk-width-medium-1-2 parsley-row" style="margin-top:1em">
                                <!-- <label for="val_select">Heard us by*</label> -->
                                <div class="uk-input-group">
                                    <span class="uk-input-group-addon">
                                        <i class="md-list-addon-icon material-icons md-36">hearing</i>
                                    </span>
                                    <select name="manager" id="manager" required data-md-selectize-inline>
                                        <option value="" disabled selected>Manager</option>
                                        {{-- <option value="{{$customer->user_id}}" selected></option> --}}
                                        <option value="{{Auth::user()->id()}}" {{$customer->user_id == Auth::user()->id()?"selected" : ""}}>Me</option>
                                        @foreach($users as $user)
                                        <option value="{{$user->id}}" {{$customer->user_id == $user->id?"selected" : ""}}>{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-1">
                                <div class="parsley-row">
                                    <label for="message">{{trans('main.Description')}}</label>
                                    <textarea class="md-input" name="message" cols="10" rows="4" data-parsley-trigger="keyup" data-parsley-minlength="5" data-parsley-maxlength="500" data-parsley-validation-threshold="10" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment..">{{$customer->notes}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </form>
    </div>
</div>
<div class="parsley-row  uk-margin-small-top">
    <select id="neighbours_search" class="search_attr" name="neighbour[]" data-md-selectize multiple data-md-selectize-bottom>
        <option value="" disabled selected> District </option>
        @foreach($neighbours as $neighbour)
        <option value="{{ $neighbour->id }}">{{ $neighbour->name }}</option>
        @endforeach
    </select>
</div>

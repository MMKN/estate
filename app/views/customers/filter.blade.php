<div id="page_heading" class="uk-margin-medium-bottom" >
    <div class="uk-grid" >
        <form id="customer_filter" class="uk-grid uk-width-medium-10-10" data-uk-grid-margin>
            
            <h1 class="uk-width-medium-3-10 uk-margin-bottom uk-text-center" style="margin-top:12px;"><i class="material-icons md-36 uk-margin-right" style="vertical-align:text-top;">face</i>{{trans('main.customers')}}</h1>
            <!-- Filter begin -->
            <div class="uk-width-medium-3-10 uk-margin-bottom uk-text-center">
                <label for="contact_list_search">{{trans('main.customer_search')}}</label>
                <input class="md-input" type="text" id="contact_list_search" />
            </div>
            <div class="uk-width-medium-2-10  uk-margin-bottom">
                <div class="parsley-row  uk-margin-small-top">
                    <select name="customers" class="search_attr" required data-md-selectize id="customer_filter_customers">
                        <option value="0" selected>{{trans('main.VIP_Customers')}}</option>
                        <option value="1">{{trans('main.website_customers')}}</option>
                        <option value="2">{{trans('main.owners')}}</option>
                    </select>
                </div>
            </div>
            <div class="uk-width-medium-2-10 uk-text-center uk-margin-bottom">
                <a onclick="toggleAdvancedFilter()" class="md-btn md-btn-flat uk-margin-small-top md-btn-wave" data-uk-tooltip="{pos:'bottom'}" title="{{trans('main.advanced_filter')}}"><i class="material-icons md-24">filter_list</i></a>
            </div>
            <!-- advanced filter -->
            <div class="uk-grid uk-width-medium-10-10" id="customers_filter" style="display:none;">
                <div class="uk-width-large-2-10 uk-width-medium-3-10 uk-width-small-1-2 uk-margin-bottom">
                    <div class="parsley-row  uk-margin-small-top">
                        <select name="purpose" class="search_attr" required data-md-selectize id="customer_filter_purpose">
                            <option value="" disabled selected>{{trans('main.Purpose')}}</option>
                            <option value="2">{{trans('main.All')}}</option>
                            <option value="0">{{trans('main.Sale')}}</option>
                            <option value="1">{{trans('main.Rent')}}</option>
                        </select>
                    </div>
                </div>
                <div class="uk-width-large-2-10 uk-width-medium-3-10 uk-width-small-1-2 uk-margin-bottom">
                    <div class="parsley-row  uk-margin-small-top">
                        <select name="type" class="search_attr" required data-md-selectize  id="customer_filter_type">
                            <option value="" disabled selected>{{trans('main.type')}}</option>
                            @foreach($unit_types as $unit_type)
                            <option value="{{ $unit_type->id }}">{{ $unit_type->value}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="uk-width-large-2-10 uk-width-medium-3-10 uk-width-small-1-2 uk-margin-bottom">
                    <div class="parsley-row  uk-margin-small-top">
                        <select name="city" class="search_attr" required data-md-selectize id="customer_filter_city">
                            <option value="" disabled selected>{{trans('main.City')}}</option>
                            @foreach($cities as $city)
                            <option value="{{ $city->id }}">{{ $city->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="uk-width-large-2-10 uk-width-medium-3-10 uk-width-small-1-2 uk-margin-bottom" id="customer_filter_neighbour">
                    <div class="parsley-row  uk-margin-small-top">
                        <select id="neighbours_search" class="search_attr" name="neighbour[]" data-md-selectize multiple data-md-selectize-bottom disabled="">
                            <option value="" disabled selected>{{trans('main.District')}}</option>
                        </select>
                    </div>
                </div>
                <div class="uk-width-large-2-10 uk-width-medium-3-10 uk-width-small-1-2 uk-margin-bottom" >
                    <div class="uk-input-group" style="margin-top:13px;">
                        <input class="search_attr"value="1" type="checkbox" id="is_compound" name="is_compound" data-switchery data-switchery-color="#ffa000" />
                        <label for="is_compound" class="inline-label">{{trans('main.Compound')}}</label>
                    </div>
                </div>
                <div class="uk-wdith-1-1 uk-container-center uk-margin-top uk-margin-bottom">
                    <div class="">
                        <a onclick="customersFilter()" class="md-btn md-btn-primary md-btn-wave-light md-btn-icon uk-margin-small-top"><i class="uk-icon-search"></i>{{trans('main.Search')}}</a>
                        <a onclick="resetFilter()" class="md-btn md-btn-flat md-btn-wave md-btn-icon uk-margin-small-top"><i class="uk-icon-refresh"></i>{{trans('main.Reset')}}</a>
                    </div>
                </div>
             
            </div>
        </form>
    </div>
    <!-- Filter end -->
</div>
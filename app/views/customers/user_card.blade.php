<div class=" uk-width-medium-1-3 uk-width-large-1-5 uk-width-small-1-2 uk-margin-medium-bottom">
    <li class="md-card md-card-hover" style="cursor: pointer" onclick="getUserProfile({{$person->id}})" data-uk-filter="  ,Sherif mohamed 01010002000 sherif@minionz.net">
        <div class="md-card-content">
            <div class="">
                <img class="md-user-image md-list-addon-avatar" src="http://graph.facebook.com/v2.5/{{ $customer->id }}/picture?type=large" alt=""/>
            </div>
            <div class="md-list-content uk-margin-top">
                <span class="md-list-heading"><h4> {{ $person->name }}</h4></span>
                <span class="uk-text-small uk-texttpust-muted"><i class="material-icons uk-margin-small-right">phone</i> {{ $person->mobile_1 }}</span><br>
                <span class="uk-text-small uk-text-muted uk-text-truncate"><i class="material-icons uk-margin-small-right">email</i>{{ $person->email }}</span>
            </div>
        </div>
    </li>
</div>
<div class="uk-text-center uk-width-medium-1-3 uk-width-large-1-5 uk-width-small-1-2 uk-margin-medium-bottom">
    <a target="_blank" href="{{URL::to('/')}}/profile-page/{{ $customer->id }}">
        <li class="md-card md-card-hover" style="cursor: pointer">
            <div class="md-card-content">
                <div class="">
                    <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/user.png" alt=""/>
                </div>
                <div class="md-list-content uk-margin-top">
                    <span class="md-list-heading"><h4> {{ $customer->name }}</h4></span>
                    <span class="uk-text-small uk-text-muted"><i class="material-icons uk-margin-small-right">phone</i> {{ $customer->mobile_1 }}</span><br>
                    <span class="uk-text-small uk-text-muted uk-text-truncate"><i class="material-icons uk-margin-small-right">email</i>{{ $customer->email }}</span>
                </div>
            </div>
        </li>
    </a>
</div>
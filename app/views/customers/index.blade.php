@extends('_layouts.core')
@section('archieved')
<div id="menu_top_dropdown" class="uk-float-right uk-hidden-small">
    <div class="uk-button-dropdown" data-uk-dropdown="">
        <a href="#" class="top_menu_toggle"><i class="md-icon material-icons md-24 md-light">&#xE5D4;</i></a>
        <div class="uk-dropdown uk-dropdown-small uk-dropdown-flip">
            <ul class="uk-nav">
                <li><a onclick="getArchived();" id="get_archived_customers" class="uk-text-center">{{trans('main.Archieved_Customers')}}</a></li>
            </ul>
        </div>
    </div>
</div>
@stop
@section('content')
<div id= "main_index">
    <!-- main view of the page start -->
    <article id="main">
        <!-- Filter and search -->
        @include('customers.filter')
        <!-- Contact list grid begin -->
        <div class="uk-grid hierarchical_show" id="contact_list">
            @include('customers.customers_grid')
        </div>
        <div class="uk-grid" id="paginator">
            <div class="uk-width-medium-1-1 pager">
                {{$customers->links()}}
            </div>
        </div>
        <!-- add new call to action-->
        <div onclick="addWizard()" class="md-fab-wrapper uk-hidden-small" {{ isset($page) ? 'style="display:none;"' : '' }}>
            <a class="md-fab md-fab-accent md-fab-wave-light" >
                <i class="material-icons">&#xE145;</i>
            </a>
        </div>
    </article>
    <!-- main view of the page start -->

    <article id="archivedView" style="display:none;">
        <div class="md-card" id="archived_title">
            <div class="md-card-content uk-grid uk-text-center uk-margin-bottom">
                <h3 class="uk-width-1-1 heading_b uk-text-center"><i class="md-icon material-icons uk-margin-right">people</i>{{trans('main.Archieved_Customers')}}</h3>
            </div>
        </div>
        <!-- Contact list grid begin -->
        <div class="uk-grid hierarchical_show" id="archived">
        </div>
        <!-- Go back to home -->
        <div onclick="goHome(); refreshHome(); deleted = 0;state = 'customers.main'; $('#archived').children().remove();" class="md-fab-wrapper">
            <a class="md-fab md-fab-accent md-fab-wave-light">
                <i class="material-icons">arrow_back</i>
            </a>
        </div>
    </article>

    <!-- customer profile view  -->
    <article id="user_profile" style="display:none;">
    </article>


    <!-- here goes any wizard (Add / edit / clone) -->
    <article id="wizardView" style="display:none;">
        <div id="wizard" class=" uk-width-medium-8-10 uk-container-center" >
        </div>
        <!-- Go back to home -->
        <div onclick="goHome(); $('#wizard').children().remove();" class="md-fab-wrapper">
            <a class="md-fab md-fab-accent md-fab-wave-light">
                <i class="material-icons">arrow_back</i>
            </a>
        </div>
    </article>


    <!-- add new customer form begin-->
    <article id="addCustomer" class="uk-width-medium-8-10 uk-container-center add-box" style="display:none">
        @include('customers.add_customer')
        <!-- Go back to home -->
        <div onclick="goHome()" class="md-fab-wrapper">
            <a class="md-fab md-fab-accent md-fab-wave-light">
                <i class="material-icons">arrow_back</i>
            </a>
        </div>
    </article>

    <!-- add new Customer form end-->
    <article id="edit_box" class="uk-width-medium-8-10 uk-container-center edit-box" style="display:none" >
        <div class="md-card">
            <div class="md-card-content" id="edit_form">
            </div>
        </div>
    </article>

    <article id="add_property_box" class="uk-width-medium-8-10 uk-container-center edit-box" style="display:none" >
        <div class="md-card">
            <div class="md-card-content" id="add_property">
            </div>
        </div>
    </article>


    <article id="loaderGif" class="" style="display:none" >
        <div class="uk-width-medium-1-6 uk-container-center">
            <div class="md-preloader uk-container-center"><svg xmlns="http://www.w3.org/2000/svg" version="1.1" height="96" width="96" viewbox="0 0 75 75"><circle cx="37.5" cy="37.5" r="33.5" stroke-width="4"/></svg></div>
        </div>
    </article>
    
    <!-- Customer profile end -->
    <article id="empty">
    </article>
    <!-- delete form begin -->
    <article id="archieve_box" class="uk-width-medium-2-5 uk-text-center  uk-container-center" style="display:none">
        <div class="md-card ">
            <div class="md-card-content">
                <div class="uk-grid">
                    <h3 class="uk-width-medium-1-1- uk-margin">{{trans('main.customer_delete')}}</h3>
                    <div class="uk-width-medium-1-2 uk-margin">
                        <a class="md-btn md-btn-danger" id="confirm_delete" href="#">{{trans('main.Delete')}}</a>
                    </div>
                    <div class="uk-width-medium-1-2 uk-margin">
                        <a class="md-btn md-btn-success" id="archieve_cancel" href="#">{{trans('main.Cancel')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </article>
    <!-- customer profile modal for sms -->
    <div class="uk-modal" id="modal_sms">
        <div class="uk-modal-dialog">
            <button type="button" class="uk-modal-close uk-close"></button>
            <div class="uk-margin user_content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1">
                        <div class="parsley-row">
                            <label for="message">Message (20 chars min, 100 max)</label>
                            <textarea class="md-input" id="sms_msg" name="message" cols="10" rows="4" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-validation-threshold="10" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.."></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-center">
                <button class="md-btn md-btn-flat md-btn-primary sms_send_button uk-text-center" id="sms_send_button" onclick="sendSms()"> Send </button>
            </div>
        </div>
    </div>
</div>

@include('general.add_notice')
@section('scripts')
<!--  google maps functions -->
<!-- <script src="{{ URL::asset('assets/js/pages/plugins_google_maps.min.js') }}"></script> -->
<script src="{{ URL::asset('js/general_handler.js') }}"></script>
<script src="{{ URL::asset('js/customer_handler.js') }}"></script>
<script src="{{ URL::asset('js/customers/filter.js') }}"></script>
<script src="{{ URL::asset('js/customers/customer-profile.js') }}"></script>
<script src="{{ URL::asset('js/customers/owner-profile.js') }}"></script>
<script src="{{ URL::asset('js/customers/routes.js') }}"></script>
<script type="text/javascript">
var base_url = "{{ URL::to('/') }}";
var msg_id   = 0;
var property_id;
altair_wizard.advanced_wizard();
</script>
@stop
@stop
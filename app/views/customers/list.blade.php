@if(count($customers) || (isset($users) && count($users)))
    <ul class="uk-grid uk-width-medium-1-1 uk-text-center md-list md-list-addon uk-margin-bottom">
    @foreach($customers as $person)
        @include('customers.person_card')
    @endforeach
    </ul>
    @if(isset($users))
    <ul class="uk-grid uk-width-medium-1-1 uk-text-center md-list md-list-addon uk-margin-bottom">
    @foreach($users as $person)
        @include('customers.person_card')
    @endforeach
    </ul>
    @endif
@else
<div class="uk-width-medium-1-1 uk-text-center">
    <h3 style="background-color: #fff; padding: 0.6em;"> {{trans('main.No_Customers')}} </h3>
</div>
@endif

<div class="uk-grid" data-uk-grid-margin data-uk-grid-match >
    <!-- Main content -->
    <div class="uk-width-1-1">
        <div class="md-card">
            <!-- user heading -->
            <div class="user_heading">
                <div class="user_heading_content clearfix">
                    <h2 class="heading_b uk-float-left"> <i class="material-icons md-48 md-light uk-text-middle">&#xE853;</i> &nbsp;&nbsp;{{$customer->name}}</h2>
                    @if($customer->trashed())
                    <div class="uk-grid uk-float-right">
                        <button class="md-btn md-btn-warning " onclick="unarchiveCustomer()">{{trans('main.unarchieve')}}</button>
                        <button class="md-btn md-btn-danger " onclick="UIkit.modal.confirm('Are you sure?', function(){ deleteCustomer(current_id); });">{{trans('main.Delete')}}</button>                    

                    </div>

                    @endif
                    @if(!$customer->trashed())
                    <ul class="user_stats uk-float-right">
                        <li>
                            <a href="#new_issue" data-uk-modal="{ center:true }" class="md-btn md-btn-flat md-btn-wave-light md-btn-icon" style="color:#fff;"> <i class="uk-icon-bell" style="color:#fff" ></i>{{trans('main.add_task')}}</a>

                        </li>
                    </ul>
                    @endif

                </div>
                @if(!$customer->trashed())
                <div class="md-fab-wrapper uk-hidden-small">
                    <div class="md-fab md-fab-toolbar md-fab-small md-fab-accent md-fab-wave-light">
                        <i class="material-icons">more_horiz</i>
                        <div class="md-fab-toolbar-actions"  id="actions">
                            <button type="submit" onclick="customerEditWizard()" class="edit_customere" id="edit_customer" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" title="{{trans('main.Edit')}}"><i class="material-icons md-color-white">edit</i></button>
                            <button type="submit"  onclick="UIkit.modal.confirm('Are you sure?', function(){ deleteCustomer(current_id); });" class="archieve_customer" id="archieve_customer" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" title="{{trans('main.Delete')}}"><i class="material-icons md-color-white">&#xE872;</i></button>
                        </div>
                    </div>
                </div>
                    @endif

            </div>

            <!-- user Content -->
            <div class="user_content">
                <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'scale'}" data-uk-sticky="{top:48, media: 960, boundary: true,showup:true,animation: 'uk-animation-slide-top'}">
                    <li class="uk-active"><a href="#">{{trans('main.Suitable_properties')}}</a></li>
                    <li><a href="#">{{trans('main.Personal_information')}}</a></li>
                </ul>

                <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">


                    <!-- Suitable content -->
                    <li>
                        <!-- Properties Grid  -->
                        <h3 class="full_width_in_card heading_c">
                            {{trans('main.Exact_Properties')}}
                        </h3>

                        <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-4 uk-grid-width-large-1-5 uk-text-center">
                            @if(count($property))
                                @foreach($property as $property)
                                   <a href="{{ URL::route('properties.property.admin' , $property->id) }}" target="_blank">
                                       @include('properties.property_card')
                                   </a>
                                @endforeach
                            @else
                                <div class="uk-width-medium-1-1 uk-margin-medium-bottom">
                                    <i class="material-icons md-48 uk-text-warning">warning</i>
                                </div>
                            @endif
                        </div>

                        <h3 class="full_width_in_card heading_c">
                            {{trans('main.Similar_Properties')}}
                        </h3>
                        <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-4 uk-grid-width-large-1-5 uk-text-center">
                            @if(count($suggestions))
                                @foreach($suggestions as $property)
                                  <a href="{{ URL::route('properties.property.admin' , $property->id) }}" target="_blank">
                                      @include('properties.property_card')
                                  </a>
                                @endforeach
                            @else
                                <div class="uk-width-medium-1-1 uk-margin-medium-bottom">
                                    <i class="material-icons md-48 uk-text-warning">warning</i>
                                </div>
                            @endif
                                
                            
                        </div>

                        <h3 class="full_width_in_card heading_c">
                            {{trans('main.Price_Properties')}}
                        </h3>

                        <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-large-1-5 uk-text-center uk-margin-small-top" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
                            @if(count($price_properties))
                                @foreach($price_properties as $property)
                                <a href="{{ URL::route('properties.property.admin' , $property->id) }}" target="_blank">
                                    @include('properties.property_card')
                                </a>
                                @endforeach
                            @else
                                <div class="uk-width-medium-1-1 uk-margin-medium-bottom">
                                    <i class="material-icons md-48 uk-text-warning">warning</i>
                                </div>
                            @endif 
                        </div>

                    </li>


                    <!-- Customer Info -->
                    <li>
                        <div class="uk-grid uk-margin-small-top uk-margin-large-bottom" data-uk-grid-margin>

                            <!-- Property information -->
                            <div class="uk-width-large-3-4">
                                <h4 class="full_width_in_card heading_c">{{trans('main.customer_preferences')}}</h4>
                                <div class="uk-grid uk-grid-divider uk-grid-medium">
                                    <div class="uk-width-medium-1-2">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-large-1-3">
                                                <span class="uk-text-muted uk-text-small">{{trans('main.Purpose')}}</span>
                                            </div>
                                            <div class="uk-width-large-2-3">
                                                <span class="uk-text-large uk-text-middle">
                                                    @if($request->purpose == 0)
                                                    {{trans('main.Sale')}}
                                                    @else
                                                    {{trans('main.Rent')}}
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <hr class="uk-grid-divider">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-large-1-3">
                                                <span class="uk-text-muted uk-text-small">{{trans('main.Property_type')}}</span>
                                            </div>
                                            <div class="uk-width-large-2-3">
                                                <span class="uk-text-large uk-text-middle">
                                                    <?php $unit_types = RequestUnit::whereRequest_id($request->id)->get(); //explode("|" , $request->unit_type); ?>
                                                    @foreach($unit_types as $key => $unit_type)
                                                    {{(new UnitType())->getUnitType($unit_type->unit_type_id)}}
                                                    @if(isset($unit_types[$key + 1]))
                                                    -
                                                    @endif
                                                    @endforeach
                                                </span>
                                            </div>
                                        </div>
                                        <hr class="uk-grid-divider">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-large-1-3">
                                                <span class="uk-text-muted uk-text-small">{{trans('main.City')}}</span>
                                            </div>
                                            <div class="uk-width-large-2-3">
                                                <span class="uk-text-large uk-text-middle">{{(new City())->getCity($request->city)}}</span>
                                            </div>
                                        </div>
                                        <hr class="uk-grid-divider">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-large-1-3">
                                                <span class="uk-text-muted uk-text-small">{{trans('main.District')}}</span>
                                            </div>
                                            <div class="uk-width-large-2-3">

                                                <span class="uk-text-large uk-text-middle">
                                                    <?php $neighbours = RequestNeighbour::whereRequest_id($request->id)->get(); //explode("|" , $request->neighbour); ?>
                                                    @foreach($neighbours as $key => $neighbour)
                                                    {{(new Neighbour())->getNeighbour($neighbour->neighbour_id)}}
                                                    @if(isset($neighbours[$key + 1]))
                                                    -
                                                    @endif
                                                    @endforeach
                                                </span>

                                            </div>
                                        </div>
                                        <hr class="uk-grid-divider">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-large-1-3">
                                                <span class="uk-text-muted uk-text-small">{{trans('main.Finishing')}}</span>
                                            </div>
                                            <div class="uk-width-large-2-3">
                                                <span class="uk-text-large uk-text-middle">
                                                    <?php $finishings = RequestFinishing::whereRequest_id($request->id)->get(); //explode("|" , $request->finishing); ?>
                                                    @foreach($finishings as $key => $finishing)
                                                    {{(new Finishing())->getFinishing($finishing->finishing_id)}}
                                                    @if(isset($finishings[$key + 1]))
                                                    -
                                                    @endif
                                                    @endforeach
                                                </span>
                                            </div>
                                        </div>
                                        <hr class="uk-grid-divider">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-large-1-3">
                                                <span class="uk-text-muted uk-text-small">{{trans('main.Compound')}}</span>
                                            </div>
                                            <div class="uk-width-large-2-3">
                                                <span class="uk-text-large uk-text-middle">
                                                    @if($request->is_compound == 1)
                                                    {{trans('main.Yes')}}
                                                    @else
                                                    {{trans('main.No')}}
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <hr class="uk-grid-divider">
                                        <p>
                                            <span class="uk-text-muted uk-text-large uk-display-block uk-margin-small-bottom">{{trans('main.Features')}}</span>
                                            <?php $features = RequestFeature::whereRequest_id($request->id)->get(); //explode("|" , $request->features); ?>
                                            @foreach(explode("|" , $request->features) as $feature)
                                            <span class="uk-badge uk-badge-success">{{(new Feature())->getFeature($feature)}}</span>
                                            @endforeach
                                        </p>
                                        <hr class="uk-grid-divider uk-hidden-large">
                                    </div>
                                    <div class="uk-width-medium-1-2">

                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-large-1-3">
                                                <span class="uk-text-muted uk-text-small">{{trans('main.Price_from')}}</span>
                                            </div>
                                            <div class="uk-width-large-2-3">
                                                <span class="uk-text-large uk-text-middle">{{$request->price_from}}</span>
                                            </div>
                                        </div>
                                        <hr class="uk-grid-divider">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-large-1-3">
                                                <span class="uk-text-muted uk-text-small">{{trans('main.Price_to')}}</span>
                                            </div>
                                            <div class="uk-width-large-2-3">

                                                <span class="uk-text-large uk-text-middle">{{$request->price_to}}</span>

                                            </div>
                                        </div>

                                        <hr class="uk-grid-divider">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-large-1-3">
                                                <span class="uk-text-muted uk-text-small">{{trans('main.Area')}}</span>
                                            </div>
                                            <div class="uk-width-large-2-3">
                                                <span class="uk-text-large uk-text-middle">{{$request->area}}</span>
                                            </div>
                                        </div>

                                        <hr class="uk-grid-divider">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-large-1-3">
                                                <span class="uk-text-muted uk-text-small">{{trans('main.Floor')}}</span>
                                            </div>
                                            <div class="uk-width-large-2-3">
                                                <span class="uk-text-large uk-text-middle">
                                                    <?php $floors = RequestFloor::whereRequest_id($request->id)->get(); //explode("|" , $request->floor); ?>
                                                    @foreach($floors as $key => $floor)
                                                    {{(new Floor())->getFloor($floor->floor_id)}}
                                                    @if(isset($floors[$key + 1]))
                                                    -
                                                    @endif
                                                    @endforeach
                                                </span>
                                            </div>
                                        </div>
                                        <hr class="uk-grid-divider">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-large-1-3">
                                                <span class="uk-text-muted uk-text-small">{{trans('main.Rooms_No')}}</span>
                                            </div>
                                            <div class="uk-width-large-2-3">
                                                <span class="uk-text-large uk-text-middle">{{$request->rooms_no}}</span>
                                            </div>
                                        </div>
                                        <hr class="uk-grid-divider">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-large-1-3">
                                                <span class="uk-text-muted uk-text-small">{{trans('main.Toilets_No')}}</span>
                                            </div>
                                            <div class="uk-width-large-2-3">
                                                <span class="uk-text-large uk-text-middle">{{$request->toilets_no}}</span>
                                            </div>
                                        </div>

                                        <hr class="uk-grid-divider">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-large-1-3">
                                                <span class="uk-text-muted uk-text-small">{{trans('main.Payment_style')}}</span>
                                            </div>
                                            <div class="uk-width-large-2-3">
                                                <span class="uk-text-large uk-text-middle">
                                                    <?php $payment_styles = RequestPayment::whereRequest_id($request->id)->get(); //explode("|" , $request->payment_style); ?>
                                                    @foreach($payment_styles as $key => $payment_style)
                                                    {{(new PaymentType())->getPaymentStyle($payment_style->payment_id)}}
                                                    @if(isset($payment_styles[$key + 1]))
                                                    -
                                                    @endif
                                                    @endforeach
                                                </span>
                                            </div>
                                        </div>

                                        <hr class="uk-grid-divider">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-large-1-3">
                                                <span class="uk-text-muted uk-text-small">{{trans('main.Objective')}}</span>
                                            </div>
                                            <div class="uk-width-large-2-3">
                                                <span class="uk-text-large uk-text-middle">{{(new Objective())->getObjective($request->objective)}}</span>
                                            </div>
                                        </div>
                                        <hr class="uk-grid-divider">
                                        <p>
                                            <span class="uk-text-muted uk-text-small uk-display-block uk-margin-small-bottom">{{trans('main.Description')}}</span>
                                            {{$customer->notes}}
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <!-- Contact information -->
                            <div class="uk-width-large-1-4">
                                <h4 class="full_width_in_card heading_c">{{trans('main.Personal_information')}}</h4>
                                <ul class="md-list md-list-addon">
                                    <li>
                                        <div class="md-list-addon-element">
                                            <i class="md-list-addon-icon material-icons">&#xE158;</i>
                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading">{{$customer->email}}</span>
                                            <span class="uk-text-small uk-text-muted">{{trans('main.Email')}}</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-addon-element">
                                            <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading">{{$customer->mobile_1}}</span>
                                            <span class="uk-text-small uk-text-muted">{{trans('main.Mobile_main')}}</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-addon-element">
                                            <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading">{{$customer->mobile_2}}</span>
                                            <span class="uk-text-small uk-text-muted">{{trans('main.Other_phones')}}</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-addon-element">
                                            <i class="md-list-addon-icon uk-icon-calendar-plus-o"></i>

                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading">{{$customer->relation_start}}</span>
                                            <span class="uk-text-small uk-text-muted">{{trans('main.Date_added')}}</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-addon-element">
                                            <i class="md-list-addon-icon uk-icon-commenting"></i>
                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading">{{$customer->heard_from}}</span>
                                            <span class="uk-text-small uk-text-muted">{{trans('main.Heard_us_by')}}</span>
                                        </div>
                                    </li>
                                </ul>
                                <br>
                                <br>

                            <br>
                            <br>
                            <div class="uk-width-medium-1-1 uk-hidden">
                                <div class="uk-width-medium-1-1">
                                    <a class="md-btn md-btn-primary uk-text-middle uk-text-center " id="sms_button" onclick="sendSms()"><i class="material-icons md-24 md-light">&#xE163;</i>{{trans('main.sms')}} </a>
                                </div>
                                <div class="uk-width-medium-1-1">
                                    <a class="md-btn md-btn-success uk-text-middle uk-text-center " disabled id="sms_success" style="display:none;cursor:default;" ><i class="material-icons md-24 md-light">&#xE163;</i> تم ارسال الرسالة</a>
                                </div>
                                <div class="uk-width-medium-1-1">
                                    <a class="md-btn md-btn-danger uk-text-middle uk-text-center " disabled id="sms_fail" style="display:none;cursor:default;" ><i class="material-icons md-24 md-light">&#xE163;</i> رصيد غير كافي</a>
                                </div>
                                <br>
                                <br>
                                <div class="uk-width-medium-1-1">
                                    <input type="checkbox" onchange="toggleActive()" data-switchery data-switchery-size="large" data-switchery-color="#1e88e5" {{($customer->active == 1?'checked': '')}} id="switch_demo_primary" />
                                    <label for="switch_demo_primary" class="inline-label">{{trans('main.Link_Active')}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Customer profile end -->

<!-- go back call to action -->
<div id="second_icon" class="md-fab-wrapper" style="display:none;">
    <a class="md-fab md-fab-accent md-fab-wave-light go_back">
        <i class="material-icons">arrow_back</i>
    </a>
</div>
<!-- Add floating icon end -->
<!-- add new call to action-->
<div id="first_icon" class="md-fab-wrapper" {{ isset($page) ? 'style="display:none;"' : '' }}>
    <a class="md-fab md-fab-accent md-fab-wave-light" id="addNew">
        <i class="material-icons">&#xE145;</i>
    </a>
</div>
<!-- Add floating icon end -->
<div id="third_icon" class="md-fab-wrapper"  style="display:none;">
    <a class="md-fab md-fab-accent md-fab-wave-light">
        <i class="material-icons">&#xE166;</i>
    </a>
</div>

<!-- fab customers -->
<div id="call_to_action_customers" class="md-fab-wrapper" style="display:none;">
    <a class="md-fab  md-fab-accent md-fab-wave-light"  id="go_back"><i class="material-icons">arrow_back</i></a>
</div>
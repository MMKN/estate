@if(count($notifications))
<table class="uk-table uk-table-hover uk-table-nowrap" id="res_table">
    <thead>
        <tr>
            <th class="uk-text-center">{{trans('main.Description')}}</th>
            <th class="uk-text-center">{{trans('main.Type')}}</th>
            <th class="uk-text-center">{{trans('main.Created')}}</th>
            <th class="uk-text-center">{{trans('main.Status')}}</th>
        </tr>
    </thead>
    <tbody id="table_results">
            @foreach($notifications as $notification)
                <tr>
                    <td class="uk-text-center">
                        <a onclick="notification_details({{ $notification->id }} )" class="notification_link">
                            {{ mb_strimwidth($notification->description , 0, 30, "...") }} 
                        </a>
                    </td>
                    <td class="uk-text-center"> 
                        {{ $notification->getType($notification->type) }} 
                    </td>
                    <td class="uk-text-small uk-text-center">
                        {{ date("j M" , strtotime($notification->created_at)) }}
                    </td>
                    <td class="uk-text-center">
                        {{ $notification->getStatus($notification->status) }}
                    </td>
                </tr>
            @endforeach
       
    </tbody>
</table>
<div class="uk-grid" style="margin-top:30px;">
    <div class="uk-width-medium-1-1 pager">

{{$notifications->links()}}

    </div>
</div>
@else
    <h3 class="uk-text-muted"> {{trans('main.no_tasks_found')}} </h3>
@endif
@extends('_layouts.core')
    @section('notification_header')
        @include('notifications.notification_details')
    @stop

    

    @section('content')
    <div id="notification_content">  
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-overflow-container uk-margin-bottom" id="filter_results">
                    @include('notifications.notification_filter')
                </div>
            </div>
        </div>

        <div class="md-fab-wrapper" id="add_btn">
            <a class="md-fab md-fab-accent md-fab-wave-light" href="#new_issue" data-uk-modal="{ center:true }">
                <i class="material-icons">&#xE145;</i>
            </a>
        </div>
    </div>

    @include('general.add_notice')
    @stop
    @section('scripts')



    <script>
    var base_url = '{{ URL::route("notifications.index") }}';
        $(function() {
            // enable hires images
            altair_helpers.retina_images();
            // fastClick (touch devices)
            if(Modernizr.touch) {
                FastClick.attach(document.body);
            }
        });
    </script>
    <script type="text/javascript" src="{{ URL::asset('js/notifications.js') }}"></script>
    @stop
@stop
@extends('_layouts.core')
@section('content')
    
    <div id="page_content">

        <div id="page_heading" data-uk-sticky="{top:48, media: 960, boundary: true,showup:true,animation: 'uk-animation-slide-top'}">
            <div class="heading_actions" style="margin-top:0px;">
                @if($is_admin == 1)
                    <a class="md-btn md-btn-flat" href="#edit_issue" data-uk-modal="{ center:true }">{{trans('main.Edit')}}</a>
                @endif
                
                @if($notification->status == 1)
                    <button href="#" onclick="toggleIssue({{ $notification->id }} , this)" class="md-btn md-btn-flat btn-flat-wave ">{{trans('main.Re-Open')}}</button>
                @else
                    <button href="#" onclick="toggleIssue({{ $notification->id }} , this)" class=" md-btn md-btn-flat btn-flat-wave">{{trans('main.Close_Issue')}}</button>
                @endif
                @if($is_admin == 1)
                    <a class="md-btn md-btn-flat-wave uk-text-danger md-btn-flat" href="#" onclick="deleteIssue({{ $notification->id }})" data-uk-modal="{ center:true }">{{trans('main.Delete')}}</a>
                @endif
            </div>
            <h1> <span style="font-weight:bold;">Key</span> : {{ $notification->key }} </h1>
            {{-- <span class="uk-text-small uk-text-muted uk-text-nowrap"> 
                {{ $notification->key }} 
            </span> --}}
        </div>

        <div id="page_content_inner">

            <div class="md-card">
                <div class="md-card-content">
                    
                    <div class="uk-grid uk-grid-divider" data-uk-grid-margin>
                        <div class="uk-width-medium-3-4">
                            <div class="uk-margin-large-bottom">
                                <h2 class="heading_c uk-margin-small-bottom">{{trans('main.Description')}}</h2>
                                {{ $notification->description }}
                            </div>
                            @if( ($notification->type == 1 || $notification->type == 2) && $notification->property_id)
                                <div class="md-card-list-item-content-wrapper">
                                    <!-- here goes description if exist  -->
                                     @if($notification->type == 1)
                                        <!-- <ul class="md-list md-list-addon uk-margin-bottom">
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <img class="md-user-image md-list-addon-avatar" src="URL::asset('assets/img/avatars/user.png')" alt=""/>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">{{ $user->name }}</span>
                                                    <span class="uk-text-small uk-texttpust-muted">{{ $user->mobile }}</span>
                                                </div>
                                            </li>
                                        </ul> -->
                                    @endif
                                    <div class="md-card-list-item-content">

                                        <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-large-2-5 uk-text-center uk-margin-small-top" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
                                             <div style="padding-bottom:27px;">
                                                <a href="{{ URL::route('properties.property.admin' , $notification->property_id) }}" target="_blank">
                                                    <div class="md-card md-card-hover" style="cursor:pointer;" id="{{ $notification->property_id }}">
                                                        <div class="md-card-head head_background" style="background-image: url('{{ (new Property())->getMainImage($notification->property_id) }}')">
                                                            <div class="md-card-head-menu" data-uk-dropdown="" aria-haspopup="true" aria-expanded="false">

                                                            </div>
                                                            <h2 class="md-card-head-text">
                                                                {{ (new Property())->getUnitType($notification->property_id) }}
                                                            </h2>
                                                            <div class="md-card-head-subtext">
                                                                <span>{{(new Property())->getPrice($notification->property_id)}}&#163;</span>
                                                            </div>
                                                        </div>
                                                        <div class="md-card-content">
                                                            <ul class="md-list md-list-addon">
                                                                <li>
                                                                    <div class="md-list-addon-element">
                                                                        <i class="material-icons md-36 uk-text-primary ">&#xE0C8;</i>
                                                                    </div>
                                                                    <div class="md-list-content">
                                                                        <span class="md-list-heading">{{ (new Property())->city($notification->property_id)}}</span>
                                                                        <span class="uk-text-small uk-text-muted">{{ (new Property())->neighbour($notification->property_id) }}</span>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @elseif($notification->type == 3)   
                                <ul class="md-list md-list-addon uk-margin-bottom">
                                    <li>
                                        <div class="md-list-addon-element">
                                            <img class="md-user-image md-list-addon-avatar" src="{{URL::asset('assets/img/avatars/user.png')}}" alt=""/>
                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a target="_blank" href="{{URL::to('/')}}/profile-page/{{ $customer->id }}">{{ $customer->name }}</a></span>
                                            <span class="uk-text-small uk-texttpust-muted">{{ $customer->mobile_1 }}</span>
                                        </div>
                                    </li>
                                </ul>
                            @endif
                                
                            
                            <div class="uk-margin-large-bottom" style="margin-top:2em;">
                                <h2 class="heading_c uk-margin-small-bottom">{{trans('main.Comments')}}</h2>
                                <ul class="uk-comment-list" id="comments_list">
                                    @foreach($notes as $note)
                                        <li>
                                            <article class="uk-comment">
                                                <header class="uk-comment-header">
                                                    <img class="md-user-image uk-comment-avatar" src="{{URL::asset('assets/img/avatars/user.png')}}" alt="">
                                                    <h4 class="uk-comment-title"> {{ $note->getAdmin($note->admin_id) }} </h4>
                                                    <div class="uk-comment-meta"> {{ $note->created_at->diffForHumans() }} </div>
                                                </header>
                                                <div class="uk-comment-body">
                                                    <p>
                                                        {{ $note->body }}
                                                    </p>
                                                </div>
                                            </article>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <textarea cols="30" rows="4" class="md-input note_area" placeholder="{{ trans('main.add_comment') }}"></textarea>
                            <a class="md-btn md-btn-wave uk-margin-top" onclick="addNote(this)">{{ trans('main.add_comment') }}</a>

                        
                        </div>


                        <div class="uk-width-medium-1-4">
                            <div class="uk-margin-medium-bottom">
                                <!-- <p>
                                    {{trans('main.Priority')}} &nbsp;
                                    {{ $notification->getPriority($notification->priority) }}
                                </p> -->
                                <p>
                                    {{trans('main.Status')}} &nbsp;
                                    <span id="status-label">{{ $notification->getStatus($notification->status) }}</span>
                                </p>
                            </div>
                            <h2 class="heading_c uk-margin-small-bottom">{{trans('main.Details')}}</h2>
                            <ul class="md-list md-list-addon">
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons">&#xE8DF;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading"> {{ date("Y-m-d h:i a" , strtotime($notification->created_at)) }} </span>
                                        <span class="uk-text-small uk-text-muted">{{trans('main.Created')}}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons">&#xE8B5;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading"> {{ date("Y-m-d h:i a" , strtotime($notification->updated_at)) }} </span>
                                        <span class="uk-text-small uk-text-muted">{{trans('main.Updated')}}</span>
                                    </div>
                                </li>
                            </ul>
                            <br/>
                            <h2 class="heading_c uk-margin-small-bottom">{{ trans('main.All_Assignees') }}</h2>
                            <ul class="md-list md-list-addon">
                                @foreach($assignees as $assignee)
                                    <li>
                                        <div class="md-list-addon-element">
                                            <img class="md-user-image md-list-addon-avatar" src="{{URL::asset('assets/img/avatars/user.png')}}" alt=""/>
                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading">{{ $notification->getAssignee($assignee->user_id) }}</span>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="uk-modal" id="edit_issue">
        <div class="uk-modal-dialog">
            <form class="uk-form-stacked" id="edit_notification_form">

                <!-- <div class="uk-margin-medium-bottom">
                    <label for="task_title">{{trans('main.Title')}}</label>
                    <input type="text" class="md-input" name="title" value="{{ $notification->title }}"/>
                </div> -->

                <div class="uk-margin-medium-bottom">
                    <label for="task_description">{{trans('main.Description')}}</label>
                    <textarea class="md-input" name="description">{{ $notification->description }}</textarea>
                </div>

                <div class="uk-margin-medium-bottom uk-width-1-1">
                    <label for="task_assignee" class="uk-form-label">{{trans('main.Assignee')}}</label>
                    {{ Form::select('assignee[]' , User::where('id' , '!=' , Auth::user()->id())->lists('name' , 'id') , $selected , ['class' => 'uk-form-width-large' , 'multiple' , 'id' => 'edit_assignee']) }}
                </div>
                <!-- <div class="uk-margin-medium-bottom">
                    <label for="priority" class="uk-form-label">{{trans('main.Priority')}}</label>
                    {{ Form::select('priority' , [0 => 'Urgent' , 1 => 'Normal' , 2 => 'Low'] , $notification->priority , ['class' => 'uk-form-width-medium' , 'data-md-selectize-inline']) }}
                </div> -->

                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-wave md-btn-flat uk-modal-close">{{trans('main.Close')}}</button>
                    <button type="button" class="md-btn md-btn-wave md-btn-flat md-btn-flat-primary" onclick="editNotification()">{{trans('main.Edit')}}</button>
                </div>
            </form>
        </div>
    </div>
    <div class="md-fab-wrapper" id="">
        <a class="md-fab md-fab-accent md-fab-wave-light" href="{{URL::route('notifications.index')}}">
            <i class="material-icons">&#xE7F4;</i>
        </a>
    </div>
@stop
    @section('scripts')
        <script>
        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>



    <!-- page specific plugins -->

    <!--  mailbox functions -->
    <script src="{{ URL::asset('assets/js/pages/page_mailbox.min.js')}}"></script>
    
    <!-- page specific plugins -->
    <!-- ionrangeslider -->
    <script src="../bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>


    <script>
    var base_url = '{{ URL::route("notifications.index") }}';
        $(function() {
            // enable hires images
            altair_helpers.retina_images();
            // fastClick (touch devices)
            if(Modernizr.touch) {
                FastClick.attach(document.body);
            }
        });
    </script>
    <script type="text/javascript" src="{{ URL::asset('js/notifications.js') }}"></script>
    <script type="text/javascript">
        notification_id = {{ $notification->id }}

        function confirmDelete(){
            var id = notification_id;
            $.ajax({
                async:false,
                type : 'POST',
                url  : base_url + '/delete-notification',
                data : {id : id},
                success : function(data){
                    console.log("delete success");
                    window.location = "{{URL::to('/')}}" + "/notifications";
                }
            });
        }

        $('#edit_assignee').selectize({
            plugins: {
                'remove_button': {
                    label: ''
                }
            },
            placeholder: 'Add Property Type and Press Enter',

            render: {
                option: function(data, escape){
                    return  '<div class="option">' +
                        '<span>' + escape(data.title) + '</span>' +
                        '</div>';
                },
                item: function(data, escape){
                    return '<div class="item">' + escape(data.title) + '</div>';
                }
            },
            maxItems: null,
            valueField: 'value',
            labelField: 'title',
            searchField: 'title',
            create: true,
            onDropdownOpen: function($dropdown){
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function() {
                            $dropdown.css({'margin-top':'0'})
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },
            onDropdownClose: function($dropdown){
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function() {
                            $dropdown.css({'margin-top':''})
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            }
        });
    </script>
    @stop
@stop

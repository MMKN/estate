@if(count($notes))
	@foreach($notes as $note)
		<div class="timeline_item">
		   <div class="timeline_icon timeline_icon_primary"><a href="#" onclick="UIkit.modal.prompt('', '', function(val){ UIkit.modal.alert('Hello '+(val || 'Mr noname')+'!'); });"><i class="material-icons">edit</i></a></div>
		   <div class="timeline_date">
		       {{ date("j" , strtotime($note->created_at)) }} <span> {{ date("M" , strtotime($note->created_at)) }} </span>
		   </div>
		   <div class="timeline_content">
		   		{{ $note->body }}
		   </div>
		</div>
	@endforeach
@else
	<p>No Notes For This Notification</p>
@endif
	

	

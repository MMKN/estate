<li>
    <article class="uk-comment">
        <header class="uk-comment-header">
            <img class="md-user-image uk-comment-avatar" src="assets/img/avatars/user.png" alt="">
            <h4 class="uk-comment-title"> {{ $note->getAdmin($note->admin_id) }} </h4>
            <div class="uk-comment-meta"> {{ $note->created_at->diffForHumans() }} </div>
        </header>
        <div class="uk-comment-body">
            <p>
                {{ $note->body }}
            </p>
        </div>
    </article>
</li>
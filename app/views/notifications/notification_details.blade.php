<!-- Notification details -->
<div id="page_content">

    <div id="page_content_inner">
        <div class="md-card">
            <div class="md-card-toolbar">

                <div class="md-card-toolbar-actions">
                    <!-- menu for computer -->
                    <div class="uk-hidden-small">
                        @if($is_admin == 1)
                        <a class="md-btn md-btn-flat md-btn-small" href="#edit_issue" data-uk-modal="{ center:true }">{{trans('main.Edit')}}</a>
                        @endif
                        @if($notification->status == 1)
                        <button href="#" onclick="toggleIssue({{ $notification->id }} , this)" class="md-btn md-btn-flat btn-flat-wave ">{{trans('main.Re-Open')}}</button>
                        @else
                        <button href="#" onclick="toggleIssue({{ $notification->id }} , this)" class=" md-btn md-btn-flat btn-flat-wave">{{trans('main.Close_Issue')}}</button>
                        @endif
                        @if($is_admin == 1)
                        <a class="md-btn md-btn-flat-wave uk-text-danger md-btn-flat" href="#" onclick="deleteIssue({{ $notification->id }})" data-uk-modal="{ center:true }">{{trans('main.Delete')}}</a>
                        @endif
                    </div>
                    <!-- menu for mobile -->
                    <div class="md-card-dropdown uk-visible-small" data-uk-dropdown="{pos:'bottom-right'}">
                        <i class="md-icon material-icons">&#xE5D4;</i>
                        <div class="uk-dropdown">
                            <ul class="uk-nav">
                                @if($is_admin == 1)
                                <li>
                                    <a class="md-btn md-btn-flat md-btn-small" href="#edit_issue" data-uk-modal="{ center:true }">{{trans('main.Edit')}}</a>
                                </li>
                                @endif
                                
                                @if($notification->status == 1)
                                <li>
                                    <a href="#" onclick="toggleIssue({{ $notification->id }} , this)" class="md-btn md-btn-flat btn-flat-wave ">{{trans('main.Re-Open')}}</a>
                                    
                                </li>
                                @else
                                <li>
                                    <a href="#" onclick="toggleIssue({{ $notification->id }} , this)" class=" md-btn md-btn-flat btn-flat-wave">{{trans('main.Close_Issue')}}</a>
                                </li>
                                @endif
                                @if($is_admin == 1)
                                <li>
                                    <a class="md-btn md-btn-flat-wave uk-text-danger md-btn-flat" href="#" onclick="deleteIssue({{ $notification->id }})" data-uk-modal="{ center:true }">{{trans('main.Delete')}}</a>                                    
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>

                <h3 class="md-card-toolbar-heading-text">
                     <span style="font-weight:bold;">Reference num.</span> : {{ $notification->key }} 
                </h3>

            </div>


            <div class="md-card-content uk-margin-top">

                <div class="uk-grid uk-grid-divider" data-uk-grid-margin>
                    <div class="uk-width-medium-3-4 uk-margin-large-bottom">
                        <div class="uk-margin-large-bottom">
                            <h2 class="heading_c uk-margin-small-bottom">{{trans('main.Description')}}</h2>
                            {{ $notification->description }}
                        </div>
                        @if( ($notification->type == 1 || $notification->type == 2) && $notification->property_id)
                        <div class="md-card-list-item-content-wrapper">
                            <!-- here goes description if exist  -->
                            @if($notification->type == 1)
                            <!-- <ul class="md-list md-list-addon uk-margin-bottom">
                                <li>
                                    <div class="md-list-addon-element">
                                        <img class="md-user-image md-list-addon-avatar" src="URL::asset('assets/img/avatars/user.png')" alt=""/>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">{{ $user->name }}</span>
                                        <span class="uk-text-small uk-texttpust-muted">{{ $user->mobile }}</span>
                                    </div>
                                </li>
                            </ul> -->
                            @endif
                            <div class="md-card-list-item-content">
                                <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-large-2-5 uk-margin-small-top" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
                                    <div class="uk-margin-medium-bottom">
                                        <a href="{{ URL::route('properties.property.admin' , $notification->property_id) }}" target="_blank">
                                            <div class="md-card md-card-hover" style="cursor:pointer;" id="{{ $notification->id }}">
                                                <div class="md-card-head head_background" style="box-shadow: rgba(0,0,0,0.1) 0px 40px 20px inset;background-image: url('{{ (new Property())->getMainImage($notification->property_id) }}')">
                                                    <h2 class="md-card-head-text" style="text-shadow: 2px 2px 5px #000; font-size:22px">
                                                    {{ (new Property())->getUnitType($notification->unit_type) }}
                                                    </h2>
                                                    <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-bottom-right uk-margin-bottom uk-margin-top" style="font-size:18px;">{{(new Property())->getPrice($notification->property_id)}} &#163;</div>
                                                </div>
                                                <div class="md-card-content">
                                                    <ul class="md-list">
                                                        <li>
                                                            <div class="md-list-content">
                                                                <span class="md-list-heading">{{ (new Property())->city($notification->property_id)}} </span>
                                                                <span class="uk-text-small uk-text-muted">{{ (new Property())->neighbour($notification->property_id) }}</span>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @elseif($notification->type == 3)
                            @include('customers.customer_card_blank')
                        @endif
                        
                        
                        <div class="uk-margin-large-bottom" style="margin-top:2em;">
                            <h2 class="heading_c uk-margin-small-bottom">{{trans('main.Comments')}}</h2>
                            <ul class="uk-comment-list" id="comments_list">
                                @foreach($notes as $note)
                                <li>
                                    <article class="uk-comment">
                                        <header class="uk-comment-header">
                                            <img class="md-user-image uk-comment-avatar" src="assets/img/avatars/user.png" alt="">
                                            <h4 class="uk-comment-title"> {{ $note->getAdmin($note->admin_id) }} </h4>
                                            <div class="uk-comment-meta"> {{ $note->created_at->diffForHumans() }} </div>
                                        </header>
                                        <div class="uk-comment-body">
                                            <p>
                                                {{ $note->body }}
                                            </p>
                                        </div>
                                    </article>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <textarea cols="30" rows="4" class="md-input note_area" placeholder="{{ trans('main.add_comment') }}"></textarea>
                        <a class="md-btn md-btn-wave uk-margin-top" onclick="addNote(this)">{{ trans('main.add_comment') }}</a>
                    </div>
                    <div class="uk-width-medium-1-4">
                        <div class="uk-margin-medium-bottom">
                            <!-- <p>
                                {{trans('main.Priority')}} &nbsp;
                                {{ $notification->getPriority($notification->priority) }}
                            </p> -->
                            <p>
                                {{trans('main.Status')}} &nbsp;
                                <span id="status-label">{{ $notification->getStatus($notification->status) }}</span>
                            </p>
                        </div>
                        <h2 class="heading_c uk-margin-small-bottom">{{trans('main.Details')}}</h2>
                        <ul class="md-list md-list-addon">
                            <li>
                                <div class="md-list-addon-element">
                                    <i class="md-list-addon-icon material-icons">&#xE8DF;</i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading"> {{ date("Y-m-d h:i a" , strtotime($notification->created_at)) }} </span>
                                    <span class="uk-text-small uk-text-muted">{{trans('main.Created')}}</span>
                                </div>
                            </li>
                            <li>
                                <div class="md-list-addon-element">
                                    <i class="md-list-addon-icon material-icons">&#xE8B5;</i>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading"> {{ date("Y-m-d h:i a" , strtotime($notification->updated_at)) }} </span>
                                    <span class="uk-text-small uk-text-muted">{{trans('main.Updated')}}</span>
                                </div>
                            </li>
                        </ul>
                        <br/>
                        <h2 class="heading_c uk-margin-small-bottom">{{ trans('main.All_Assignees') }}</h2>
                        <ul class="md-list md-list-addon">
                            @foreach($assignees as $assignee)
                            <li>
                                <div class="md-list-addon-element">
                                    <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/user.png" alt=""/>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">{{ $notification->getAssignee($assignee->user_id) }}</span>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="md-fab-wrapper" onclick="back_from_details()" id="back_btn">
    <a class="md-fab md-fab-accent md-fab-wave-light">
        <i class="material-icons">&#xE5C4;</i>
    </a>
</div>
<div class="uk-modal" id="edit_issue">
    <div class="uk-modal-dialog">
        <form onsubmit="return editNotification();" class="uk-form-stacked" id="edit_notification_form">
            <!-- <div class="uk-margin-medium-bottom">
                <label for="task_title">{{trans('main.Title')}}</label>
                <input type="text" class="md-input" name="title" value="{{ $notification->title }}"/>
            </div> -->
            <div class="uk-margin-medium-bottom">
                <label for="task_description">{{trans('main.Description')}}</label>
                <textarea class="md-input" name="description" required="true">{{ $notification->description }}</textarea>
            </div>
            <div class="uk-margin-medium-bottom uk-width-1-1">
                <label for="task_assignee" class="uk-form-label">{{trans('main.Assignee')}}</label>
                {{ Form::select('assignee[]' , User::where('id' , '!=' , Auth::user()->id())->lists('name' , 'id') , $selected , ['class' => 'uk-form-width-large' , 'multiple' , 'id' => 'edit_assignee']) }}
            </div>
            <!-- <div class="uk-margin-medium-bottom">
                <label for="priority" class="uk-form-label">{{trans('main.Priority')}}</label>
                {{ Form::select('priority' , [0 => 'Urgent' , 1 => 'Normal' , 2 => 'Low'] , $notification->priority , ['class' => 'uk-form-width-medium' , 'data-md-selectize-inline']) }}
            </div> -->
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-wave md-btn-flat uk-modal-close">{{trans('main.Close')}}</button>
                <button type="submit" class="md-btn md-btn-wave md-btn-flat md-btn-flat-primary">{{trans('main.Edit')}}</button>
            </div>
        </form>
    </div>
</div>
<div class="uk-width-large-8-10 uk-container-center notification_container">
  <div class="md-card-list">
    <div class="md-card-list-header heading_list">Today</div>
    <div class="md-card-list-header md-card-list-header-combined heading_list" style="display: none">All Messages</div>
    <ul class="hierarchical_slide">
      @if(count($today))
      @foreach($today as $notification)
      <li>
        <div class="md-card-list-item-menu" >
          <a onclick="done( '{{ $notification->user_id }}' , {{ $notification->property_id }})" class="md-icon material-icons">done</a>
        </div>
        <span class="md-card-list-item-date " style="width:60px;"> {{ date("j M" , strtotime($notification->created_at)) }} </span>
        {{ $notification->getStatus($notification->done) }}
        <div class="md-card-list-item-select">
          <input type="checkbox" data-md-icheck />
        </div>
        <!-- <div class="md-card-list-item-avatar-wrapper">
          <img src="assets/img/avatars/avatar_01_tn.png" class="md-card-list-item-avatar" alt="" />
        </div> -->
        <div class="md-card-list-item-sender">
          <span>{{ $notification->getUserName($notification->user_id) }}</span>
        </div>
        <div class="md-card-list-item-subject">
          <div class="md-card-list-item-sender-small">
            <span>{{ $notification->getUserName($notification->user_id) }}</span>
          </div>
          {{-- <span>Requested a Call</span> --}}
          <span>{{ $notification->getPhoneNumber($notification->user_id) }}</span>
        </div>
        <div class="md-card-list-item-content-wrapper">
          <!-- here goes description if exist  -->
          <div class="md-card-list-item-content">
            <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-large-2-5 uk-text-center uk-margin-small-top" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
              <div style="padding-bottom:27px;">
                <a href="{{ URL::route('properties.property.admin' , $notification->property_id) }}" target="_blank">
                  <div class="md-card md-card-hover" style="cursor:pointer;" id="{{ $notification->property_id }}">
                    <div class="md-card-head head_background" style="background-image: url('{{ (new Property())->getMainImage($notification->property_id) }}')">
                      <div class="md-card-head-menu" data-uk-dropdown="" aria-haspopup="true" aria-expanded="false">
                      </div>
                      <h2 class="md-card-head-text">
                      {{ (new Property())->getUnitType($notification->property_id) }}
                      </h2>
                      <div class="md-card-head-subtext">
                        <span>{{(new Property())->getPrice($notification->property_id)}}&#163;</span>
                      </div>
                    </div>
                    <div class="md-card-content">
                      <ul class="md-list md-list-addon">
                        <li>
                          <div class="md-list-addon-element">
                            <i class="material-icons md-36 uk-text-primary ">&#xE0C8;</i>
                          </div>
                          <div class="md-list-content">
                            <span class="md-list-heading">{{ (new Property())->city($notification->property_id)}}</span>
                            <span class="uk-text-small uk-text-muted">{{ (new Property())->neighbour($notification->property_id) }}</span>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
          <div class="md-card-list-item-content">
            here is a timeline with any updates regarding the notification
            <div class="timeline">
              <div class="timeline_item">
                <div class="timeline_icon timeline_icon_primary"><a href="#" onclick="UIkit.modal.prompt('', '', function(val){ UIkit.modal.alert('Hello '+(val || 'Mr noname')+'!'); });"><i class="material-icons">edit</i></a></div>
                <div class="timeline_date">
                  21 <span>Sep</span>
                </div>
                <div class="timeline_content">
                  Something about the customer goes here something about the customer goes here!
                </div>
              </div>
            </div>
          </div>
          <form class="md-card-list-item-reply uk-container-center ">
            <label for="mailbox_reply_7254">Add update</label>
            <textarea class="md-input md-input-full" name="mailbox_reply_7254" id="mailbox_reply_7254" cols="30" rows="1"></textarea>
            <button class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
          </form>
        </div>
      </li>
      @endforeach
      @else
      <li> <h5 style="text-align:center; margin:0; padding:0;"> No Notifications For Today </h5> </li>
      @endif
    </ul>
  </div>
  <div class="md-card-list">
    <div class="md-card-list-header heading_list">Yesterday</div>
    <ul class="hierarchical_slide">
      @if(count($yesterday))
      @foreach($yesterday as $notification)
      <li>
        <div class="md-card-list-item-menu" >
          <a onclick="done( '{{ $notification->user_id }}' , {{ $notification->property_id }})" class="md-icon material-icons">done</a>
        </div>
        <span class="md-card-list-item-date " style="width:60px;">{{ date("j M" , strtotime($notification->created_at)) }}</span>
        {{ $notification->getStatus($notification->done) }}
        <div class="md-card-list-item-select">
          <input type="checkbox" data-md-icheck />
        </div>
        <!-- <div class="md-card-list-item-avatar-wrapper">
          <img src="assets/img/avatars/avatar_01_tn.png" class="md-card-list-item-avatar" alt="" />
        </div> -->
        <div class="md-card-list-item-sender">
          <span>{{ $notification->getUserName($notification->user_id) }}</span>
        </div>
        <div class="md-card-list-item-subject">
          <div class="md-card-list-item-sender-small">
            <span>{{ $notification->getUserName($notification->user_id) }}</span>
          </div>
          {{-- <span>Requested a Call</span> --}}
          <span>{{ $notification->getPhoneNumber($notification->user_id) }}</span>
        </div>
        <div class="md-card-list-item-content-wrapper">
          <!-- here goes description if exist  -->
          <div class="md-card-list-item-content">
            <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-large-2-5 uk-text-center uk-margin-small-top" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
              <div style="padding-bottom:27px;">
                <a href="{{ URL::route('properties.property.admin' , $notification->property_id) }}" target="_blank">
                  <div class="md-card md-card-hover" style="cursor:pointer;" id="{{ $notification->property_id }}">
                    <div class="md-card-head head_background" style="background-image: url('{{ (new Property())->getMainImage($notification->property_id) }}')">
                      <div class="md-card-head-menu" data-uk-dropdown="" aria-haspopup="true" aria-expanded="false">
                      </div>
                      <h2 class="md-card-head-text">
                      {{ (new Property())->getUnitType($notification->property_id) }}
                      </h2>
                      <div class="md-card-head-subtext">
                        <span>{{(new Property())->getPrice($notification->property_id)}}&#163;</span>
                      </div>
                    </div>
                    <div class="md-card-content">
                      <ul class="md-list md-list-addon">
                        <li>
                          <div class="md-list-addon-element">
                            <i class="material-icons md-36 uk-text-primary ">&#xE0C8;</i>
                          </div>
                          <div class="md-list-content">
                            <span class="md-list-heading">{{ (new Property())->city($notification->property_id)}}</span>
                            <span class="uk-text-small uk-text-muted">{{ (new Property())->neighbour($notification->property_id) }}</span>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
          <div class="md-card-list-item-content">
            here is a timeline with any updates regarding the notification
            <div class="timeline">
              <div class="timeline_item">
                <div class="timeline_icon timeline_icon_primary"><a href="#" onclick="UIkit.modal.prompt('', '', function(val){ UIkit.modal.alert('Hello '+(val || 'Mr noname')+'!'); });"><i class="material-icons">edit</i></a></div>
                <div class="timeline_date">
                  21 <span>Sep</span>
                </div>
                <div class="timeline_content">
                  Something about the customer goes here something about the customer goes here!
                </div>
              </div>
            </div>
          </div>
          <form class="md-card-list-item-reply uk-container-center ">
            <label for="mailbox_reply_7254">Add update</label>
            <textarea class="md-input md-input-full" name="mailbox_reply_7254" id="mailbox_reply_7254" cols="30" rows="1"></textarea>
            <button class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
          </form>
        </div>
      </li>
      @endforeach
      @else
      <li> <h5 style="text-align:center; margin:0; padding:0;"> No Notifications For Yesterday </h5> </li>
      @endif
    </ul>
  </div>
  <div class="md-card-list">
    <div class="md-card-list-header heading_list">This Month</div>
    <ul class="hierarchical_slide">
      @if(count($month))
      @foreach($month as $notification)
      <li>
        <div class="md-card-list-item-menu" >
          <a onclick="done( '{{ $notification->user_id }}' , {{ $notification->property_id }})" class="md-icon material-icons">done</a>
        </div>
        <span class="md-card-list-item-date " style="width:60px;">{{ date("j M" , strtotime($notification->created_at)) }}</span>
        {{ $notification->getStatus($notification->done) }}
        <div class="md-card-list-item-select">
          <input type="checkbox" data-md-icheck />
        </div>
        <!-- <div class="md-card-list-item-avatar-wrapper">
          <img src="assets/img/avatars/avatar_01_tn.png" class="md-card-list-item-avatar" alt="" />
        </div> -->
        <div class="md-card-list-item-sender">
          <span>{{ $notification->getUserName($notification->user_id) }}</span>
        </div>
        <div class="md-card-list-item-subject">
          <div class="md-card-list-item-sender-small">
            <span>{{ $notification->getUserName($notification->user_id) }}</span>
          </div>
          {{-- <span>Requested a Call</span> --}}
          <span>{{ $notification->getPhoneNumber($notification->user_id) }}</span>
        </div>
        <div class="md-card-list-item-content-wrapper">
          <!-- here goes description if exist  -->
          <div class="md-card-list-item-content">
            <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-large-2-5 uk-text-center uk-margin-small-top" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
              <div style="padding-bottom:27px;">
                <a href="{{ URL::route('properties.property.admin' , $notification->property_id) }}" target="_blank">
                  <div class="md-card md-card-hover" style="cursor:pointer;" id="{{ $notification->property_id }}">
                    <div class="md-card-head head_background" style="background-image: url('{{ (new Property())->getMainImage($notification->property_id) }}')">
                      <div class="md-card-head-menu" data-uk-dropdown="" aria-haspopup="true" aria-expanded="false">
                      </div>
                      <h2 class="md-card-head-text">
                      {{ (new Property())->getUnitType($notification->property_id) }}
                      </h2>
                      <div class="md-card-head-subtext">
                        <span>{{(new Property())->getPrice($notification->property_id)}}&#163;</span>
                      </div>
                    </div>
                    <div class="md-card-content">
                      <ul class="md-list md-list-addon">
                        <li>
                          <div class="md-list-addon-element">
                            <i class="material-icons md-36 uk-text-primary ">&#xE0C8;</i>
                          </div>
                          <div class="md-list-content">
                            <span class="md-list-heading">{{ (new Property())->city($notification->property_id)}}</span>
                            <span class="uk-text-small uk-text-muted">{{ (new Property())->neighbour($notification->property_id) }}</span>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
          <div class="md-card-list-item-content">
            here is a timeline with any updates regarding the notification
            <div class="timeline">
              <div class="timeline_item">
                <div class="timeline_icon timeline_icon_primary"><a href="#" onclick="UIkit.modal.prompt('', '', function(val){ UIkit.modal.alert('Hello '+(val || 'Mr noname')+'!'); });"><i class="material-icons">edit</i></a></div>
                <div class="timeline_date">
                  21 <span>Sep</span>
                </div>
                <div class="timeline_content">
                  Something about the customer goes here something about the customer goes here!
                </div>
              </div>
            </div>
          </div>
          <form class="md-card-list-item-reply uk-container-center ">
            <label for="mailbox_reply_7254">Add update</label>
            <textarea class="md-input md-input-full" name="mailbox_reply_7254" id="mailbox_reply_7254" cols="30" rows="1"></textarea>
            <button class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
          </form>
        </div>
      </li>
      @endforeach
      @else
      <h1>No For Month</h1>
      @endif
    </ul>
  </div>
  <div class="md-card-list">
    <div class="md-card-list-header heading_list">Older Messages</div>
    <ul class="hierarchical_slide">
      @if(count($notifications))
      @foreach($notifications as $notification)
      <li>
        <div class="md-card-list-item-menu" >
          <a onclick="done( '{{ $notification->user_id }}' , {{ $notification->property_id }})" class="md-icon material-icons">done</a>
        </div>
        <span class="md-card-list-item-date " style="width:60px;">{{ date("j M" , strtotime($notification->created_at)) }}</span>
        {{ $notification->getStatus($notification->done) }}
        <div class="md-card-list-item-select">
          <input type="checkbox" data-md-icheck />
        </div>
        {{--  <div class="md-card-list-item-avatar-wrapper">
          <img src="assets/img/avatars/avatar_01_tn.png" class="md-card-list-item-avatar" alt="" />
        </div> --}}
        <div class="md-card-list-item-sender">
          <span>{{ $notification->getUserName($notification->user_id) }}</span>
        </div>
        <div class="md-card-list-item-subject">
          <div class="md-card-list-item-sender-small">
            <span>{{ $notification->getUserName($notification->user_id) }}</span>
          </div>
          {{-- <span>Requested a Call</span> --}}
          <span>{{ $notification->getPhoneNumber($notification->user_id) }}</span>
        </div>
        <div class="md-card-list-item-content-wrapper">
          <!-- here goes description if exist  -->
          <div class="md-card-list-item-content">
            <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-large-2-5 uk-text-center uk-margin-small-top" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
              <div style="padding-bottom:27px;">
                <a href="{{ URL::route('properties.property.admin' , $notification->property_id) }}" target="_blank">
                  <div class="md-card md-card-hover" style="cursor:pointer;" id="{{ $notification->property_id }}">
                    <div class="md-card-head head_background" style="background-image: url('{{ (new Property())->getMainImage($notification->property_id) }}')">
                      <div class="md-card-head-menu" data-uk-dropdown="" aria-haspopup="true" aria-expanded="false">
                      </div>
                      <h2 class="md-card-head-text">
                      {{ (new Property())->getUnitType($notification->property_id) }}
                      </h2>
                      <div class="md-card-head-subtext">
                        <span>{{(new Property())->getPrice($notification->property_id)}}&#163;</span>
                      </div>
                    </div>
                    <div class="md-card-content">
                      <ul class="md-list md-list-addon">
                        <li>
                          <div class="md-list-addon-element">
                            <i class="material-icons md-36 uk-text-primary ">&#xE0C8;</i>
                          </div>
                          <div class="md-list-content">
                            <span class="md-list-heading">{{ (new Property())->city($notification->property_id)}}</span>
                            <span class="uk-text-small uk-text-muted">{{ (new Property())->neighbour($notification->property_id) }}</span>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
          <div class="md-card-list-item-content">
            here is a timeline with any updates regarding the notification
            <div class="timeline">
              <div class="timeline_item">
                <div class="timeline_icon timeline_icon_primary"><a href="#" onclick="UIkit.modal.prompt('', '', function(val){ UIkit.modal.alert('Hello '+(val || 'Mr noname')+'!'); });"><i class="material-icons">edit</i></a></div>
                <div class="timeline_date">
                  21 <span>Sep</span>
                </div>
                <div class="timeline_content">
                  Something about the customer goes here something about the customer goes here!
                </div>
              </div>
            </div>
          </div>
          <form class="md-card-list-item-reply uk-container-center ">
            <label for="mailbox_reply_7254">Add update</label>
            <textarea class="md-input md-input-full" name="mailbox_reply_7254" id="mailbox_reply_7254" cols="30" rows="1"></textarea>
            <button class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
          </form>
        </div>
      </li>
      @endforeach
      @else
      <h1>No Notifications</h1>
      @endif
    </ul>
  </div>
</div>
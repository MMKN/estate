@extends('_layouts.core')
    @section('notification_header')
        <div id="page_heading" class="page_heading uk-margin-bottom">
            <form id="filter_form">
                <div class="uk-grid" data-uk-grid-margin="">
                  <h1 class="uk-width-medium-3-10 uk-margin-bottom uk-text-center" style="margin-top:12px;"><i class="material-icons md-36 uk-margin-right" style="vertical-align:text-top;">notifications</i>{{trans('main.Notifications')}}</h1>

                    <div class="uk-width-medium-2-10">
                        <div class="parsley-row  uk-margin-small-top">
                            <select name="type" required data-md-selectize>
                                <option value="5" selected>{{trans('main.All_types')}}</option>
                                <option value="0">{{trans('main.General')}}</option>
                                <option value="1">{{trans('main.Call_requests')}}</option>
                                <option value="2">{{trans('main.Properties_Notices')}}</option>
                                <option value="3">{{trans('main.Customers_Notices')}}</option>
                            </select>
                        </div>
                    </div>

                    <div class="uk-width-medium-2-10">
                        <div class="parsley-row  uk-margin-small-top">
                            <select id="status" name="status" required data-md-selectize>
                                <option value="0">{{trans('main.Open')}}</option>
                                <option value="1">{{trans('main.Closed')}}</option>
                                <option value="2">{{trans('main.Reopened')}}</option>
                                <option value="4" selected>{{trans('main.All_Status')}}</option>
                            </select>
                        </div>
                    </div>

                    <div class="uk-width-medium-2-10">
                        <div class="parsley-row  uk-margin-small-top">
                            <select id="status" name="assignee" required data-md-selectize>
                                <option value="0">{{trans('main.All_Assignees')}}</option>
                                <option value="{{(Auth::user()->id())}}">{{trans('main.me')}}</option>
                                @foreach($admins as $admin)
                                <option value="{{$admin->id}}">{{$admin->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-10 uk-text-center uk-margin-bottom">
                        <a id="advanced_filter" class="md-btn md-btn-flat uk-margin-small-top md-btn-wave" data-uk-tooltip="{pos:'bottom'}" title="{{trans('main.advanced_filter')}}"><i class="material-icons md-24">filter_list</i></a>
                    </div>
                    <div class="uk-grid uk-width-1-1" id="more_filters" style="display:none;">
                        
                       <div class="uk-width-small-1-2 uk-width-medium-2-10">
                           <div class="uk-input-group">
                               <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                               <label for="uk_dp_start">{{trans('main.Start_Date')}}</label>
                               <input class="md-input" type="text" id="uk_dp_start" name="start_date">
                           </div>
                       </div>
                       
                       <div class="uk-width-small-1-2  uk-width-medium-2-10">
                           <div class="uk-input-group">
                               <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                               <label for="uk_dp_end">{{trans('main.End_Date')}}</label>
                               <input class="md-input" type="text" id="uk_dp_end" name="end_date">
                           </div>
                       </div>

                        <!-- <div class="uk-width-medium-2-10">
                            <div class="uk-input-group">
                               <span class="uk-input-group-addon">
                                   <a href="#"><i class="material-icons">search</i></a>
                               </span>
                               
                                <label for="contact_list_search">{{trans('main.Search_id')}}</label>
                                <input name="key" onkeyup="searchKeyword(this)"  class="md-input" type="text"/>
                               
                            </div>                       
                        </div>  -->
                    </div>
                    
                    <input type="hidden" value="true" name="filter">
                </div>
            </form>
        </div>
    @stop

    

    @section('content')
    <div id="notification_content">  
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-overflow-container uk-margin-bottom" id="filter_results">
                    @include('notifications.notification_filter')
                </div>
            </div>
        </div>

        <div class="md-fab-wrapper" id="add_btn">
            <a class="md-fab md-fab-accent md-fab-wave-light" href="#new_issue" data-uk-modal="{ center:true }">
                <i class="material-icons">&#xE145;</i>
            </a>
        </div>
    </div>

    @include('general.add_notice')
    @stop
    @section('scripts')



    <script>
    var base_url = '{{ URL::route("notifications.index") }}';
        $(function() {
            // enable hires images
            altair_helpers.retina_images();
            // fastClick (touch devices)
            if(Modernizr.touch) {
                FastClick.attach(document.body);
            }
        });
    </script>
    <script type="text/javascript" src="{{ URL::asset('js/notifications.js') }}"></script>
    @stop
@stop
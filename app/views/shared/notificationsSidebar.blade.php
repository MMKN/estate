<!-- notifications sidebar -->
<aside id="sidebar_secondary" class="tabbed_sidebar">
    <ul class="uk-tab uk-tab-icons uk-tab-grid" data-uk-tab="{connect:'#dashboard_sidebar_tabs', animation:'slide-horizontal'}">
    </ul>
    <div class="scrollbar-inner">
        <ul id="dashboard_sidebar_tabs" class="uk-switcher">
            <li>
                <div class="timeline timeline_small uk-margin-bottom" id="notification_timeline">
                    @if(count($sb_notifications))
                    @foreach($sb_notifications as $notification)
                    <div class="timeline_item ">
                        <div class="timeline_icon">
                            {{-- <i class="material-icons">&#xE85D;</i> --}}
                            <img class="md-user-image" src="uploads/profile/{{User::whereId($notification->notifier)->pluck('image')}}" alt="" />
                        </div>
                        <div class="timeline_content {{$notification->read == '0'?'md-bg-light-blue-50':''}}">
                            <a onclick="updateNotificationState(this)" href="{{URL::route('notifications.details-page').'/'.$notification->notice_id}}" target="_blank">{{ $notification->message }}</a>
                        </div>
                        <div class="timeline_date">
                            {{ date("j" , strtotime($notification->created_at)) }} <span> {{ date("M h:i a" , strtotime($notification->created_at)) }} </span>
                        </div>
                    </div>
                    @endforeach
                </div>
                <button class="md-btn md-btn-flat" onclick="loadMore()" id="loadButton" style="display:block; margin:0 auto; width:80%; text-align:center; font-weight:bold; margin-top:2.5em;">{{trans('main.More_Comments')}}</button>
                @else
                <span>{{trans('main.no_notifications')}}</span>
                @endif
            </li>
        </ul>
    </div>
    <button type="button" class="chat_sidebar_close uk-close"></button>
    <div class="chat_submit_box">
        <div class="uk-input-group">
            <input type="text" class="md-input" name="submit_message" id="submit_message" placeholder="Send message">
            <span class="uk-input-group-addon">
                <a href="#"><i class="material-icons md-24">&#xE163;</i></a>
            </span>
        </div>
    </div>
</aside>
<header id="header_main">
    <div class="header_main_content">
        <nav class="uk-navbar">
            <!-- secondary sidebar switch -->
            <div id="menu_top_dropdown" class="uk-float-left uk-visible-small">
                <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
                    <a href="#" class="top_menu_toggle"><i class="md-icon material-icons md-24 md-light">&#xE8F0;</i></a>
                    <div class="uk-dropdown uk-dropdown-width-2">
                        <div class="uk-grid uk-dropdown-grid" data-uk-grid-margin>
                            <div class="uk-width-1">
                                <div class="uk-grid uk-grid-width-medium-1-2 uk-margin-top uk-margin-bottom uk-text-center" data-uk-grid-margin>
                                    <a class="uk-margin-medium-bottom" href="{{ URL::route('properties.index') }}">
                                        <i class="material-icons md-36">&#xE88A;</i>
                                        <span class="uk-text-muted uk-display-block">{{trans('main.properties')}}</span>
                                    </a>
                                    <a class="uk-margin-medium-bottom" href="{{ URL::route('customers.index') }}">
                                        <i class="material-icons md-36">face</i>
                                        <span class="uk-text-muted uk-display-block">{{trans('main.customers')}}</span>
                                    </a>
                                    <a class="uk-margin-medium-bottom" href="{{ URL::route('notifications.index') }}">
                                        <i class="material-icons md-36">notifications</i>
                                        <span class="uk-text-muted uk-display-block">{{trans('main.Notifications')}}</span>
                                    </a>
                                    <a class="uk-margin-medium-bottom" href="{{ URL::route('users.index') }}">
                                        <i class="material-icons md-36">supervisor_account</i>
                                        <span class="uk-text-muted uk-display-block">{{trans('main.Users')}}</span>
                                    </a>
                                    @if(Auth::user()->user()->is_admin == 1)
                                    <a class="uk-margin-medium-bottom uk-hidden-small" href="{{ URL::route('settings.index') }}">
                                        <i class="material-icons md-36">&#xE8B8;</i>
                                        <span class="uk-text-muted uk-display-block">{{trans('main.settings')}}</span>
                                    </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
                <a href="{{ URL::route('properties.index') }}" data-uk-tooltip="{pos:'bottom'}" title="{{trans('main.properties')}}" class="top_menu_toggle"><i class="md-icon material-icons md-24 md-light">&#xE88A;</i></a>
            </div>
             <div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
                <a href="{{ URL::route('customers.index') }}" class="top_menu_toggle" data-uk-tooltip="{pos:'bottom'}" title="{{trans('main.customers')}}"><i class="md-icon material-icons md-24 md-light">face</i></a>
            </div>
             <div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
                <a href="{{ URL::route('notifications.index') }}" class="top_menu_toggle" data-uk-tooltip="{pos:'bottom'}" title="{{trans('main.Notifications')}}"><i class="md-icon material-icons md-24 md-light">notifications</i></a>
            </div>
             <div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
                <a href="{{ URL::route('users.index') }}" class="top_menu_toggle" data-uk-tooltip="{pos:'bottom'}" title="{{trans('main.Users')}}"><i class="md-icon material-icons md-24 md-light">supervisor_account</i></a>
            </div>

            @if(Auth::user()->user()->is_admin == 1)
             <div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
                <a href="{{ URL::route('settings.index') }}" class="top_menu_toggle" data-uk-tooltip="{pos:'bottom'}" title="{{trans('main.settings')}}"><i class="md-icon material-icons md-24 md-light">&#xE8B8;</i></a>
            </div>
            @endif


            <div class="uk-navbar-flip">
                <ul class="uk-navbar-nav user_actions">
                    <li class="uk-visible-large"><a href="#" id="full_screen_toggle" class="user_action_icon "><i class="material-icons md-24 md-light" data-uk-tooltip="{pos:'bottom'}" title="{{trans('main.maximize')}}">&#xE5D0;</i></a></li>
                    <li class="">
                        <a href="#" id="sidebar_secondary_toggle" class="user_action_icon sSwitch_right sidebar_secondary_check"><i class="material-icons md-24 md-light">question_answer</i>
                            @if(count($count) && $count != 0)
                            <span class="uk-badge" style="background:#f00;" id="notifications_count">
                                {{ $count }}
                            </span>
                            @else
                            @endif
                            
                        </a>
                    </li>
                    <li data-uk-dropdown="{mode:'click'}">
                        <a href="#" class="user_action_image"><img class="md-user-image" src="{{URL::asset('uploads/profile/' . Session::get('image') )}}" alt="" /></a>
                        <div class="uk-dropdown uk-dropdown-small uk-dropdown-flip">
                            <ul class="uk-nav js-uk-prevent">
                                <li><a class="uk-text-center" href="{{ URL::route('postLogout') }}">{{trans('main.Logout')}}</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div id="menu_top_dropdown" class="uk-float-right">
                <div class="uk-button-dropdown" data-uk-dropdown="">
                    <a href="#" class="top_menu_toggle"><i class="md-icon material-icons md-24 md-light">&#xE3b7;</i></a>
                    <div id="style_switcher"  class="uk-dropdown uk-dropdown-small uk-dropdown-flip">
                        <div>
                        <ul class="switcher_app_themes" id="theme_switcher">
                            <li class="app_style_default active_theme" data-app-theme="">
                                <span class="app_color_main"></span>
                                <span class="app_color_accent"></span>
                            </li>
                            <li class="switcher_theme_a" data-app-theme="app_theme_a">
                                <span class="app_color_main"></span>
                                <span class="app_color_accent"></span>
                            </li>
                            <li class="switcher_theme_b" data-app-theme="app_theme_b">
                                <span class="app_color_main"></span>
                                <span class="app_color_accent"></span>
                            </li>
                            <li class="switcher_theme_c" data-app-theme="app_theme_c">
                                <span class="app_color_main"></span>
                                <span class="app_color_accent"></span>
                            </li>
                            <li class="switcher_theme_d" data-app-theme="app_theme_d">
                                <span class="app_color_main"></span>
                                <span class="app_color_accent"></span>
                            </li>
                            <li class="switcher_theme_e" data-app-theme="app_theme_e">
                                <span class="app_color_main"></span>
                                <span class="app_color_accent"></span>
                            </li>
                            <li class="switcher_theme_f" data-app-theme="app_theme_f">
                                <span class="app_color_main"></span>
                                <span class="app_color_accent"></span>
                            </li>
                            <li class="switcher_theme_g" data-app-theme="app_theme_g">
                                <span class="app_color_main"></span>
                                <span class="app_color_accent"></span>
                            </li>
                            <li class="switcher_theme_h" data-app-theme="app_theme_h">
                                <span class="app_color_main"></span>
                                <span class="app_color_accent"></span>
                            </li>
                            <li class="switcher_theme_i" data-app-theme="app_theme_i">
                                <span class="app_color_main"></span>
                                <span class="app_color_accent"></span>
                            </li>
                            <li class="switcher_theme_dark" data-app-theme="app_theme_dark">
                                <span class="app_color_main"></span>
                                <span class="app_color_accent"></span>
                            </li>
                        </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if(Auth::user()->user()->is_admin == 1)
            @yield('archieved')
            @endif
        </nav>
    </div>
</header>
<audio id="notification_sound" src="{{URL::asset('uploads/fb.mp3')}}" preload="auto"></audio>
  <!-- header scripts -->

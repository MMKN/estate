<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no" />
        <link rel="icon" type="image/png" href="{{ URL::asset('assets/img/favicon-16x16.png') }}" sizes="16x16">
        <link rel="icon" type="image/png" href="{{ URL::asset('assets/img/favicon-32x32.png') }}" sizes="32x32">
        <title>{{trans('main.project_name')}}</title>
        <!-- uikit -->
        <link rel="stylesheet" href="{{ URL::asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}" media="all">
        <!-- flag icons -->
        <link rel="stylesheet" href="{{ URL::asset('assets/icons/flags/flags.min.css') }}" media="all">
        <!-- style switcher -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/style_switcher.min.css') }}" media="all">
        <!-- theme admin -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/main.min.css') }}" media="all">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/themes/custom_my_theme.css') }}" media="all">
        <!-- themes -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/themes/themes_combined.min.css') }}" media="all">
        <!-- estatify theme -->

        <!-- additional styles for plugins -->
        <!-- weather icons -->
        <link rel="stylesheet" href="{{ URL::asset('bower_components/weather-icons/css/weather-icons.min.css') }}" media="all">
        <!-- Custom styles -->
        <link rel="stylesheet" href="{{ URL::asset('assets/js/lightbox2-master/src/css/lightbox.css') }}" media="all">
        
        <style>
            .page_heading_active #header_main {
                box-shadow: 0 3px 6px rgba(0,0,0,.16),0 3px 6px rgba(0,0,0,.23);
            }
            .uk-table td{
            border-bottom: 0px;
            }
            #style_switcher .switcher_app_themes li.app_style_default .app_color_main {
                background: #585858;
            }
            #style_switcher .switcher_app_themes li.app_style_default .app_color_accent {
                background: #f78c1f;
            }
            #style_switcher{   
             position: absolute;
            }
            .md-bg-light-blue-600 {
                background-color: #727272!important;
            }
            .uk-overlay-background {
                background: rgba(0,0,0,.1);
            }
            .app_theme_dark .uk-text-muted {
                color: #d3d3d3!important;
            }
            .app_theme_dark .uk-tab>li.uk-active>a {
                border-bottom-color: #fefefe;
            }
        </style>
    </head>

@if(count($properties))
@foreach($properties as $property)
    @include('properties.property_card')
@endforeach
@else
<div class="uk-width-medium-1-1">
    <h3 style="background-color: #fff; padding: 0.6em;">{{trans('main.No_Properies_Available')}}</h3>

    <div class="full_width_in_card heading_c" style="float:left;">
    </div>
</div>
@endif
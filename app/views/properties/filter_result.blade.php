<div class="uk-width-1-1 uk-container-center">
    <h3 class="heading_c uk-overlay-background uk-margin-large uk-margin-bottom" style="padding:0.6em;">{{trans('main.Exact_Properties')}}</h3>
</div>
@if(count($properties))
@foreach($properties as $property)
    @include('properties.property_card')
@endforeach
@else
<div class="uk-width-medium-1-1 uk-margin-medium-bottom">
    <i class="material-icons md-48 uk-text-warning">warning</i>
</div>
@endif

<div class="uk-width-medium-1-1">
    <h3 class="heading_c uk-overlay-background uk-margin-large uk-margin-bottom" style="padding:0.6em;">{{trans('main.Suggested_Properties')}}</h3>
</div>
@if(count($suggestions))
@foreach($suggestions as $property)
    @include('properties.property_card')
@endforeach
@else
<div class="uk-width-medium-1-1" style="padding-bottom:1.4em;">
    <i class="material-icons md-48 uk-text-warning">warning</i>
</div>
@endif





@if($price_set)
    <div class="uk-width-medium-1-1">
        <h3 class="heading_c uk-overlay-background uk-margin-large uk-margin-bottom" style="padding:0.6em;">{{trans('main.Price_Properties')}}</h3>
    </div>
    @if(count($price_properties))
    @foreach($price_properties as $property)
        @include('properties.property_card')
    @endforeach
    @else
    <div class="uk-width-medium-1-1">
        <i class="material-icons md-48 uk-text-warning">warning</i>
    </div>
    @endif
@endif
<!-- filter begin-->
<div class="md-card">
    <form action="{{ URL::route('properties.filter') }}" id="search_form">
        <div class="md-card-content">
            <div id="search_title" class="uk-grid">
                <div class="uk-width-1-1 uk-text-center">
                    <span class="uk-display-inline"><i class="uk-margin-right material-icons md-36" style="vertical-align:-9px;">home</i> <h2 class="uk-display-inline" style="margin-top:20px;"> {{trans('main.properties')}}</h2></span>
                </div>
            </div>
            <div class="uk-grid">
                <div class="uk-width-small-1-1 uk-width-medium-1-3 uk-width-large-1-5 uk-text-center uk-margin-bottom">
                    <div class="uk-form-row parsley-row uk-margin-small-top " style="padding-top:5px;">
                        <span class="icheck-inline uk-margin-small-top">
                            <input class="search_attr_change ichk add-sale" id="sale" type="radio" name="purpose" value="0" checked data-md-icheck required />
                            <label for="sale" class="inline-label">{{trans('main.Sale')}}</label>
                        </span>
                        <span class="icheck-inline">
                            <input class="search_attr_change ichk" id="rent" type="radio" name="purpose" value="1" data-md-icheck />
                            <label for="rent" class="inline-label">{{trans('main.Rent')}}</label>
                        </span>
                    </div>
                </div>
                <div class="uk-width-medium-1-3 uk-width-large-2-10 uk-margin-bottom">
                    <div class="uk-margin-small-top">
                        <select class="search_attr" id="unit_type_search" name="unit_type[]" data-md-selectize multiple data-md-selectize-bottom>
                            <option value="" disabled selected>{{trans('main.Property_type')}}</option>
                            @foreach($unit_types as $unit_type)
                            <option value="{{ $unit_type->id }}">{{ $unit_type->value}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="uk-width-medium-1-3 uk-width-large-2-10 uk-margin-bottom">
                    <div class="parsley-row  uk-margin-small-top">
                        <select name="city" class="search_attr" required data-md-selectize id="city_search">
                            <option value="" disabled selected>{{trans('main.City')}}</option>
                            @foreach($cities as $city)
                            <option value="{{ $city->id }}">{{ $city->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="uk-width-medium-1-3 uk-width-large-2-10 uk-text-center uk-margin-bottom" id="neighbour_search">
                    <div class="parsley-row  uk-margin-small-top">
                        <select id="neighbours_search" class="search_attr" name="neighbour[]" data-md-selectize multiple data-md-selectize-bottom disabled="">
                            <option value="" disabled selected>{{trans('main.District')}}</option>
                        </select>
                    </div>
                </div>
                <!-- New Price select system -->
                <div class="uk-width-1-2 uk-width-medium-1-3 uk-width-large-1-10 uk-margin-bottom">
                    <label>{{trans('main.Price_from')}}</label>
                    <input class="md-input masked_input search_attr" name="price_from" type="text" data-inputmask="'alias': 'numeric', 'prefix': '', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false , 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                </div>
                <div class="uk-width-1-2 uk-width-medium-1-3 uk-width-large-1-10 uk-margin-bottom">
                    <label>{{trans('main.Price_to')}}</label>
                    <input class="md-input masked_input search_attr" name="price_to" type="text" data-inputmask="'alias': 'numeric', 'prefix': '', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false , 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                </div>
                
                <!-- advanced options -->
                <div class="uk-grid uk-width-1-1" id="advanced_seach" style="display:none;">
                    <div class="uk-width-medium-1-4 uk-margin-bottom">
                        <div class="parsley-row uk-margin-small-top">
                            <select class="search_attr" id="floor_search" name="floor[]" data-md-selectize multiple data-md-selectize-bottom>
                                <option value="" disabled selected>{{trans('main.Floor')}}</option>
                                @foreach($floors as $floor)
                                <option value="{{ $floor->id }}">{{ $floor->value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-4 uk-margin-bottom">
                        <div class="parsley-row  uk-margin-small-top">
                            <select class="search_attr" id="finishing_search" name="finishing[]" data-md-selectize multiple data-md-selectize-bottom>
                                <option value="" disabled selected>{{trans('main.Finishing')}}</option>
                                @foreach($finishings as $finishing)
                                <option value="{{ $finishing->id }}">{{ $finishing->value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-4 uk-margin-bottom">
                        <div class="parsley-row  uk-margin-small-top">
                            <select class="search_attr" id="payment_search" name="payment_style[]" data-md-selectize multiple data-md-selectize-bottom>
                                <option value="" disabled selected>{{trans('main.Payment_style')}}</option>
                                @foreach($payment_types as $payment_type)
                                <option value="{{ $payment_type->id }}">{{ $payment_type->value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="uk-width-medium-1-4 uk-margin-bottom">
                        <div class="parsley-row  uk-margin-small-top">
                            <select class="search_attr" id="objective_search" name="objective" required data-md-selectize>
                                <option value="" disabled selected>{{trans('main.Objective')}}</option>
                                @foreach($objectives as $objective)
                                <option value="{{ $objective->id }}">{{ $objective->value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="uk-width-medium-1-4 uk-margin-bottom">
                        <div class="uk-input-group " style="margin-top:13px;">
                            <input class="search_attr"value="1" type="checkbox" id="is_compound" name="is_compound" data-switchery data-switchery-color="#ffa000" />
                            <label for="is_compound" class="inline-label">{{trans('main.Compound')}}</label>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-4 uk-margin-bottom">
                        <label>{{trans('main.Area')}}</label>
                        <input class="md-input masked_input search_attr" name="area" type="text" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 0, 'digitsOptional': false , 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                    </div>
                    <div class="uk-width-medium-1-4 uk-margin-bottom">
                        <label>{{trans('main.Rooms_No')}}</label>
                        <input class="md-input masked_input search_attr" name="rooms_no" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false , 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                    </div>
                    <div class="uk-width-medium-1-4 uk-margin-bottom ">
                        <div class="parsley-row  uk-margin-small-top">
                            <select class="search_attr" id="sort_by" name="sort_by" data-md-selectize>
                                <option value="">{{trans('main.sort_by')}}</option>
                                <option value="date" >{{trans('main.sort_date')}}</option>
                                <option value="highest" >{{trans('main.sort_highest')}}</option>
                                <option value="lowest" >{{trans('main.sort_lowest')}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="uk-width-medium-2-10" style="display:none">
                    <label>{{trans('main.Toilets_No')}}</label>
                    <input class="md-input masked_input search_attr" name="toilets_no" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false , 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                </div>

                <!-- Search actions -->
                <div class="uk-wdith-1-1 uk-container-center uk-margin-top">
                    <div class="">

                        <a onclick="filter()" class="md-btn md-btn-primary md-btn-wave-light md-btn-icon uk-margin-small-top"><i class="uk-icon-search"></i>{{trans('main.Search')}}</a>
                        <a onclick="resetSearch()" class="md-btn md-btn-flat md-btn-wave md-btn-icon uk-margin-small-top"><i class="uk-icon-refresh"></i>{{trans('main.Reset')}}</a>
                        <i onclick="getCustomerWizard()" class="uk-hidden-small md-icon material-icons md-24" data-uk-tooltip="{pos:'bottom'}" title="{{trans('main.add_new')}}">person_add</i>
                        <i onclick="advancedFilterToggle()" class="md-icon material-icons md-24" data-uk-tooltip="{pos:'bottom'}" title="{{trans('main.advanced_filter')}}">filter_list</i>

                    </div>
                </div>

            </div>
        </div>
    </form>
</div>
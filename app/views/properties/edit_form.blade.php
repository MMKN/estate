<div class="md-card">
    <div class="md-card-content">
        <!-- wizard_advanced_form -->
        <form class="uk-form-stacked" id="edit_property_form" action="{{ URL::route('properties.edit') }}">
            <div id="wizard_advanced_edit">
                <!-- first section -->
                <h3>{{trans('main.Edit_Property')}}</h3>
                <section>
                    <div class="uk-grid" data-uk-grid-margin>
                        <!-- <div class="uk-width-medium-1-2 parsley-row">
                        <label for="wizard_fullname">Title<span class="req">*</span></label>
                        <input type="text" name="title" required class="md-input" />
                        </div> -->
                        <div class="uk-width-medium-4-10 parsley-row">
                            <label for="wizard_address">{{trans('main.Street_No_landmarks')}}<span class="req">*</span></label>
                            <input type="text" name="address" id="wizard_address" value="{{ $property->address }}" required class="md-input" />
                        </div>
                        <div class="uk-width-medium-2-10 parsley-row">
                            <!--  plot instead of block no.    -->
                            <label for="wizard_plot">{{trans('main.Plot_no')}}<span class="req">*</span></label>
                            <input type="text" name="plot_no" id="wizard_plot" value="{{ $property->plot_no }}" required class="md-input" />
                        </div>
                        <div class="uk-width-medium-2-10 parsley-row">
                            <label>{{trans('main.Apartment_no')}}<span class="req">*</span></label>
                            <input type="text" name="apartment_no" value="{{ $property->apartment_no }}" id="wizard_apartment" required class="md-input">
                        </div>
                        <div class="uk-width-medium-2-10 parsley-row" style="margin-top:18px;">
                            <div class="uk-input-group">
                                <span class="icheck-inline">
                                    <input type="radio" name="purpose" class="wizard-icheck purpose" id="purpose_sale" value="0"  required {{ $property->getPurposeState($property->purpose , 0) }} />
                                    <label class="inline-label" for="purpose_sale">{{trans('main.Sale')}}</label>
                                </span>
                                <span class="icheck-inline">
                                    <input type="radio" name="purpose" id="purpose_rent"  class="wizard-icheck purpose" value="1"  required {{ $property->getPurposeState($property->purpose , 1) }}/>
                                    <label class="inline-label" for="purpose_rent">{{trans('main.Rent')}}</label>
                                </span>
                            </div>
                        </div>
                     </div>

                    <div class="uk-grid" data-uk-grid-margin>

                        <div class="uk-width-medium-2-10 parsley-row">
                            <label>{{trans('main.Price')}}<span class="req">*</span></label>
                            <input class="md-input masked_input" name="price" value="{{ $property->price }}" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false , 'placeholder': '0'" data-inputmask-showmaskonhover="false" required />
                        </div>
                        <div class="uk-width-medium-2-10 parsley-row">
                            <label for="wizard_price">{{trans('main.Area')}}<span class="req">*</span></label>
                            <input class="md-input masked_input" name="area" value="{{ $property->area }}" id="wizard_price" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false,  'placeholder': '0'" data-inputmask-showmaskonhover="false" required />
                        </div>
                        <div class="uk-width-medium-2-10 parsley-row">
                            <label for="wizard_price">{{trans('main.Rooms_No')}}<span class="req">*</span></label>
                            <input class="md-input masked_input" name="rooms_no" id="" value="{{ $property->rooms_no }}" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false,  'placeholder': '0'" data-inputmask-showmaskonhover="false" required />
                        </div>

                        <div class="uk-width-medium-2-10 parsley-row">
                            <label for="wizard_price">{{trans('main.Toilets_No')}}<span class="req">*</span></label>
                            <input class="md-input masked_input" name="toilets_no" value="{{ $property->toilets_no }}" id="wizard_price" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false,  'placeholder': '0'" data-inputmask-showmaskonhover="false" required />
                        </div>
                        
                        <div class="uk-width-medium-2-10">
                            <div class="uk-input-group " style="margin-top:13px;">
                                <input type="checkbox" value="1" data-switchery data-switchery-color="#ffa000" id="switch_demo_success" name="is_compound" {{ $property->getIsCompoundState($property->is_compound) }}/>
                                <label for="switch_demo_success" class="inline-label">{{trans('main.Compound')}}</label>
                            </div>
                        </div>
                    </div>

                    <div class="uk-grid">
                        <div class="uk-width-medium-1-4 ">
                            <div class="parsley-row  uk-margin-small-top">
                                <select id="val_select" required  name="unit_type">
                                    <option value="" disabled selected>{{trans('main.Property_type')}}</option>
                                    <option value="{{ $property->unit_type }}" selected>{{ $property->getUnitType($property->unit_type) }}</option>
                                    @foreach($unit_types as $unit_type)
                                    <option value="{{ $unit_type->id }}">{{ $unit_type->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-4 ">
                            <div class="parsley-row  uk-margin-small-top">
                                <select required  name="city">
                                    <option value="" disabled selected>{{trans('main.City')}}</option>
                                    <option value="{{ $property->city }}" selected>{{ $property->getCity($property->city) }}</option>
                                    @foreach($cities as $city)
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-4">
                            <div class="parsley-row  uk-margin-small-top">
                                <select id="val_select" required  name="neighbour">
                                    <option value="" disabled selected>{{$property->District}}</option>
                                    <option value="{{ $property->neighbour }}" selected>{{ $property->getNeighbour($property->neighbour) }}</option>
                                    @foreach($neighbours as $neighbour)
                                    <option value="{{ $neighbour->id }}">{{ $neighbour->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-4 ">
                            <div class="parsley-row  uk-margin-small-top">
                                <select id="val_select" required  name="floor">
                                    <option value="" disabled selected>{{trans('main.Floor')}}</option>
                                    <option value="{{ $property->floor }}" selected>{{ $property->getFloor($property->floor)->value }}</option>
                                    @foreach($floors as $floor)
                                    <option value="{{ $floor->id }}">{{ $floor->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="uk-grid">
                        <div class="uk-width-medium-1-4 ">
                            <div class="parsley-row  uk-margin-small-top">
                                <select id="val_select" required  name="finishing">
                                    <option value="" disabled selected>{{trans('main.Finishing')}}</option>
                                    <option value="{{ $property->finishing }}" selected>{{ $property->getFinishing($property->finishing) }}</option>
                                    @foreach($finishings as $finishing)
                                    <option value="{{ $finishing->id }}">{{ $finishing->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-4 ">
                            <div class="parsley-row  uk-margin-small-top">
                                <select id="val_select" required  name="payment_style">
                                    <option value="" disabled selected>{{trans('main.Payment_style')}}</option>
                                    <option value="{{ $property->payment_style }}" selected>{{ $property->getPaymentType($property->payment_style) }}</option>
                                    @foreach($payment_types as $payment_type)
                                    <option value="{{ $payment_type->id }}">{{ $payment_type->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-4 ">
                            <div class="parsley-row  uk-margin-small-top">
                                <select id="objective_select" required  name="objective">
                                    <option value="" disabled>{{trans('main.Objective')}}</option>
                                    <option value="{{ $property->objective_id }}" selected>{{ $property->getObjective($property->objective_id) }}</option>
                                    @foreach($objectives as $objective)
                                    <option value="{{ $objective->id }}">{{ $objective->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-4 parsley-row">
                            <div class="parsley-row uk-margin-small-top">
                                <select id="legal" required  name="legal">
                                    <option value="" disabled>{{trans('main.legal')}}</option>
                                    <option value="{{ $property->legal }}" selected>{{ $property->legal }}</option>
                                    @foreach($legals as $key => $value)
                                        <option value="{{ $value }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- <div class="uk-width-medium-1-4 parsley-row">
                            <label for="wizard_price">{{trans('main.Public_services')}}<span class="req">*</span></label>
                            <input class="md-input masked_input" name="distance_facilites" value="{{ $property->distance_facilites }}" id="wizard_price" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false,  'placeholder': '0'" data-inputmask-showmaskonhover="false" required />
                        </div> -->
                        <div class="uk-width-medium-1-4 parsley-row">
                        <div class="parsley-row uk-margin-small-top">
                            <select id="legal" required  name="distance_facilites">
                                <option value="" disabled>{{trans('main.Public_services')}}</option>

                                <option value="{{ $property->distance_facilites }}" selected>{{ $property->distance_facilites }}</option>
                                <option value="لا يوجد">لا يوجد</option>
                                <option value="تقسيط علي ستة أشهر">تقسيط علي ستة أشهر</option>
                                <option value="تقسيط علي سنة">تقسيط علي سنة</option>
                                <option value="تقسيط علي سنة ونصف">تقسيط علي سنة ونصف</option>
                                <option value="تقسيط علي سنتين">تقسيط علي سنتين</option>
                                <option value="تقسيط علي سنتين ونصف">تقسيط علي سنتين ونصف</option>
                                <option value="تقسيط علي ثلاث سنوات">تقسيط علي ثلاث سنوات</option>
                                <option value="تقسيط علي أربع سنوات">تقسيط علي أربع سنوات</option>
                                <option value="تقسيط علي خمس سنوات">تقسيط علي خمس سنوات</option>


                            </select>
                        </div>
                    </div>
                    </div>

                    <div class="uk-grid">
                        <div class="uk-width-4-10">
                            <label>{{trans('main.Description')}}<span class="req">*</span></label>
                            <textarea name="description" id="description" class="md-input">{{ $property->description }}</textarea>
                        </div>
                        <div class="uk-width-medium-4-10 parsley-row">

                        <h3>{{trans('main.Features')}}</h3>

                        <div class="uk-grid" id="features">
                            @foreach($features as $feature)
                            <div class="uk-width-medium-1-2" style="margin: 25px 0px;">
                                <input type="checkbox" id="{{ $feature->value }}_feature" name="features[]" data-md-icheck value="{{ $feature->id }}" {{ $feature->checkIfSelected($feature->id , $property->id) }}/>
                                <label for="{{ $feature->value }}_feature" class="inline-label"> {{ $feature->value }} </label>
                            </div>
                            @endforeach
                        </div>
                         </div>
                         <div class="uk-width-medium-2-10">
                             <div class="uk-input-group " style="margin-top:13px;">
                                 <input type="checkbox" value="1" data-switchery data-switchery-color="#ffa000" id="is_published" name="is_published" {{ $property->getIsPublishedState($property->is_published) }}/>
                                 <label for="is_published" class="inline-label">{{trans('main.Published')}}</label>
                             </div>
                         </div>
                         <div class="uk-width-medium-2-10">
                             <div class="uk-input-group " style="margin-top:13px;">
                                 <input type="checkbox" value="1" data-switchery data-switchery-color="#ffa000" id="show_map" name="show_map" {{ $property->show_map == '1'?"checked":""}}/>
                                 <label for="show_map" class="inline-label">{{trans('main.show_map')}}</label>
                             </div>
                         </div>
                    </div>



                <div class="uk-grid">

                    <div class="uk-width-medium-1-1">
                        <h3>{{trans('main.Location')}}</h3>
                        <div id="map_edit" style="width:100%; height:350px;"></div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2 uk-margin-medium-top">
                                <label class="req">{{trans('main.Latitude')}}<span>*</span></label>
                                <input type="text" value="{{ $property->latitude }}" class="md-input" name="latitude">


                            </div>
                            <div class="uk-width-medium-1-2 uk-margin-medium-top">
                                <label class="req">{{trans('main.Longitude')}}<span>*</span></label>
                                <input type="text" value="{{ $property->longitude }}" class="md-input" name="longitude">


                            </div>
                        </div>
                    </div>
                </div>

            </section>

            <!-- second section -->
            <h3>{{trans('main.Photos')}}</h3>
            <section>
                <div class="uk-grid uk-margin-large-bottom" data-uk-grid-margin>
                    <div class="uk-width-1-1">
                        <div id="file_upload-drop" class="uk-file-upload">
                            <a class="uk-form-file md-btn" onclick="onApiLoad()">{{trans('main.Choose_file')}}</a>
                        </div>
                    </div>
                </div>

                <div id="result" class="uk-grid" style="margin-top:20px;" data-uk-grid-margin>

                    @foreach($images as $image)
                    <div class="uk-width-1-4 image_container">
                        <div class="md-card">
                            <div class="md-card-head head_background" style="background-image: url(https://drive.google.com/thumbnail?authuser=0&sz=w320&id={{ $image->url }})">
                                <div class="ytp-gradient-top"></div>
                                <div class="md-card-head-menu">
                                    <i class="md-icon material-icons md-36 md-light" onclick="remove_image(this)">clear</i>
                                </div>
                            </div>
                            <div class="md-card-content">
                                <p>
                                    <input type="hidden" name="images[]" value="{{ $image->url }}" />
                                    <input id ="{{ $image->id }}" type="radio" name="google_image" value="{{ $image->url }}" data-md-icheck {{ $image->getState($image->id) }} />
                                    <label for="{{ $image->id }}" class="inline-label">{{trans('main.main_image')}}</label>
                                </p>
                                <p>
                                    <input type="hidden" name="published[]" value="0" />
                                    <input id="google_image_{{$image->id}}" type="checkbox" name="published[]" value="1" {{ $image->getState($image->id) }} data-md-icheck/>
                                    <label for="google_image_{{$image->id}}">{{trans('main.Published')}}</label>
                                </p>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </section>

            <!-- third section -->
            <h3>{{trans('main.Personal_information')}}</h3>
            <section>
                <div class="md-card-content ">
                    <div class="uk-grid" data-uk-grid-margin>

                        <div class="uk-width-medium-1-2">
                            <div class="parsley-row">
                                <div class="uk-input-group">
                                    <span class="uk-input-group-addon">
                                        <i class="md-list-addon-icon material-icons md-36">&#xE853;</i>
                                    </span>
                                    <label for="fullname">{{trans('main.Full_Name')}}<span class="req">*</span></label>
                                    <input type="text" name="name" required class="md-input" value="{{ $owner->name }}" />
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-2">
                            <div class="parsley-row">
                                <div class="uk-input-group">
                                    <span class="uk-input-group-addon">

                                        <i class="md-list-addon-icon material-icons md-36">&#xE158;</i>
                                    </span>
                                    <label for="email">{{trans('main.Email')}}<span class="req">*</span></label>
                                    <input type="email" name="email" data-parsley-trigger="change" value="{{ $owner->email }}"  class="md-input" />
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="uk-grid" data-uk-grid-margin>

                        <div class="uk-width-medium-1-2">
                            <div>
                                <div class="uk-input-group">
                                    <span class="uk-input-group-addon">
                                        <i class="md-list-addon-icon material-icons md-36">&#xE0B0;</i>
                                    </span>
                                    <label>{{trans('main.Mobile_main')}}</label>
                                    <input type="text" class="md-input masked_input" value="{{ $owner->mobile1 }}"  name="mobile" value="" data-inputmask="'mask': '999 - 999 99 999'" data-inputmask-showmaskonhover="false" required />
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-2">
                            <div>
                                <div class="uk-input-group">
                                    <span class="uk-input-group-addon">
                                        <i class="md-list-addon-icon material-icons md-36">&#xE0B0;</i>
                                    </span>
                                    <label>{{trans('main.Other_phones')}}</label>
                                    <input type="text" class="md-input" value="{{ $owner->landline }}" name="landline" value="" />
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="uk-grid" data-uk-grid-margin>

                        <div class="uk-width-medium-2-3">
                            <div class="parsley-row">
                                <label for="message">{{trans('main.Description')}}</label>
                                <textarea class="md-input" name="notes" cols="10" rows="6" data-parsley-trigger="keyup" data-parsley-maxlength="500" data-parsley-validation-threshold="10" data-parsley-minlength-message = "Come on! You need to enter at least a 20 caracters long comment.."> {{ $owner->notes }} </textarea>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-3">
                            <div class="parsley-row uk-margin-top" >
                                {{ Form::select('heard' , [ 'olx' => 'Olx' , 'facebook' => 'Facebook' , 'mouth' => 'Word Of Mouth' , 'other' => 'Other' ] , $property->heard  , ['required']) }}
                            </div>
                        </div>

                    </div>
                </section>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
function initMapEdit(){
    var map_edit = new google.maps.Map(document.getElementById('map_edit'), {
        zoom: 12,
        center: {lat: {{ $property->checkLat($property->id) }}, lng: {{ $property->checkLong($property->id) }}}
    });
    marker = new google.maps.Marker({
        map: map_edit,
        draggable: true,
        animation: google.maps.Animation.DROP,
        position: {lat: {{ $property->checkLat($property->id) }}, lng: {{ $property->checkLong($property->id) }}}
    });
    marker.addListener('dragend', toggleBounce);
}
</script>
<!-- <script async defer src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDZU-S9mUwRSmHBdbNiB9-3wITNXciVvcU&signed_in=true&callback=initMapEdit"></script> -->

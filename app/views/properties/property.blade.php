<!-- main content begin -->
<section class="uk-width-10-10 uk-margin-bottom">
    <div class="md-card">
        <!-- Property heading -->
        <div class="user_heading" data-uk-sticky="{top:48, media: 960, boundary: true,showup:true,animation: 'uk-animation-slide-top'}" >
            
            <div class="user_heading_avatar" style="margin-top:13px">
                <i class="material-icons md-48 md-light uk-text-middle"></i>
            </div>
            <div class="user_heading_content " >
                <h2 class="heading_b uk-margin-bottom uk-float-left ">
                <span class="uk-text-truncate">{{ $property->getUnitType($property->unit_type) }}</span>
                <span class="sub-heading"> {{ $property->key }}  </span>
                </h2>
                @if($property->trashed())
                <button class="md-btn md-btn-warning uk-float-right" onclick="unarchiveProperty()">{{trans('main.unarchieve')}}</button>
                @endif
                <ul class="user_stats uk-float-right">
                    <li>
                        <a href="#new_issue" data-uk-modal="{ center:true }" class="md-btn md-btn-flat md-btn-wave-light md-btn-icon" style="color:#fff;"> <i class="uk-icon-bell" style="color:#fff" ></i>{{trans('main.add_task')}}</a>
                    </li>
                </ul>
            </div>
            <div class="md-fab-wrapper uk-hidden-small">
                <div class="md-fab md-fab-toolbar md-fab-small md-fab-accent md-fab-wave-light">
                    <i class="material-icons">more_horiz</i>
                    <div class="md-fab-toolbar-actions"  id="actions">
                        <button onclick="propertyCloneWizard({{ $property->id }})" type="submit" id="clone_property" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" title="Clone"><i class="material-icons md-color-white">collections</i></button>
                        <button onclick="propertyEditWizard({{ $property->id }})" type="submit" id="edit_property" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" title="{{trans('main.Edit')}}"><i class="material-icons md-color-white">edit</i></button>
                        <button onclick="UIkit.modal.confirm('Are you sure?', function(){ deleteProperty({{$property->id}})  });" type="submit" id="archieve_property" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" title="{{trans('main.Delete')}}"><i class="material-icons md-color-white">&#xE872;</i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- user Content -->
        <div class="user_content">
            <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'scale'}">
                <li class="uk-active"><a href="#">{{trans('main.Basic_Info')}}</a></li>
                <li><a href="#" onclick="initializeMap()">{{trans('main.map')}}</a></li>
                <li> <a href="#">{{trans('main.customers')}}</a> </li>
                <li><a href="#">{{trans('main.tasks')}}</a></li>
                <li><a href="#">{{trans('main.Timeline')}}</a></li>
                <li><a href="#">{{trans('main.Other_Informations')}}</a></li>
            </ul>
            <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
                <!-- Suitable content -->
                <li>
                    <!-- property images -->
                    <h4 class="full_width_in_card heading_c">{{trans('main.Images')}}</h4>
                    <div class="uk-grid">
                        @foreach($images as $image)
                        <div class="uk-width-medium-1-3 uk-margin-medium-bottom" >
                            <a href="http://drive.google.com/uc?export=view&id={{ $image->url }}" data-lightbox="image-1">
                                <img src="https://drive.google.com/thumbnail?authuser=0&sz=w320&id={{ $image->url }}" alt=""/>
                            </a>
                        </div>
                        @endforeach
                    </div>
                    <div class="uk-grid uk-margin-small-top uk-margin-large-bottom" data-uk-grid-margin>
                        <!-- Property information -->
                        <div class="uk-width-medium-1-1">
                            <h4 class="full_width_in_card heading_c">{{trans('main.Property_preferences')}}</h4>
                            <div class="uk-grid uk-grid-divider">
                                <!-- First Column -->
                                <div class="uk-width-large-1-3 uk-width-medium-1-3">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">{{trans('main.Purpose')}}</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle">{{ $property->getPurpose($property->purpose) }}</span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">{{trans('main.City')}}</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle">{{ $property->getCity($property->city) }}</span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">{{trans('main.District')}}</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle">{{ $property->getNeighbour($property->neighbour) }}</span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">{{trans('main.Compound')}}</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle">{{ $property->isCompound($property->is_compound) }}</span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">
                                    <p>
                                        <span class="uk-text-muted uk-text-large uk-display-block uk-margin-small-bottom">{{trans('main.Features')}}</span>
                                        @foreach($property->getFeatures($property->id) as $feature)
                                        <span class="uk-badge uk-badge-success"> {{ $feature }} </span>
                                        @endforeach
                                    </p>
                                    <hr class="uk-grid-divider">
                                    <p>
                                        <span class="uk-text-muted uk-text-small uk-display-block uk-margin-small-bottom">{{trans('main.Description')}}</span> {{ $property->description }}
                                    </p>
                                    <hr class="uk-grid-divider uk-hidden-large">
                                </div>
                                <!-- Second section -->
                                <div class="uk-width-large-1-3 uk-width-medium-1-3">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">{{trans('main.Property_type')}}</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle">{{ $property->getUnitType($property->unit_type) }}</span>
                                        </div>
                                    </div>
                                    
                                    <hr class="uk-grid-divider">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">{{trans('main.Floor')}}</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle">{{ $property->getFloor($property->floor)->value }}</span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">{{trans('main.Finishing')}}</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle">{{ $property->getFinishing($property->finishing) }}</span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">{{trans('main.Objective')}}</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle"> {{ $property->getObjective($property->objective_id) }} </span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">{{trans('main.Payment_style')}}</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle">{{ $property->getPaymentType($property->payment_style) }}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- Third column  -->
                                <div class="uk-width-large-1-3 uk-width-medium-1-3">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">{{trans('main.Price')}}</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle">{{ $property->price }} &#163;</span>
                                        </div>
                                    </div>
                                    
                                    
                                    <hr class="uk-grid-divider">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">{{trans('main.Area')}}</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle">{{ $property->area }} m</span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">{{trans('main.Rooms_No')}}</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle">{{ $property->rooms_no }}</span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">{{trans('main.Toilets_No')}}</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle">{{ $property->toilets_no }}</span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">{{trans('main.Distance_Facilites')}}</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle">{{ $property->distance_facilites }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Property Location -->
                        
                    </div>
                </li>
                <!-- map location -->
                <li>
                    @if($property->checkLat($property->id) != 29.95869952693869)
                    <!-- Property Location -->
                    <div class="uk-width-medium-1-1">
                        <h4 class="full_width_in_card heading_c">{{trans('main.Location')}}</h4>
                        <div id="propertyMap" style="width:100%; height:450px;"></div>
                    </div>
                    @endif
                </li>

                <!-- customers -->
                <li>
                    <h3 class="heading_c uk-overlay-background uk-margin-large-top uk-margin-large uk-margin-bottom" style="padding:0.6em;">{{trans('main.Matching_Customers')}}</h3>
                    @if(count($exactCustomers))
                    <ul class="uk-grid uk-width-medium-1-1 uk-text-center md-list md-list-addon uk-margin-bottom">
                        @foreach($exactCustomers as $customer)
                            @include('customers.customer_card_blank')
                        @endforeach
                    </ul>
                    @else
                    <p class="uk-margin-left uk-text-muted"><i class="material-icons md-24 uk-text-warning uk-margin-large-bottom">warning</i> {{trans('main.no_match')}}</p>
                    @endif
                    <h3 class="heading_c uk-overlay-background uk-margin-large uk-margin-bottom uk-margin-top" style="padding:0.6em;">{{trans('main.Similar_Customers')}}</h3>
                    @if(count($suggestedCustomers))
                    <ul class="uk-grid uk-width-medium-1-1 uk-text-center md-list md-list-addon uk-margin-bottom">
                        @foreach($suggestedCustomers as $customer)
                            @include('customers.customer_card_blank')
                        @endforeach
                    </ul>
                    @else
                    <p class="uk-margin-left uk-text-muted"><i class="material-icons md-24 uk-text-warning uk-margin-large-bottom">warning</i> {{trans('main.no_match')}}</p>
                    @endif
                    <h3 class="heading_c uk-overlay-background uk-margin-large uk-margin-bottom uk-margin-top" style="padding:0.6em;">{{trans('main.Price_Customers')}}</h3>
                    @if(count($priceCustomers))
                    <ul class="uk-grid uk-width-medium-1-1 uk-text-center md-list md-list-addon uk-margin-bottom">
                        @foreach($priceCustomers as $customer)
                            @include('customers.customer_card_blank')
                        @endforeach
                    </ul>
                    @else
                    <p class="uk-margin-left uk-text-muted"><i class="material-icons md-24 uk-text-warning">warning</i> {{trans('main.no_match')}}</p>
                    @endif
                </li>

                <!-- Tasks -->
                <li class="uk-margin-small-top">
                    <div class="">
                    @if(count($notifications))
                        <table class="uk-table" id="res_table">
                            <thead>
                                <tr>
                                    <th class="uk-text-center">{{trans('main.Description')}}</th>
                                    <th class="uk-text-center">{{trans('main.Type')}}</th>
                                    <th class="uk-text-center">{{trans('main.Created')}}</th>
                                    <th class="uk-text-center">{{trans('main.Status')}}</th>
                                </tr>
                            </thead>
                            <tbody id="table_results">
                                @foreach($notifications as $notification)
                                <tr>
                                    <td class="uk-text-center">
                                        <a href="{{URL::to('/notifications') . '/details-page/'.$notification->id}}" target="_blank" class="notification_link">
                                            {{ mb_strimwidth($notification->description , 0, 30, "...") }}
                                        </a>
                                    </td>
                                    <td class="uk-text-center">
                                        {{ $notification->getType($notification->type) }}
                                    </td>
                                    <td class="uk-text-small uk-text-center">
                                        {{ date("j M Y" , strtotime($notification->created_at)) }}
                                    </td>
                                    <td class="uk-text-center">
                                        {{ $notification->getStatus($notification->status) }}
                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    @else
                    <h3 class="uk-text-muted uk-margin-large-top uk-text-center"> {{trans('main.no_tasks_found')}} </h3>
                    @endif
                    </div>    
                </li>
                   
                    
                <li>
                    <div>
                        <div class="uk-input-group">
                                <div class="md-input-wrapper">
                                    <input type="text" class="md-input" name="submit_message" id="submit_message" placeholder="{{trans('main.write_message')}}">
                                    <span class="md-input-bar"></span>
                                </div>
                                <span class="uk-input-group-addon">
                                    <a onclick="addTimeline()" class="md-btn md-btn-flat md-btn-wave md-btn-icon">
                                        <i class="uk-icon-plus"></i>
                                        Add note
                                    </a>
                                </span>
                                                         
                        </div>
                        <div class="timeline">
                            @foreach($messages as $message)
                            <div class="timeline_item" id="entry_{{$message->id}}">
                                <a class="timeline_icon timeline_icon_danger" onclick="msg_id = {{$message->id}} ;$('#edit_message').val('{{$message->message}}');" data-uk-modal="{target:'#modal-timeline'}"><i class="material-icons md-36">cancel</i></a>
                                <div class="timeline_date">
                                    {{date('d' , strtotime($message->created_at))}} <span>{{date('M' , strtotime($message->created_at))}}</span>
                                </div>
                                <div class="timeline_content" id="msg_{{$message->id}}">
                                    {{$message->message}}
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </li>
                <!-- Owner Info -->
                <li>
                    <div class="uk-grid uk-margin-small-top uk-margin-large-bottom" data-uk-grid-margin>
                        <!-- Contact information -->
                        <div class="uk-width-medium-1-2">
                            <h4 class="full_width_in_card heading_c">{{trans('main.Owner_Info')}}</h4>
                            <ul class="md-list md-list-addon">
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons">&#xE87C;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">{{ $property->getOwner($property->owner_id)->name }}</span>
                                        <span class="uk-text-small uk-text-muted">{{trans('main.Full_Name')}}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons">&#xE158;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">{{ $property->getOwner($property->owner_id)->email }}</span>
                                        <span class="uk-text-small uk-text-muted">{{trans('main.Email')}}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">{{ $property->getOwner($property->owner_id)->mobile1 }}</span>
                                        <span class="uk-text-small uk-text-muted">{{trans('main.Mobile_main')}}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">{{ $property->getOwner($property->owner_id)->landline }}</span>
                                        <span class="uk-text-small uk-text-muted">{{trans('main.Other_phones')}}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon uk-icon-calendar-plus-o"></i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">{{ $property->getOwner($property->owner_id)->created_at }}</span>
                                        <span class="uk-text-small uk-text-muted">{{trans('main.Date_added')}}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon uk-icon-commenting"></i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">{{ $property->heard }}</span>
                                        <span class="uk-text-small uk-text-muted">{{trans('main.Heard_us_by')}}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons">note</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">{{ $property->getOwner($property->owner_id)->notes }}</span>
                                        <span class="uk-text-small uk-text-muted">{{trans('main.Customers_Notices')}}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons">&#xE84F;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">{{ $property->legal }}</span>
                                        <span class="uk-text-small uk-text-muted">{{trans('main.legal_papers')}}</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- Contact information -->
                        <div class="uk-width-medium-1-2">
                            <h4 class="full_width_in_card heading_c">{{trans('main.property_address')}}</h4>
                            <ul class="md-list md-list-addon">
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons">&#xE84F;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">{{ $property->address }}</span>
                                        <span class="uk-text-small uk-text-muted">{{trans('main.Street_No_landmarks')}}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons">&#xE0AF;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">{{ $property->plot_no }}</span>
                                        <span class="uk-text-small uk-text-muted">{{trans('main.Plot_no')}}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons">grade</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">{{ $property->getFloor($property->floor)->value }}</span>
                                        <span class="uk-text-small uk-text-muted">{{trans('main.Floor')}}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons">list</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">{{ $property->apartment_no }}</span>
                                        <span class="uk-text-small uk-text-muted">{{trans('main.Apartment_no')}}</span>
                                    </div>
                                </li>
                                
                                <!-- <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons">&#xE1B3;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">Piece 12</span>
                                        <span class="uk-text-small uk-text-muted">Piece</span>
                                    </div>
                                </li> -->
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons">&#xE0C8;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">{{ $property->getNeighbour($property->neighbour) }}</span>
                                        <span class="uk-text-small uk-text-muted">{{trans('main.District')}}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons">&#xE87A;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">{{ $property->getCity($property->city) }}</span>
                                        <span class="uk-text-small uk-text-muted">{{trans('main.City')}}</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    <!-- user Content end -->
    </div>
</section>

<!-- Go back -->
<!-- Go back from peroperty to home view -->
<div onclick="backFromProperty()" id="call_to_action" class="md-fab-wrapper">
    <a class="md-fab  md-fab-accent md-fab-wave-light"><i class="material-icons">arrow_back</i></a>
</div>


<!-- main content end -->
<script>
function initializeMap(){
    setTimeout(function(){
        initPropertyMap();
    },1000);
}
var marker;
function initPropertyMap(){
var map = new google.maps.Map(document.getElementById('propertyMap'), {
zoom: 12,
center: {lat: {{ $property->checkLat($property->id) }} , lng: {{ $property->checkLong($property->id) }} }
});
marker = new google.maps.Marker({
map: map,
draggable: false,
animation: google.maps.Animation.DROP,
position: {lat: {{ $property->checkLat($property->id) }} , lng: {{ $property->checkLong($property->id) }} }
});
marker.addListener('dragend', toggleBounce);
setTimeout(function() {
    google.maps.event.trigger(map, "resize");
    $(window).trigger('resize');
}, 500);
}
</script>
<div class="md-card">
    <div class="md-card-content">
        <!-- wizard_advanced_form -->
        <form class="uk-form-stacked" id="add_property_form" action="{{ URL::route('properties.add') }}">
            <div id="wizard_advanced">
                <!-- first section -->
                <h3>{{trans('main.Add_New_Property')}}</h3>
                <section>
                    <div class="uk-grid" data-uk-grid-margin>
                        <!-- <div class="uk-width-medium-1-2 parsley-row">
                            <label for="wizard_fullname">Title<span class="req">*</span></label>
                            <input type="text" name="title" required class="md-input" />
                        </div> -->
                        <div class="uk-width-medium-4-10 parsley-row">
                            <label for="wizard_address">{{trans('main.Street_No_landmarks')}}<span class="req">*</span></label>
                            <input type="text" name="address" id="wizard_address" required class="md-input" />
                        </div>
                        <div class="uk-width-medium-2-10 parsley-row">
                            <!--  plot instead of block no.    -->
                            <label for="wizard_plot">{{trans('main.Plot_no')}}<span class="req">*</span></label>
                            <input type="text" name="plot_no" id="wizard_plot" required class="md-input" />
                        </div>
                        <div class="uk-width-medium-2-10 parsley-row">
                            <!-- apartment no instead of unit number -->
                            <label>{{trans('main.Apartment_no')}}<span class="req">*</span></label>
                            <input type="text" name="apartment_no" id="wizard_apartment" required class="md-input">
                        </div>
                        
                        <div class="uk-width-medium-2-10 parsley-row" style="margin-top:18px;">
                            <div class="uk-input-group">
                                <span class="icheck-inline">
                                    <input type="radio" name="purpose" class="wizard-icheck ichk purpose" id="purpose_sale" value="0"  required checked/>
                                    <label class="inline-label" for="purpose_sale">{{trans('main.Sale')}}</label>
                                </span>
                                <span class="icheck-inline">
                                    <input type="radio" name="purpose" id="purpose_rent"  class="wizard-icheck ichk purpose" value="1"  required/>
                                    <label class="inline-label" for="purpose_rent">{{trans('main.Rent')}}</label>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-2-10 parsley-row">
                            <label>{{trans('main.Price')}}<span class="req">*</span></label>
                            <input class="md-input masked_input" name="price" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false , 'placeholder': '0'" data-inputmask-showmaskonhover="false" required />
                        </div>
                        <div class="uk-width-medium-2-10 parsley-row">
                            <label for="wizard_price">{{trans('main.Area')}}<span class="req">*</span></label>
                            <input class="md-input masked_input" name="area" id="wizard_price" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': '', 'autoGroup': true, 'digits': 0, 'digitsOptional': false,  'placeholder': '0'" data-inputmask-showmaskonhover="false" required />
                        </div>
                        <div class="uk-width-medium-2-10 parsley-row">
                            <label for="wizard_price">{{trans('main.Rooms_No')}}<span class="req">*</span></label>
                            <input class="md-input masked_input" name="rooms_no" id="" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false,  'placeholder': '0'" data-inputmask-showmaskonhover="false" required />
                        </div>
                        <div class="uk-width-medium-2-10 parsley-row">
                            <label for="wizard_price">{{trans('main.Toilets_No')}}<span class="req">*</span></label>
                            <input class="md-input masked_input" name="toilets_no" id="wizard_price" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false,  'placeholder': '0'" data-inputmask-showmaskonhover="false" required />
                        </div>
                        
                        <div class="uk-width-medium-2-10">
                            <div class="uk-input-group " style="margin-top:13px;">
                                <input type="checkbox" value="1" data-switchery data-switchery-color="#ffa000" id="is_compound_add" name="is_compound"/>
                                <label for="is_compound_add" class="inline-label">{{trans('main.Compound')}}</label>
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-4 ">
                            <div class="parsley-row  uk-margin-small-top">
                                <select id="unit_type_select" required  name="unit_type">
                                    <option value="" disabled selected>{{trans('main.Property_type')}}</option>
                                    @foreach($unit_types as $unit_type)
                                    <option value="{{ $unit_type->id }}">{{ $unit_type->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-4 ">
                            <div class="parsley-row  uk-margin-small-top">
                                <select required  name="city" id="city_add">
                                    <option value="" disabled selected>{{trans('main.City')}}</option>
                                    @foreach($cities as $city)
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-4" id="neighbour_add">
                            <div class="parsley-row  uk-margin-small-top">
                                <select id="val_select" required  name="neighbour" disabled="">
                                    <option value="" disabled selected>{{trans('main.District')}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-4 ">
                            <div class="parsley-row  uk-margin-small-top">
                                <select id="floor_select" required  name="floor">
                                    <option value="" disabled selected>{{trans('main.Floor')}}</option>
                                    @foreach($floors as $floor)
                                    <option value="{{ $floor->id }}">{{ $floor->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-4 ">
                            <div class="parsley-row  uk-margin-small-top">
                                <select id="finishing_select" required  name="finishing">
                                    <option value="" disabled selected>{{trans('main.Finishing')}}</option>
                                    @foreach($finishings as $finishing)
                                    <option value="{{ $finishing->id }}">{{ $finishing->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-4 ">
                            <div class="parsley-row  uk-margin-small-top">
                                <select id="objective_select" required  name="objective">
                                    <option value="" disabled selected>{{trans('main.Objective')}}</option>
                                    @foreach($objectives as $objective)
                                    <option value="{{ $objective->id }}">{{ $objective->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-4 parsley-row">
                            <div class="parsley-row uk-margin-small-top">
                                <select id="legal" required  name="legal">
                                    <option value="" disabled selected>{{trans('main.legal')}}</option>
                                    @foreach($legals as $key => $value)
                                    <option value="{{ $value }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-4 ">
                            <div class="parsley-row  uk-margin-small-top">
                                <select id="payment_select" required  name="payment_style">
                                    <option value="" disabled selected>{{trans('main.Payment_style')}}</option>
                                    @foreach($payment_types as $payment_type)
                                    <option value="{{ $payment_type->id }}">{{ $payment_type->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- legal -->
                        <div class="uk-width-medium-1-4 parsley-row">
                            <div class="parsley-row uk-margin-small-top">
                                <select id="legal" required  name="distance_facilites">
                                    <option value="" disabled selected>{{trans('main.Public_services')}}</option>
                                    <option value="لا يوجد">لا يوجد</option>
                                    <option value="تقسيط علي ستة أشهر">تقسيط علي ستة أشهر</option>
                                    <option value="تقسيط علي سنة">تقسيط علي سنة</option>
                                    <option value="تقسيط علي سنة ونصف">تقسيط علي سنة ونصف</option>
                                    <option value="تقسيط علي سنتين">تقسيط علي سنتين</option>
                                    <option value="تقسيط علي سنتين ونصف">تقسيط علي سنتين ونصف</option>
                                    <option value="تقسيط علي ثلاث سنوات">تقسيط علي ثلاث سنوات</option>
                                    <option value="تقسيط علي أربع سنوات">تقسيط علي أربع سنوات</option>
                                    <option value="تقسيط علي خمس سنوات">تقسيط علي خمس سنوات</option>
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2 parsley-row">
                            <label>{{trans('main.Description')}}<span class="req">*</span></label>
                            <textarea name="description" id="description" class="md-input"></textarea>
                        </div>
                        <!-- <div class="uk-width-medium-4-10 parsley-row">
                            <label for="wizard_price">{{trans('main.Public_services')}}<span class="req"></span></label>
                            <input class="md-input masked_input" name="distance_facilites" id="wizard_price" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false,  'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                        </div> -->
                        <!-- features -->
                        <div class="uk-width-medium-6-10 parsley-row">
                            <h3>{{trans('main.Features')}}</h3>
                            <div class="uk-grid" id="features">
                                @foreach($features as $feature)
                                <div class="" style="margin: 25px 0px;">
                                    <input type="checkbox" name="features[]" class="ichk check-feature" data-md-icheck  id="{{ $feature->value }}_feature" value="{{ $feature->id }}"/>
                                    <label for="{{ $feature->value }}_feature" class="inline-label">{{ $feature->value}}</label>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="uk-width-4-10 uk-margin-medium-top">
                            <!-- publish online -->
                            <div class="uk-width-medium-10-10">
                                <div class="uk-input-group " style="margin-top:13px;">
                                    <input type="checkbox" value="1" data-switchery data-switchery-color="#ffa000" checked id="is_published" name="is_published"/>
                                    <label for="is_published" class="inline-label">{{trans('main.Published')}}</label>
                                </div>
                            </div>
                            <!-- publish location -->
                            <div class="uk-width-medium-10-10 uk-margin-top">
                                <div class="uk-input-group " style="margin-top:13px;">
                                    <input type="checkbox" value="1" data-switchery data-switchery-color="#ffa000" id="show_map" name="show_map"/>
                                    <label for="show_map" class="inline-label">{{trans('main.show_map')}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="uk-width-medium-1-1">
                            <h3>{{trans('main.Location')}}</h3>
                            <div id="map" style="width:100%; height:350px;"></div>
                            <!-- <div id="gmap_markers" class="gmap" style="width:100%;height:250px;"></div> -->
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2 uk-margin-medium-top">
                                    <label class="req">{{trans('main.Latitude')}}<span>*</span></label>
                                    <input type="text" class="md-input" name="latitude" readonly="true" >
                                </div>
                                <div class="uk-width-medium-1-2 uk-margin-medium-top">
                                    <label class="req">{{trans('main.Longitude')}}<span>*</span></label>
                                    <input type="text" class="md-input" name="longitude"  readonly="true">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- second section -->
                <h3>{{trans('main.Photos')}}</h3>
                <section>
                    <div class="uk-grid uk-margin-large-bottom" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div id="file_upload-drop" class="uk-file-upload">
                                <a class="uk-form-file md-btn" onclick="onApiLoad()">{{trans('main.Choose_file')}}</a>
                            </div>
                        </div>
                    </div>
                    <div id="result" class="uk-grid" style="margin-top:20px;" data-uk-grid-margin>
                    </div>
                </section>
                <!-- third section -->
                <h3>{{trans('main.Personal_information')}}</h3>
                <section>
                    <button class="md-btn md-btn-flat" id="toggler" onclick="toggleOwner(this)">Existing Owner?</button>
                    <div class="md-card-content" id="newOwner">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <div class="parsley-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="md-list-addon-icon material-icons md-36">&#xE853;</i>
                                        </span>
                                        <label for="fullname">{{trans('main.Full_Name')}}<span class="req">*</span></label>
                                        <input type="text" name="name" required class="md-input" />
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="parsley-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="md-list-addon-icon material-icons md-36">&#xE158;</i>
                                        </span>
                                        <label for="email">{{trans('main.Email')}}<span class="req">*</span></label>
                                        <input type="email" name="email" data-parsley-trigger="change"  class="md-input" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <div>
                                    <div class="uk-input-group parsley-row">
                                        <span class="uk-input-group-addon">
                                            <i class="md-list-addon-icon material-icons md-36">&#xE0B0;</i>
                                        </span>
                                        <label>{{trans('main.Mobile_main')}}</label>
                                        <input type="text" class="md-input masked_input"  name="mobile" value="" data-inputmask="'mask': '999 - 999 99 999'" data-inputmask-showmaskonhover="false" required />
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div>
                                    <div class="uk-input-group parsley-row">
                                        <span class="uk-input-group-addon">
                                            <i class="md-list-addon-icon material-icons md-36">&#xE0B0;</i>
                                        </span>
                                        <label>{{trans('main.Other_phones')}}</label>
                                        <input type="text" class="md-input" name="landline" value=""  />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-2-3">
                                <div class="parsley-row">
                                    <label for="message">{{trans('main.Description')}}</label>
                                    <textarea class="md-input" name="notes" cols="10" rows="6" data-parsley-trigger="keyup" data-parsley-minlength="5" data-parsley-maxlength="500" data-parsley-validation-threshold="10" data-parsley-minlength-message = "Come on! You need to enter at least a 20 caracters long comment.."></textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-3">
                                <div class="parsley-row uk-margin-top" >
                                    {{ Form::select('heard' , [ 'olx' => 'OLX' , 'facebook' => 'Facebook' , 'mouth' => 'Word of mouth' , 'other' => 'Others' ] , '' , ['required']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="md-card-content" id="pickOwner" style="display:none;">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <div class="parsley-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="md-list-addon-icon material-icons md-36">&#xE853;</i>
                                        </span>
                                        <select id="exist_owner" name="exist_owner">
                                            <option value="" disabled selected>Select Owner</option>
                                            @if(isset($owners))
                                            @foreach($owners as $owner)
                                            <option value="{{$owner->id}}">{{$owner->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </form>
    </div>
</div>
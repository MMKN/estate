<div class="uk-margin-medium-bottom">
    <div onclick="getProperty({{ $property->id }})" class="md-card md-card-hover" style="cursor:pointer;" id="{{ $property->id }}">
        <div class="md-card-head head_background" style="box-shadow: rgba(0,0,0,0.1) 0px 40px 20px inset; background-image: url('{{ $property->getMainImage($property->id) }}')">
            <h2 class="md-card-head-text" style="text-shadow: 2px 2px 5px #000; font-size:22px">
            {{ $property->getUnitType($property->unit_type) }}
            </h2>
            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-bottom-right uk-margin-bottom uk-margin-top" style="font-size:18px;">{{$property->price}} &#163;</div>
        </div>
        <div class="md-card-content">
            <ul class="md-list">
                <li>
                    <div class="md-list-content">
                        <span class="md-list-heading">{{$property->getCity($property->city)}} </span>
                        <span class="uk-text-small uk-text-muted">{{$property->getNeighbour($property->neighbour)}} </span>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
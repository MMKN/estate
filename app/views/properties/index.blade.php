@extends('_layouts.core')
@section('archieved')
<div id="menu_top_dropdown" class="uk-float-right uk-hidden-small">
    <div class="uk-button-dropdown" data-uk-dropdown="">
        <a href="#" class="top_menu_toggle"><i class="md-icon material-icons md-24 md-light">&#xE5D4;</i></a>
        <div class="uk-dropdown uk-dropdown-small uk-dropdown-flip">
            <ul class="uk-nav">
                <li><a onclick="getArchivedProperties()" class="uk-text-center uk-dropdown-close" class="">{{trans('main.Archieved_properties')}}</a></li>
            </ul>
        </div>
    </div>
</div>

@stop
@section('content')
<div id="main_index">

    <!-- main view begin -->
    <article id="main" class="hierarchical_show">
        <!-- Filter and search begin -->
        @include('properties.filter')
        <!-- Properties Grid begin  -->
        <div id="view_box" class="uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-4 uk-grid-width-large-1-5 uk-text-center">
            @if(count($properties))
            
            @foreach($properties as $property)
                @include('properties.property_card')
            @endforeach
            
            @else
            <div class="uk-width-medium-1-1">
                <h3 style="background-color: #fff; padding: 0.6em;">{{trans('main.No_Properies_Available')}}</h3>
                <div class="full_width_in_card heading_c" style="float:left;">
                </div>
            </div>
            @endif
        </div>
        <!-- pagination -->
        <div class="uk-grid">
            <div class="uk-width-medium-1-1 pager">
                {{$properties->links()}}
            </div>
        </div>
        <!-- add new call to action-->
        <div onclick="addPropertyWizard()" id="addNew" class="md-fab-wrapper uk-hidden-small" {{ isset($page) ? 'style="display:none;"' : '' }}>
            <a class="md-fab md-fab-accent md-fab-wave-light" >
                <i class="material-icons">&#xE145;</i>
            </a>
        </div>
    </article>

    <!-- archived properties view -->
    <article id="archivedView" style="display:none;">
        <!-- Archived view -->
        <div class="md-card" id="archived_title">
            <div class="md-card-content uk-grid uk-text-center uk-margin-bottom">
                <h3 class="uk-width-1-1 heading_b uk-text-center"><i class="md-icon material-icons uk-margin-right">home</i>{{trans('main.Archieved_properties')}}</h3>
            </div>
        </div>
        <div id="archived" class="uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-4 uk-grid-width-large-1-5 uk-text-center">
        </div>
        <!-- Go back to home -->
        <div onclick="goHome(); refreshHome(); deleted = 0;state = 'properties.main'; $('#archived').children().remove();" class="md-fab-wrapper">
            <a class="md-fab md-fab-accent md-fab-wave-light">
                <i class="material-icons">arrow_back</i>
            </a>
        </div>
    </article>

    <!-- property view Box -->
    <article id="profile_box" class="uk-grid" style="display:none;">
    </article>

    <!-- here goes any wizard (Add / edit / clone) -->
    <article id="wizardView" style="display:none;">
        <div id="wizard" class=" uk-width-medium-8-10 uk-container-center" >
        </div>
        <!-- Go back to home -->
        <div onclick="goHome(); $('#wizard').children().remove();" class="md-fab-wrapper">
            <a class="md-fab md-fab-accent md-fab-wave-light">
                <i class="material-icons">arrow_back</i>
            </a>
        </div>
    </article>


    <article id="addNew_form" class="uk-width-medium-8-10 uk-container-center" style="display:none;">
    </article>
    <article id="edit_box" class=" uk-width-medium-8-10 uk-container-center" style="display:none;">
    </article>
    <article id="add_customer_box" class="uk-width-medium-8-10 uk-container-center add-box" style="display:none">
    </article>
    
</div>

@include('general.add_notice')
<!-- Properties Scripts -->
@section('scripts')

<script src="{{ URL::asset('js/general_handler.js') }}"></script>
<script src="{{ URL::asset('js/properties/property_handler.js') }}"></script>
<script src="{{ URL::asset('js/properties/routing.js') }}"></script>
<script type="text/javascript">
var base_url = "{{ URL::route('properties.index') }}";
var property_id;
</script>


@stop
<!-- Properties Scripts -->
@stop
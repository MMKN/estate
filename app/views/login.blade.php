<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">
    <title>{{trans('main.project_name')}}</title>
    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>
    <!-- uikit -->
    <link rel="stylesheet" href="{{ URL::asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}">
    <!-- altair admin -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/main.min.css') }}" media="all">
    <!-- altair admin login page -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/login_page.min.css') }}" >
    <link rel="stylesheet" href="assets/css/themes/custom_my_theme.css" media="all">

</head>
<body class="login_page">
    <div class="login_page_wrapper">
        <div class="md-card" id="login_card">
            <div class="md-card-content large-padding">
                <div class="login_heading">
                <img class="md-user-image" style="width:50%" src="assets/img/favicon.png">
                </div>
                <h3 id="greetingUser" class="uk-text-center" style="display:none;">You are awesome :))</h3>
                <form id="login_form">
                    <div class="uk-form-row">
                        <label for="login_username">{{trans('main.Email')}}</label>
                        <input class="md-input" type="email" name="email" />
                    </div>
                    <div class="uk-form-row">
                        <label for="login_password">{{trans('main.Password')}}</label>
                        <input class="md-input" type="password" name="password" />
                    </div>
                    <div class="uk-margin-medium-top">
                        <a type="submit" id="sign_in" class="md-btn md-btn-flat md-btn-block">{{trans('main.Sign_In')}}</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="uk-notify uk-notify-bottom-center" style="display: none;" id="notification_area">

    </div>

    <!-- common functions -->
    <script src="{{ URL::asset('assets/js/common.min.js') }}"></script>
    <!-- uikit functions -->
    <script src="{{ URL::asset('assets/js/uikit_custom.min.js') }}"></script>
    <!-- altair common functions/helpers -->
    <script src="{{ URL::asset('assets/js/altair_admin_common.js') }}"></script>
    <!-- altair login page functions -->
    <script src="{{ URL::asset('assets/js/pages/login.min.js') }}"></script>
    <script src="{{ URL::asset('js/login.js') }}"></script>

    <script type="text/javascript">
        var base_url = "{{ URL::route('postLogin') }}";
        $('.md-input').keypress(function(e) {
            var key = e.which || e.keyCode;
            if(key == 13){
                sign_in($('#sign_in'));  
            }
        });
    </script>
</body>
</html>

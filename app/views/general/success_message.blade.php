<div class="uk-notify-message uk-notify-message-success" style="opacity: 1; margin-top: 0px; margin-bottom: 10px;">
    <a class="uk-close"></a>
    <div style="text-align:center;">
        {{ $message }}
    </div>
</div>

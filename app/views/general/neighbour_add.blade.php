<div class="parsley-row uk-margin-small-top">
    <select id="neighbour_select" data-md-selectize data-md-selectize-bottom required  name="neighbour">
        <option value="" disabled selected>District</option>
        @foreach($neighbours as $neighbour)
        <option value="{{ $neighbour->id }}">{{ $neighbour->name }}</option>
        @endforeach
    </select>
</div>

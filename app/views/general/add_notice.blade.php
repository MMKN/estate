<div class="uk-modal" id="new_issue">
    <div class="uk-modal-dialog">
        <form onsubmit="return saveNotice()" id="form_validation" class="uk-form-stacked">
            <!-- <div class="uk-margin-medium-bottom">
                <label for="task_title">{{trans('main.Title')}}</label>
                <input type="text" class="md-input" id="task_title" name="title"/>
            </div> -->
            <div class="uk-margin-medium-bottom parsley-row">
                <label for="task_description">{{trans('main.Description')}}</label>
                <textarea autofocus class="md-input" id="task_description" name="body" required></textarea>
            </div>
            <div class="uk-margin-medium-bottom uk-width-1-1 parsley-row">
                <label for="task_assignee" class="uk-form-label">{{trans('main.Assignee')}}</label>
                <select class="uk-form-width-large" name="assignee" id="assignee" multiple required placeholder="{{ trans('main.Add_assignee') }}">
                    <option value="{{Auth::user()->id()}}">{{trans('main.me')}}</option>
                    @foreach($admins as $admin)
                    <option value="{{$admin->id}}">{{$admin->name}}</option>
                    @endforeach
                </select>
            </div>
            
            <!-- <div class="uk-margin-medium-bottom">
                <label for="priority" class="uk-form-label">{{trans('main.Priority')}}</label>
                {{ Form::select('priority' , [0 => 'عالية' , 1 => 'متوسطة' , 2 => 'منخفضة'] , null , ['class' => 'uk-form-width-medium' , 'id' => 'priority' , 'data-md-selectize-inline']) }}
            </div> -->
            
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">{{trans('main.Close')}}</button>
                <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary" id="save_button">{{trans('main.Add')}}</button>
            </div>
        </form>
    </div>
</div>
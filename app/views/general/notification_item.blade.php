@foreach($notifications as $notification)
    <div class="timeline_item ">
        <div class="timeline_icon timeline_icon_success">
            <i class="material-icons">&#xE85D;</i>
        </div>
        <div class="timeline_content {{$notification->read == '0'?'md-bg-light-blue-50':''}}">
            <a  onclick="updateNotificationState(this)" href="{{URL::route('notifications.details-page').'/'.$notification->notice_id}}" target="_blank">{{ $notification->message }}</a>
        </div>
        <div class="timeline_date">
            {{ date("j" , strtotime($notification->created_at)) }} <span> {{ date("M" , strtotime($notification->created_at)) }} </span>
        </div>
    </div>
@endforeach

<select class="search_attr" id="price_to" name="price_to" required data-md-selectize>
      <option value="" disabled selected>{{trans('main.Price_to')}}</option>
      @foreach($prices as $price)
      <option value="{{$price}}">{{$price}}</option>
      @endforeach
</select>
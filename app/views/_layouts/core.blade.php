<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en">
    <!--<![endif]-->
    @include('shared.head')
    
    <body class="app_my_theme">


        <!-- main header -->
        @include('shared.header')
        


        <!-- main content begin -->
        <div id="page_content">
            <main id="page_content_inner">
            <div id="notification_details" style="display:none;"></div>
            @yield('notification_header')
            @yield('content')
            {{$content or ''}}
            </main>
        </div>
        <!-- main content end-->
        
        <!-- notifications begin -->
        @include('shared.notificationsSidebar')



        <!-- notes modal add/edit -->
        <div class="uk-modal" id="modal-timeline">
            <div class="uk-modal-dialog">
                <div class="uk-modal-header">
                </div>
                <input type="text" class="md-input" name="edit_message" id="edit_message">
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" onclick="editMessage()" class="md-btn md-btn-flat md-btn-flat-warning">{{trans('main.Save')}}</button>
                    <button type="button" onclick="deleteMessage()" class="md-btn md-btn-flat md-btn-flat-danger">{{trans('main.Delete')}}</button>
                </div>
            </div>
        </div>
        
        
        <!-- /////////////////////////// -->
        <!-- /////Scripts start ///////-->
        <!-- ////////////////////////// -->
        <!-- common functions -->
        <script src="{{ URL::asset('assets/js/common.min.js') }}"></script>
        <!-- uikit functions -->
        <script src="{{ URL::asset('assets/js/uikit_custom.min.js') }}"></script>
        <!-- altair common functions/helpers -->
        <script src="{{ URL::asset('assets/js/altair_admin_common.min.js') }}"></script>
        <!-- Wizard scripts -->
        <!-- parsley (validation) -->
        <script>
        // load parsley config (altair_admin_common.js)
        altair_forms.parsley_validation_config();
        // load extra validators
        altair_forms.parsley_extra_validators();
        </script>
        <script src="{{ URL::asset('bower_components/parsleyjs/dist/parsley.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/custom/wizard_steps.min.js') }}"></script>
        <script src="{{ URL::asset('js/wizard_init.js') }}"></script>
        
        <!-- ionrangeslider -->
        <script src="{{ URL::asset('bower_components/ion.rangeslider/js/ion.rangeSlider.min.js') }}"></script>
        <!-- inputmask-->
        <script src="{{ URL::asset('bower_components/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/pages/components_notifications.min.js') }}"></script>
        <script async src="{{ URL::asset('bower_components/uikit/js/components/lightbox.min.js') }}"></script>
        <script async src="{{ URL::asset('assets/js/lightbox2-master/src/js/lightbox.js') }}"></script>
        <!--  forms advanced functions -->
        <script src="{{ URL::asset('assets/js/pages/forms_advanced.min.js') }}"></script>
        
        <!-- Notifications scripts -->
        <!-- <script src="https://cdn.socket.io/socket.io-1.4.5.js"></script> -->
        <script src="//js.pusher.com/3.0/pusher.min.js"></script>
        <script  type="text/javascript">
            function updateNotifications() {
                $.ajax({
                    type: 'GET',
                    url: root_url + '/notifications/update-notifications',
                    data: {},
                    success: function(data) {
                        $("#notification_timeline").html(data['notifications']);
                        console.log(data['count']);
                        $("#notifications_count").text(data['count']);
                    }
                });
            };
            // property type input tags field initialization
            $('#assignee').selectize({
                plugins: {
                    'remove_button': {
                        label: ''
                    }
                },
                render: {
                    option: function(data, escape) {
                        return '<div class="option">' +
                            '<span>' + escape(data.title) + '</span>' +
                            '</div>';
                    },
                    item: function(data, escape) {
                        return '<div class="item">' + escape(data.title) + '</div>';
                    }
                },
                maxItems: null,
                valueField: 'value',
                labelField: 'title',
                searchField: 'title',
                create: true,
                onDropdownOpen: function($dropdown) {
                    $dropdown
                        .hide()
                        .velocity('slideDown', {
                            begin: function() {
                                $dropdown.css({
                                    'margin-top': '0'
                                })
                            },
                            duration: 200,
                            easing: easing_swiftOut
                        })
                },
                onDropdownClose: function($dropdown) {
                    $dropdown
                        .show()
                        .velocity('slideUp', {
                            complete: function() {
                                $dropdown.css({
                                    'margin-top': ''
                                })
                            },
                            duration: 200,
                            easing: easing_swiftOut
                        })
                }
            });

            var pusher = new Pusher('740b3e5ad1236ca3a8d8');
            var channel = pusher.subscribe('channel_{{Auth::user()->id()}}');
            channel.bind('event_notify', function(data) {
                document.getElementById('notification_sound').play();
                UIkit.notify({
                    message: data.message,
                    status: 'info',
                    timeout: 5000,
                    pos: 'bottom-left'
                });
                updateNotifications();
            });
        </script>

        <!-- Google stuff scripts -->
        <script async src="https://apis.google.com/js/api.js?onload"></script>
        <script async  src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDZU-S9mUwRSmHBdbNiB9-3wITNXciVvcU&signed_in=true"></script>
        <script async  type="text/javascript">
        var images;
        // The Browser API key obtained from the Google Developers Console.
        var developerKey = 'AIzaSyAbuhP4UMFAxT6I8loeuGh6SMWV2skl3wQ';
        // The Client ID obtained from the Google Developers Console. Replace with your own Client ID.
        var clientId = "970756305068-udi7lcq8b3kcq6r1ugnjlufshtpcrgcl.apps.googleusercontent.com"
            // Scope to use to access user's photos.
        var scope = ['https://www.googleapis.com/auth/drive'];
        var pickerApiLoaded = false;
        var oauthToken;
        // Use the API Loader script to load google.picker and gapi.auth.
        function onApiLoad() {
            gapi.load('auth', {
                'callback': onAuthApiLoad
            });
            gapi.load('picker', {
                'callback': onPickerApiLoad
            });
        }

        function onAuthApiLoad() {
            window.gapi.auth.authorize({
                    'client_id': clientId,
                    'scope': scope,
                    'immediate': false
                },
                handleAuthResult);
        }

        function onPickerApiLoad() {
            pickerApiLoaded = true;
            createPicker();
        }

        function handleAuthResult(authResult) {
            if (authResult && !authResult.error) {
                oauthToken = authResult.access_token;
                createPicker();
            }
        }
        // Create and render a Picker object for picking user Photos.
        function createPicker() {
            if (pickerApiLoaded && oauthToken) {
                var picker = new google.picker.PickerBuilder().
                addView(google.picker.ViewId.FOLDERS).
                addView(new google.picker.DocsUploadView()).
                enableFeature(google.picker.Feature.MULTISELECT_ENABLED).
                setOAuthToken(oauthToken).
                setDeveloperKey(developerKey).
                setCallback(pickerCallback).
                build();
                picker.setVisible(true);
            }
        }

            // A simple callback implementation.
            function pickerCallback(data) {
                images = new Array();
                var url = 'nothing';
                if (data[google.picker.Response.ACTION] == google.picker.Action.PICKED) {
                    var doc = data[google.picker.Response.DOCUMENTS];
                    console.log(doc);
                    var x = '';
                    for (var i = 0; i < doc.length; i++) {
                        // var img = 'http://drive.google.com/uc?export=view&id=' + doc[i].id;
                        var id = doc[i].id;
                        var img = 'https://drive.google.com/thumbnail?authuser=0&sz=w320&id=' + id;
                        x += '<div class="uk-width-1-4 image_container"><div class="md-card"><div class="md-card-head head_background" style="background-image: url(' + img + ')"><div class="ytp-gradient-top"></div><div class="md-card-head-menu"><i class="md-icon material-icons md-36 md-light" onclick="remove_image(this)">clear</i></div></div><div class="md-card-content"><p><input type="hidden" name="images[]" value="' + id + '" /><input type="radio" name="google_image" id="google_image_' + i + '" value="' + id + '" data-md-icheck /><label class="inline-label" for="google_image_' + i + '">{{trans('main.main_image')}}</label></p><p><input type="hidden" name="published[]" value="0" /><input id="google_img_' + i + '" type="checkbox" name="published[]" value="1" checked /><label for="google_img_' + i + '">{{trans('main.Published')}}</label></p></div></div></div>';
                        
                        images[images.length] = img;
                    }
                    $('#result').append(x);
                    altair_form_adv.masked_inputs();
                    altair_md.inputs();
                    altair_md.checkbox_radio();
                    altair_forms.init();
                    // to refresh the height of the wizard
                    setTimeout(function() {
                        $(window).trigger('resize');
                    }, 1000);
                }
            }

        </script>
        <script src="{{ URL::asset('js/shared.js') }}"></script>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-80818498-1', 'auto');
          ga('send', 'pageview');

        </script>
        @yield('scripts')
        {{$scrips or ''}}
    </body>
</html>
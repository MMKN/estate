@foreach($cities as $city)
<tr>
    <td>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-5 uk-text-center">
                <h4 class="heading_a uk-margin-small-bottom">{{ $city->name }}</h4>
            </div>
            <div class="uk-width-3-5">
                @foreach($city->getNeighbourOfCity($city->id) as $neighbour)
                    <span class="uk-badge uk-badge-success">{{ $neighbour->name }}</span>
                @endforeach
            </div>
            <div class="uk-width-1-5 uk-margin">
                <a name="{{ $city->id }}" data-uk-modal="{target:'#modal_city_edit'}" class="get_edit_city_modal"><i class="md-icon md-36 material-icons uk-text-success" >&#xE254;</i></a>
                <a name="{{ $city->id }}" data-uk-modal="{target:'#modal_delete'}"    class="get_delete_city_modal"><i class="md-icon material-icons uk-margin-large-left md-36 uk-text-danger">delete</i></a>
            </div>
        </div>
    </td>
</tr>
@endforeach

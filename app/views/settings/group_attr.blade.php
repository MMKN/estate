<div class="uk-input-group">
    <span class="uk-input-group-addon">
        <i class="material-icons md-36">chat</i>
    </span>
    <select id="group_add" name="attr[]" multiple>
        <option value="" disabled selected> {{ $type }} </option>
        @if($type == 'Districts')
            @foreach($attrs as $attr)
                <option value="{{ $attr->id }}">{{ $attr->name }}</option>
            @endforeach
        @else
            @foreach($attrs as $attr)
                <option value="{{ $attr->id }}">{{ $attr->value }}</option>
            @endforeach
        @endif
    </select>
</div>

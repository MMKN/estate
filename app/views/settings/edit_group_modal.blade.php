<div class="uk-modal-dialog">
    <button type="button" class="uk-modal-close uk-close"></button>
    <div class="uk-margin user_content">
        <form id="form_validation" class="uk-form-stacked edit_group_form">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-1-1">
                    <div class="uk-margin-top parsley-row">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon">
                                <i class="material-icons md-36">forum</i>
                            </span>
                            <label>{{trans('main.Group_name')}}</label>
                            <input type="text" name="group_name" class="md-input" value="{{ $group->name }}" required />
                        </div>
                    </div>

                    <div class="uk-margin-top parsley-row">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon">
                                <i class="material-icons md-36">content_copy</i>
                            </span>
                            <div>
                                {{ Form::select('group_type' , [ '1' => 'Floor' , '0' => 'Districts'] , $group->type , ['required data-md-selectize-inline' , 'class' => 'group_type']) }}
                            </div>
                        </div>
                    </div>

                    <div class="uk-margin-top parsley-row" id="edit_attributes_list">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon">
                                <i class="material-icons md-36">chat</i>
                            </span>
                            <select id="group_edit" class="group_districts_floors" name="attr[]" multiple>
                                @if($group->type == 0)
                                    @foreach($group->getDistricts($group->id) as $district)
                                        <option value="{{ $district->neighbour_id }}" selected>{{ $district->getDistrict($district->neighbour_id) }}</option>
                                    @endforeach
                                    @foreach($neighbours as $neighbour)
                                        <option value="{{ $neighbour->id }}">{{ $neighbour->name }}</option>
                                    @endforeach
                                @else
                                    @foreach($group->getFloors($group->id) as $floor)
                                        <option value="{{ $floor->floor_id }}" selected>{{ $floor->getFloor($floor->floor_id) }}</option>
                                    @endforeach
                                    @foreach($floors as $floor)
                                        <option value="{{ $floor->id }}">{{ $floor->value }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>


                    <div class="uk-margin-top uk-text-center">
                        <button type="submit" class="md-btn md-btn-primary">{{trans('main.Save')}}</button>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>
</div>

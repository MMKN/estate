<div class="uk-modal-dialog">
    <button type="button" class="uk-modal-close uk-close"></button>
    <div class="uk-margin user_content">

        <form id="form_validation" class="uk-form-stacked edit_city_form">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-1-1">
                    <div class="uk-margin-top parsley-row">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon">
                                <i class="material-icons md-36">explore</i>
                            </span>
                            <label>{{trans('main.City')}}</label>
                            <input type="text" class="md-input city_name_edit" name="city_name" value="{{ $city->name }}" required />
                        </div>
                    </div>

                    <div class="uk-margin-top parsley-row">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon">
                                <i class="material-icons md-36">gps_fixed</i>
                            </span>
                            <select id="districts_edit" class="districts_edit" name="districts[]" multiple>
                                @foreach($neighbours as $neighbour)
                                <option value="{{ $neighbour->name }}" selected>{{ $neighbour->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <!-- <div class="uk-margin-top uk-text-center">
                        <button type="submit" class="md-btn md-btn-primary">Save</button>
                    </div> -->

                </div>
            </div>
        </form>
    </div>
</div>

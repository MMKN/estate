<div class="uk-modal-dialog">
    <button type="button" class="uk-modal-close uk-close"></button>
    <div class="uk-margin user_content">

        <form class="uk-form-stacked {{ $class }}">
            <div class="uk-grid" data-uk-grid-margin>
                <h3 class="uk-width-medium-1-1- uk-margin">{{ $message }}</h3>
                <div class="uk-width-medium-1-2 uk-margin">
                    <button type="submit" class="md-btn md-btn-danger">{{trans('main.Delete')}}</button>
                    <a class="md-btn md-btn-success go_back" onclick="UIkit.modal('#modal_delete').hide()">{{trans('main.Cancel')}}</a>
                </div>
            </div>
        </form>
    </div>
</div>

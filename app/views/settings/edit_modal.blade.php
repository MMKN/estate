<div class="uk-modal-dialog">
    <button type="button" class="uk-modal-close uk-close"></button>
    <div class="uk-margin user_content">

        <form id="form_validation" class="uk-form-stacked edit_user_form">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-1-1">

                    <div class="uk-margin-top parsley-row">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon">
                                <i class="material-icons md-36">face</i>
                            </span>
                            <label>{{trans('main.Full_Name')}}</label>
                            <input type="text" name="name" class="md-input" value="{{ $user->name }}" required />
                        </div>
                    </div>

                    <div class="uk-margin-top parsley-row">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon">
                                <i class="material-icons md-36">email</i>
                            </span>
                            <label>{{trans('main.Email')}}</label>
                            <input type="email" name="email" data-parsley-trigger="change" value="{{ $user->email }}" class="md-input" required />
                        </div>
                    </div>

                    <div class="uk-margin-top parsley-row">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon">
                                <i class="material-icons md-36">call</i>
                            </span>
                            <label>{{trans('main.Mobile_main')}}</label>
                            <input type="text" name="mobile" value="{{ $user->mobile }}"  class="md-input" required />
                        </div>
                    </div>

                    <div class="uk-margin-top parsley-row">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon">
                                <i class="material-icons md-36">lock</i>
                            </span>
                            <input type="password" name="password" class="md-input" placeholder="Password"/>
                            <a href="" class="uk-form-password-toggle" data-uk-form-password>{{trans('main.show')}}</a>
                        </div>
                    </div>

                    <div class="uk-margin-top parsley-row">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon">
                                <i class="material-icons md-36">security</i>
                            </span>
                            <!-- {{ Form::select('role' , [ '1' => 'Admin' , '0' => 'Agent'] , $user->is_admin , ['required data-md-selectize-inline']) }} -->
                            <select required data-md-selectize-inline name="role">
                                <option value="" disabled selected>User role</option>
                                @if(Auth::user()->user()->is_admin)
                                <option value="1" {{$user->is_admin?"selected":""}}>{{trans('main.Admin')}}</option>
                                @endif
                                <option value="0" {{$user->is_admin==0?"selected":""}}>{{trans('main.Agent')}}</option>
                            </select>
                        </div>
                    </div>


                    <div class="uk-margin-top parsley-row">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon">
                                <i class="material-icons md-36">image</i>
                            </span>
                            <div class="uk-input-group uk-margin-large-left">
                                <div class="user_heading_avatar fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        <img src="uploads/profile/{{ $user->image }}" id="image_preview_edit" alt="user avatar"/>
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                    <div class="user_avatar_controls">
                                        <span class="btn-file">
                                            <span class="fileinput-new"><i class="material-icons">&#xE2C6;</i></span>
                                            <span class="fileinput-exists"><i class="material-icons">&#xE86A;</i></span>
                                            <input type="file" name="image" id="user_image_edit">
                                        </span>
                                        <a href="#" class="btn-file fileinput-exists" data-dismiss="fileinput"><i class="material-icons">&#xE5CD;</i></a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="uk-margin-top uk-text-center">
                        <button type="submit" class="md-btn md-btn-primary">{{trans('main.Save')}}</button>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>

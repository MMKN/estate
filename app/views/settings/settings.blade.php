@extends('_layouts.core')
@section('content')
<!-- settings page begin -->
<article id="main">
    <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
        <!-- Main content -->
        <div class="uk-width-1-1">
            <div class="md-card">
                <!-- main page header -->
                <div class="user_heading">
                    <div class="user_heading_content clearfix">
                        <h2 class="heading_b"> <i class="material-icons md-48 md-light uk-text-middle">&#xE8B8;</i> &nbsp;&nbsp;{{trans('main.settings')}}</h2>
                    </div>
                </div>
            </div>
            <!-- setting tabs begin -->
            <div class="uk-width-1-1">
                <div class="md-card">
                    <div class="uk-width-medium-8-10 uk-container-center">
                        <div class="md-card-content">
                            <ul class="uk-tab" data-uk-tab="{connect:'#tabs_anim1', animation:'scale'}" data-uk-sticky="{top:48, media: 960, boundary: true,showup:true,animation: 'uk-animation-slide-top'}">
                                <!-- <li class="uk-active"><a href="#">{{trans('main.Users')}}</a></li> -->
                                <li class="uk-active"><a href="#">{{trans('main.Filter')}}</a></li>
                                <li><a href="#">{{trans('main.City_districts')}}</a></li>
                                <li><a href="#">{{trans('main.Groups')}}</a></li>
                                <li ><a href="#">{{trans('main.Data_Export')}}</a></li>
                            </ul>
                            <ul id="tabs_anim1" class="uk-switcher uk-margin">
                                <!-- Filter settings -->
                                <li class="uk-margin-large-bottom">
                                    <h2 class="full_width_in_card heading_c uk-margin-small-top">{{trans('main.General_settings')}}</h2>
                                    <!-- properties types input field -->
                                    <div class="uk-gird  uk-margin-medium-top">
                                        <div class="uk-width-2-10 uk-float-left ">
                                            <h3 class="uk-text-center uk-padding" for="product_edit_tags_control">{{trans('main.Property_type')}}</h3>
                                        </div>
                                        <div class="uk-width-7-10 uk-float-left">
                                            <select id="unit_type" class="unit_type" multiple>
                                                @foreach($unit_types as $unit_type)
                                                <option value="{{ $unit_type->value }}" selected>{{ $unit_type->value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <!-- floors input field -->
                                    <div class="uk-gird  uk-margin-large-top">
                                        <div class="uk-width-2-10 uk-float-left ">
                                            <h3 class="uk-text-center uk-padding" for="product_edit_tags_control">{{trans('main.Floors')}}</h3>
                                        </div>
                                        <div class="uk-width-7-10 uk-float-left">
                                            <select id="floor" class="floor" multiple>
                                                @foreach($floors as $floor)
                                                <option value="{{ $floor->value }}" selected>{{ $floor->value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <!-- finishing input field  -->
                                    <div class="uk-gird  uk-margin-large-top">
                                        <div class="uk-width-2-10 uk-float-left ">
                                            <h3 class="uk-text-center uk-padding" for="product_edit_tags_control">{{trans('main.Finishing')}}</h3>
                                        </div>
                                        <div class="uk-width-7-10 uk-float-left">
                                            <select id="finishing" class="finishing" multiple>
                                                @foreach($finishings as $finishing)
                                                <option value="{{ $finishing->value }}" selected>{{ $finishing->value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <!-- payment styles input filed -->
                                    <div class="uk-gird  uk-margin-large-top">
                                        <div class="uk-width-2-10 uk-float-left ">
                                            <h3 class="uk-text-center uk-padding" for="product_edit_tags_control">{{trans('main.Payment_style')}}</h3>
                                        </div>
                                        <div class="uk-width-7-10 uk-float-left">
                                            <select id="payment_type" class="payment_type" multiple>
                                                @foreach($payment_types as $payment_type)
                                                <option value="{{ $payment_type->value }}" selected>{{ $payment_type->value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <!-- objective input filed -->
                                    <div class="uk-gird  uk-margin-large-top">
                                        <div class="uk-width-2-10 uk-float-left ">
                                            <h3 class="uk-text-center uk-padding" for="product_edit_tags_control">{{trans('main.Objective')}}</h3>
                                        </div>
                                        <div class="uk-width-7-10 uk-float-left">
                                            <select id="objective" class="objective" multiple>
                                                @foreach($objectives as $objective)
                                                <option value="{{ $objective->value }}" selected>{{ $objective->value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <!-- features input filed -->
                                    <div class="uk-gird  uk-margin-large-top">
                                        <div class="uk-width-2-10 uk-float-left ">
                                            <h3 class="uk-text-center uk-padding" for="product_edit_tags_control">{{trans('main.Features')}}</h3>
                                        </div>
                                        <div class="uk-width-7-10 uk-float-left">
                                            <select id="feature" class="feature" multiple>
                                                @foreach($features as $feature)
                                                <option value="{{ $feature->value }}" selected>{{ $feature->value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <!-- settings language -->
                                    <div class="uk-gird  uk-margin-large-top">
                                        <div class="uk-width-2-10 uk-float-left ">
                                            <h3 class="uk-text-center uk-padding" for="product_edit_tags_control"> {{ trans('main.Language') }} </h3>
                                        </div>
                                        <div class="uk-width-medium-3-10 uk-float-left">
                                            <div class="parsley-row  uk-margin-small-top">
                                                <select name="language" class="search_attr" required data-md-selectize id="language">
                                                    <option value="" disabled selected>{{trans('main.Language')}}</option>
                                                    @foreach($languages as $language)
                                                    <option value="{{ $language->value }}" {{ $language->getMainLang($language->value) }} > {{ $language->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <br>
                                    <br>
                                </li>
                                
                                
                                <!-- City and districts -->
                                <li class="uk-margin-large-bottom">
                                    <div class="uk-input-group uk-width-1-1">
                                        <h2 class="full_width_in_card heading_c uk-margin-medium-bottom">
                                        <div class="uk-input-group-addon">{{trans('main.Manage_Cities_districts')}}</div>
                                        <div class="uk-input-group-addon">
                                            <a class="md-btn md-btn-primary" data-uk-modal="{target:'#modal_city_add'}">&nbsp;&nbsp;{{trans('main.add_city')}}</a>
                                        </div>
                                        </h2>
                                    </div>
                                    <div class="uk-overflow-container">
                                        <table class="uk-table uk-table-hover uk-table-nowrap">
                                            <tbody id="cities_table">
                                                @foreach($cities as $city)
                                                <tr>
                                                    <td>
                                                        <div class="uk-grid" data-uk-grid-margin>
                                                            <div class="uk-width-1-5 uk-text-left">
                                                                <p class=" uk-margin-small-bottom">{{ $city->name }}</p>
                                                            </div>
                                                            <div class=" uk-width-3-5">
                                                                <p class="uk-grid">
                                                                    @foreach($city->getNeighbourOfCity($city->id) as $neighbour)
                                                                    <span class="uk-badge uk-margin-bottom uk-margin-right uk-badge-success">{{ $neighbour->name }}</span>
                                                                    @endforeach
                                                                </p>
                                                            </div>
                                                            <div class="uk-width-1-5 uk-margin">
                                                                <a name="{{ $city->id }}" data-uk-modal="{target:'#modal_city_edit'}" class="get_edit_city_modal"><i class="md-icon md-36 material-icons uk-text-success" >&#xE254;</i></a>
                                                                <a name="{{ $city->id }}" data-uk-modal="{target:'#modal_delete'}"    class="get_delete_city_modal"><i class="md-icon material-icons uk-margin-large-left md-36 uk-text-danger">delete</i></a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </li>
                                <!-- groups -->
                                <li class="uk-margin-large-bottom">
                                    <div class="uk-input-group uk-width-1-1">
                                        <h2 class="full_width_in_card heading_c uk-margin-medium-bottom">
                                        <div class="uk-input-group-addon"> {{trans('main.Manage_Groups')}}  </div>
                                        <div class="uk-input-group-addon">
                                            <a class="md-btn md-btn-primary uk-text-middle" data-uk-modal="{target:'#modal_group_add'}">&nbsp;&nbsp;{{trans('main.Add')}}</a>
                                        </div>
                                        </h2>
                                    </div>
                                    <div class="uk-overflow-container">
                                        <table class="uk-table uk-table-hover">
                                            <tbody id="groups_table">
                                                @foreach($groups as $group)
                                                <tr class="uk-grid">
                                                    <td class="uk-width-1-5 uk-text-center">
                                                        {{ $group->name }}
                                                    </td>
                                                    <td class="uk-width-1-5 uk-text-left">
                                                        {{ $group->getType($group->type) }}
                                                    </td>
                                                    <td class="uk-width-2-5 uk-text-center">
                                                        @if($group->type == 0)
                                                        <p class="uk-grid">
                                                            @foreach($group->getDistricts($group->id) as $district)
                                                            <span class="uk-badge uk-margin-bottom uk-margin-right uk-badge-success">{{ $district->getDistrict($district->neighbour_id) }}</span>
                                                            @endforeach
                                                        </p>
                                                        @else
                                                        <p class="uk-grid">
                                                            @foreach($group->getFloors($group->id) as $floor)
                                                            <span class="uk-badge uk-margin-bottom uk-margin-right uk-badge-success">{{ $floor->getFloor($floor->floor_id) }}</span>
                                                            @endforeach
                                                        </p>
                                                        @endif
                                                    </td>
                                                    <td class="uk-width-1-5">
                                                        <a class="get_edit_group_modal" name="{{ $group->id }}" data-uk-modal="{target:'#modal_group_edit'}"><i class="md-icon  material-icons uk-text-success" >&#xE254;</i></a>
                                                        <a class="get_delete_group_modal" name="{{ $group->id }}" data-uk-modal="{target:'#modal_delete'}"><i class="md-icon material-icons uk-margin-large-left uk-text-danger">delete</i></a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <h2 class="full_width_in_card heading_c uk-margin-large-top">{{trans('main.Exact_filter_margins')}}</h2>
                                    <div class="uk-grid">
                                        <div class="uk-width-1-3 uk-text-center uk-margin-top parsley-row">
                                            <div class="uk-input-group">
                                                <span class="uk-input-group-addon">
                                                    <i class="material-icons md-36">dashboard</i>
                                                </span>
                                                <label>{{trans('main.Area_margin')}}</label>
                                                <input value="{{ Margin::getValue('area' , 0) }}" class="md-input masked_input margins" name="0" title="area" type="text" data-inputmask="'alias': 'numeric',  'autoGroup': true, 'digits': 0, 'digitsOptional': false , 'prefix': '%','placeholder': '0'" data-inputmask-showmaskonhover="false" />
                                            </div>
                                        </div>
                                        <div class="uk-width-1-3 uk-text-center uk-margin-top parsley-row">
                                            <div class="uk-input-group">
                                                <span class="uk-input-group-addon">
                                                    <i class="material-icons md-36">eject</i>
                                                </span>
                                                <label>{{trans('main.Rooms_margin')}}</label>
                                                <input value="{{ Margin::getValue('rooms_no' , 0) }}" class="md-input masked_input margins" name="0" title="rooms_no" type="text" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 0, 'digitsOptional': false , 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                                            </div>
                                        </div>
                                        <div class="uk-width-1-3 uk-text-center uk-margin-top parsley-row">
                                            <div class="uk-input-group">
                                                <span class="uk-input-group-addon">
                                                    <i class="material-icons md-36">subject</i>
                                                </span>
                                                <label>{{trans('main.Toilets_margin')}}</label>
                                                <input value="{{ Margin::getValue('toilets_no' , 0) }}" class="md-input masked_input margins" name="0" title="toilets_no" type="text" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 0, 'digitsOptional': false , 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                                            </div>
                                        </div>
                                    </div>
                                    <h2 class="full_width_in_card heading_c uk-margin-large-top">{{trans('main.Similar_filter_margins')}}</h2>
                                    <div class="uk-grid">
                                        <div class="uk-width-1-3 uk-text-center uk-margin-top parsley-row">
                                            <div class="uk-input-group">
                                                <span class="uk-input-group-addon">
                                                    <i class="material-icons md-36">dashboard</i>
                                                </span>
                                                <label>{{trans('main.Area_margin')}}</label>
                                                <input value="{{ Margin::getValue('area' , 1) }}" class="md-input masked_input margins" name="1" title="area" type="text" data-inputmask="'alias': 'numeric',  'autoGroup': true, 'digits': 0, 'digitsOptional': false , 'prefix': '%','placeholder': '0'" data-inputmask-showmaskonhover="false" />
                                            </div>
                                        </div>
                                        <div class="uk-width-1-3 uk-text-center uk-margin-top parsley-row">
                                            <div class="uk-input-group">
                                                <span class="uk-input-group-addon">
                                                    <i class="material-icons md-36">eject</i>
                                                </span>
                                                <label>{{trans('main.Rooms_margin')}}</label>
                                                <input value="{{ Margin::getValue('rooms_no' , 1) }}" class="md-input masked_input margins" name="1" title="rooms_no" type="text" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 0, 'digitsOptional': false , 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                                            </div>
                                        </div>
                                        <div class="uk-width-1-3 uk-text-center uk-margin-top parsley-row">
                                            <div class="uk-input-group">
                                                <span class="uk-input-group-addon">
                                                    <i class="material-icons md-36">subject</i>
                                                </span>
                                                <label>{{trans('main.Toilets_margin')}}</label>
                                                <input value="{{ Margin::getValue('toilets_no' , 1) }}" class="md-input masked_input margins" name="1" title="toilets_no" type="text" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 0, 'digitsOptional': false , 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <!-- Data export tab content -->
                                <li class="uk-margin-small-top">
                                    <h2 class="full_width_in_card heading_c uk-margin-medium-bottom"> {{trans('main.Export_to')}}</h2>
                                    <div>
                                        <ul class="md-list md-list-addon">
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="md-list-addon-icon material-icons uk-text-success">&#xE884;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <h3><a class="md-list-heading" href="{{ URL::route('settings.export.properties') }}"> {{trans('main.Download_Properties')}}</a></h3>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="md-list-addon-icon material-icons uk-text-success">&#xE884;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <h3><a class="md-list-heading" href="{{ URL::route('settings.export.customers') }}">{{trans('main.Download_Customers')}}</a></h3>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- settings  end -->
        <!-- add new user model begin -->
        <div class="uk-modal" id="modal_add">
            <div class="uk-modal-dialog">
                <button type="button" class="uk-modal-close uk-close"></button>
                <div class="uk-margin user_content">
                    <form id="form_validation" class="uk-form-stacked add_user_form">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-1-1">
                                <div class="uk-margin-top parsley-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="material-icons md-36">face</i>
                                        </span>
                                        <label>{{trans('main.Full_Name')}}</label>
                                        <input type="text" class="md-input" name="name" required />
                                    </div>
                                </div>
                                <div class="uk-margin-top parsley-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="material-icons md-36">email</i>
                                        </span>
                                        <label>{{trans('main.Email')}}</label>
                                        <input type="email" name="email" data-parsley-trigger="change" class="md-input" required />
                                    </div>
                                </div>
                                <div class="uk-margin-top parsley-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="material-icons md-36">call</i>
                                        </span>
                                        <label>{{trans('main.Mobile_main')}}</label>
                                        <input type="text" name="mobile"  class="md-input"  required />
                                    </div>
                                </div>
                                <div class="uk-margin-top parsley-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="material-icons md-36">lock</i>
                                        </span>
                                        <input type="password" name="password" class="md-input" placeholder="Password" value="" required/>
                                        <a href="" class="uk-form-password-toggle" data-uk-form-password>{{trans('main.show')}}</a>
                                    </div>
                                </div>
                                <div class="uk-margin-top parsley-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="material-icons md-36">security</i>
                                        </span>
                                        <div>
                                            <select name="role" id="role" required data-md-selectize-inline>
                                                <option value="" disabled selected>User role</option>
                                                <option value="1">{{trans('main.Admin')}}</option>
                                                <option value="0">{{trans('main.Agent')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-margin-top parsley-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="material-icons md-36">image</i>
                                        </span>
                                        <div class="uk-input-group uk-margin-large-left">
                                            <div class="user_heading_avatar fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail">
                                                    <img src="assets/img/blank.png" id="image_preview" alt="user avatar"/>
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                                <div class="user_avatar_controls">
                                                    <span class="btn-file">
                                                        <span class="fileinput-new"><i class="material-icons">&#xE2C6;</i></span>
                                                        <span class="fileinput-exists"><i class="material-icons">&#xE86A;</i></span>
                                                        <input type="file" name="image" id="user_image">
                                                    </span>
                                                    <a href="#" class="btn-file fileinput-exists" data-dismiss="fileinput"><i class="material-icons">&#xE5CD;</i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-margin-top uk-text-center">
                                    <button type="submit" id="add_user_button" class="md-btn md-btn-primary">{{trans('main.Save')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- edit user model begin -->
        <div class="uk-modal" id="modal_edit">
        </div>
        <!-- edit user model end -->
        <!-- delete user model begin -->
        <div class="uk-modal" id="modal_delete">
        </div>
        <!-- delete user model end -->
        <!-- add city model begin -->
        <div class="uk-modal" id="modal_city_add">
            <div class="uk-modal-dialog">
                <button type="button" class="uk-modal-close uk-close"></button>
                <div class="uk-margin user_content">
                    <form id="form_validation" class="uk-form-stacked add_city_form">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-1-1">
                                <div class="uk-margin-top parsley-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="material-icons md-36">explore</i>
                                        </span>
                                        <label>{{trans('main.City')}}</label>
                                        <input type="text" class="md-input" name="name" required />
                                    </div>
                                </div>
                                <div class="uk-margin-top parsley-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="material-icons md-36">gps_fixed</i>
                                        </span>
                                        <select id="districts_add" name="districts[]" required multiple>
                                        </select>
                                    </div>
                                </div>
                                <div class="uk-margin-top uk-text-center">
                                    <button type="submit" class="md-btn md-btn-primary">{{trans('main.Save')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- edit city model begin -->
        <div class="uk-modal" id="modal_city_edit">
        </div>
        <!-- add group model begin -->
        <div class="uk-modal" id="modal_group_add">
            <div class="uk-modal-dialog">
                <button type="button" class="uk-modal-close uk-close"></button>
                <div class="uk-margin user_content">
                    <form id="form_validation" class="uk-form-stacked add_group_form">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-1-1">
                                <div class="uk-margin-top parsley-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="material-icons md-36">forum</i>
                                        </span>
                                        <label>{{trans('main.Group_name')}}</label>
                                        <input type="text" name="group_name" class="md-input" required />
                                    </div>
                                </div>
                                <div class="uk-margin-top parsley-row">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="material-icons md-36">content_copy</i>
                                        </span>
                                        <div>
                                            <select id="group_type" name="group_type" class="group_type" required data-md-selectize-inline>
                                                <option value="" disabled selected>{{trans('main.type')}}</option>
                                                <option value="0">{{trans('main.District')}}</option>
                                                <option value="1">{{trans('main.Floor')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-margin-top parsley-row" id="attributes_list">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon">
                                            <i class="material-icons md-36">chat</i>
                                        </span>
                                        <select id="group_add" class="group_districts_floors" name="attr[]" required multiple>
                                            <option value="" disabled selected> Attributs </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="uk-margin-top uk-text-center">
                                    <button type="submit" class="md-btn md-btn-primary">{{trans('main.Save')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- edit group model begin -->
        <div class="uk-modal" id="modal_group_edit">
        </div>
    </div>
</article>
<div class="uk-notify uk-notify-bottom-center" style="display:none; z-index:9999; " id="notification_area">
</div>
@stop


@section('scripts')
<!--  dropdown menu intialization functions -->
<script src="{{ URL::asset('js/tag_init.js') }}"></script>
<script src="{{ URL::asset('js/settings_handler.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/forms_validation.min.js') }}"></script>

<script type="text/javascript">
var base_url = "{{ URL::route('settings.index') }}";
var property_id;
</script>
<!-- Properties Scripts -->
@stop
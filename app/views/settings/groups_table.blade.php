@foreach($groups as $group)
<tr>
    <td class="uk-text-center">
        {{ $group->name }}
    </td>
    <td class="uk-text-center">
        {{ $group->getType($group->type) }}
    </td>

    <td class="uk-text-center">
        @if($group->type == 0)
        @foreach($group->getDistricts($group->id) as $district)
        <span class="uk-badge uk-badge-success">{{ $district->getDistrict($district->neighbour_id) }}</span>
        @endforeach
        @else
        @foreach($group->getFloors($group->id) as $floor)
        <span class="uk-badge uk-badge-success">{{ $floor->getFloor($floor->floor_id) }}</span>
        @endforeach
        @endif
    </td>

    <td>
        <a class="get_edit_group_modal" name="{{ $group->id }}" data-uk-modal="{target:'#modal_group_edit'}"><i class="md-icon md-36 material-icons uk-text-success" >&#xE254;</i></a>
        <a class="get_delete_group_modal" name="{{ $group->id }}" data-uk-modal="{target:'#modal_delete'}"><i class="md-icon material-icons uk-margin-large-left md-36 uk-text-danger">delete</i></a>
    </td>
</tr>
@endforeach

@extends('_layouts.core')
@section('content')

        <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
            <!-- Main content -->
            <div class="uk-width-1-1">
                <div class="md-card">

                    <!-- user heading -->
                    <div class="user_heading">
                        <div class="user_heading_content clearfix">
                            <h2 class="heading_b uk-float-left"> <i class="material-icons md-48 md-light uk-text-middle">&#xE853;</i> &nbsp;&nbsp;{{$customer->name}}</h2>
                        </div>
                    </div>

                    <!-- user Content -->
                    <div class="user_content">
                        <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'scale'}" data-uk-sticky="{ top: 0, media: 960 }">
                            <li class="uk-active"><a href="#">{{trans('main.Suitable_properties')}}</a></li>
                            <li><a href="#">{{trans('main.Personal_information')}}</a></li>
                        </ul>

                        <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">


                            <!-- Suitable content -->
                            <li>
                                <!-- Properties Grid  -->
                                <h3 class="full_width_in_card heading_c">
                                    {{trans('main.Exact_Properties')}}
                                </h3>

                                <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-large-1-5 uk-text-center uk-margin-small-top" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
                                @if(count($property))
                                    @foreach($property as $prop)
                                    <div style="padding-bottom:27px;">

                                        <a href="{{ URL::route('properties.property.customer' , $prop->id) }}" target="_blank">
                                            <div class="md-card md-card-hover " style="cursor:pointer;" id="{{ $prop->id }}">
                                                <div class="md-card-head head_background" style="background-image: url('{{ $prop->getMainImage($prop->id) }}')">
                                                    <div class="md-card-head-menu" data-uk-dropdown="" aria-haspopup="true" aria-expanded="false">

                                                    </div>
                                                    <h2 class="md-card-head-text">
                                                        {{ $prop->getUnitType($prop->unit_type) }}
                                                    </h2>
                                                    <div class="md-card-head-subtext">
                                                        <span>{{$prop->price}}&#163;</span>
                                                    </div>
                                                </div>
                                                <div class="md-card-content">
                                                    <ul class="md-list">
                                                        <li>
                                                            <div class="md-list-content">
                                                                <span class="md-list-heading">{{(new City())->getCity($prop->city)}}</span>
                                                                <span class="uk-text-small uk-text-muted">{{(new Neighbour())->getNeighbour($prop->neighbour)}}</span>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    @endforeach
                                @else
                                    <div class="uk-width-medium-1-1 uk-margin-medium-bottom">
                                        <i class="material-icons md-48 uk-text-warning">warning</i>
                                    </div>
                                @endif
                                    
                                    
                                </div>

                                <h3 class="full_width_in_card heading_c">
                                    {{trans('main.Similar_Properties')}}
                                </h3>
                                <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-large-1-5 uk-text-center uk-margin-small-top" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
                                @if(count($suggestions))
                                    @foreach($suggestions as $suggestion)
                                        <div>
                                            <div style="margin-bottom:27px;">
                                                <div class="md-card md-card-hover  get_view_box" style="cursor:pointer;" id="{{ $property->id }}">
                                                    <div class="md-card-head head_background" style="box-shadow: rgba(0,0,0,0.1) 0px 40px 20px inset;background-image: url('{{ $property->getMainImage($property->id) }}')">
                                                        <h2 class="md-card-head-text" style="    text-shadow: 2px 2px 5px #000; font-size:22px">
                                                            {{ $property->getUnitType($property->unit_type) }}
                                                        </h2>
                                                        <!-- <div class="md-card-head-subtext">
                                                            <span>{{$property->price}}&#163;</span>
                                                        </div> -->
                                                        <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-bottom-right uk-margin-bottom uk-margin-top" style="font-size:18px;">{{$property->price}}&#163;</div>
                                                    </div>
                                                    <div class="md-card-content">
                                                        <ul class="md-list">
                                                            <li>
                                                                <div class="md-list-content">
                                                                    <span class="md-list-heading">{{$property->getCity($property->city)}} </span>
                                                                    <span class="uk-text-small uk-text-muted">{{$property->getNeighbour($property->neighbour)}} </span>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="uk-width-medium-1-1 uk-margin-medium-bottom">
                                        <i class="material-icons md-48 uk-text-warning">warning</i>
                                    </div>
                                @endif
                                </div>

                                <h3 class="full_width_in_card heading_c">
                                    {{trans('main.Price_Properties')}}
                                </h3>

                                <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-large-1-5 uk-text-center uk-margin-small-top" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
                                @if(count($price_properties))
                                    @foreach($price_properties as $price_property)
                                    <div style="padding-bottom:27px;">

                                        <a href="{{ URL::route('properties.property.customer' , $price_property->id) }}" target="_blank">
                                            <div class="md-card md-card-hover " style="cursor:pointer;" id="{{ $price_property->id }}">
                                                <div class="md-card-head head_background" style="background-image: url('{{ $price_property->getMainImage($price_property->id) }}')">
                                                    <div class="md-card-head-menu" data-uk-dropdown="" aria-haspopup="true" aria-expanded="false">

                                                    </div>
                                                    <h2 class="md-card-head-text">
                                                        {{ $price_property->getUnitType($price_property->unit_type) }}
                                                    </h2>
                                                    <div class="md-card-head-subtext">
                                                        <span>{{$price_property->price}}&#163;</span>
                                                    </div>
                                                </div>
                                                <div class="md-card-content">
                                                    <ul class="md-list">
                                                        <li>
                                                            <div class="md-list-content">
                                                                <span class="md-list-heading">{{(new City())->getCity($price_property->city)}}</span>
                                                                <span class="uk-text-small uk-text-muted">{{(new Neighbour())->getNeighbour($price_property->neighbour)}}</span>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    @endforeach
                                @else
                                    <div class="uk-width-medium-1-1 uk-margin-medium-bottom">
                                        <i class="material-icons md-48 uk-text-warning">warning</i>
                                    </div>
                                @endif
                                    
                                </div>

                            </li>



                            <!-- Customer Info -->
                            <li>
                                <div class="uk-grid uk-margin-small-top uk-margin-large-bottom" data-uk-grid-margin>

                                    <!-- Property information -->
                                    <div class="uk-width-large-4-4">
                                        <h4 class="full_width_in_card heading_c">{{trans('main.customer_preferences')}}</h4>
                                        <div class="uk-grid uk-grid-divider uk-grid-medium">
                                            <div class="uk-width-large-1-2">
                                                <div class="uk-grid uk-grid-small">
                                                    <div class="uk-width-large-1-3">
                                                        <span class="uk-text-muted uk-text-small">{{trans('main.Purpose')}}</span>
                                                    </div>
                                                    <div class="uk-width-large-2-3">
                                                        <span class="uk-text-large uk-text-middle">
                                                            @if($request->purpose == 0)
                                                            Sale
                                                            @else
                                                            Rent
                                                            @endif
                                                        </span>
                                                    </div>
                                                </div>
                                                <hr class="uk-grid-divider">
                                                <div class="uk-grid uk-grid-small">
                                                    <div class="uk-width-large-1-3">
                                                        <span class="uk-text-muted uk-text-small">{{trans('main.Property_type')}}</span>
                                                    </div>
                                                    <div class="uk-width-large-2-3">
                                                        <span class="uk-text-large uk-text-middle">
                                                            <?php $unit_types = RequestUnit::whereRequest_id($request->id)->get(); //explode("|" , $request->unit_type); ?>
                                                            @foreach($unit_types as $key => $unit_type)
                                                            {{(new UnitType())->getUnitType($unit_type->unit_type_id)}}
                                                            @if(isset($unit_types[$key + 1]))
                                                            -
                                                            @endif
                                                            @endforeach
                                                        </span>
                                                    </div>
                                                </div>
                                                <hr class="uk-grid-divider">
                                                <div class="uk-grid uk-grid-small">
                                                    <div class="uk-width-large-1-3">
                                                        <span class="uk-text-muted uk-text-small">{{trans('main.City')}}</span>
                                                    </div>
                                                    <div class="uk-width-large-2-3">
                                                        <span class="uk-text-large uk-text-middle">{{(new City())->getCity($request->city)}}</span>
                                                    </div>
                                                </div>
                                                <hr class="uk-grid-divider">
                                                <div class="uk-grid uk-grid-small">
                                                    <div class="uk-width-large-1-3">
                                                        <span class="uk-text-muted uk-text-small">{{trans('main.District')}}</span>
                                                    </div>
                                                    <div class="uk-width-large-2-3">

                                                        <span class="uk-text-large uk-text-middle">
                                                            <?php $neighbours = RequestNeighbour::whereRequest_id($request->id)->get(); //explode("|" , $request->neighbour); ?>
                                                            @foreach($neighbours as $key => $neighbour)
                                                            {{(new Neighbour())->getNeighbour($neighbour->neighbour_id)}}
                                                            @if(isset($neighbours[$key + 1]))
                                                            -
                                                            @endif
                                                            @endforeach
                                                        </span>

                                                    </div>
                                                </div>
                                                <hr class="uk-grid-divider">
                                                <div class="uk-grid uk-grid-small">
                                                    <div class="uk-width-large-1-3">
                                                        <span class="uk-text-muted uk-text-small">{{trans('main.Finishing')}}</span>
                                                    </div>
                                                    <div class="uk-width-large-2-3">
                                                        <span class="uk-text-large uk-text-middle">
                                                            <?php $finishings = RequestFinishing::whereRequest_id($request->id)->get(); //explode("|" , $request->finishing); ?>
                                                            @foreach($finishings as $key => $finishing)
                                                            {{(new Finishing())->getFinishing($finishing->finishing_id)}}
                                                            @if(isset($finishings[$key + 1]))
                                                            -
                                                            @endif
                                                            @endforeach
                                                        </span>
                                                    </div>
                                                </div>
                                                <hr class="uk-grid-divider">
                                                <div class="uk-grid uk-grid-small">
                                                    <div class="uk-width-large-1-3">
                                                        <span class="uk-text-muted uk-text-small">{{trans('main.Compound')}}</span>
                                                    </div>
                                                    <div class="uk-width-large-2-3">
                                                        <span class="uk-text-large uk-text-middle">
                                                            @if($request->is_compound == 1)
                                                            Yes
                                                            @else
                                                            No
                                                            @endif
                                                        </span>
                                                    </div>
                                                </div>
                                                <hr class="uk-grid-divider">
                                                <p>
                                                    <span class="uk-text-muted uk-text-large uk-display-block uk-margin-small-bottom">Features</span>
                                                    <?php $features = RequestFeature::whereRequest_id($request->id)->get(); //explode("|" , $request->features); ?>
                                                    @foreach(explode("|" , $request->features) as $feature)
                                                    <span class="uk-badge uk-badge-success">{{(new Feature())->getFeature($feature)}}</span>
                                                    @endforeach
                                                </p>
                                                <hr class="uk-grid-divider uk-hidden-large">
                                            </div>
                                            <div class="uk-width-large-1-2">

                                                <div class="uk-grid uk-grid-small">
                                                    <div class="uk-width-large-1-3">
                                                        <span class="uk-text-muted uk-text-small">{{trans('main.Price_from')}}</span>
                                                    </div>
                                                    <div class="uk-width-large-2-3">
                                                        <span class="uk-text-large uk-text-middle">{{$request->price_from}}</span>
                                                    </div>
                                                </div>
                                                <hr class="uk-grid-divider">
                                                <div class="uk-grid uk-grid-small">
                                                    <div class="uk-width-large-1-3">
                                                        <span class="uk-text-muted uk-text-small">{{trans('main.Price_to')}}</span>
                                                    </div>
                                                    <div class="uk-width-large-2-3">

                                                        <span class="uk-text-large uk-text-middle">{{$request->price_to}}</span>

                                                    </div>
                                                </div>

                                                <hr class="uk-grid-divider">
                                                <div class="uk-grid uk-grid-small">
                                                    <div class="uk-width-large-1-3">
                                                        <span class="uk-text-muted uk-text-small">{{trans('main.Area')}}</span>
                                                    </div>
                                                    <div class="uk-width-large-2-3">
                                                        <span class="uk-text-large uk-text-middle">{{$request->area}}</span>
                                                    </div>
                                                </div>

                                                <hr class="uk-grid-divider">
                                                <div class="uk-grid uk-grid-small">
                                                    <div class="uk-width-large-1-3">
                                                        <span class="uk-text-muted uk-text-small">{{trans('main.Floor')}}</span>
                                                    </div>
                                                    <div class="uk-width-large-2-3">
                                                        <span class="uk-text-large uk-text-middle">
                                                            <?php $floors = RequestFloor::whereRequest_id($request->id)->get(); //explode("|" , $request->floor); ?>
                                                            @foreach($floors as $key => $floor)
                                                            {{(new Floor())->getFloor($floor->floor_id)}}
                                                            @if(isset($floors[$key + 1]))
                                                            -
                                                            @endif
                                                            @endforeach
                                                        </span>
                                                    </div>
                                                </div>
                                                <hr class="uk-grid-divider">
                                                <div class="uk-grid uk-grid-small">
                                                    <div class="uk-width-large-1-3">
                                                        <span class="uk-text-muted uk-text-small">{{trans('main.Rooms_No')}}</span>
                                                    </div>
                                                    <div class="uk-width-large-2-3">
                                                        <span class="uk-text-large uk-text-middle">{{$request->rooms_no}}</span>
                                                    </div>
                                                </div>
                                                <hr class="uk-grid-divider">
                                                <div class="uk-grid uk-grid-small">
                                                    <div class="uk-width-large-1-3">
                                                        <span class="uk-text-muted uk-text-small">{{trans('main.Toilets_No')}}</span>
                                                    </div>
                                                    <div class="uk-width-large-2-3">
                                                        <span class="uk-text-large uk-text-middle">{{$request->toilets_no}}</span>
                                                    </div>
                                                </div>

                                                <hr class="uk-grid-divider">
                                                <div class="uk-grid uk-grid-small">
                                                    <div class="uk-width-large-1-3">
                                                        <span class="uk-text-muted uk-text-small">{{trans('main.Payment_style')}}</span>
                                                    </div>
                                                    <div class="uk-width-large-2-3">
                                                        <span class="uk-text-large uk-text-middle">
                                                            <?php $payment_styles = RequestPayment::whereRequest_id($request->id)->get(); //explode("|" , $request->payment_style); ?>
                                                            @foreach($payment_styles as $key => $payment_style)
                                                            {{(new PaymentType())->getPaymentStyle($payment_style->payment_id)}}
                                                            @if(isset($payment_styles[$key + 1]))
                                                            -
                                                            @endif
                                                            @endforeach
                                                        </span>
                                                    </div>
                                                </div>

                                                <hr class="uk-grid-divider">
                                                <div class="uk-grid uk-grid-small">
                                                    <div class="uk-width-large-1-3">
                                                        <span class="uk-text-muted uk-text-small">{{trans('main.Objective')}}</span>
                                                    </div>
                                                    <div class="uk-width-large-2-3">
                                                        <span class="uk-text-large uk-text-middle">{{(new Objective())->getObjective($request->objective)}}</span>
                                                    </div>
                                                </div>
                                                <hr class="uk-grid-divider">
                                                <p>
                                                    <span class="uk-text-muted uk-text-small uk-display-block uk-margin-small-bottom">{{trans('main.Description')}}</span>
                                                    {{$customer->notes}}
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        @include('general.add_notice')
        
@stop
@section('scripts')
<script src="{{ URL::asset('js/general_handler.js') }}"></script>
<script src="{{ URL::asset('js/customer_handler.js') }}"></script>
<script src="{{ URL::asset('js/customers/filter.js') }}"></script>
<script src="{{ URL::asset('js/customers/customer-profile.js') }}"></script>
<script src="{{ URL::asset('js/customers/owner-profile.js') }}"></script>
<script src="{{ URL::asset('js/customers/routes.js') }}"></script>
<script type="text/javascript">
var base_url = "{{ URL::to('/') }}";
var msg_id   = 0;
var property_id;
altair_wizard.advanced_wizard();
</script>
@stop



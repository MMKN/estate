 <!-- Begin delete property box -->
    <article id="delete_box" class="uk-width-medium-2-5 uk-text-center uk-container-center"style="display:none;">
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid">
                    <h3 class="uk-width-medium-1-1- uk-margin">{{trans('main.Are_you_sure_delete_property')}}</h3>
                    <div class="uk-width-medium-1-2 uk-margin">
                        <a class="md-btn md-btn-danger" id="delete_button">{{trans('main.Delete')}}</a>
                    </div>
                    <div class="uk-width-medium-1-2 uk-margin">
                        <a class="md-btn md-btn-success go_back">{{trans('main.Cancel')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </article>

    <!-- add new property form end-->
    <article class="uk-notify uk-notify-bottom-center" style="display:none;" id="notification_box"></article>
<!-- / -->

$(document).on('click', '.get_view_box_customer', function() {
    property_id = $(this).prop('id');
    $.ajax({
        type: 'GET',
        url: base_url + '/properties/property',
        data: {
            id: property_id
        },
        success: function(data) {
            $('#profile_box').html(data['property']);
            // altair_md.card_show_hide($("#main_index"), void 0, function() {
            //     $("#profile_box").show().siblings().hide();
            // }, void 0);
            $("#profile_box").slideDown();
            $("#profile_box").siblings().slideUp();
            setTimeout(initMap, 1000);
            $('#call_to_action').fadeIn("280");
            $('#first_icon').fadeOut("280");
        }
    });
});

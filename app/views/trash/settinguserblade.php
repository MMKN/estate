<!-- user management -->
                                                <!--
                                                <li class="uk-margin-small-top">
                                                    <div class="uk-input-group uk-width-1-1">
                                                        <h2 class="full_width_in_card heading_c uk-margin-medium-bottom">
                                                        <div class="uk-input-group-addon">{{trans('main.Manage_system_users')}}</div>
                                                        <div class="uk-input-group-addon">
                                                            <a class="md-btn md-btn-primary" data-uk-modal="{target:'#modal_add'}"><i class="material-icons md-36 md-light uk-text-middle">&#xE145;</i> &nbsp;&nbsp;{{trans('main.add_new')}}</a>
                                                        </div>
                                                        </h2>
                                                    </div>
                                                    <div class="uk-overflow-container">
                                                        <table class="uk-table uk-table-hover uk-table-nowrap">
                                                            <tbody id="users_table">
                                                                @foreach($users as $user)
                                                                <tr>
                                                                    <td>
                                                                        <div class="uk-grid" data-uk-grid-margin>
                                                                            <div class="uk-width-1-5 uk-text-center">
                                                                                <img class="md-user-image-large" src="uploads/profile/{{$user->image}}" alt=""/>
                                                                            </div>
                                                                            <div class="uk-width-3-5">
                                                                                <h4 class="heading_a uk-margin-small-bottom"> {{ $user->name }}</h4>
                                                                                <p class="uk-margin-remove"><span class="uk-text-muted"></span> {{ $user->mobile }}</p>
                                                                                <p class="uk-margin-remove"><span class="uk-text-muted"></span> {{ $user->email }}</p>
                                                                            </div>
                                                                            <div class="uk-width-1-5 uk-margin">
                                                                                <a name="{{ $user->id }}" class="get_edit_modal"   data-uk-modal="{target:'#modal_edit'}"><i class="md-icon md-36 material-icons uk-text-success">&#xE254;</i></a>
                                                                                <a name="{{ $user->id }}" class="get_delete_modal" data-uk-modal="{target:'#modal_delete'}"><i class="md-icon material-icons uk-margin-large-left md-36 uk-text-danger">delete</i></a>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </li>
                                                -->
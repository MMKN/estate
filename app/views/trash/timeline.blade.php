<div class="timeline_item" id="entry_{{$ct->id}}">
    <a class="timeline_icon timeline_icon_danger" onclick="msg_id = {{$ct->id}} ;$('#edit_message').val('{{$ct->message}}');" data-uk-modal="{target:'#modal-timeline'}"><i class="material-icons md-36">cancel</i></a>
    <div class="timeline_date">
        {{date('d' , strtotime($ct->created_at))}} <span>{{date('M' , strtotime($ct->created_at))}}</span>
    </div>
    <div class="timeline_content" id="msg_{{$ct->id}}">
        {{$ct->message}}
    </div>
</div>
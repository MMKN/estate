 <div class="uk-width-small-1-2 uk-width-medium-1-4 uk-width-large-1-5 uk-width-small-1-2 uk-margin-medium-bottom">
                <div onclick="getProfile({{$customer->id}})" data-uk-filter="  ,Sherif mohamed 01010002000 sherif@minionz.net">
                    <div class="md-card md-card-hover" style="cursor:pointer;">
                        <div class="md-card-head md-bg-light-blue-600">
                            <div class="uk-text-center">
                                <img class="md-card-head-avatar" src="assets/img/avatars/avatar.png" alt="" />
                            </div>
                            <h3 class="md-card-head-text uk-text-center md-color-white">
                            {{ $customer->name }}
                            </h3>
                        </div>
                        <div class="md-card-content">
                            <ul class="md-list md-list-addon">
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons"></i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">{{trans('main.Mobile_main')}}</span>
                                        <span class="uk-text-small uk-text-muted">{{ $customer->mobile_1 }}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons"></i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">{{trans('main.Email')}}</span>
                                        <span class="uk-text-small uk-text-muted uk-text-truncate">{{ $customer->email }}</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
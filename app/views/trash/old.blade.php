@extends('_layouts.core')
    @section('notification_header')
    <div id="page_heading" data-uk-sticky="{ top: 0, media: 960 }">
        

        <div class="heading_actions">

            <div class="md-top-bar-actions-right">
                <div class="md-top-bar-icons">
                    <i id="mailbox_list_split" class=" md-icon material-icons">&#xE8EE;</i>
                    <i id="mailbox_list_combined" class="md-icon material-icons">&#xE8F2;</i>
                </div>
            </div>
            <a style="display:none;" href="#" data-uk-tooltip="{pos:'bottom'}" title="Archive"><i class="md-icon material-icons">&#xE149;</i></a>
            <div data-uk-dropdown style="display:none;">
                <i class="md-icon material-icons">&#xE5D4;</i>
                <div class="uk-dropdown uk-dropdown-small">
                    <ul class="uk-nav">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Other Action</a></li>
                        <li><a href="#">Other Action</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <form id="filter_form">
            <div class="uk-grid">
                
                <h1 class=" uk-width-2-10 " style="margin-top:8px;"><i class="md-icon material-icons uk-margin-right">&#xE7F4;</i>Notifications</h1>
               <!--  <div class="uk-width-2-10 uk-tab-center " style="border-bottom:0px">
                    <ul class="uk-subnav uk-subnav-pill uk-margin-remove">
                        <li class="uk-active"><a href="#">All</a></li>
                        <li><a href="#">Call requests</a></li>
                        <li><a href="#">Customers</a></li>
                    </ul>
                </div> -->


                <div class="uk-width-medium-2-10">
                    <div class="parsley-row  uk-margin-small-top">
                        <select id="status" name="status" required data-md-selectize>
                            <option value="" disabled selected>All</option>
                            <option value="1">Read</option>
                            <option value="0">Unread</option>

                        </select>
                    </div>
                </div>
                   
                <div class="uk-grid uk-width-5-10" data-uk-grid-margin>
                    <div class="uk-width-large-1-2 uk-width-medium-1-1">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                            <label for="uk_dp_start">Start Date</label>
                            <input class="md-input" type="text" id="uk_dp_start" name="start_date">
                        </div>
                    </div>
                    <div class="uk-width-large-1-2 uk-width-medium-1-1">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                            <label for="uk_dp_end">End Date</label>
                            <input class="md-input" type="text" id="uk_dp_end" name="end_date">
                        </div>
                    </div>
                </div>
                <div class="uk-width-medium-5-10" style="display:none;">
                    <div class="uk-input-group">
                       <span class="uk-input-group-addon">
                           <a href="#"><i class="material-icons">search</i></a>
                       </span>
                        <label for="contact_list_search">Search for anything!</label>
                        <input class="md-input" type="text"/>
                       
                    </div>

                   
                </div> 
                    
                
            </div>
        </form>
        
    </div>
    @stop
    @section('content')
    
    
    <div id="page_content">
        <div id="page_content_inner">

            <div class="md-card-list-wrapper" id="mailbox">
                <div class="uk-width-large-8-10 uk-container-center notification_container">

                    <div class="md-card-list">
                        <div class="md-card-list-header heading_list">Today</div>
                        <div class="md-card-list-header md-card-list-header-combined heading_list" style="display: none">All Messages</div>
                        <ul class="hierarchical_slide">
                        @if(count($today))
                            @foreach($today as $notification)
                                <li onclick="changeID(this , {{ $notification->id }})">
                                    {{-- <div class="md-card-list-item-menu" class="notification_button">
                                        <a onclick="done( '{{ $notification->user_id }}' , {{ $notification->property_id }})" class="md-icon material-icons">done</a> --}}
                                    <div class="md-card-list-item-menu" id="notification_button">
                                        @if($notification->done == 0)
                                        <a onclick="done( '{{ $notification->user_id }}' , {{ $notification->property_id }},this)" class="md-icon material-icons">done</a>
                                        @else
                                        <a onclick="undone( '{{ $notification->user_id }}' , {{ $notification->property_id }},this)" class="md-icon material-icons">clear</a>
                                        @endif
                                    </div>
                                    <span class="md-card-list-item-date " style="width:60px;"> {{ date("j M" , strtotime($notification->created_at)) }} </span>
                                    <div class="notification_status"> {{ $notification->getStatus($notification->done) }} </div>
                                    <div class="md-card-list-item-select">
                                        <input type="checkbox" class="toggle_icon" data-md-icheck/>
                                    </div>
                                    <!-- <div class="md-card-list-item-avatar-wrapper">
                                        <img src="assets/img/avatars/avatar_01_tn.png" class="md-card-list-item-avatar" alt="" />
                                    </div> -->
                                    <div class="md-card-list-item-sender">
                                        <span>{{ $notification->getUserName($notification->user_id) }}</span>
                                    </div>
                                    <div class="md-card-list-item-subject">
                                        <div class="md-card-list-item-sender-small">
                                            <span>{{ $notification->getUserName($notification->user_id) }}</span>
             
                                        {{-- <span>Requested a Call</span> --}}
                                        <span>{{ $notification->getPhoneNumber($notification->user_id) }}</span>
                                    </div>
                                    <div class="md-card-list-item-content-wrapper">
                                        <!-- here goes description if exist  -->
                                        <div class="md-card-list-item-content">

                                            <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-large-2-5 uk-text-center uk-margin-small-top" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
                                                 <div style="padding-bottom:27px;">
                                                    <a href="{{ URL::route('properties.property.admin' , $notification->property_id) }}" target="_blank">
                                                        <div class="md-card md-card-hover " style="cursor:pointer;" id="{{ $notification->property_id }}">
                                                            <div class="md-card-head head_background" style="background-image: url('{{ (new Property())->getMainImage($notification->property_id) }}')">
                                                                <div class="md-card-head-menu" data-uk-dropdown="" aria-haspopup="true" aria-expanded="false">

                                                                </div>
                                                                <h2 class="md-card-head-text">
                                                                    {{ (new Property())->getUnitType($notification->property_id) }}
                                                                </h2>
                                                                <div class="md-card-head-subtext">
                                                                    <span>{{(new Property())->getPrice($notification->property_id)}}&#163;</span>
                                                                </div>
                                                            </div>
                                                            <div class="md-card-content">
                                                                <ul class="md-list md-list-addon">
                                                                    <li>
                                                                        <div class="md-list-addon-element">
                                                                            <i class="material-icons md-36 uk-text-primary ">&#xE0C8;</i>
                                                                        </div>
                                                                        <div class="md-list-content">
                                                                            <span class="md-list-heading">{{ (new Property())->city($notification->property_id)}}</span>
                                                                            <span class="uk-text-small uk-text-muted">{{ (new Property())->neighbour($notification->property_id) }}</span>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="md-card-list-item-content">
                                           here is a timeline with any updates regarding the notification
                                           <div class="timeline">
                                               
                                           </div>                         
                                       </div>
                                       <form class="md-card-list-item-reply uk-container-center ">
                                           <label for="mailbox_reply_7254">Add update</label>
                                           <textarea class="md-input md-input-full note_area" cols="30" rows="1"></textarea>
                                           <button class="md-btn md-btn-flat md-btn-flat-primary" onclick="addNote(this)"> Save </button>
                                       </form>
                                    </div>
                                </li>
                            @endforeach
                        @else
                            <li> <h5 style="text-align:center; margin:0; padding:0;"> No Notifications For Today </h5> </li>
                        @endif
                        </ul>
                    </div>


                    <div class="md-card-list">
                        <div class="md-card-list-header heading_list">Yesterday</div>
                        <ul class="hierarchical_slide">
                            @if(count($yesterday))
                                @foreach($yesterday as $notification)
                                    <li onclick="changeID(this , {{ $notification->id }})">
                                       <div class="md-card-list-item-menu" >
                                          @if($notification->done == 0)
                                          <a onclick="done( '{{ $notification->user_id }}' , {{ $notification->property_id }},this)" class="md-icon material-icons">done</a>
                                          @else
                                          <a onclick="undone( '{{ $notification->user_id }}' , {{ $notification->property_id }},this)" class="md-icon material-icons">clear</a>
                                          @endif
                                      </div>
                                       <span class="md-card-list-item-date " style="width:60px;">{{ date("j M" , strtotime($notification->created_at)) }}</span>
                                       {{ $notification->getStatus($notification->done) }}
                                       <div class="md-card-list-item-select">
                                           <input type="checkbox" class="toggle_icon" data-md-icheck />
                                       </div>
                                       <!-- <div class="md-card-list-item-avatar-wrapper">
                                           <img src="assets/img/avatars/avatar_01_tn.png" class="md-card-list-item-avatar" alt="" />
                                       </div> -->
                                       <div class="md-card-list-item-sender">
                                           <span>{{ $notification->getUserName($notification->user_id) }}</span>
                                       </div>
                                       <div class="md-card-list-item-subject">
                                           <div class="md-card-list-item-sender-small">
                                               <span>{{ $notification->getUserName($notification->user_id) }}</span>
                                           </div>
                                           {{-- <span>Requested a Call</span> --}}
                                           <span>{{ $notification->getPhoneNumber($notification->user_id) }}</span>
                                       </div>
                                       <div class="md-card-list-item-content-wrapper">
                                           <!-- here goes description if exist  -->
                                           <div class="md-card-list-item-content">
                                               <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-large-2-5 uk-text-center uk-margin-small-top" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
                                                 <div style="padding-bottom:27px;">
                                                    <a href="{{ URL::route('properties.property.admin' , $notification->property_id) }}" target="_blank">
                                                        <div class="md-card md-card-hover " style="cursor:pointer;" id="{{ $notification->property_id }}">
                                                            <div class="md-card-head head_background" style="background-image: url('{{ (new Property())->getMainImage($notification->property_id) }}')">
                                                                <div class="md-card-head-menu" data-uk-dropdown="" aria-haspopup="true" aria-expanded="false">

                                                                </div>
                                                                <h2 class="md-card-head-text">
                                                                    {{ (new Property())->getUnitType($notification->property_id) }}
                                                                </h2>
                                                                <div class="md-card-head-subtext">
                                                                    <span>{{(new Property())->getPrice($notification->property_id)}}&#163;</span>
                                                                </div>
                                                            </div>
                                                            <div class="md-card-content">
                                                                <ul class="md-list md-list-addon">
                                                                    <li>
                                                                        <div class="md-list-addon-element">
                                                                            <i class="material-icons md-36 uk-text-primary ">&#xE0C8;</i>
                                                                        </div>
                                                                        <div class="md-list-content">
                                                                            <span class="md-list-heading">{{ (new Property())->city($notification->property_id)}}</span>
                                                                            <span class="uk-text-small uk-text-muted">{{ (new Property())->neighbour($notification->property_id) }}</span>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                           </div>
                                           <div class="md-card-list-item-content">
                                               here is a timeline with any updates regarding the notification
                                               <div class="timeline">
                                                   
                                               </div>                         
                                           </div>
                                           <form class="md-card-list-item-reply uk-container-center ">
                                               <label for="mailbox_reply_7254">Add update</label>
                                               <textarea class="md-input md-input-full note_area" cols="30" rows="1"></textarea>
                                               <button class="md-btn md-btn-flat md-btn-flat-primary" onclick="addNote(this)">Save</button>
                                           </form>
                                       </div>
                                    </li>
                                @endforeach
                            @else
                                <li> <h5 style="text-align:center; margin:0; padding:0;"> No Notifications For Yesterday </h5> </li>
                            @endif
                        </ul>
                    </div>


                    <div class="md-card-list">
                        <div class="md-card-list-header heading_list">This Month</div>
                        <ul class="hierarchical_slide">
                        @if(count($month))
                            @foreach($month as $notification)
                                <li>
                                    <div class="md-card-list-item-menu" >
                                        @if($notification->done == 0)
                                        <a onclick="done( '{{ $notification->user_id }}' , {{ $notification->property_id }},this)" class="md-icon material-icons">done</a>
                                        @else
                                        <a onclick="undone( '{{ $notification->user_id }}' , {{ $notification->property_id }},this)" class="md-icon material-icons">clear</a>
                                        @endif
                                    </div>
                                    <span class="md-card-list-item-date " style="width:60px;">{{ date("j M" , strtotime($notification->created_at)) }}</span>
                                    {{ $notification->getStatus($notification->done) }}
                                    <div class="md-card-list-item-select">
                                        <input type="checkbox" class="toggle_icon" data-md-icheck />
                                    </div>
                                    <!-- <div class="md-card-list-item-avatar-wrapper">
                                        <img src="assets/img/avatars/avatar_01_tn.png" class="md-card-list-item-avatar" alt="" />
                                    </div> -->
                                    <div class="md-card-list-item-sender">
                                        <span>{{ $notification->getUserName($notification->user_id) }}</span>
                                    </div>
                                    <div class="md-card-list-item-subject">
                                        <div class="md-card-list-item-sender-small">
                                            <span>{{ $notification->getUserName($notification->user_id) }}</span>
                                        </div>
                                        {{-- <span>Requested a Call</span> --}}
                                        <span>{{ $notification->getPhoneNumber($notification->user_id) }}</span>
                                    </div>
                                    <div class="md-card-list-item-content-wrapper">
                                        <!-- here goes description if exist  -->
                                        <div class="md-card-list-item-content">
                                           <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-large-2-5 uk-text-center uk-margin-small-top" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
                                                 <div style="padding-bottom:27px;">
                                                    <a href="{{ URL::route('properties.property.admin' , $notification->property_id) }}" target="_blank">
                                                        <div class="md-card md-card-hover " style="cursor:pointer;" id="{{ $notification->property_id }}">
                                                            <div class="md-card-head head_background" style="background-image: url('{{ (new Property())->getMainImage($notification->property_id) }}')">
                                                                <div class="md-card-head-menu" data-uk-dropdown="" aria-haspopup="true" aria-expanded="false">

                                                                </div>
                                                                <h2 class="md-card-head-text">
                                                                    {{ (new Property())->getUnitType($notification->property_id) }}
                                                                </h2>
                                                                <div class="md-card-head-subtext">
                                                                    <span>{{(new Property())->getPrice($notification->property_id)}}&#163;</span>
                                                                </div>
                                                            </div>
                                                            <div class="md-card-content">
                                                                <ul class="md-list md-list-addon">
                                                                    <li>
                                                                        <div class="md-list-addon-element">
                                                                            <i class="material-icons md-36 uk-text-primary ">&#xE0C8;</i>
                                                                        </div>
                                                                        <div class="md-list-content">
                                                                            <span class="md-list-heading">{{ (new Property())->city($notification->property_id)}}</span>
                                                                            <span class="uk-text-small uk-text-muted">{{ (new Property())->neighbour($notification->property_id) }}</span>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="md-card-list-item-content">
                                           here is a timeline with any updates regarding the notification
                                           <div class="timeline">
                                               
                                           </div>                         
                                       </div>
                                       <form class="md-card-list-item-reply uk-container-center ">
                                           <label for="mailbox_reply_7254">Add update</label>
                                           <textarea class="md-input md-input-full note_area" cols="30" rows="1"></textarea>
                                           <button class="md-btn md-btn-flat md-btn-flat-primary" onclick="addNote(this)">Save</button>
                                       </form>
                                    </div>
                                </li>
                            @endforeach
                        @else
                            <h1>No For Month</h1>
                        @endif
                        </ul>
                    </div>


                    <div class="md-card-list">
                        <div class="md-card-list-header heading_list">Older Messages</div>
                        <ul class="hierarchical_slide">
                        @if(count($notifications))
                            @foreach($notifications as $notification)
                                <li>
                                    <div class="md-card-list-item-menu" >
                                        @if($notification->done == 0)
                                        <a onclick="done( '{{ $notification->user_id }}' , {{ $notification->property_id }},this)" class="md-icon material-icons">done</a>
                                        @else
                                        <a onclick="undone( '{{ $notification->user_id }}' , {{ $notification->property_id }},this)" class="md-icon material-icons">clear</a>
                                        @endif
                                    </div>
                                    <span class="md-card-list-item-date " style="width:60px;">{{ date("j M" , strtotime($notification->created_at)) }}</span>
                                    {{ $notification->getStatus($notification->done) }}
                                    <div class="md-card-list-item-select">
                                        <input type="checkbox" class="toggle_icon" data-md-icheck />
                                    </div>
                                   {{--  <div class="md-card-list-item-avatar-wrapper">
                                        <img src="assets/img/avatars/avatar_01_tn.png" class="md-card-list-item-avatar" alt="" />
                                    </div> --}}
                                    <div class="md-card-list-item-sender">
                                        <span>{{ $notification->getUserName($notification->user_id) }}</span>
                                    </div>
                                    <div class="md-card-list-item-subject">
                                        <div class="md-card-list-item-sender-small">
                                            <span>{{ $notification->getUserName($notification->user_id) }}</span>
                                        </div>
                                        {{-- <span>Requested a Call</span> --}}
                                        <span>{{ $notification->getPhoneNumber($notification->user_id) }}</span>
                                    </div>
                                    <div class="md-card-list-item-content-wrapper">
                                        <!-- here goes description if exist  -->
                                        <div class="md-card-list-item-content">
                                            <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-large-2-5 uk-text-center uk-margin-small-top" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
                                                 <div style="padding-bottom:27px;">
                                                    <a href="{{ URL::route('properties.property.admin' , $notification->property_id) }}" target="_blank">
                                                        <div class="md-card md-card-hover " style="cursor:pointer;" id="{{ $notification->property_id }}">
                                                            <div class="md-card-head head_background" style="background-image: url('{{ (new Property())->getMainImage($notification->property_id) }}')">
                                                                <div class="md-card-head-menu" data-uk-dropdown="" aria-haspopup="true" aria-expanded="false">

                                                                </div>
                                                                <h2 class="md-card-head-text">
                                                                    {{ (new Property())->getUnitType($notification->property_id) }}
                                                                </h2>
                                                                <div class="md-card-head-subtext">
                                                                    <span>{{(new Property())->getPrice($notification->property_id)}}&#163;</span>
                                                                </div>
                                                            </div>
                                                            <div class="md-card-content">
                                                                <ul class="md-list md-list-addon">
                                                                    <li>
                                                                        <div class="md-list-addon-element">
                                                                            <i class="material-icons md-36 uk-text-primary ">&#xE0C8;</i>
                                                                        </div>
                                                                        <div class="md-list-content">
                                                                            <span class="md-list-heading">{{ (new Property())->city($notification->property_id)}}</span>
                                                                            <span class="uk-text-small uk-text-muted">{{ (new Property())->neighbour($notification->property_id) }}</span>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="md-card-list-item-content">
                                           here is a timeline with any updates regarding the notification
                                           <div class="timeline">
                                               
                                           </div>                         
                                       </div>
                                       <form class="md-card-list-item-reply uk-container-center ">
                                           <label for="mailbox_reply_7254">Add update</label>
                                           <textarea class="md-input md-input-full note_area" cols="30" rows="1"></textarea>
                                           <button class="md-btn md-btn-flat md-btn-flat-primary" onclick="addNote(this)">Save</button>
                                       </form>
                                    </div>
                                </li>
                            @endforeach
                        @else
                            <li> <h5 style="text-align:center; margin:0; padding:0;"> No Notifications For Today </h5> </li>
                        @endif
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>



    

    <div class="md-fab-wrapper" style="display:none;">
        <a class="md-fab md-fab-accent" href="#mailbox_new_message" data-uk-modal="{center:true}">
            <i class="material-icons">add</i>
        </a>
    </div>

    <div class="uk-modal" id="mailbox_new_message">
        <div class="uk-modal-dialog">
            <button class="uk-modal-close uk-close" type="button"></button>
            <form>
                <div class="uk-modal-header">
                    <h3 class="uk-modal-title">Compose Message</h3>
                </div>
                <div class="uk-margin-medium-bottom">
                    <label for="mail_new_to">To</label>
                    <input type="text" class="md-input" id="mail_new_to"/>
                </div>
                <div class="uk-margin-large-bottom">
                    <label for="mail_new_message">Message</label>
                    <textarea name="mail_new_message" id="mail_new_message" cols="30" rows="6" class="md-input"></textarea>
                </div>
                <div id="mail_upload-drop" class="uk-file-upload">
                    <p class="uk-text">Drop file to upload</p>
                    <p class="uk-text-muted uk-text-small uk-margin-small-bottom">or</p>
                    <a class="uk-form-file md-btn">choose file<input id="mail_upload-select" type="file"></a>
                </div>
                <div id="mail_progressbar" class="uk-progress uk-hidden">
                    <div class="uk-progress-bar" style="width:0">0%</div>
                </div>
                <div class="uk-modal-footer">
                    <a href="#" class="md-icon-btn"><i class="md-icon material-icons">&#xE226;</i></a>
                    <button type="button" class="uk-float-right md-btn md-btn-flat md-btn-flat-primary">Send</button>
                </div>
            </form>
        </div>
    </div>
    <!-- google web fonts -->
@section('scripts')
    <script>
        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>

    <!-- common functions -->
    <script src="../assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="../assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="../assets/js/altair_admin_common.min.js"></script>

    <!-- page specific plugins -->

    <!--  mailbox functions -->
    <script src="{{ URL::asset('assets/js/pages/page_mailbox.min.js')}}"></script>
    <!--  forms advanced functions -->
    <script src="{{ URL::asset('assets/js/pages/forms_advanced.min.js')}}"></script>
    
    <!-- page specific plugins -->
    <!-- ionrangeslider -->
    <script src="../bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>


    <script>
    var base_url = '{{ URL::route("notifications.index") }}';
        $(function() {
            // enable hires images
            altair_helpers.retina_images();
            // fastClick (touch devices)
            if(Modernizr.touch) {
                FastClick.attach(document.body);
            }
        });
    </script>
<script type="text/javascript" src="{{ URL::asset('js/notifications.js') }}"></script>
@stop
@stop
</body>
</html>
<!-- side content-->
<aside class="uk-width-large-3-10">
    <div class="md-card">
        <div class="md-card-content">
            <h3 class="heading_c">{{trans('main.Matching_Customers')}}</h3>
            @if(count($exactCustomers))
            <ul class="md-list md-list-addon uk-margin-bottom">
                @foreach($exactCustomers as $exactCustomer)
                <li>
                    <div class="md-list-addon-element">
                        <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/user.png" alt=""/>
                    </div>
                    <div class="md-list-content">
                        <span class="md-list-heading"><a target="_blank" href="{{URL::to('/')}}/profile-page/{{ $exactCustomer->id }}">{{ $exactCustomer->name }}</a></span>
                        <span class="uk-text-small uk-texttpust-muted">{{ $exactCustomer->mobile_1 }}</span>
                    </div>
                </li>
                @endforeach
            </ul>
            @else
            <span class="uk-text-small uk-text-muted">{{trans('main.no_match')}}</span>
            @endif
            <h3 class="heading_c">{{trans('main.Similar_Customers')}}</h3>
            @if(count($suggestedCustomers))
            <ul class="md-list md-list-addon uk-margin-bottom">
                @foreach($suggestedCustomers as $suggestedCustomer)
                <li>
                    <div class="md-list-addon-element">
                        <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/user.png" alt=""/>
                    </div>
                    <div class="md-list-content">
                        <span class="md-list-heading"><a target="_blank" href="{{ URL::to('/')}}/profile-page/{{$suggestedCustomer->id }}">{{ $suggestedCustomer->name }}</a></span>
                        <span class="uk-text-small uk-text-muted">{{ $suggestedCustomer->mobile_1 }}</span>
                    </div>
                </li>
                @endforeach
            </ul>
            @else
            <span class="uk-text-small uk-text-muted">{{trans('main.no_match')}}</span>
            @endif
            <h3 class="heading_c">{{trans('main.Price_Customers')}}</h3>
            @if(count($priceCustomers))
            <ul class="md-list md-list-addon uk-margin-bottom">
                @foreach($priceCustomers as $priceCustomer)
                <li>
                    <div class="md-list-addon-element">
                        <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/user.png" alt=""/>
                    </div>
                    <div class="md-list-content">
                        <span class="md-list-heading"><a target="_blank" href="{{ URL::to('/')}}/profile-page/{{$priceCustomer->id }}">{{ $priceCustomer->name }}</a></span>
                        <span class="uk-text-small uk-text-muted">{{ $priceCustomer->mobile_1 }}</span>
                    </div>
                </li>
                @endforeach
            </ul>
            @else
            <span class="uk-text-small uk-text-muted">{{trans('main.no_match')}}</span>
            @endif
            <!--Use it when no data is found-->
            <!-- <p class="uk-text-primary">{{trans('main.No_data_found')}}</p> -->
        </div>
    </div>
</aside>
    <!-- secondary sidebar end -->
    <div class="uk-modal" id="new_issue_property">
        <div class="uk-modal-dialog">
            <form class="uk-form-stacked">
                <div class="uk-margin-medium-bottom">
                    <label for="task_description">{{trans('main.Description')}}</label>
                    <textarea class="md-input" id="task_description" name="body" required></textarea>
                </div>
                <div class="uk-margin-medium-bottom">
                    <label for="task_assignee" class="uk-form-label">{{trans('main.Assignee')}}</label>
                    <select class="uk-form-width-medium" name="assignee" id="assignee" required data-md-selectize-inline>
                        @foreach($admins as $admin)
                        <option value="{{$admin->id}}">{{$admin->name}}</option>
                        @endforeach
                    </select>
                </div>
                
                
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">{{trans('main.Close')}}</button>
                    <button type="button" class="md-btn md-btn-flat md-btn-flat-primary" onclick="saveNoticeProprty(this)" id="s">{{trans('main.Add')}}</button>
                </div>
            </form>
        </div>
    </div>
@if ($paginator->getLastPage() > 1)
 
    <ul class="uk-pagination">
    <li class="{{ ($paginator->getCurrentPage() == 1) ? ' uk-disabled' : '' }}"><a onclick="getPage(this , 1)" ><i class="uk-icon-angle-double-left"></i></a></li>
    {{ ($paginator->getCurrentPage() > 3?"<li>...</li>":"") }}
    @for ($i = ($paginator->getCurrentPage() > 3?$paginator->getCurrentPage() - 2:1); $i <= ($paginator->getCurrentPage() < $paginator->getLastPage()- 2?$paginator->getCurrentPage() + 2:$paginator->getLastPage()); $i++)
    <li class="{{ ($paginator->getCurrentPage() == $i) ? ' uk-active' : '' }}"><a onclick="getPage(this , '{{ $i }}')">{{ $i }}</a></li>
    @endfor
    {{ ($paginator->getCurrentPage() < $paginator->getLastPage()- 2?"<li>...</li>":"") }}
    <li class="{{ ($paginator->getCurrentPage() == $paginator->getLastPage()) ? ' uk-disabled' : '' }}"><a onclick="getPage(this , {{$paginator->getLastPage() }})" ><i class="uk-icon-angle-double-right"></i></a></li>
    </ul>
 
@endif

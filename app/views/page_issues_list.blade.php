<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="16x16">

    <title>Semsar Online</title>


    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- matchMedia polyfill for testing media queries in JS -->
    <!--[if lte IE 9]>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.js"></script>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.addListener.js"></script>
    <![endif]-->

</head>
<body class=" ">
    <!-- main header -->
    <header id="header_main">
        <div class="header_main_content">
            <nav class="uk-navbar">
                <div class="main_logo_top">
                    <a href="index.html"><img src="assets/img/white.png" alt="" height="15" width="71"/></a>
                </div>
                <!-- if the user is not logged in this button will appear and once he loged in it should disappear -->
                <div class="uk-navbar-flip" style="display:none">
                    <!-- register button begin -->
                    <a class="md-btn md-btn-warning" data-uk-modal="{target:'#register_modal'}" href="#" style="margin-top: 6px;"><i class="material-icons md-24 md-light">person</i> Register / Login with facebook</a>
                    <!-- register model begin -->
                    <div class="uk-modal" id="register_modal">
                        <div class="uk-modal-dialog">
                            <button type="button" class="uk-modal-close uk-close"></button>
                            <p>There will be a button here bta3 el fb login / register</p>
                        </div>
                    </div>

                </div>            
                <div class="uk-navbar-flip">
                    <a class="md-btn md-btn-warning" href="#" style="margin-top: 6px;"><i class="material-icons md-24 md-light">person</i> My account</a>


                </div>            
                <!-- <div class="uk-navbar-flip" data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                    <a class="md-btn md-btn-warning" href="#" style="margin-top: 6px;"><i class="material-icons md-24 md-light">person</i> My account</a>

                    <div class="uk-dropdown uk-dropdown-small uk-text-center">
                        <ul class="uk-nav js-uk-prevent">
                            <li><a href="page_user_profile.html">My profile</a></li>
                            <li><a href="login.html">Logout</a></li>
                        </ul>
                    </div>

                </div> -->
                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav user_actions">
                        <li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">&#xE5D0;</i></a></li>
                        <!-- notification begin -->
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            <a href="#" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE7F4;</i><span class="uk-badge" style="background:#f00">1</span></a>
                            <div class="uk-dropdown uk-dropdown-xlarge">
                                <div class="md-card-content">
                                    <ul data-uk-tab="{connect:'#header_alerts',animation:'slide-horizontal'}">
                                    </ul>
                                    <ul id="" class="uk-margin">
                                        <li>
                                            <ul class="md-list md-list-addon">
                                                <li style="cursor:pointer">
                                                    <div class="md-list-addon-element">
                                                        <i class="material-icons md-36 uk-text-warning">new_releases</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="#">We have found you new matching properties</a></span>

                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="uk-text-center uk-margin-top uk-margin-small-bottom">
                                                <a href="#" class="md-btn md-btn-primary js-uk-prevent">Check it now</a>
                                            </div>
                                        </li> 
                                    </ul>
                                </div>
                            </div>
                        </li>

                    </ul>
                </div>

            </nav>
        </div>
        <div class="header_main_search_form">
            <i class="md-icon header_main_search_close material-icons">&#xE5CD;</i>
            <form class="uk-form">
                <input type="text" class="header_main_search_input" />
                <button class="header_main_search_btn uk-button-link"><i class="md-icon material-icons">&#xE8B6;</i></button>
            </form>
        </div>
    </header><!-- main header end -->



    <div id="page_content">
        <div id="page_heading" data-uk-sticky="{ top: 0, media: 960 }">
            
            <div class="uk-grid">
                <h3 class=" uk-width-2-10 " style="margin-top:8px;"><i class="md-icon material-icons uk-margin-right">&#xE7F4;</i>Notifications</h3 >
               <!--  <div class="uk-width-2-10 uk-tab-center " style="border-bottom:0px">
                    <ul class="uk-subnav uk-subnav-pill uk-margin-remove">
                        <li class="uk-active"><a href="#">All</a></li>
                        <li><a href="#">Call requests</a></li>
                        <li><a href="#">Customers</a></li>
                    </ul>
                </div> -->
                <div class="uk-width-medium-1-10">
                    <div class="parsley-row  uk-margin-small-top">
                        <select id="val_select" required data-md-selectize>
                            <option value="all" disabled selected>All</option>
                            <option value="call_request">Call requests</option>
                            <option value="customer_notice">Customers Notices</option>
                            <option value="Property_notice">Properties Notices</option>

                        </select>
                    </div>
                </div>
                <div class="uk-width-medium-1-10">
                    <div class="parsley-row  uk-margin-small-top">
                        <select id="val_select" required data-md-selectize>
                            <option value="all" disabled selected>Opened</option>
                            <option value="call_request">Closed</option>
                            <option value="customer_notice">All</option>

                        </select>
                    </div>
                </div>
                   
                <div class="uk-grid uk-width-4-10" data-uk-grid-margin>
                    <div class="uk-width-large-1-2 uk-width-medium-1-1">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                            <label for="uk_dp_start">Start Date</label>
                            <input class="md-input" type="text" id="uk_dp_start">
                        </div>
                    </div>
                    <div class="uk-width-large-1-2 uk-width-medium-1-1">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                            <label for="uk_dp_end">End Date</label>
                            <input class="md-input" type="text" id="uk_dp_end">
                        </div>
                    </div>
                </div>
                <div class="uk-width-medium-2-10">
                    <div class="uk-input-group">
                       <span class="uk-input-group-addon">
                           <a href="#"><i class="material-icons">search</i></a>
                       </span>
                        <label for="contact_list_search">Search for anything!</label>
                        <input class="md-input" type="text"/>
                       
                    </div>

                   
                </div> 

            </div>
        </div>
        


        <div id="page_content_inner">
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-overflow-container uk-margin-bottom">
                        <table class="uk-table uk-table-align-vertical uk-table-nowrap tablesorter tablesorter-altair" id="ts_issues">
                            <thead>
                                <tr>
                                    <th class="uk-text-center">Key</th>
                                    <th>Title</th>
                                    <th>Assignee</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <!-- if number of row displayes is less than 15 don't show footer -->
                            <!-- <tfoot>
                                <tr>
                                    <th class="uk-text-center">Key</th>
                                    <th>Title</th>
                                    <th>Assignee</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th>Status</th>
                                </tr>
                            </tfoot> -->
                            <tbody>

                                <tr>
                                    <td class="uk-text-center"><span class="uk-text-small uk-text-muted uk-text-nowrap">ac276</span></td>
                                    <td>
                                        <a href="page_issue_details.html"> Inventore ipsa vitae rerum delectus sit.</a>
                                    </td>
                                    <td>Shanie Wuckert</td>
                                    <td class="uk-text-small">14/Jun/16</td>
                                    <td class="uk-text-small">19/Jun/16</td>
                                    <td><span class="uk-badge uk-badge-danger uk-text-upper">Open</span></td>
                                </tr>


                                <tr>
                                    <td class="uk-text-center"><span class="uk-text-small uk-text-muted uk-text-nowrap">w1j181</span></td>
                                    <td>
                                        <a href="page_issue_details.html"> Vel sit molestiae dolorum minima aut.</a>
                                    </td>
                                    <td>Bernie Raynor</td>
                                    <td class="uk-text-small">24/Jun/16</td>
                                    <td class="uk-text-small">15/Jun/16</td>
                                    <td><span class="uk-badge uk-badge-success uk-text-upper">closed</span></td>
                                </tr>


                                <tr>
                                    <td class="uk-text-center"><span class="uk-text-small uk-text-muted uk-text-nowrap">w1j181</span></td>
                                    <td>
                                        <a href="page_issue_details.html"> Vel sit molestiae dolorum minima aut.</a>
                                    </td>
                                    <td>Bernie Raynor</td>
                                    <td class="uk-text-small">24/Jun/16</td>
                                    <td class="uk-text-small">15/Jun/16</td>
                                    <td><span class="uk-badge uk-badge-warning uk-text-upper">reopened</span></td>
                                </tr>


                            

                            </tbody>
                        </table>
                    </div>
                    <ul class="uk-pagination ts_pager">
                        <li data-uk-tooltip title="Select Page">
                            <select class="ts_gotoPage ts_selectize"></select>
                        </li>
                        <li class="first"><a href="javascript:void(0)"><i class="uk-icon-angle-double-left"></i></a></li>
                        <li class="prev"><a href="javascript:void(0)"><i class="uk-icon-angle-left"></i></a></li>
                        <li><span class="pagedisplay"></span></li>
                        <li class="next"><a href="javascript:void(0)"><i class="uk-icon-angle-right"></i></a></li>
                        <li class="last"><a href="javascript:void(0)"><i class="uk-icon-angle-double-right"></i></a></li>
                        <li data-uk-tooltip title="Page Size">
                            <select class="pagesize ts_selectize">
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                            </select>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>

    <div class="md-fab-wrapper">
        <a class="md-fab md-fab-accent" href="#new_issue" data-uk-modal="{ center:true }">
            <i class="material-icons">&#xE145;</i>
        </a>
    </div>

    <div class="uk-modal" id="new_issue">
        <div class="uk-modal-dialog">
            <form class="uk-form-stacked">
                <div class="uk-margin-medium-bottom">
                    <label for="task_title">{{trans('main.Title')}}</label>
                    <input type="text" class="md-input" id="Task_title" name="snippet_title"/>
                </div>
                <div class="uk-margin-medium-bottom">
                    <label for="task_description">{{trans('main.Description')}}</label>
                    <textarea class="md-input" id="task_description" name="task_description"></textarea>
                </div>
                <div class="uk-margin-medium-bottom">
                    <label for="task_assignee" class="uk-form-label">{{trans('main.Assignee')}}</label>
                    <select class="uk-form-width-medium" name="task_assignee" id="task_assignee" data-md-selectize-inline>
                        <option value="user_me">Me</option>
                        <option value="user_1">Nina Bartoletti</option>
                        <option value="user_2">Eliza Gottlieb</option>
                        <option value="user_3">Verner McKenzie</option>
                        <option value="user_4">Brandon Hegmann</option>
                    </select>
                </div>
                
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button><button type="button" class="md-btn md-btn-flat md-btn-flat-primary" id="snippet_new_save">Add Issue</button>
                </div>
            </form>
        </div>
    </div>
    <!-- google web fonts -->
    <script>
        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>

    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="assets/js/altair_admin_common.min.js"></script>

    <!-- page specific plugins -->
    <!-- tablesorter -->
    <script src="bower_components/tablesorter/dist/js/jquery.tablesorter.min.js"></script>
    <script src="bower_components/tablesorter/dist/js/jquery.tablesorter.widgets.min.js"></script>
    <script src="bower_components/tablesorter/dist/js/widgets/widget-alignChar.min.js"></script>
    <script src="bower_components/tablesorter/dist/js/extras/jquery.tablesorter.pager.min.js"></script>

    <!--  issues list functions -->
    <script src="assets/js/pages/pages_issues.min.js"></script>
    
    <script>
        $(function() {
            // enable hires images
            altair_helpers.retina_images();
            // fastClick (touch devices)
            if(Modernizr.touch) {
                FastClick.attach(document.body);
            }
        });
    </script>

</body>
</html>
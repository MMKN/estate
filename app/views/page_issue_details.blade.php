<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="16x16">

    <title>Semsar Online</title>


    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- matchMedia polyfill for testing media queries in JS -->
    <!--[if lte IE 9]>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.js"></script>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.addListener.js"></script>
    <![endif]-->

</head>
<body class=" ">
    <!-- main header -->
    <header id="header_main">
        <div class="header_main_content">
            <nav class="uk-navbar">
                <div class="main_logo_top">
                    <a href="index.html"><img src="assets/img/white.png" alt="" height="15" width="71"/></a>
                </div>
                <!-- if the user is not logged in this button will appear and once he loged in it should disappear -->
                <div class="uk-navbar-flip" style="display:none">
                    <!-- register button begin -->
                    <a class="md-btn md-btn-warning" data-uk-modal="{target:'#register_modal'}" href="#" style="margin-top: 6px;"><i class="material-icons md-24 md-light">person</i> Register / Login with facebook</a>
                    <!-- register model begin -->
                    <div class="uk-modal" id="register_modal">
                        <div class="uk-modal-dialog">
                            <button type="button" class="uk-modal-close uk-close"></button>
                            <p>There will be a button here bta3 el fb login / register</p>
                        </div>
                    </div>

                </div>            
                <div class="uk-navbar-flip">
                    <a class="md-btn md-btn-warning" href="#" style="margin-top: 6px;"><i class="material-icons md-24 md-light">person</i> My account</a>


                </div>            
                <!-- <div class="uk-navbar-flip" data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                    <a class="md-btn md-btn-warning" href="#" style="margin-top: 6px;"><i class="material-icons md-24 md-light">person</i> My account</a>

                    <div class="uk-dropdown uk-dropdown-small uk-text-center">
                        <ul class="uk-nav js-uk-prevent">
                            <li><a href="page_user_profile.html">My profile</a></li>
                            <li><a href="login.html">Logout</a></li>
                        </ul>
                    </div>

                </div> -->
                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav user_actions">
                        <li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">&#xE5D0;</i></a></li>
                        <!-- notification begin -->
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            <a href="#" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE7F4;</i><span class="uk-badge" style="background:#f00">1</span></a>
                            <div class="uk-dropdown uk-dropdown-xlarge">
                                <div class="md-card-content">
                                    <ul data-uk-tab="{connect:'#header_alerts',animation:'slide-horizontal'}">
                                    </ul>
                                    <ul id="" class="uk-margin">
                                        <li>
                                            <ul class="md-list md-list-addon">
                                                <li style="cursor:pointer">
                                                    <div class="md-list-addon-element">
                                                        <i class="material-icons md-36 uk-text-warning">new_releases</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="#">We have found you new matching properties</a></span>

                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="uk-text-center uk-margin-top uk-margin-small-bottom">
                                                <a href="#" class="md-btn md-btn-primary js-uk-prevent">Check it now</a>
                                            </div>
                                        </li> 
                                    </ul>
                                </div>
                            </div>
                        </li>

                    </ul>
                </div>

            </nav>
        </div>
        <div class="header_main_search_form">
            <i class="md-icon header_main_search_close material-icons">&#xE5CD;</i>
            <form class="uk-form">
                <input type="text" class="header_main_search_input" />
                <button class="header_main_search_btn uk-button-link"><i class="md-icon material-icons">&#xE8B6;</i></button>
            </form>
        </div>
    </header><!-- main header end -->





<div class="uk-modal" id="edit_issue">
    <div class="uk-modal-dialog">
        <form class="uk-form-stacked">
            <div class="uk-margin-medium-bottom">
                <label for="task_title">{{trans('main.Title')}}</label>
                <input type="text" class="md-input" id="Task_title" name="snippet_title"/>
            </div>
            <div class="uk-margin-medium-bottom">
                <label for="task_description">{{trans('main.Description')}}</label>
                <textarea class="md-input" id="task_description" name="task_description"></textarea>
            </div>
            <div class="uk-margin-medium-bottom">
                <label for="task_assignee" class="uk-form-label">{{trans('main.Assignee')}}</label>
                <select class="uk-form-width-medium" name="task_assignee" id="task_assignee" data-md-selectize-inline>
                    <option value="user_me">Me</option>
                    <option value="user_1">Nina Bartoletti</option>
                    <option value="user_2">Eliza Gottlieb</option>
                    <option value="user_3">Verner McKenzie</option>
                    <option value="user_4">Brandon Hegmann</option>
                </select>
            </div>
            
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button><button type="button" class="md-btn md-btn-flat md-btn-flat-primary" id="snippet_new_save">Add Issue</button>
            </div>
        </form>
    </div>
</div>
    <!-- google web fonts -->
    <script>
        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>

    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="assets/js/altair_admin_common.min.js"></script>


    <script>
        $(function() {
            // enable hires images
            altair_helpers.retina_images();
            // fastClick (touch devices)
            if(Modernizr.touch) {
                FastClick.attach(document.body);
            }
        });
    </script>


   
</body>
</html>
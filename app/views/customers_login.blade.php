<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">
    <title>{{trans('main.project_name')}}</title>
    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>
    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css"/>
    <!-- altair admin login page -->
    <link rel="stylesheet" href="assets/css/login_page.min.css" />
</head>
<body class="login_page">
    <div class="login_page_wrapper">
        <div class="md-card" id="login_card">
            <div class="md-card-content large-padding">
                <h3 style="text-align:center; padding-bottom:2em;">Welcome To Osool</h3>
                <!-- <div class="login_heading">
                    <div class="user_avatar"></div>
                </div> -->
                <form id="login_form">
                    <div class="uk-form-row">
                        <label for="login_username">Mobile</label>
                        <input class="md-input" type="email" name="email" />
                    </div>
                    <div class="uk-form-row">
                        <label for="login_password">Password</label>
                        <input class="md-input" type="password" name="password" />
                    </div>
                    <div class="uk-margin-medium-top">
                        <a id="sign_in_customer" class="md-btn md-btn-primary md-btn-block md-btn-large">Sign In</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="uk-notify uk-notify-bottom-center" style="display: none;" id="notification_area">

    </div>
    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- altair core functions -->
    <script src="assets/js/altair_admin_common.min.js"></script>
    <!-- altair login page functions -->
    <script src="assets/js/pages/login.min.js"></script>
    <script src="js/general_handler.js"></script>
    <script type="text/javascript">
        var base_url = '{{ URL::route('getCustomerLogin') }}';
        $(document).keypress(function(e) {
            if(e.which == 13){
                sign_in_customer();  
            }
        });
    </script>
</body>
</html>

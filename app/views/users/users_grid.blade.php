<!-- Contact list grid begin -->
@if(count($users))
    @foreach($users as $user)
    <div class="uk-width-small-1-2 uk-width-medium-1-4 uk-width-large-1-5 uk-margin-medium-bottom">
        <div onclick="getProfile('{{$user->id}}')" class="user-card" data-uk-filter="{{ $user->name }} , {{ $user->mobile }}, {{ $user->email }} "  data-uk-modal="{target:'#modal_edit'}">
            <div class="md-card md-card-hover" style="cursor:pointer;">
                <div class="md-card-head">

                    <div class="md-card-head-menu" data-uk-dropdown="{pos:'bottom-right'}">
                        <i class="md-icon material-icons">&#xE5D4;</i>
                        <div class="uk-dropdown uk-dropdown-small">
                            <ul class="uk-nav">
                            @if($user->trashed())
                                <li><a onclick="UIkit.modal.confirm('Are you sure?', function(){ unarchive_user({{$user->id}}) });" onclick="" class="uk-text-danger"> {{trans('main.unarchieve')}} </a></li>
                            @else
                                <li><a onclick="UIkit.modal.confirm('Are you sure?', function(){ deleteUser({{$user->id}}) });" class="uk-text-danger"> {{trans('main.Delete')}} </a></li>
                            @endif
                            </ul>
                        </div>
                    </div>

                    <div class="uk-text-center">
                        <img class="md-card-head-avatar" src="{{URL::asset('uploads/profile/' . $user->image )}}" alt="" />
                    </div>
                    <h3 class="md-card-head-text uk-text-center">
                        {{ $user->name }}
                    </h3>
                </div>
                <div class="md-card-content">
                    <ul class="md-list md-list-addon">
                        <li>
                            <div class="md-list-addon-element">
                                <i class="md-list-addon-icon material-icons"></i>
                            </div>
                            <div class="md-list-content">
                                <span class="md-list-heading">{{trans('main.Mobile_main')}}</span>

                                <span class="uk-text-small uk-text-muted">{{ $user->mobile }}</span>

                            </div>
                        </li>
                        <li>
                            <div class="md-list-addon-element">
                                <i class="md-list-addon-icon material-icons"></i>
                            </div>
                            <div class="md-list-content">
                                <span class="md-list-heading">{{trans('main.Email')}}</span>
                                <span class="uk-text-small uk-text-muted uk-text-truncate">{{ $user->email }}</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @endforeach
@else
<div class="uk-width-medium-1-1 uk-text-center">
    <h3 style="background-color: #fff; padding: 0.6em;">{{trans('main.No_Customers')}}</h3>

    <div class="full_width_in_card heading_c" style="float:left;">
    </div>
</div>
@endif

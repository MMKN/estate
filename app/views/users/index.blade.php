@section('archieved')
<div id="menu_top_dropdown" class="uk-float-right uk-hidden-small">
    <div class="uk-button-dropdown" data-uk-dropdown="">
        <a href="#" class="top_menu_toggle"><i class="md-icon material-icons md-24 md-light">&#xE5D4;</i></a>
        <div class="uk-dropdown uk-dropdown-small uk-dropdown-flip">
            <ul class="uk-nav">
                <li><a id="get_archived_users" class="uk-text-center">{{trans('main.Archived_Users')}}</a></li>
            </ul>
        </div>
    </div>
</div>
@stop
<div id= "main_index">
    <!-- main view of the page start -->
    <article id="main">
        <!-- page header -->
        <div id="page_heading" class="uk-margin-medium-bottom" >
            <div class="uk-grid" >
                <h1 class="uk-width-medium-1-1  uk-margin-bottom uk-text-center" style="margin-top:12px;"><i class=" material-icons md-36 uk-margin-right" style="vertical-align:text-top;">supervisor_account</i>{{trans('main.Users')}}</h1>
               
                
            </div>
            <!-- Filter end -->
        </div>
        
        <!-- Contact list grid begin -->
        <h3 class="heading_b uk-text-center grid_no_results" style="display:none">No results found</h3>
        <div class="uk-grid" id="employees_list">
            @include('users.users_grid')
        </div>
        <div class="uk-grid" id="paginator">
            <div class="uk-width-medium-1-1 pager">
                {{$users->links()}}
            </div>
        </div>
        <div id="filter_grid" class="uk-grid" style="display:none;">
        </div>
        
    </article>
    <!-- main view of the page start -->
    <article id="edit_box" class="uk-width-medium-8-10 uk-container-center edit-box" style="display:none" >
        <div class="md-card">
            <div class="md-card-content" id="edit_form">
            </div>
        </div>
    </article>
    <article id="loaderGif" class="" style="display:none" >
        <div class="uk-width-medium-1-6 uk-container-center">
            <div class="md-preloader uk-container-center"><svg xmlns="http://www.w3.org/2000/svg" version="1.1" height="96" width="96" viewbox="0 0 75 75"><circle cx="37.5" cy="37.5" r="33.5" stroke-width="4"/></svg></div>
        </div>
    </article>
    <article id="profile_view">
    </article>
    <!-- Customer profile end -->
    <article id="empty">
    </article>
    <!-- edit user model begin -->
    <div class="uk-modal" id="modal_edit">
    </div>
    <!-- edit user model end -->
    <!-- delete user model begin -->
    <div class="uk-modal" id="modal_delete">
    </div>
    <!-- delete user model end -->
    <!-- delete form begin -->
    <article id="archieve_box" class="uk-width-medium-2-5 uk-text-center  uk-container-center" style="display:none">
        <div class="md-card ">
            <div class="md-card-content">
                <div class="uk-grid">
                    <h3 class="uk-width-medium-1-1- uk-margin">{{trans('main.customer_delete')}}</h3>
                    <div class="uk-width-medium-1-2 uk-margin">
                        <a class="md-btn md-btn-danger" id="confirm_delete" href="#">{{trans('main.Delete')}}</a>
                    </div>
                    <div class="uk-width-medium-1-2 uk-margin">
                        <a class="md-btn md-btn-success" id="archieve_cancel" href="#">{{trans('main.Cancel')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </article>
</div>
<div id="add_user" class="md-fab-wrapper">
    <a class="md-fab md-fab-accent md-fab-wave-light" data-uk-modal="{target:'#modal_user_add'}">
        <i class="material-icons">&#xE145;</i>
    </a>
</div>
<!-- add new user model begin -->
<div class="uk-modal" id="modal_user_add">
    <div class="uk-modal-dialog">
        <button type="button" class="uk-modal-close uk-close"></button>
        <div class="uk-margin user_content">
            <form id="form_validation" class="uk-form-stacked add_user_form">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-1-1">
                        <div class="uk-margin-top parsley-row">
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon">
                                    <i class="material-icons md-36">face</i>
                                </span>
                                <label>{{trans('main.Full_Name')}}</label>
                                <input type="text" class="md-input" name="name" required />
                            </div>
                        </div>
                        <div class="uk-margin-top parsley-row">
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon">
                                    <i class="material-icons md-36">email</i>
                                </span>
                                <label>{{trans('main.Email')}}</label>
                                <input type="email" name="email" data-parsley-trigger="change" class="md-input" required />
                            </div>
                        </div>
                        <div class="uk-margin-top parsley-row">
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon">
                                    <i class="material-icons md-36">call</i>
                                </span>
                                <label>{{trans('main.Mobile_main')}}</label>
                                <input type="text" name="mobile"  class="md-input"  required />
                            </div>
                        </div>
                        <div class="uk-margin-top parsley-row">
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon">
                                    <i class="material-icons md-36">lock</i>
                                </span>
                                <input type="password" name="password" class="md-input" placeholder="Password" value="" required/>
                                <a href="" class="uk-form-password-toggle" data-uk-form-password>{{trans('main.show')}}</a>
                            </div>
                        </div>
                        <div class="uk-margin-top parsley-row">
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon">
                                    <i class="material-icons md-36">security</i>
                                </span>
                                <div>
                                    <select name="role" id="role" required data-md-selectize-inline>
                                        <option value="" disabled selected>User role</option>
                                        @if(Auth::user()->user()->is_admin)
                                        <option value="1">{{trans('main.Admin')}}</option>
                                        @endif
                                        <option value="0">{{trans('main.Agent')}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="uk-margin-top parsley-row">
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon">
                                    <i class="uk-icon-sitemap uk-icon-medium"></i>
                                </span>
                                <div>
                                    <select name="manager" id="manager" required data-md-selectize-inline>
                                        <option value="" disabled selected>Manager</option>
                                        <option value="{{Auth::user()->id()}}" >Me</option>
                                        @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="uk-margin-top parsley-row">
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon">
                                    <i class="material-icons md-36">image</i>
                                </span>
                                <div class="uk-input-group uk-margin-large-left">
                                    <div class="user_heading_avatar fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail">
                                            <img src="assets/img/blank.png" id="image_preview" alt="user avatar"/>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                        <div class="user_avatar_controls">
                                            <span class="btn-file">
                                                <span class="fileinput-new"><i class="material-icons">&#xE2C6;</i></span>
                                                <span class="fileinput-exists"><i class="material-icons">&#xE86A;</i></span>
                                                <input type="file" name="image" id="user_image">
                                            </span>
                                            <a href="#" class="btn-file fileinput-exists" data-dismiss="fileinput"><i class="material-icons">&#xE5CD;</i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <div class="uk-margin-top uk-text-center">
                            <button type="submit" id="add_user_button" class="md-btn md-btn-primary">{{trans('main.Save')}}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="uk-notify uk-notify-bottom-center" style="display:none; z-index:9999; " id="notification_area">
</div>
@include('general.add_notice')
@section('scripts')
<!-- <script src="{{ URL::asset('js/general_handler.js') }}"></script> -->
<script src="{{ URL::asset('js/user_handler.js') }}"></script>
<script type="text/javascript">
var root_url = '{{URL::to("/")}}';
var base_url = "{{URL::to('/')}}";
var msg_id   = 0;
var property_id;
</script>
@stop
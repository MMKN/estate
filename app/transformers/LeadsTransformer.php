<?php

namespace app\transformers;


/**
* 
*/
class LeadsTransformer extends Transformer
{
	public function transform($lead)
	{
		return [
			'id' => $lead['id'],
			'description' => $lead['description'],
			'winning_propability' => $lead['winning_propability'],
			'potential_value' => $lead['potential_value'],
			'tags' => $lead['tags'],
			'no_tasks' => count($lead['tasks'])
		];
	}
}
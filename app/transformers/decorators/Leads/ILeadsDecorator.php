<?php

namespace Leads;

interface ILeadsDecorator{
	public function transformCollection($items);
}
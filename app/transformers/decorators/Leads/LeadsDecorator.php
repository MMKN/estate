<?php

namespace Leads;

/**
* 
*/
abstract class LeadsDecorator implements ILeadsDecorator
{
	protected $leads;
	
	function __construct(ILeadsDecorator $leads)
	{
		
		$this->leads = $leads;
	}

	abstract public function transformCollection($items);
}
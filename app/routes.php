<?php
	
Route::get('/' , ['uses' => 'HomeController@getLogin' , 'as' => 'getLogin']);
Route::post('/' , ['uses' => 'HomeController@postLogin' , 'as' => 'postLogin']);
Route::get('/logout' , ['uses' => 'HomeController@postLogout' , 'as' => 'postLogout']);

Route::get('/login' , ['uses' => 'HomeController@getCustomerLogin' , 'as' => 'getCustomerLogin']);
Route::post('/login' , ['uses' => 'HomeController@postCustomerLogin' , 'as' => 'postCustomerLogin']);
Route::get('/customer-logout' , ['uses' => 'HomeController@postCustomerLogout' , 'as' => 'postCustomerLogout']);

Route::group(['before' => 'customerAuth'] , function(){
	Route::get('account' , ['uses' => 'CustomersController@account' , 'as' => 'account']);
	// Route::get('property-customer' , ['as' => 'properties.property.customer']);
	Route::get('property-customer/{id}' , ['uses' => 'PropertiesController@getPropertyCustomer' , 'as' => 'properties.property.customer']);
});

Route::group(['prefix' => 'api'] , function(){

	//Leads API's
	Route::post('leads/attach-users/{id}' , 'LeadsController@postAttachUsers');
	Route::post('leads/attach-tags/{id}' , 'LeadsController@postAttachTags');
	Route::get('leads/archived' , 'LeadsController@getArchived');
	Route::resource('leads' , 'LeadsController' , ['only' => ['index' , 'show' ,'update' ,'destroy' ,'store']]);


	Route::resource('leads.tasks', 'TasksController');
	Route::resource('leads.tasks.comments', 'TaskCommentsController');
	Route::resource('tags' , 'TagsController');

	//Search API
	Route::get('contacts/search' , 'IndividualsController@globalSearch');

	//Individuals API's
	Route::get('individuals/search' , 'IndividualsController@search');
	Route::get('individuals/archived' , 'IndividualsController@getArchived');
	Route::put('individuals/restore/{id}' , 'IndividualsController@restore');
	Route::resource('individuals' , 'IndividualsController');

	//Companies API's
	Route::get('companies/archived' , 'CompaniesController@getArchived');
	Route::get('companies/search' , 'CompaniesController@search');
	Route::put('companies/restore/{id}' , 'CompaniesController@restore');
	Route::resource('companies' , 'CompaniesController');

	//Users API's
	Route::resource('employees' , 'UserController');
});

Route::get('/leads' , function(){
	return View::make('leads.index');
});

Route::group(['before' => 'auth'] , function(){
	Route::get('property-admin/{id}' , ['uses' => 'PropertiesController@getPropertyCustomer' , 'as' => 'properties.property.admin']);
	Route::controller('properties' , 'PropertiesController' ,
		[
			'getIndex'             => 'properties.index',
			'postAdd'              => 'properties.add',
			'postAddImages'        => 'properties.add.images',
			'getEdit'              => 'properties.edit',
			'postEdit'             => 'properties.update',
			'postFilter'           => 'properties.filter',
			'getProperty'          => 'properties.property',
			'getNeighbour'         => 'properties.neighbours',
			'getCustomerAdd'       => 'properties.customer.add',
		]
	);

	Route::controller('customers' , 'CustomersController' ,
		[
			'getIndex'   => 'customers.index',
			'postAdd'    => 'customers.add',
			'postEdit'   => 'customers.edit',
			'postFilter' => 'customers.filter'
		]
	);

	Route::controller('settings' , 'SettingsController' ,
		[
			'getIndex'            => 'settings.index',
			'getPropertyCsv'      => 'settings.export.properties',
			'getCustomerCsv'      => 'settings.export.customers',
			'postAddUser'         => 'settings.add.user',
			'getEditUser'         => 'settings.get.edit.user',
			'postEditUser'        => 'settings.post.edit.user',
			'postAddSetting'      => 'settings.post.add.setting',
			'postDeleteSetting'   => 'settings.post.delete.setting',
			'postAddMargin'       => 'settings.post.add.margin',
			'postAddCity'         => 'settings.post.add.city',
			'getEditCity'         => 'settings.get.edit.city',
			'postEditCity'        => 'settings.post.edit.city',
			'getDeleteUser'       => 'settings.get.delete.user',
			'getDeleteCity'       => 'settings.get.delete.city',
			'postDeleteUser'      => 'settings.post.delete.user',
			'postDeleteCity'      => 'settings.post.delete.city',
			'postAddGroup'        => 'settings.post.add.group',
			'getEditGroup'        => 'settings.get.edit.group',
			'postEditGroup'       => 'settings.post.edit.group',
			'getDeleteGroup'      => 'settings.get.delete.group',
			'postDeleteGroup'     => 'settings.post.delete.group',
			'getAttributes'       => 'settings.get.attributes',
			'postLanguage'        => 'settings.post.language',
		]
	);

	Route::controller('notifications' , 'NotificationsController' ,
		[
			'getIndex'       => 'notifications.index',
			'postDone'       => 'notifications.done',
			'getDetails'     => 'notifications.details',
			'getDetailsPage' => 'notifications.details-page'
		]
	);

	Route::controller('users' , 'UserController' ,
		[
			'getIndex'       => 'users.index'
		]
	);

	Route::get('profile' , 'CustomersController@getProfile');
	Route::get('profile/{id}' , 'CustomersController@getProfile');
	Route::get('profile-page' , 'CustomersController@getProfilePage');
	Route::get('profile-page/{id}' , 'CustomersController@getProfilePage');
	Route::get('view' , 'PropertiesController@view_property');
});

Route::get('test' , function(){
	return View::make('page_issue_details');
});
Route::get('test2' , function(){
	return View::make('page_issues_list');
});

Route::get('edit-property-images' , function(){
	$images = Image::all();
	foreach ($images as $image){
		echo $image->url . "<br/>";
		$new = str_replace("http://drive.google.com/uc?export=view&id=" , "" , $image->url);
		$image->url = $new;
		$image->update();
		echo $image->url . "<br/>";
	}

	return 'Done';
});


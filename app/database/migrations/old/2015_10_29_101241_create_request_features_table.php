<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestFeaturesTable extends Migration {

	public function up()
	{
		Schema::create('requests_feature', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('request_id');
			$table->integer('feature_id');
			$table->timestamps();
		});
	}

	
	public function down()
	{
		Schema::drop('requests_feature');
	}

}

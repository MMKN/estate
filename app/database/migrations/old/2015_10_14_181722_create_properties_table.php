<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('properties', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title' , 500);
			$table->string('address' , 500);
			$table->integer('plot_no');
			$table->integer('apartment_no');
			$table->integer('price');
			$table->integer('purpose'); // 0 sale - 1 rent
			$table->integer('unit_type');
			$table->integer('neighbour');
			$table->integer('city');
			$table->integer('finishing');
			$table->boolean('is_compound');
			$table->integer('area');
			$table->integer('rooms_no');
			$table->integer('toilets_no');
			$table->integer('floor');
			$table->integer('payment_style');
			$table->integer('distance_facilites');
			$table->integer('owner_id');
			$table->string('longitude' , 100);
			$table->string('latitude' , 100);
			$table->integer('objective_id');
			$table->text('description');
			$table->string('heard');
			$table->softDeletes();
			$table->boolean('is_published');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('properties');
	}

}

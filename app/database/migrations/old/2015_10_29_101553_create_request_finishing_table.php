<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestFinishingTable extends Migration {

	public function up()
	{
		Schema::create('requests_finishing', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('request_id');
			$table->integer('finishing_id');
			$table->timestamps();
		});
	}

	
	public function down()
	{
		Schema::drop('requests_finishing');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNeighboursGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('neighbours_groups', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('neighbour_id')->unsigned();
			$table->integer('group_id')->unsigned();
			$table->foreign('neighbour_id')->references('id')->on('neighbours')->onDelete('cascade');
			$table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('neighbours_groups');
	}

}

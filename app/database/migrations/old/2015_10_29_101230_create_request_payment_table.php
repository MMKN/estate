<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestPaymentTable extends Migration {

	public function up()
	{
		Schema::create('requests_payment', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('request_id');
			$table->integer('payment_id');
			$table->timestamps();
		});
	}

	
	public function down()
	{
		Schema::drop('requests_payment');
	}

}

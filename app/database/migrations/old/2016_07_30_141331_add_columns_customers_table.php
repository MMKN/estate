<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('customers', function(Blueprint $table)
		{
			$table->string('type');
			$table->integer('individual_id')->unsigned()->nullable();
			$table->foreign('individual_id')->references('id')->on('individuals')->onDelete('cascade');
			$table->integer('company_id')->unsigned()->nullable();
			$table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('customers', function(Blueprint $table)
		{
			$table->dropColumn('type');
			$table->dropForeign('customers_individual_id_foreign');
			$table->dropColumn('individual_id');
			$table->dropForeign('customers_company_id_foreign');
			$table->dropColumn('company_id');
		});
	}

}

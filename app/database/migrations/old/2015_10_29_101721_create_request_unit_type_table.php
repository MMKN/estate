<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestUnitTypeTable extends Migration {

	public function up()
	{
		Schema::create('requests_unit_type', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('request_id');
			$table->integer('unit_type_id');
			$table->timestamps();
		});
	}

	
	public function down()
	{
		Schema::drop('requests_unit_type');
	}

}

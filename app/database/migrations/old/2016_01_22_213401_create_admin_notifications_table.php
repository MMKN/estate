<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *admin_notifications_table (request to call - comment) ( id - from_admin_id - to_admin_id - type(4) - status(3) - priorty(3) - property_id - user_id )
	 * @return void
	 */
	public function up()
	{
		Schema::create('admin_notifications', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->text('description');
			$table->integer('from');
			$table->integer('to');
			$table->integer('type');
			$table->integer('status');
			$table->integer('priority');
			$table->integer('property_id');
			$table->bigInteger('user_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admin_notifications');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestObjectiveTable extends Migration {

	
	public function up()
	{
		Schema::create('requests_objective', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('request_id');
			$table->integer('objective_id');
			$table->timestamps();
		});
	}

	
	public function down()
	{
		Schema::drop('requests_objective');
	}

}

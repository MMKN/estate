<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('requests', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('customer_id');
			$table->integer('price_from');
			$table->integer('price_to');
			$table->string('purpose');
			$table->string('unit_type');
			$table->string('neighbour');
			$table->string('city');
			$table->string('finishing');
			$table->boolean('is_compound');
			$table->integer('area');
			$table->integer('rooms_no');
			$table->integer('toilets_no');
			$table->string('floor');
			$table->string('payment_style');
			$table->integer('distance_facilites');
			$table->string('objective');
			$table->string('features');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('requests');
	}

}

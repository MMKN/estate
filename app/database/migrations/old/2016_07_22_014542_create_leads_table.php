<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('leads', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('creator_id')->unsigned();
			$table->foreign('creator_id')->references('id')->on('users')->onDelete('cascade');
			$table->integer('company_id')->unsigned()->nullable();
			$table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
			$table->integer('individual_id')->unsigned()->nullable();
			$table->foreign('individual_id')->references('id')->on('individuals')->onDelete('cascade');
			$table->date('start_date');
			$table->string('color');
			$table->string('status');
			$table->integer('potential_value');
			$table->integer('winning_propability');
			$table->text('description');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('leads', function(Blueprint $table)
		{
			Schema::drop('leads');
		});
	}

}

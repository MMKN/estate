<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarginsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('margins', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('field_name');
			$table->boolean('type'); // 0 = % | 1 = No.
			$table->integer('value_exact');
			$table->integer('value_suggest');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('margins');
	}

}

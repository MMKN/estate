<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestFloorTable extends Migration {

	public function up()
	{
		Schema::create('requests_floor', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('request_id');
			$table->integer('floor_id');
			$table->timestamps();
		});
	}

	
	public function down()
	{
		Schema::drop('requests_floor');
	}

}

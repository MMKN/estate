<?php

use Illuminate\Database\QueryException;

class TagsController extends \APIController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return $this->respond([
			'data' => Tag::get(),
			'code' => 200
			]);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$tag = new Tag();
		$tag->name = Input::get('name');
		$tag->type = Input::get('type');

		try{
			$tag->save();
		}catch(QueryException $e){
			//if tag already exist or soft deleted restore it ... 
			if($e->getCode() == 23000){
				$tag = Tag::withTrashed()->whereName(Input::get('name'))->first();
				if($tag->trashed()){
					$tag->restore();
				}
			}
		}


		return $this->respond([
			'data' => $tag,
			'code' => 201
			]);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$tag = Tag::find($id);

		if(!$tag){
			return $this->respondNotFound('Tag Not Found');
		}

		return $this->respond([
			'data' => $tag,
			'code' => '200'
			]);
	}



	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$tag = Tag::find($id);

		//check if tag exist
		if(!$tag){
			return $this->respondNotFound('Tag Not Found');
		}

		$tag->name = Input::get('name');
		$tag->type = Input::get('type');
		$tag->update();


		return $this->respond([
			'data' => $tag,
			'code' => 200
			]);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$tag = Tag::withTrashed()->find($id);

		if(!$tag){
			return $this->respondNotFound('Tag Not Found');
		}

		$leads = $tag->leads;
		$tasks = $tag->tasks;


		if($leads->isEmpty() && $tasks->isEmpty()){
			$tag->forceDelete();
		}else{
			$tag->delete();
		}

		return $this->respond([
			'message' => 'Tag deleted successfully',
			'code' => 200
			]);
	}


}

<?php

class SettingsController extends BaseController {

    public function getIndex()
    {
        // if(Auth::user()->user()->is_admin != 1){
        //     return Redirect::to('/properties');
        // }
        $data['users'] = User::all();
        return View::make('settings.settings' , $data);
    }

    public function getExportProperties()
    {
        $table = Property::all();
        $file = fopen('properties.csv', 'w');
        foreach ($table as $row) {
            fputcsv($file, $row->toArray());
        }
        fclose($file);
    }

    public function getExportCustomers()
    {
        $table = Customer::all();
        $file = fopen('customers.csv', 'w');
        foreach ($table as $row) {
            fputcsv($file, $row->toArray());
        }
        fclose($file);
    }

    public function postAddUser()
    {
        $data = Input::all();
        dd($data);
        $validator = Validator::make($data, User::$rules);

        if ($validator->fails()) {
          $data['message'] = $validator->messages()->first();
          return Response::json(array(
            'message' => (String)View::make('general.danger_message' , $data),
            'state'   => 'fail'
          ));
        }

        $user = new User;
        if(Input::hasFile('image')){
            $file = Input::file('image');
            $filename = (String)rand(100 , 10000) . "_" . $file->getClientOriginalName();
            $file->move(public_path()."/uploads/profile/",$filename);
            $user->image    = $filename;
        }else{
            $user->image    = 'user.png';
        }
        $user->name     = $data['name'];
        $user->email    = $data['email'];
        $user->mobile   = $data['mobile'];
        $user->password = Hash::make($data['password']);
        $user->is_admin = $data['role'];
        $user->save();

        $data['users'] = User::all();
        return Response::json(array(
            'users' =>  (String)View::make('settings.users_table' , $data),
            'state' => 'success'
        ));
    }

    public function getEditUser($id)
    {
        $data['user'] = User::find($id);
        return Response::json(array(
            'modal' =>  (String)View::make('settings.edit_modal' , $data),
        ));
    }

    public function postEditUser($id)
    {
        $data = Input::all();
        $user = User::find($id);

        if(Input::hasFile('image')){
            $file = Input::file('image');
            $filename = (String)rand(100 , 10000) . "_" . $file->getClientOriginalName();
            $file->move(public_path()."/uploads/profile/",$filename);
            $user->image = $filename;
        }

        $user->name     = $data['name'];
        $user->email    = $data['email'];
        $user->mobile   = $data['mobile'];
        if(Input::has('password')){
            $user->password = Hash::make($data['password']);
        }
        if(Input::has('role')){
            $user->is_admin = $data['role'];    
        }
        $user->update();

        $data['users'] = User::all();
        return Response::json(array(
            'users' =>  (String)View::make('settings.users_table' , $data),
            'state' => 'success'
        ));
    }

    public function postAddSetting()
    {
        $table = str_replace(" " , "" , ucwords(str_replace("_", " ", Input::get('table'))));

        $value = Input::get('value');

        $setting        = new $table;
       $setting = $setting->whereValue($value)->get();
        if(count($setting)){
            return;
        }
        $setting  = new $table;
        $setting->value = $value;
        $setting->save();
    }

    public function postDeleteSetting()
    {
        $table = str_replace(" " , "" , ucwords(str_replace("_", " ", Input::get('table'))));
        $value = Input::get('value');

        $setting_id = $table::whereValue($value)->pluck('id');
        switch(Input::get('col')){
            case 'floor':
                $exist = Property::whereFloor($setting_id)->get();
                break;
            case 'unit_type':
                $exist = Property::whereUnit_type($setting_id)->get();
                break;
            case 'finishing':
                $exist = Property::whereFinishing($setting_id)->get();
                break;
            case 'objective':
                $exist = Property::whereObjective_id($setting_id)->get();
                break;
            case 'payment_style':
                $exist = Property::wherePayment_style($setting_id)->get();
                break;
            case 'feature':
                $exist = PropertyFeature::whereFeature_id($setting_id)->get();
                break;
        }
        if(!$exist->isEmpty()){
            return '1';
        }

        $setting = $table::whereValue($value)->delete();
    }

    public function postAddCity()
    {
        $data = Input::all();

        $validator = Validator::make($data, City::$rules);

        if ($validator->fails()) {
          $data['message'] = $validator->messages()->first();
          return Response::json(array(
            'message' => (String)View::make('general.danger_message' , $data),
            'state'   => 'fail'
          ));
        }


        $city       = new City;
        $city->name = $data['name'];
        $city->save();

        foreach ($data['districts'] as $district) {
            $d          = new Neighbour;
            $d->city_id = $city->id;
            $d->name    = $district;
            $d->save();
        }

        $data['cities'] = City::all();
        return Response::json(array(
            'cities_table' =>  (String)View::make('settings.cities_table' , $data),
            'state'   => 'success'
        ));
    }

    public function getEditCity($id)
    {
        $data['city'] = City::find($id);
        $data['neighbours'] = Neighbour::whereCity_id($id)->get();
        return Response::json(array(
            'modal' =>  (String)View::make('settings.edit_city_modal' , $data)
        ));
    }

    public function postEditCity($id)
    {
        $data = Input::all();
        if($data['name'] == 'city_name'){
            if($data['value'] == '') return;
            $city = City::find($id);
            $city->name = $data['value'];
            $city->update();
        }else if($data['name'] == 'district_add'){
            $neighbour = Neighbour::whereName($data['value'])->get();
            if(count($neighbour)){
                return;
            }
            // dd('$neighbour');
            $d          = new Neighbour;
            $d->city_id = $id;
            $d->name    = $data['value'];
            $d->save();
        }else{
            $neighbour = Neighbour::whereName($data['value'])->pluck('id');
            $exist = Property::whereNeighbour($neighbour)->get();
            if(count($exist)){
                $data['cities'] = City::all();
                return Response::json(array(
                    'cities_table' =>  (String)View::make('settings.cities_table' , $data),
                    'success' => 2
                ));
            }
            
            $d = Neighbour::whereName($data['value'])->delete();
        }

        $data['cities'] = City::all();
        return Response::json(array(
            'cities_table' =>  (String)View::make('settings.cities_table' , $data),
            'success' => 1
        ));
    }

    public function getDeleteUser($id)
    {
        $user            = User::find($id);
        $data['message'] = 'Are You Sure You Want To Delete User [ ' . $user->name . ' ] ?';
        $data['class']   = 'delete_user_form';
        return Response::json(array(
            'modal' =>  (String)View::make('settings.delete_modal' , $data)
        ));
    }

    public function postDeleteUser($id)
    {
        $user = User::find($id);
        $user->delete();

        $data['users'] = User::all();
        return Response::json(array(
            'users' =>  (String)View::make('settings.users_table' , $data),
            'state' => 'success'
        ));
    }

    public function getDeleteCity($id)
    {
        $city            = City::find($id);

        $data['message'] = 'Are You Sure You Want To Delete City [ ' . $city->name . ' ] ?';
        $data['class']   = 'delete_city_form';
        return Response::json(array(
            'modal' =>  (String)View::make('settings.delete_modal' , $data)
        ));
    }

    public function postDeleteCity($id)
    {
        $city = City::find($id);

        if(count($city)){
            $data['cities'] = City::all();
            return Response::json(array(
                'cities_table' =>  (String)View::make('settings.cities_table' , $data),
                'success'      => 2
            ));
        }

        $city->delete();

        $data['cities'] = City::all();
        return Response::json(array(
            'cities_table' =>  (String)View::make('settings.cities_table' , $data),
            'success'      => 1
        ));
    }

    public function postAddGroup()
    {
        $data = Input::all();

        $group       = new Group;
        $group->name = $data['group_name'];
        $group->type = $data['group_type'];
        $group->save();

        if ($data['group_type'] == 0) {
            foreach ($data['attr'] as $neighbour) {
                $n = new NeighbourGroup;
                $n->neighbour_id = $neighbour;
                $n->group_id     = $group->id;
                $n->save();
            }
        }else{
            foreach ($data['attr'] as $floor) {
                $f = new FloorGroup;
                $f->floor_id = $floor;
                $f->group_id     = $group->id;
                $f->save();
            }
        }

        $data['groups'] = Group::all();
        return Response::json(array(
            'groups' =>  (String)View::make('settings.groups_table' , $data),
            'state' => 'success'
        ));
    }

    public function getEditGroup($id)
    {
        $data['group'] = Group::find($id);
        return Response::json(array(
            'modal' =>  (String)View::make('settings.edit_group_modal' , $data),
        ));
    }

    public function postEditGroup($id)
    {
        $group = Group::find($id);
        if($group->type == 0){
            foreach (NeighbourGroup::whereGroup_id($group->id)->get() as $district) {
                $district->delete();
            }
        }else{
            foreach (FloorGroup::whereGroup_id($group->id)->get() as $floor) {
                $floor->delete();
            }
        }

        $data = Input::all();

        $group->name = $data['group_name'];
        $group->type = $data['group_type'];
        $group->update();

        if ($data['group_type'] == 0) {
            foreach ($data['attr'] as $neighbour) {
                $n = new NeighbourGroup;
                $n->neighbour_id = $neighbour;
                $n->group_id     = $id;
                $n->save();
            }
        }else{
            foreach ($data['attr'] as $floor) {
                $f = new FloorGroup;
                $f->floor_id = $floor;
                $f->group_id = $id;
                $f->save();
            }
        }

        $data['groups'] = Group::all();
        return Response::json(array(
            'groups_table' =>  (String)View::make('settings.groups_table' , $data)
        ));
    }

    public function getDeleteGroup($id)
    {
        $group           = Group::find($id);
        $data['message'] = 'Are You Sure You Want To Delete Group [ ' . $group->name . ' ] ?';
        $data['class']   = 'delete_group_form';
        return Response::json(array(
            'modal' =>  (String)View::make('settings.delete_modal' , $data)
        ));
    }

    public function postDeleteGroup($id)
    {
        $group = Group::find($id);
        if($group->type == 0){
            foreach (NeighbourGroup::whereGroup_id($group->id)->get() as $district) {
                $district->delete();
            }
        }
        $group->delete();

        $data['groups'] = Group::all();
        return Response::json(array(
            'groups_table' =>  (String)View::make('settings.groups_table' , $data)
        ));
    }

    public function postAddMargin()
    {
        $type = Input::get('type'); // 0 = exact - 1 = similar
        $field = Input::get('field');
        $value = str_replace("%" , "" , Input::get('value'));

        $margin = Margin::whereField_name($field)->get()->first(); // check if it exsists or not (add or update)
        if(!$margin){ // not exsist (add)
            $margin                    = new Margin;
            $margin->field_name        = $field;
            if ($type == 0){
                $margin->value_exact   = $value;
            }else            {
                $margin->value_suggest = $value;
            }
            $margin->save();
        }else{ // exsist (update)
            if ($type == 0)
            {
                $margin->value_exact   = $value;
            }else{
                $margin->value_suggest = $value;
            }
            $margin->update();
        }
    }

    public function getAttributes()
    {
        $data = Input::all();
        if ($data['type'] == 0) {
            $data['attrs'] = Neighbour::all();
            $data['type']  = 'Districts';
        }else{
            $data['attrs'] = Floor::all();
            $data['type']  = 'Floors';
        }

        return Response::json(array(
            'attrs' =>  (String)View::make('settings.group_attr' , $data)
        ));
    }

    public function getPropertyCsv(){
        $prop = new Property();
        $columns = $prop->getTableColumns();

        // Fetch Record from Database

        $output = "";

        // Get The Field Name

        for ($i = 0; $i < count($columns); $i++) {
            $output .= '"'.$columns[$i].'",';
        }
        $output .="\n";
        // dd($output);
        // Get Records from the table

        $properties = Property::all();

        foreach ($properties as $property) {
            foreach ($property->getAttributes() as $att) {
                $output .= $att.",";
            }
            $output .="\n";
        }

        // // Download the file

        $filename = "properties - ".date('d-m-Y').".csv";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename='.$filename);

        echo $output;


        exit;


    }

    public function getCustomerCsv(){
        $customer = new Customer();
        $columns = $customer->getTableColumns();

        // Fetch Record from Database
        $output = "";

        // Get The Field Name

        for ($i = 0; $i < count($columns); $i++) {
            $output .= '"'.$columns[$i].'",';
        }
        $output .="\n";
        // dd($output);
        // Get Records from the table

        $customers = Customer::all();

        foreach ($customers as $property) {
            foreach ($property->getAttributes() as $att) {
                $output .= $att.",";
            }
            $output .="\n";
        }

        // // Download the file

        $filename = "customers - " .date('d-m-Y').".csv";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename='.$filename);

        echo $output;


        exit;


    }


    public function postLanguage(){
        $lang = Input::get('lang');
        $languages = Language::all();
        foreach ($languages as $language) {
            $language->selected = 0;
            $language->update();
        }

        $language = Language::whereValue($lang)->get()->first();
        $language->selected = 1;
        $language->update();

        return 1;
    }
}

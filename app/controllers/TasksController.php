<?php

class TasksController extends \APIController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($lead_id)
	{
		$lead = Lead::find($lead_id);
		if(!$lead){
			return $this->respondNotFound('Lead Not Found');
		}

		return $this->respond([
			'data' => $lead->tasks,
			'code' => '200'
			]);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($lead_id)
	{
		$deadline = date('Y-m-d H:i:s',strtotime(str_replace('/' , '-' , Input::get('deadline'))));
		$lead = Lead::find($lead_id);
		if(!$lead){
			return $this->respondNotFound('Lead Not Found');
		}
		$task = new Task();
		$task->description = Input::get('description');
		$task->priority = Input::get('priority');
		$task->deadline = $deadline;
		$task->group = Input::get('group');

		$lead->tasks()->save($task);
		return $this->respond([
			'data' => $task,
			'code' => 200
			]);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($lead_id , $task_id)
	{
		try{
			$task = Lead::findOrFail($lead_id)->tasks()->where('id' , $task_id)->first();
		}catch(Illuminate\Database\Eloquent\ModelNotFoundException $e){
			return $this->respondNotFound('Lead Not Found');
		}
		
		if(!$task){
			return $this->respondNotFound('Task Not Found');
		}

		return $this->respond([
			'data' => $task,
			'code' => 200
			]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($lead_id , $task_id)
	{
		try{
			$task = Lead::findOrFail($lead_id)->tasks()->where('id' , $task_id)->first();
		}catch(Illuminate\Database\Eloquent\ModelNotFoundException $e){
			return $this->respondNotFound('Lead Not Found');
		}
		if(!$task){
			return $this->respondNotFound('Update Not Found');
		}

		$deadline = date('Y-m-d H:i:s',strtotime(str_replace('/' , '-' , Input::get('deadline'))));

		$task->priority = Input::get('priority');
		$task->deadline = $deadline;
		$task->group = Input::get('group');
		$task->description = Input::get('description');
		$task->update();

		return $this->respond([
			'data' => $task,
			'code' => '200'
			]);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($lead_id , $task_id)
	{
		try{
			$task = Lead::findOrFail($lead_id)->tasks()->withTrashed()->where('id' , $task_id)->first();
		}catch(Illuminate\Database\Eloquent\ModelNotFoundException $e){
			return $this->respondNotFound('Lead Not Found');
		}
		// $task = Task::withTrashed()->find($task_id);

		if(!$task){
			return $this->respondNotFound('Task Not Found');
		}

		if($task->trashed()){
			$task->forceDelete();
		}else{
			$task->delete();
		}

		return $this->respond([
			'message' => 'Task deleted successfully',
			'code' => 200
			]);
	}


}
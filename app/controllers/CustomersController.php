<?php

class CustomersController extends BaseController {

    public $child_users;
    public $pusher;


    public function __construct(){
        parent::__construct();
        if(Auth::user()->user()->is_admin){
            $this->child_users[] = 0;
            $this->child_users = array_merge($this->child_users , User::all()->lists('id'));
            // dd($child_users);
        }else{
            $this->child_users = UserController::getChildren(Auth::user()->id());
            array_push($this->child_users , Auth::user()->id());
        }
        
        $this->pusher = new Pusher('740b3e5ad1236ca3a8d8' ,'bf5a68796761c46e6b02' ,'180891');
    }


    public function getIndex()
    {
        // dd(var_dump($this->child_users));
        $data['customers']     = Customer::whereIn('user_id' , $this->child_users)->orderBy('created_at' , 'desc')->paginate(20);
        $data['admins']        = User::where('id' , '!=' , Auth::user()->id())->get();
        $data['users']         = User::where('id' , '!=' , Auth::user()->id())->get();
        return View::make('customers.index' , $data);
    }

    public function getPaginated(){
        $data['customers'] = Customer::whereIn('user_id' , $this->child_users)->orderBy('created_at' , 'desc')->paginate(20);
        $customers = Customer::whereIn('user_id' , $this->child_users)->orderBy('created_at' , 'desc')->paginate(20);
        return Response::json(array(
            'customers'    => (String)View::make('customers.list' , $data),
            'links'        => (String)$customers->links()
        ));
    }

    public function getOwner()
    {
        $data['customers']     = Customer::whereIn('user_id' , $this->child_users)->orderBy('created_at' , 'desc')->paginate(20);
        $data['admins']        = User::where('id' , '!=' , Auth::user()->id())->get();
        $data['users']         = User::where('id' , '!=' , Auth::user()->id())->get();
        return View::make('customers.index' , $data);
    }

    public function getPaginatedOwner(){
        $data['owners'] = Owner::whereIn('user_id' , $this->child_users)->orderBy('created_at' , 'desc')->paginate(20);
        $customers = Owner::whereIn('user_id' , $this->child_users)->orderBy('created_at' , 'desc')->paginate(20);
        return Response::json(array(
            'customers'    => (String)View::make('customers.owners_grid' , $data),
            'links'        => (String)$customers->links()
        ));
    }

    public function postAdd(){

    	$customer = new Customer();
        $password = $this->getRandomString(4);
        $customer->name           = Input::get('fullname');
        $customer->username       = str_replace(" " , "" , str_replace("-" , "" , Input::get('user_phone')));
        $customer->password       = Hash::make($password);
        $customer->email          = Input::get('email');
        $customer->mobile_1       = str_replace(" " , "" , str_replace("-" , "" , Input::get('user_phone')));
        $customer->mobile_2       = Input::get('user_phone2');
        $customer->heard_from     = Input::get('heard_from');
        $customer->relation_start = date( 'Y-m-d' , strtotime(str_replace('.' , '/' , Input::get('relation_start'))));
        $customer->notes          = Input::get('message');
        $customer->active         = 1;
        $customer->user_id        = Input::get('manager');
        $customer->unhashed       = $password;
    	$customer->save();


    	$is_compound = (Input::has('is_compound')) ? 1 : 0;

    	$request = new CustomerRequest();
    	$request->customer_id   = $customer->id;
    	// $request->unit_type     = (Input::has('unit_type') ? implode('|' , Input::get('unit_type')) : '');
    	// $request->neighbour     = (Input::has('neighbours') ? implode('|' , Input::get('neighbours')) : '');
    	// $request->finishing     = (Input::has('finishing') ? implode('|' , Input::get('finishing')) : '');
    	// $request->floor         = (Input::has('floor') ? implode('|' , Input::get('floor')) : '');
    	// $request->payment_style = (Input::has('payment_style') ? implode('|' , Input::get('payment_style')) : '');
    	$request->purpose       = Input::get('radio_purpose');
    	$request->city          = Input::get('city');
    	$request->is_compound   = $is_compound;
    	$request->rooms_no      = Input::get('no_rooms');
    	$request->toilets_no    = Input::get('no_bathrooms');
    	$request->area          = str_replace(",", "" , Input::get('area'));
    	$request->objective     = Input::get('objective');
    	$request->price_from    = str_replace('$' , '' , str_replace(",", "" , Input::get('price_from')));
    	$request->price_to      = str_replace('$' , '' , str_replace(",", "" , Input::get('price_to')));;
        $request->features      = (Input::has('features') ? implode('|' , Input::get('features')) : '');

    	$request->save();

        foreach (Input::get('unit_type') as $unit_type) {
            $request_unit = new RequestUnit();
            $request_unit->unit_type_id = $unit_type;
            $request_unit->request_id   = $request->id;
            $request_unit->save();
        }
        foreach (Input::get('neighbour') as $neighbour) {
            $request_neighbour = new RequestNeighbour();
            $request_neighbour->neighbour_id = $neighbour;
            $request_neighbour->request_id   = $request->id;
            $request_neighbour->save();
        }
        foreach (Input::get('finishing') as $finishing) {
            $request_finishing = new RequestFinishing();
            $request_finishing->finishing_id = $finishing;
            $request_finishing->request_id   = $request->id;
            $request_finishing->save();
        }
        foreach (Input::get('floor') as $floor) {
            $request_floor = new RequestFloor();
            $request_floor->floor_id     = $floor;
            $request_floor->request_id   = $request->id;
            $request_floor->save();
        }
        foreach (Input::get('payment_style') as $payment_style) {
            $request_payment = new RequestPayment();
            $request_payment->payment_id   = $payment_style;
            $request_payment->request_id   = $request->id;
            $request_payment->save();
        }
        // if (Input::has('features')) {
        //     foreach (Input::get('features') as $feature) {
        //         $request_feature = new RequestFeature();
        //         $request_feature->feature_id   = $feature;
        //         $request_feature->request_id   = $request->id;
        //         $request_feature->save();
        //     }
        // }


    	$data['customers'] = Customer::whereIn('user_id' , $this->child_users)->orderBy('created_at' , 'desc')->get();
        return View::make('customers.list' , $data)->render();
    }

    public function getEdit($id){
        $data['customer'] = Customer::find($id);
        $data['request']  = CustomerRequest::whereCustomer_id($id)->first();
        $data['request_unit_type']   = RequestUnit::whereRequest_id($data['request']->id)->lists('unit_type_id');
        $data['request_neighbours']  = RequestNeighbour::whereRequest_id($data['request']->id)->lists('neighbour_id');
        $data['request_floors']      = RequestFloor::whereRequest_id($data['request']->id)->lists('floor_id');
        $data['request_finishing']   = RequestFinishing::whereRequest_id($data['request']->id)->lists('finishing_id');
        $data['request_payment']     = RequestPayment::whereRequest_id($data['request']->id)->lists('payment_id');
        // $data['request_features']    = RequestFeature::whereRequest_id($data['request']->id)->lists('feature_id');

        // $data['request_objective']   = RequestObjective::whereRequest_id($data['request']->id)->lists('objective_id');
        $data['selected_neighbours'] = Neighbour::whereCity_id($data['request']->city)->get();
        $data['users'] = User::all();
        // dd($data['request_neighbours']);
        // return json_encode($data);
        return View::make('customers.edit' , $data)->render();
    }

    public function postEdit($id){
        $customer = Customer::find($id);
        $customer->name           = Input::get('fullname');
        $customer->username       = str_replace(" " , "" , str_replace("-" , "" , Input::get('user_phone')));
        $customer->email          = Input::get('email');
        $customer->mobile_1       = str_replace(" " , "" , str_replace("-" , "" , Input::get('user_phone')));
        $customer->mobile_2       = Input::get('user_phone2');
        $customer->heard_from     = Input::get('heard_from');
        $customer->relation_start = date( 'Y-m-d' , strtotime(str_replace('.' , '/' , Input::get('relation_start'))));
        $customer->notes          = Input::get('message');
        $customer->user_id        = Input::get('manager');
        $customer->save();


        $is_compound = (Input::has('is_compound')?1:0);

        $request = CustomerRequest::whereCustomer_id($id)->first();
        $request->customer_id   = $customer->id;
        $request->purpose       = Input::get('radio_purpose');
        $request->city          = Input::get('city');
        $request->is_compound   = $is_compound;
        $request->rooms_no      = Input::get('no_rooms');
        $request->toilets_no    = Input::get('no_bathrooms');
        $request->area          = Input::get('area');
        $request->objective     = Input::get('objective');
        $request->price_from    = str_replace(",", "" , Input::get('price_from')) ;
        $request->price_to      = str_replace(",", "" , Input::get('price_to')) ;
        $request->features      = (Input::has('features') ? implode('|' , Input::get('features')) : '');
        $request->save();

        RequestUnit::whereRequest_id($request->id)->delete();
        foreach (Input::get('unit_type') as $unit_type) {
            $request_unit = new RequestUnit();
            $request_unit->unit_type_id = $unit_type;
            $request_unit->request_id   = $request->id;
            $request_unit->save();
        }
        RequestNeighbour::whereRequest_id($request->id)->delete();
        foreach (Input::get('neighbours') as $neighbour) {
            $request_neighbour = new RequestNeighbour();
            $request_neighbour->neighbour_id = $neighbour;
            $request_neighbour->request_id   = $request->id;
            $request_neighbour->save();
        }
        RequestFinishing::whereRequest_id($request->id)->delete();
        foreach (Input::get('finishing') as $finishing) {
            $request_finishing = new RequestFinishing();
            $request_finishing->finishing_id = $finishing;
            $request_finishing->request_id   = $request->id;
            $request_finishing->save();
        }
        RequestFloor::whereRequest_id($request->id)->delete();
        foreach (Input::get('floor') as $floor) {
            $request_floor = new RequestFloor();
            $request_floor->floor_id     = $floor;
            $request_floor->request_id   = $request->id;
            $request_floor->save();
        }
        RequestPayment::whereRequest_id($request->id)->delete();
        foreach (Input::get('payment_style') as $payment_style) {
            $request_payment = new RequestPayment();
            $request_payment->payment_id   = $payment_style;
            $request_payment->request_id   = $request->id;
            $request_payment->save();
        }
        // RequestFeature::whereRequest_id($request->id)->delete();
        // if (Input::has('features')) {
        //     foreach (Input::get('features') as $feature) {
        //         $request_feature = new RequestFeature();
        //         $request_feature->feature_id   = $feature;
        //         $request_feature->request_id   = $request->id;
        //         $request_feature->save();
        //     }
        // }


        $data['customers'] = Customer::whereIn('user_id' , $this->child_users)->orderBy('created_at' , 'desc')->get();
        return View::make('customers.list' , $data)->render();
    }

    public function getProfile($id = 0)
    {
        $data['admins']        = User::where('id' , '!=' , Auth::user()->id())->get();
        $customer  = Customer::withTrashed()->whereId($id)->first();
        $request   = CustomerRequest::whereCustomer_id($customer->id)->first();

        $property         = new Property();
        $suggestions      = new Property();
        $price_properties = new Property();


        $property         = $property->wherePurpose($request->purpose);
        $suggestions      = $suggestions->wherePurpose($request->purpose);
        $price_properties = $price_properties->wherePurpose($request->purpose);
        // $property    = $property->whereObjective_id($request->objective);
        // $suggestions = $suggestions->whereObjective_id($request->objective);
        $property         = $property->where('price' , '>=' , $request->price_from);
        $suggestions      = $suggestions->where('price' , '>=' , $request->price_from);
        $price_properties = $price_properties->where('price' , '>=' , $request->price_from);
        $property         = $property->where('price' , '<=' , $request->price_to);
        $suggestions      = $suggestions->where('price' , '<=' , $request->price_to);
        $price_properties = $price_properties->where('price' , '<=' , $request->price_to);
        $property         = $property->whereIs_compound($request->is_compound);
        $suggestions      = $suggestions->whereIs_compound($request->is_compound);
        $property         = $property->whereObjective_id($request->objective);
        $suggestions      = $suggestions->whereObjective_id($request->objective);
        $property         = $property->where(function($query) use ($request){
            $query->whereIn('unit_type' , RequestUnit::whereRequest_id($request->id)->lists('unit_type_id'));
        });
        $suggestions = $suggestions->where(function($query) use ($request){
            $query->whereIn('unit_type' , RequestUnit::whereRequest_id($request->id)->lists('unit_type_id'));
        });
        $price_properties = $price_properties->where(function($query) use ($request){
            $query->whereIn('unit_type' , RequestUnit::whereRequest_id($request->id)->lists('unit_type_id'));
        });
        $property = $property->where(function($query) use ($request){
            $query->whereIn('payment_style' , RequestPayment::whereRequest_id($request->id)->lists('payment_id'));
        });
        $suggestions = $suggestions->where(function($query) use ($request){
            $query->whereIn('payment_style' , RequestPayment::whereRequest_id($request->id)->lists('payment_id'));
        });
        $property = $property->where(function($query) use ($request){
            $query->whereIn('finishing' , RequestFinishing::whereRequest_id($request->id)->lists('finishing_id'));
        });
        $suggestions = $suggestions->where(function($query) use ($request){
            $query->whereIn('finishing' , RequestFinishing::whereRequest_id($request->id)->lists('finishing_id'));
        });

        //Sherif 2/12
        $margin      = Margin::whereField_name('rooms_no')->pluck('value_exact');
        if (!$margin){
            $margin = 1;
        }
        $property    = $property->whereBetween('rooms_no' , [ $request->rooms_no - $margin , $request->rooms_no + $margin ]);
        $margin      = Margin::whereField_name('rooms_no')->pluck('value_suggest');
        if (!$margin){
            $margin = 2;
        }
        $suggestions = $suggestions->whereBetween('rooms_no' , [ $request->rooms_no - $margin , $request->rooms_no + $margin ]);
        $margin      = Margin::whereField_name('toilets_no')->pluck('value_exact');
        if (!$margin){
            $margin = 1;
        }
        $property    = $property->whereBetween('toilets_no' , [ $request->toilets_no - $margin , $request->toilets_no + $margin ]);
        $margin      = Margin::whereField_name('toilets_no')->pluck('value_suggest');
        if (!$margin){
            $margin = 2;
        }
        $suggestions = $suggestions->whereBetween('toilets_no' , [ $request->toilets_no - $margin , $request->toilets_no + $margin ]);
        //end

        $property = $property->where(function($query) use ($request){
            $query->whereIn('neighbour' , RequestNeighbour::whereRequest_id($request->id)->lists('neighbour_id'));
        });

        $group_array      = [];
        $neighbours_array = [];
        foreach (RequestNeighbour::whereRequest_id($request->id)->lists('neighbour_id') as $neighbour) {
            $group = NeighbourGroup::whereNeighbour_id($neighbour)->get();
            foreach ($group as $g) {
                array_push($group_array , $g->group_id);
            }
        }

        $neighbours = NeighbourGroup::whereIn('group_id' , array_unique($group_array))->get();
        foreach ($neighbours as $n) {
            array_push($neighbours_array , $n->neighbour_id);
        }

        $neighbours_array = array_unique($neighbours_array);

        $suggestions = $suggestions->where(function($query) use ($neighbours_array){
            $query->whereIn('neighbour' , $neighbours_array);
        });

        $property = $property->where(function($query) use ($request){
            $query->whereIn('floor' , RequestFloor::whereRequest_id($request->id)->lists('floor_id'));
        });

        $group_array  = [];
        $floors_array = [];
        foreach (RequestFloor::whereRequest_id($request->id)->lists('floor_id') as $floor) {
            $floor = FloorGroup::whereFloor_id($floor)->get();
            foreach ($floor as $f) {
                array_push($group_array , $f->group_id);
            }
        }

        $floors = FloorGroup::whereIn('group_id' , array_unique($group_array))->get();
        foreach ($floors as $f) {
            array_push($floors_array , $f->floor_id);
        }

        $floors_array = array_unique($floors_array);

        $suggestions = $suggestions->where(function($query) use ($floors_array){
            $query->whereIn('floor' , $floors_array);
        });

        $property         = $property->get();
        $suggestions      = $suggestions->get();
        $price_properties = $price_properties->get();

        $suggestions      = $suggestions->diff($property);
        $price_properties = $price_properties->diff($property);
        $price_properties = $price_properties->diff($suggestions);

        $data['customer']         = $customer;
        $data['request']          = $request;
        $data['property']         = $property;
        $data['suggestions']      = $suggestions;
        $data['price_properties'] = $price_properties;
        $data['messages']  = CustomerTimeline::whereCustomer_id($id)->get();
        // $data['notifications']    = AdminNotification::whereType(3)->whereUser_id($id)->orderBy('created_at' , 'desc')->get();
        $ids                   = AssignedUser::whereUser_id(Auth::user()->id())->lists('notice_id');
        $data['notifications']      = AdminNotification::whereType(3)->whereUser_id($id)->where(function($query) use ($ids) {
            $query->whereIn('id' , $ids)->orWhere('from' , '=' , Auth::user()->id());
        })->paginate(100);
        // return json_encode($data);
        return View::make('customers.profile' , $data);
    }

    public function getUserProfile($id = 0)
    {
        // $data['admins']   = User::where('id' , '!=' , Auth::user()->id())->get();
        $data['customer']   = PublicUser::find($id);
        $favourites   = Favourite::whereUser_id($id)->lists('property_id');
        $favourites   = Property::whereIn('id' , $favourites)->get();
        $data['favourites'] = $favourites;
        $properties   = $this->matchProperties($id);
        $data['property']         = $properties[0]->diff($favourites);
        $data['suggestions']      = $properties[1]->diff($favourites);
        $data['price_properties'] = $properties[2]->diff($favourites);
        $data['messages']  = CustomerTimeline::whereCustomer_id($id)->get();
        return View::make('customers.user-profile' , $data);
    }

    public function getOwnerProfile($id = 0)
    {
        $owner = Owner::withTrashed()->find($id);
        $data['owner']   = $owner;
        $data['properties']         = Property::whereOwner_id($owner->id)->get();
        $data['notifications']    = AdminNotification::whereType(3)->whereUser_id($id)->orderBy('created_at' , 'desc')->get();
        return View::make('customers.owner-profile' , $data);
    }


    public function getProfilePage($id = 0)
    {
        $data['admins']        = User::where('id' , '!=' , Auth::user()->id())->get();
        $customer  = Customer::withTrashed()->whereId($id)->first();
        $request   = CustomerRequest::whereCustomer_id($customer->id)->first();

        $property    = new Property();
        $suggestions = new Property();
        $price_properties = new Property();

        $price_properties = $price_properties->whereCity($request->city);

        $property    = $property->wherePurpose($request->purpose);
        $suggestions = $suggestions->wherePurpose($request->purpose);
        $price_properties = $price_properties->wherePurpose($request->purpose);
        // $property    = $property->whereObjective_id($request->objective);
        // $suggestions = $suggestions->whereObjective_id($request->objective);
        $property    = $property->where('price' , '>=' , $request->price_from);
        $suggestions = $suggestions->where('price' , '>=' , $request->price_from);
        $price_properties = $price_properties->where('price' , '>=' , $request->price_from);
        $property    = $property->where('price' , '<=' , $request->price_to);
        $suggestions = $suggestions->where('price' , '<=' , $request->price_to);
        $price_properties = $price_properties->where('price' , '<=' , $request->price_to);
        $property    = $property->whereIs_compound($request->is_compound);
        $suggestions = $suggestions->whereIs_compound($request->is_compound);
        $property    = $property->whereObjective_id($request->objective);
        $suggestions = $suggestions->whereObjective_id($request->objective);
        $property    = $property->where(function($query) use ($request){
            $query->whereIn('unit_type' , RequestUnit::whereRequest_id($request->id)->lists('unit_type_id'));
        });
        $suggestions = $suggestions->where(function($query) use ($request){
            $query->whereIn('unit_type' , RequestUnit::whereRequest_id($request->id)->lists('unit_type_id'));
        });
        $property = $property->where(function($query) use ($request){
            $query->whereIn('payment_style' , RequestPayment::whereRequest_id($request->id)->lists('payment_id'));
        });
        $suggestions = $suggestions->where(function($query) use ($request){
            $query->whereIn('payment_style' , RequestPayment::whereRequest_id($request->id)->lists('payment_id'));
        });
        $property = $property->where(function($query) use ($request){
            $query->whereIn('finishing' , RequestFinishing::whereRequest_id($request->id)->lists('finishing_id'));
        });
        $suggestions = $suggestions->where(function($query) use ($request){
            $query->whereIn('finishing' , RequestFinishing::whereRequest_id($request->id)->lists('finishing_id'));
        });
        // $margin      = Margin::whereField_name('rooms_no')->pluck('value_exact');
        // $property    = $property->whereBetween('rooms_no' , [ $request->rooms_no - $margin , $request->rooms_no + $margin ]);
        // $margin      = Margin::whereField_name('rooms_no')->pluck('value_suggest');
        // $suggestions = $suggestions->whereBetween('rooms_no' , [ $request->rooms_no - $margin , $request->rooms_no + $margin ]);
        // $margin      = Margin::whereField_name('toilets_no')->pluck('value_exact');
        // $property    = $property->whereBetween('toilets_no' , [ $request->toilets_no - $margin , $request->toilets_no + $margin ]);
        // $margin      = Margin::whereField_name('toilets_no')->pluck('value_suggest');
        // $suggestions = $suggestions->whereBetween('toilets_no' , [ $request->toilets_no - $margin , $request->toilets_no + $margin ]);
        $property = $property->where(function($query) use ($request){
            $query->whereIn('neighbour' , RequestNeighbour::whereRequest_id($request->id)->lists('neighbour_id'));
        });

        $group_array      = [];
        $neighbours_array = [];
        foreach (RequestNeighbour::whereRequest_id($request->id)->lists('neighbour_id') as $neighbour) {
            $group = NeighbourGroup::whereNeighbour_id($neighbour)->get();
            foreach ($group as $g) {
                array_push($group_array , $g->group_id);
            }
        }

        $neighbours = NeighbourGroup::whereIn('group_id' , array_unique($group_array))->get();
        foreach ($neighbours as $n) {
            array_push($neighbours_array , $n->neighbour_id);
        }

        $neighbours_array = array_unique($neighbours_array);

        $suggestions = $suggestions->where(function($query) use ($neighbours_array){
            $query->whereIn('neighbour' , $neighbours_array);
        });

        $property = $property->where(function($query) use ($request){
            $query->whereIn('floor' , RequestFloor::whereRequest_id($request->id)->lists('floor_id'));
        });

        $group_array  = [];
        $floors_array = [];
        foreach (RequestFloor::whereRequest_id($request->id)->lists('floor_id') as $floor) {
            $floor = FloorGroup::whereFloor_id($floor)->get();
            foreach ($floor as $f) {
                array_push($group_array , $f->group_id);
            }
        }

        $floors = FloorGroup::whereIn('group_id' , array_unique($group_array))->get();
        foreach ($floors as $f) {
            array_push($floors_array , $f->floor_id);
        }

        $floors_array = array_unique($floors_array);

        $suggestions = $suggestions->where(function($query) use ($floors_array){
            $query->whereIn('floor' , $floors_array);
        });

        $property         = $property->get();
        $suggestions      = $suggestions->get();
        $price_properties = $price_properties->get();

        $suggestions      = $suggestions->diff($property);
        $price_properties = $price_properties->diff($property);
        $price_properties = $price_properties->diff($suggestions);

        // dd(URL::asset('assets/img/gallery/Image17.jpg'));
        $data['customer']         = $customer;
        $data['request']          = $request;
        $data['property']         = $property;
        $data['suggestions']      = $suggestions;
        $data['price_properties'] = $price_properties;
        // $data['notifications']    = AdminNotification::whereType(3)->whereUser_id($id)->orderBy('created_at' , 'desc')->get();
        $ids                   = AssignedUser::whereUser_id(Auth::user()->id())->lists('notice_id');
        $data['notifications']      = AdminNotification::whereType(3)->whereUser_id($id)->where(function($query) use ($ids) {
            $query->whereIn('id' , $ids)->orWhere('from' , '=' , Auth::user()->id());
        })->paginate(100);
        // return json_encode($data);
        $data['page']        = "";
        $data['messages']  = CustomerTimeline::whereCustomer_id($id)->get();
        return View::make('customers.profile2' , $data);
    }

    public function postDelete($id)
    {
        $customer = Customer::withTrashed()->find($id);
        if($customer->trashed()){
            $customer->forceDelete();
        }else{
            Customer::destroy($id);
        }


        $data['customers'] = Customer::orderBy('created_at' , 'desc')->get();
        return View::make('customers.list' , $data)->render();

    }

    public function postUnarchive($id){
        $customer = Customer::withTrashed()->find($id);

        $customer->restore();

        $data['customers'] = Customer::orderBy('created_at' , 'desc')->get();
        return View::make('customers.list' , $data)->render();
    }

    public function postDeleteOwner($id)
    {
        $owner = Owner::withTrashed()->find($id);
        if($owner->trashed()){
            $owner->forceDelete();
        }else{
            Owner::destroy($id);
        }


        $data['owners'] = Owner::orderBy('created_at' , 'desc')->paginate(20);
        return View::make('customers.owners_grid' , $data)->render();

    }

    public function postUnarchiveOwner($id){
        $owner = Owner::withTrashed()->find($id);

        $owner->restore();

        $data['owners'] = Owner::orderBy('created_at' , 'desc')->paginate(20);
        return View::make('customers.owners_grid' , $data)->render();
    }

    public function postSms($id){

        $customer = Customer::find($id);

        $user = "49222396";
        $password = "A426855a";
        $baseurl ="http://sms.com.eg";

        $sms = 'Hello username:' . $customer->username . ' ,password:' . $customer->unhashed . ' url: www.osool-eg.com';

        $text = urlencode($sms);
        $to = $customer->mobile_1;
        // auth call
        $url = "$baseurl/api/send.aspx?username=$user&password=$password&language=1&sender=Osool&mobile=$to&message=$text";

        // do auth call
        // $response = '';
        // $response = file_get_contents($url);
        function curl($url){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            //curl_setopt($ch, CURLOPT_TIMEOUT_MS, 1);
            curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
            $data = curl_exec($ch);
            curl_close($ch);
            return $data;
        }

        return curl($url);
    }

    public function postSearch(){
        if (!Input::get('string')) return 0;
        $data['customers'] = Customer::whereIn('user_id' , $this->child_users)->where(function($query) {
            $query->where('name' , 'LIKE' , '%'.Input::get('string').'%')->orWhere('email' , 'LIKE' , '%'.Input::get('string').'%')->orWhere('mobile_1' , 'LIKE' , '%'.Input::get('string').'%');
        })->get();
        $data['users']     = PublicUser::where('name' , 'LIKE' , '%'.Input::get('string').'%')->orWhere('mobile' , 'LIKE' , '%'.Input::get('string').'%')->get();
        return View::make('customers.list' , $data)->render();
    }

    public function getDeletedCustomers(){
        $customers = Customer::whereIn('user_id' , $this->child_users)->onlyTrashed()->get();
        $data['customers'] = $customers;
        return View::make('customers.list' , $data)->render();
    }

    public function getDeletedOwners(){
        $owner = Owner::whereIn('user_id' , $this->child_users)->onlyTrashed()->get();
        $data['owners'] = $owner;
        return View::make('customers.owners_grid' , $data)->render();
    }

    public function postAddMessage(){
        $id  = Input::get('id');
        $msg = Input::get('msg');

        $ct = new CustomerTimeline();
        $ct->customer_id = $id;
        $ct->message = $msg;

        $ct->save();

        $data['ct'] = $ct;

        return View::make('customers.timeline' , $data);

        return 1;
    }

    public function postEditMessage($id){
        $msg = Input::get('msg');
        $ct  = CustomerTimeline::find($id);
        $ct->message = $msg;
        $ct->save();

        return 1;
    }

    public function postDeleteMessage($id){
        $ct = CustomerTimeline::destroy($id);

        return 1;
    }

    public function getFetchNeighbour($city_id){
        $data['neighbours'] = Neighbour::whereCity_id($city_id)->get();


        return Response::json(array(
            'select'  => (String)View::make('customers.neighbour_select' , $data)
        ));
    }

    public function getRandomString($name_length = 8){
        $alpha_numeric = 'abcdefghijklmnopqrstuvwxyz0123456789';
        return substr(str_shuffle($alpha_numeric), 0, $name_length);
    }

    public function postToggleActive($id){
        $customer = Customer::find($id);


        $customer->active = ($customer->active == 1?'0' : '1');
        $customer->save();
    }

    public function account(){
        $customer = Customer::find(Session::get('id'));
        $request  = CustomerRequest::whereCustomer_id($customer->id)->first();

        $property         = new Property();
        $suggestions      = new Property();
        $price_properties = new Property();

        $property         = $property->wherePurpose($request->purpose);
        $suggestions      = $suggestions->wherePurpose($request->purpose);
        $price_properties = $price_properties->wherePurpose($request->purpose);
        $property         = $property->whereObjective_id($request->objective);
        $suggestions      = $suggestions->whereObjective_id($request->objective);
        $property         = $property->where('price' , '>=' , $request->price_from);
        $suggestions      = $suggestions->where('price' , '>=' , $request->price_from);
        $price_properties = $price_properties->where('price' , '>=' , $request->price_from);
        $property         = $property->where('price' , '<=' , $request->price_to);
        $suggestions      = $suggestions->where('price' , '<=' , $request->price_to);
        $price_properties = $price_properties->where('price' , '<=' , $request->price_to);
        $property         = $property->whereIs_compound($request->is_compound);
        $suggestions      = $suggestions->whereIs_compound($request->is_compound);
        $property         = $property->whereObjective_id($request->objective);
        $suggestions      = $suggestions->whereObjective_id($request->objective);
        $property         = $property->where(function($query) use ($request){
            $query->whereIn('unit_type' , RequestUnit::whereRequest_id($request->id)->lists('unit_type_id'));
        });
        $suggestions = $suggestions->where(function($query) use ($request){
            $query->whereIn('unit_type' , RequestUnit::whereRequest_id($request->id)->lists('unit_type_id'));
        });
        $price_properties = $price_properties->where(function($query) use ($request){
            $query->whereIn('unit_type' , RequestUnit::whereRequest_id($request->id)->lists('unit_type_id'));
        });
        $property = $property->where(function($query) use ($request){
            $query->whereIn('payment_style' , RequestPayment::whereRequest_id($request->id)->lists('payment_id'));
        });
        $suggestions = $suggestions->where(function($query) use ($request){
            $query->whereIn('payment_style' , RequestPayment::whereRequest_id($request->id)->lists('payment_id'));
        });
        $property = $property->where(function($query) use ($request){
            $query->whereIn('finishing' , RequestFinishing::whereRequest_id($request->id)->lists('finishing_id'));
        });
        $suggestions = $suggestions->where(function($query) use ($request){
            $query->whereIn('finishing' , RequestFinishing::whereRequest_id($request->id)->lists('finishing_id'));
        });
        $margin      = Margin::whereField_name('rooms_no')->pluck('value_exact');
        $property    = $property->whereBetween('rooms_no' , [ $request->rooms_no - $margin , $request->rooms_no + $margin ]);
        $margin      = Margin::whereField_name('rooms_no')->pluck('value_suggest');
        $suggestions = $suggestions->whereBetween('rooms_no' , [ $request->rooms_no - $margin , $request->rooms_no + $margin ]);
        $margin      = Margin::whereField_name('toilets_no')->pluck('value_exact');
        $property    = $property->whereBetween('toilets_no' , [ $request->toilets_no - $margin , $request->toilets_no + $margin ]);
        $margin      = Margin::whereField_name('toilets_no')->pluck('value_suggest');
        $suggestions = $suggestions->whereBetween('toilets_no' , [ $request->toilets_no - $margin , $request->toilets_no + $margin ]);
        $property = $property->where(function($query) use ($request){
            $query->whereIn('neighbour' , RequestNeighbour::whereRequest_id($request->id)->lists('neighbour_id'));
        });

        $group_array      = [];
        $neighbours_array = [];
        foreach (RequestNeighbour::whereRequest_id($request->id)->lists('neighbour_id') as $neighbour) {
            $group = NeighbourGroup::whereNeighbour_id($neighbour)->get();
            foreach ($group as $g) {
                array_push($group_array , $g->group_id);
            }
        }

        $neighbours = NeighbourGroup::whereIn('group_id' , array_unique($group_array))->get();
        foreach ($neighbours as $n) {
            array_push($neighbours_array , $n->neighbour_id);
        }

        $neighbours_array = array_unique($neighbours_array);

        $suggestions = $suggestions->where(function($query) use ($neighbours_array){
            $query->whereIn('neighbour' , $neighbours_array);
        });

        $property = $property->where(function($query) use ($request){
            $query->whereIn('floor' , RequestFloor::whereRequest_id($request->id)->lists('floor_id'));
        });

        $group_array  = [];
        $floors_array = [];
        foreach (RequestFloor::whereRequest_id($request->id)->lists('floor_id') as $floor) {
            $floor = FloorGroup::whereFloor_id($floor)->get();
            foreach ($floor as $f) {
                array_push($group_array , $f->group_id);
            }
        }

        $floors = FloorGroup::whereIn('group_id' , array_unique($group_array))->get();
        foreach ($floors as $f) {
            array_push($floors_array , $f->floor_id);
        }

        $floors_array = array_unique($floors_array);

        $suggestions = $suggestions->where(function($query) use ($floors_array){
            $query->whereIn('floor' , $floors_array);
        });

        $property         = $property->get();
        $suggestions      = $suggestions->get();
        $price_properties = $price_properties->get();

        $suggestions      = $suggestions->diff($property);
        $price_properties = $price_properties->diff($property);
        $price_properties = $price_properties->diff($suggestions);

        // dd(URL::asset('assets/img/gallery/Image17.jpg'));
        $data['customer']          = $customer;
        $data['request']           = $request;
        $data['property']          = $property;
        $data['suggestions']       = $suggestions;
        $data['price_properties']  = $price_properties;

        return View::make('customer_profile' , $data);
    }

    public function getNeighbour($city_id)
    {
        $data['neighbours'] = Neighbour::whereCity_id($city_id)->get();

        return Response::json(array(
            'select'  => (String)View::make('general.customer_filter_neighbours' , $data)
        ));
    }

    
    public function postFilter(){
        $input = Input::all();
        if ($input['customers'] == 0 || !$input['customers']) {
            $exactMatch = new CustomerRequest();

            if(Input::has('purpose')){
                if (Input::get('purpose') == 2) {
                    $exactMatch    = $exactMatch->whereIn('purpose' , [0 , 1]);
                }else{
                    $exactMatch    = $exactMatch->wherePurpose(Input::get('purpose'));
                }
            }

            if(Input::has('type')){
                $request_unit = RequestUnit::whereUnit_type_id(Input::get('type'))->lists('request_id');
                $exactMatch    = $exactMatch->whereIn('id' , $request_unit);
            }

            if(Input::has('city')){
                $exactMatch    = $exactMatch->whereCity(Input::get('city'));
            }

            if(Input::has('neighbour')){
                $exactMatch    = $exactMatch->whereIn('id' , RequestNeighbour::whereIn('neighbour_id' , Input::get('neighbour'))->lists('request_id'));
            }

            if(Input::has('is_compound')){
                $exactMatch    = $exactMatch->whereIs_compound(Input::get('is_compound'));
            }

            $data['customers'] = Customer::whereIn('user_id' , $this->child_users)->whereIn('id' , $exactMatch->lists('customer_id'))->get();

        }else{

            $exactMatch = new PublicUser();

            if(Input::has('purpose')){
                if (Input::get('purpose') == 2) {
                    $exactMatch    = $exactMatch->whereIn('purpose' , [0 , 1]);
                }else{
                    $exactMatch    = $exactMatch->wherePurpose(Input::get('purpose'));
                }
            }

            if(Input::has('type')){
                $exactMatch = $exactMatch->where('unit_type' , 'LIKE' , "%|". Input::get('type') . "|%");
            }

            if(Input::has('city')){
                $exactMatch    = $exactMatch->whereCity(Input::get('city'));
            }

            if(Input::has('neighbour')){
                foreach (Input::get('neighbour') as $neighbour) {
                    $exactMatch = $exactMatch->where('neighbour' , 'LIKE' , "%|". $neighbour . "|%");
                }
            }

            if(Input::has('is_compound')){
                $exactMatch    = $exactMatch->whereIs_compound(Input::get('is_compound'));
            }
            $data['customers'] = $exactMatch->get();
        }
        

        
        return Response::json(array(
            'grid'  => (String)View::make('customers.customers_grid' , $data)
        ));
    }

    public function postAddNotice(){
        $validator = Validator::make(Input::all() , [ 'body' => 'required']);
        if($validator->fails()){
            return $validator->messages()->first();
        }

        $task = new AdminNotification();

        $task->title       = '';
        $task->description = Input::get('body');
        $task->from        = Auth::user()->id();
        // $task->to          = Input::get('assignee');
        $task->type        = 3;
        $task->priority    = 0; 
        $task->user_id     = Input::get('current_id');;
        $task->key         = $task->generate_random_string(7);
        $task->save();

        $ids                   = AssignedUser::whereUser_id(Auth::user()->id())->lists('notice_id');
        // $data['notifications']      = AdminNotification::whereIn('id' , $ids)->whereType(3)->whereUser_id(Input::get('current_id'))->paginate(100);
        $data['notifications']      = AdminNotification::whereType(3)->whereUser_id(Input::get('current_id'))->where(function($query) use ($ids) {
            $query->whereIn('id' , $ids)->orWhere('from' , '=' , Auth::user()->id());
        })->paginate(100);

        foreach (Input::get('assignee') as $assignee) {
            $assigned = new AssignedUser();
            $assigned->notice_id = $task->id;
            $assigned->user_id = $assignee;
            $assigned->save();

            if($assignee != Auth::user()->id()){
                $notification = new AdminNotifiy();
                $notification->notifier  = Auth::user()->id();
                $notification->notice_id = $task->id;
                $notification->user_id   = $assignee;
                $notification->message   = User::whereId(Auth::user()->id())->pluck("name") . trans('main.comment_task')  ."-" . mb_strimwidth($task->description, 0, 30, "...");
                $notification->read      = 0;
                $notification->save();
                $this->pusher->trigger('channel_' . $assignee, 'event_notify', array('message' => $notification->message) );
            }
        }

        return Response::json(array(
            'table'    => (String)View::make('notifications.notification_filter' , $data),
            'code'     => 1
        ));
    }

    public function getSwitch(){
        if (Input::get('target') == 0){
            $customers = Customer::orderBy('created_at' , 'desc')->paginate(20);
            $data['customers'] = $customers;
            // return View::make('customers.customers_grid' , $data);
            return Response::json(array(
                'list'    => (String)View::make('customers.customers_grid' , $data),
                'links'        => (String)$customers->links()
            ));
        }elseif(Input::get('target') == 2){
            $owners = Owner::orderBy('created_at' , 'desc')->paginate(20);
            $data['owners'] = $owners;
            return Response::json(array(
                'list'    => (String)View::make('customers.owners_grid' , $data),
                'links'        => (String)$owners->links()
            ));
        }else{
            $users= PublicUser::orderBy('created_at' , 'desc')->paginate(20);
            $data['customers'] = $users;
            return Response::json(array(
                'list'    => (String)View::make('customers.users_grid' , $data),
                'links'        => (String)$users->links()
            ));
        }
    }

    private function matchProperties($user_id = 0){

        $user = PublicUser::whereId($user_id)->first();

        $property         = new Property();
        $suggestions      = new Property();
        $price_properties = new Property();
        $price_properties = $price_properties->whereCity($user->city);
        $property         = $property->wherePurpose($user->purpose);
        $suggestions      = $suggestions->wherePurpose($user->purpose);
        $price_properties = $price_properties->wherePurpose($user->purpose);
        
        // $property    = $property->whereObjective_id($request->objective);
        // $suggestions = $suggestions->whereObjective_id($request->objective);
        $property         = $property->where('price' , '>=' , $user->price_from);
        $suggestions      = $suggestions->where('price' , '>=' , $user->price_from);
        $price_properties = $price_properties->where('price' , '>=' , $user->price_from);
        $property         = $property->where('price' , '<=' , $user->price_to);
        $suggestions      = $suggestions->where('price' , '<=' , $user->price_to);
        $price_properties = $price_properties->where('price' , '<=' , $user->price_to);

        if($user->objective != ""){
            $property         = $property->whereObjective_id($user->objective);
            $suggestions      = $suggestions->whereObjective_id($user->objective);
        }
        if($user->is_compound != ""){
            $property         = $property->whereIs_compound($user->is_compound);
            $suggestions      = $suggestions->whereIs_compound($user->is_compound);
        }
        
        
        $property         = $property->where(function($query) use ($user){
            $query->whereIn('unit_type' , explode("|" , $user->unit_type));
        });
        $suggestions = $suggestions->where(function($query) use ($user){
            $query->whereIn('unit_type' , explode("|" , $user->unit_type));
        });

        $property = $property->where(function($query) use ($user){
            $query->whereIn('payment_style' , explode("|" , $user->payment_type));
        });
        $suggestions = $suggestions->where(function($query) use ($user){
            $query->whereIn('payment_style' , explode("|" , $user->payment_type));
        });

        $property = $property->where(function($query) use ($user){
            $query->whereIn('finishing' , explode("|" , $user->finishing));
        });
        $suggestions = $suggestions->where(function($query) use ($user){
            $query->whereIn('finishing' , explode("|" , $user->finishing));
        });

        if($user->neighbour != ""){
            $property = $property->where(function($query) use ($user){
                $query->whereIn('neighbour' , explode("|" , $user->neighbour));
            });
            $group_array      = [];
            $neighbours_array = [];
            foreach ( explode("|" , $user->neighbour) as $neighbour) {
                $group = NeighbourGroup::whereNeighbour_id($neighbour)->get();
                foreach ($group as $g) {
                    array_push($group_array , $g->group_id);
                }
            }

            $neighbours = NeighbourGroup::whereIn('group_id' , array_unique($group_array))->get();
            foreach ($neighbours as $n) {
                array_push($neighbours_array , $n->neighbour_id);
            }

            $neighbours_array = array_unique($neighbours_array);

            $suggestions = $suggestions->where(function($query) use ($neighbours_array){
                $query->whereIn('neighbour' , $neighbours_array);
            });

            $property = $property->where(function($query) use ($user){
                $query->whereIn('floor' ,  explode("|" , $user->floor));
            });
        }   
        
        if($user->floor != ""){
            $group_array  = [];
            $floors_array = [];
            foreach (explode("|" , $user->floor) as $floor) {
                $floor = FloorGroup::whereFloor_id($floor)->get();
                foreach ($floor as $f) {
                    array_push($group_array , $f->group_id);
                }
            }

            $floors = FloorGroup::whereIn('group_id' , array_unique($group_array))->get();
            foreach ($floors as $f) {
                array_push($floors_array , $f->floor_id);
            }

            $floors_array = array_unique($floors_array);

            $suggestions = $suggestions->where(function($query) use ($floors_array){
                $query->whereIn('floor' , $floors_array);
            });
        }
        
        $property         = $property->get();
        $suggestions      = $suggestions->get();
        $price_properties = $price_properties->get();

        $suggestions      = $suggestions->diff($property);
        $price_properties = $price_properties->diff($property);
        $price_properties = $price_properties->diff($suggestions);


        return [$property , $suggestions , $price_properties];
    }

    public function getAddProperty($owner_id){
        $data['owner_id'] = $owner_id;
        return View::make('customers.add_property_form' , $data)->render();
    }

    public function postAddProperty(){
        $pc = new PropertyController();
        // Input::merge(['exist_owner' => $owner_id]);
        $pc->postAdd();
    }

}
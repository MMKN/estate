<?php

class UserController extends \APIController {

	public $users_ids;

	public function __construct(){
		parent::__construct();
		$this->users_ids = $this->getChildren(Auth::user()->id());
		array_unshift($this->users_ids, Auth::user()->id());
	}

	public function index()
	{
		$users = User::all();

		return $this->respond([
			'data' => $users,
			'code' => 201
			]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		// $user = User::find(Auth::user()->id());
		// $users_ids = $this->getChildren($user->id);
		if(Auth::user()->user()->is_admin){
			$users = User::latest()->paginate(20);
		}else{
			$users = User::whereIn('id' , $this->users_ids)->orderBy('created_at' , 'desc')->paginate(20);
		}
		
		$data['users'] = $users;
		$data['admins']        = User::where('id' , '!=' , Auth::user()->id())->get();
		$data['page']        = 'users';
		return View::make('_layouts.core' , $data)->nest('content' , 'users.index' , $data);
	}

	public function getPaginated(){
	    if(Auth::user()->user()->is_admin){
	    	$users = $data['users'] = User::latest()->paginate(20);
	    }else{
	    	$users = $data['users'] = User::whereIn('id',$this->users_ids)->orderBy('created_at' , 'desc')->paginate(20);
	    }
	    return Response::json(array(
	        'users'    => (String)View::make('users.users_grid' , $data),
	        'links'        => (String)$users->links()
	    ));
	}


	public function postAdd(){
		$data = Input::all();
		$validator = Validator::make($data, User::$rules);

		if ($validator->fails()) {
		  $data['message'] = $validator->messages()->first();
		  return Response::json(array(
		    'message' => (String)View::make('general.danger_message' , $data),
		    'state'   => 'fail'
		  ));
		}

		$user = new User;
		// if(Input::hasFile('image')){
		//     $file = Input::file('image');
		//     $filename = (String)rand(100 , 10000) . "_" . $file->getClientOriginalName();
		//     $file->move(public_path()."/uploads/profile/",$filename);
		//     $user->image    = $filename;
		// }else{
		//     $user->image    = 'user.png';
		// }
		$user->name     = $data['name'];
		$user->email    = $data['email'];
		$user->mobile   = $data['mobile'];
		$user->password = Hash::make($data['password']);
		$user->is_admin = $data['role'];
		$user->manager = $data['manager'];
		$user->image    = 'user.png';
		if($user->save()){
			if(Auth::user()->user()->is_admin){
				$users = $data['users'] = User::latest()->paginate(20);
			}else{
				$users = $data['users'] = User::whereIn('id',$this->users_ids)->orderBy('created_at' , 'desc')->paginate(20);
			}
			return Response::json(array(
			    'users' =>  View::make('users.users_grid' , $data)->render(),
			    'state' => 'success'
			));
		}
	}


	public static function getChildren($parent) {
	    $results = User::whereManager($parent)->withTrashed()->get();
	    $children = array();
	    $i = 0;
	    foreach($results as $result) {
	        // $children[$i] = array();
	        $children[] = $result->id;
	        $children = array_merge($children , UserController::getChildren($result->id));
	    	$i++;
	    }
		return $children;
	}

	public function getFilter(){
		$user = User::find(Auth::user()->id());
		$users_ids = $this->getChildren($user->id);
		$users = new User();
		$query = Input::get('query');
		if(Auth::user()->user()->is_admin){
			$users = $data['users'] = User::all();
		}else{
			$users = $data['users'] = User::whereIn('id',$this->users_ids);
		}
		$users = $users->where(function($q) use ($query){
			$q->where('name' ,'LIKE', '%'.$query.'%')->orWhere('email' ,'LIKE', '%'.$query.'%')->orWhere('mobile' ,'LIKE', '%'.$query.'%');
		})->get();
		$data['users'] = $users;


		return Response::json(
			[
			'grid' => (String)View::make('users.users_grid' , $data)
			]
		);
	}

	public function postDeleteUser(){
		$id = Input::get('id');
		$user = User::withTrashed()->find($id);
		if($user->trashed()){
		    $user->forceDelete();
		}else{
		    User::destroy($id);
		}


		if(Auth::user()->user()->is_admin){
			$users = $data['users'] = User::latest()->paginate(20);
		}else{
			$users = $data['users'] = User::whereIn('id',$this->users_ids)->orderBy('created_at' , 'desc')->paginate(20);
		}
		return View::make('users.users_grid' , $data)->render();
	}

	public function getDeletedUsers(){
		$users = User::onlyTrashed()->whereIn('id' , $this->users_ids)->get();
		if(Auth::user()->user()->is_admin){
			$users = $data['users'] = User::onlyTrashed();
		}else{
			$users = $data['users'] = User::onlyTrashed()->whereIn('id' , $this->users_ids)->get();
		}

		$data['users'] = $users;
		return (String)View::make('users.users_grid' , $data);
	}

	public function postUnarchive($id){
	    $user = User::withTrashed()->find($id);

	    $user->restore();

	    if(Auth::user()->user()->is_admin){
			$users = $data['users'] = User::latest()->paginate(20);
		}else{
			$users = $data['users'] = User::whereIn('id',$this->users_ids)->orderBy('created_at' , 'desc')->paginate(20);
		}
	    return (String)View::make('users.users_grid' , $data)->render();
	}
}
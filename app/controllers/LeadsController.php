<?php

use app\transformers\LeadsTransformer;

class LeadsController extends \APIController {
	private $leadsTransformer;

	public function __construct(LeadsTransformer $leadsTransformer)
	{
		$this->leadsTransformer = $leadsTransformer;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$leads = Lead::with('tags' , 'users','tasks' , 'tasks.comments')->get();

		return $this->respond([
			'data' => $this->leadsTransformer->transformCollection($leads) ,
			'code' => 200
			]);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$start_date = date('Y-m-d H:i:s',strtotime(str_replace('/' , '-' , Input::get('start_date'))));
		$validator = Validator::make(Input::all() , Lead::$rules);
		if($validator->fails()){
			return $this->respondNotValid($validator->errors());
		}
		$lead = new Lead();
		$lead->creator_id = Input::get('creator_id');
		if(Input::get('customer_type') == "b2c"){
			$lead->individual_id = Input::get('customer_id');
		}else if(Input::get('customer_type') == "b2b"){
			$lead->company_id = Input::get('customer_id');
		}
		$lead->description = Input::get('description');
		$lead->status = Input::get('status');
		$lead->potential_value = Input::get('potential_value');
		$lead->winning_probability = Input::get('winning_probability');

		try{
			$lead->save();
		}catch(\Illuminate\Database\QueryException $e){
			return $this->respondNotValid($e->getMessage());
		}


		if(Input::has('tags')){
			try{
				$lead->tags()->attach(Input::get('tags'));
			}catch(\Illuminate\Database\QueryException $e){
			}
		}

		if(Input::has('members')){
			try{
				$lead->users()->attach(Input::get('members'));
			}catch(\Illuminate\Database\QueryException $e){
			}
		}

		return $this->respond([
			'data' => $lead,
			'code' => 201
			]);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$lead = Lead::with('tasks' , 'tags')->find($id);

		if(!$lead){
			return $this->respondNotFound('Lead Not Found');
		}

		return $this->respond([
			'data' => $lead,
			'code' => '200'
			]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$lead = Lead::find($id);

		//check if lead exist
		if(!$lead){
			return $this->respondNotFound('Lead Not Found');
		}
		
		$validator = Validator::make(Input::all() , Lead::$rules);
		if($validator->fails()){
			return $this->respondNotValid($validator->errors());
		}

		$start_date = date('Y-m-d H:i:s',strtotime(str_replace('/' , '-' , Input::get('start_date'))));

		$lead->creator_id = Input::get('creator_id');
		if(Input::get('customer_type') == "b2c"){
			$lead->individual_id = Input::get('customer_id');
		}else if(Input::get('customer_type') == "b2b"){
			$lead->company_id = Input::get('customer_id');
		}
		$lead->description = Input::get('description');
		$lead->status = Input::get('status');
		$lead->potential_value = Input::get('potential_value');
		$lead->winning_probability = Input::get('winning_probability');

		//try to update
		try{
			$lead->update();
		}catch(\Illuminate\Database\QueryException $e){
			return $this->respondNotValid($e->getMessage());
		}

		if(Input::has('tags')){
			try{
				$lead->tags()->sync(Input::get('tags'));
			}catch(\Illuminate\Database\QueryException $e){
				return $this->respondNotValid('integrity error');
			}
		}

		if(Input::has('members')){
			try{
				$lead->users()->sync(Input::get('users'));
			}catch(\Illuminate\Database\QueryException $e){
				return $this->respondNotValid('integrity error');
			}
		}

		return $this->respond([
			'data' => $lead,
			'code' => 200
			]);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$lead = Lead::withTrashed()->find($id);

		if(!$lead){
			return $this->respondNotFound('Lead Not Found');
		}

		if($lead->trashed()){
			$lead->forceDelete();
		}else{
			$lead->delete();
		}

		return $this->respond([
			'message' => 'Lead deleted successfully',
			'code' => 200
			]);

	}

	public function getArchived()
	{
		$leads = Lead::onlyTrashed();
		return $this->respond([
			'data' => $leads,
			'code' => 200
			]);
	}

	public function postAttachTags($id)
	{
		$tags = Input::get('tags');

		$lead = Lead::with('tags')->find($id);
		try{
			$lead->tags()->sync($tags);
		}catch(\Illuminate\Database\QueryException $e){
			return $this->respondNotValid('integrity error');
		}
		

		return $this->respond([
			'message' => 'tags updated successfully',
			'code' => 200
			]);
	}

	public function postAttachUsers()
	{
		$users = Input::get('users');

		$lead = Lead::with('users')->find($id);
		try{
			$lead->users()->sync($users);
		}catch(\Illuminate\Database\QueryException $e){
			return $this->respondNotValid('integrity error');
		}
		

		return $this->respond([
			'message' => 'tags updated successfully',
			'code' => 200
			]);	
	}

}

<?php

class TaskCommentsController extends \APIController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($lead_id , $task_id)
	{
		$lead = Lead::find($lead_id);
		if(!$lead){
			return $this->respondNotFound('Lead Not Found');
		}

		$task = $lead->tasks()->where('id' , $task_id)->first();

		if(!$task){
			return $this->respondNotFound('Task Not Found');
		}

		return $this->respond([
			'data' => $task->comments,
			'code' => '200'
			]);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($lead_id , $task_id)
	{
		try{
			$task = Lead::findOrFail($lead_id)->tasks()->where('id' , $task_id)->first();
		}catch(Illuminate\Database\Eloquent\ModelNotFoundException $e){
			return $this->respondNotFound('Lead Not Found');
		}
		if(!$task){
			return $this->respondNotFound('Task Not Found');
		}
		$comment = new TaskComment();
		$comment->comment = Input::get('comment');
		$comment->creator_id = Input::get('creator_id');
		$task->comments()->save($comment);

		return $this->respond([
			'data' => $task,
			'code' => 200
			]);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($lead_id , $task_id , $comment_id)
	{
		try{
			$task = Lead::findOrFail($lead_id)->tasks()->findOrFail($task_id);
		}catch(Illuminate\Database\Eloquent\ModelNotFoundException $e){
			return $this->respondNotFound('Not Found');
		}
		$comment = $task->comments()->find($comment_id);
		
		if(!$comment){
			return $this->respondNotFound('Comment Not Found');
		}

		return $this->respond([
			'data' => $comment,
			'code' => 200
			]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($lead_id , $task_id , $comment_id)
	{
		try{
			$task = Lead::findOrFail($lead_id)->tasks()->findOrFail($task_id);
		}catch(Illuminate\Database\Eloquent\ModelNotFoundException $e){
			return $this->respondNotFound('Not Found');
		}
		$comment = $task->comments()->find($comment_id);
		if(!$comment){
			return $this->respondNotFound('Comment Not Found');
		}

		$comment->comment = Input::get('comment');
		$comment->update();

		return $this->respond([
			'data' => $comment,
			'code' => '200'
			]);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($lead_id , $task_id , $comment_id)
	{
		try{
			$task = Lead::findOrFail($lead_id)->tasks()->findOrFail($task_id);
		}catch(Illuminate\Database\Eloquent\ModelNotFoundException $e){
			return $this->respondNotFound('Not Found');
		}
		$comment = $task->comments()->withTrashed()->find($comment_id);

		if(!$comment){
			return $this->respondNotFound('Comment Not Found');
		}

		if($comment->trashed()){
			$comment->forceDelete();
		}else{
			$comment->delete();
		}

		return $this->respond([
			'message' => 'Comment deleted successfully',
			'code' => 200
			]);
	}


}

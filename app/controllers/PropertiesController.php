<?php

class PropertiesController extends BaseController {

    public $child_owners;
    public $pusher;
    public $child_users;

    public function __construct(){
        parent::__construct();
        $this->child_users = UserController::getChildren(Auth::user()->id());
        array_push($this->child_users , Auth::user()->id());
        $this->child_owners = Owner::whereIn('user_id' , $this->child_users)->get();
        $this->pusher = new Pusher('740b3e5ad1236ca3a8d8' ,'bf5a68796761c46e6b02' ,'180891');
    }

    public function getIndex(){
        // $data['properties']    = Property::latest()->whereIn('owner_id' , $this->child_owners->lists('id') )->paginate(30);
        $data['properties']    = Property::latest()->paginate(30);
        $data['admins']        = User::where('id' , '!=' , Auth::user()->id())->get();
        return View::make('properties.index' , $data);
    }

    public function getPaginated(){
        // dd(Input::get('page'));
        $data['properties'] = Property::orderBy('created_at' , 'desc')->paginate(30);
        $properties = Property::orderBy('created_at' , 'desc')->paginate(30);
        return Response::json(array(
            'properties'   => (String)View::make('properties.grid' , $data),
            'links'        =>  (String)$properties->links()
        ));
    }

    public function getAdd()
    {
        $data['owners'] = Owner::all();
        $cc  = new CustomersController();
        $data['users']  = User::whereIn('id' , $cc->child_users)->get();
        // dd($data['users']);
        return View::make('properties.add_form' , $data);
    }

    public function postAdd(){
        $inputs = Input::all();

        $published = [];
        if(Input::has('published')){
            $array = Input::get('published');
            $count = count(Input::get('published'));
            $i = 0;
            while ($i <= $count-1){
                if($i == $count-1){
                    $published[] = 0;
                    break;
                }else if($array[$i] == 0 && $array[$i+1] == 1){
                    $published[] = 1;
                    $i += 2;
                }else{
                    $published[] = 0;
                    $i++;
                }
            }
        }

        // if it's not exsist add it it to inputs array
        if(!Input::has('is_compound')){
            $inputs['is_compound'] = 0;
        }

        if(!Input::has('is_published')){
            $inputs['is_published'] = 0;
        }

        if(!Input::has('show_map')){
            $inputs['show_map'] = 0;
        }

        if(Input::has('exist_owner')){
            $owner = Owner::find(Input::get('exist_owner'));
        }else{
            $owner            = new Owner;
            $owner->name      = $inputs['name'];
            $owner->email     = $inputs['email'];
            $owner->mobile1   = str_replace(' ' , '' , str_replace("-" , "" , $inputs['mobile']));
            $owner->landline  = $inputs['landline'];
            $owner->notes     = $inputs['notes'];
            // $owner->user_id   = $inputs['manager'];
            $owner->heard_from= $inputs['heard'];
            $owner->save();
        }



        $property = new Property;
        $property->price              = str_replace("," , "" , $inputs['price']);
        $property->purpose            = $inputs['purpose'];
        $property->address            = $inputs['address'];
        $property->plot_no            = $inputs['plot_no'];
        $property->legal              = $inputs['legal'];
        $property->apartment_no       = $inputs['apartment_no'];
        $property->description        = $inputs['description'];
        $property->unit_type          = $inputs['unit_type'];
        $property->city               = $inputs['city'];
        $property->neighbour          = $inputs['neighbour'];
        $property->is_compound        = $inputs['is_compound'];
        $property->is_published       = $inputs['is_published'];
        $property->finishing          = $inputs['finishing'];
        $property->payment_style      = $inputs['payment_style'];
        $property->floor              = $inputs['floor'];
        $property->area               = str_replace("," , "" , $inputs['area']);
        $property->rooms_no           = str_replace("," , "" , $inputs['rooms_no']);
        $property->toilets_no         = str_replace("," , "" , $inputs['toilets_no']);
        $property->distance_facilites = str_replace("," , "" , $inputs['distance_facilites']);
        $property->latitude           = $inputs['latitude'];
        $property->longitude          = $inputs['longitude'];
        // $property->heard              = $inputs['heard'];
        $property->objective_id       = $inputs['objective'];
        $property->owner_id           = $owner->id;
        $property->key                = $this->generate_random_string();
        $property->show_map           = $inputs['show_map'];
        $property->save();

        if(Input::has('features')){
            foreach ($inputs['features'] as $feature) {
                $property_feature = new PropertyFeature;
                $property_feature->property_id = $property->id;
                $property_feature->feature_id  = $feature;
                $property_feature->save();
            }
        }

        if(Input::has('images')){
            $offset = 0;
            foreach (Input::get('images') as $image) {
                $i              = new Image;
                $i->url         = $image;
                $i->property_id = $property->id;
                $i->main        = (Input::get('google_image') == $image) ? 1 : 0;
                $i->published   = $published[$offset];
                $i->save();
                $offset++;
            }
        }
        
        $users_notified = $this->publicCustomer($property->id);
        $exacts = $users_notified['exact'];

        foreach ($exacts as $exact) {
            $notif = new Notification();
            $notif->user_id     = $exact->id;
            $notif->property_id = $property->id;
            $notif->read        = 0;
            $notif->save();
        }

        $data['properties'] = Property::latest()->get();
        return Response::json(array(
            'grid'    => (String)View::make('properties.grid' , $data),
        ));
    }



    public function getEdit($id){
        $data['property']          = Property::find($id);
        $data['purpose']           = $data['property']->purpose; // to check which is selected (sale or rent)
        $data['owner']             = $data['property']->getOwner($data['property']->owner_id);
        $data['selected_features'] = PropertyFeature::whereProperty_id($data['property']->id)->get();
        $data['images']            = Image::whereProperty_id($data['property']->id)->get();

        return Response::json(array(
            'property' =>  (String)View::make('properties.edit_form' , $data),
        ));
    }

    public function postEdit($id){
        $inputs   = Input::all();
        // dd($inputs);
        $property = Property::find($id);
        $owner    = Owner::withTrashed()->find($property->owner_id);
        $published = [];
        if(Input::has('published')){
            $array = Input::get('published');
            $count = count(Input::get('published'));
            $i = 0;
            while ($i <= $count-1){
                if($i == $count-1){
                    $published[] = 0;
                    break;
                }else if($array[$i] == 0 && $array[$i+1] == 1){
                    $published[] = 1;
                    $i += 2;
                }else{
                    $published[] = 0;
                    $i++;
                }
            }
        }

        $owner->name      = $inputs['name'];
        $owner->email     = $inputs['email'];
        $owner->mobile1   = str_replace(' ' , '' , str_replace("-" , "" , $inputs['mobile']));
        $owner->landline  = $inputs['landline'];
        $owner->notes     = $inputs['notes'];
        // $owner->user_id   = $inputs['manager'];
        $owner->update();

        // if it's not exsist add it to inputs array
        if(!Input::has('is_compound')){
            $inputs['is_compound'] = 0;
        }
        if(!Input::has('is_published')){
            $inputs['is_published'] = 0;
        }
        if(!Input::has('show_map')){
            $inputs['show_map'] = 0;
        }

        $property->price              = str_replace("," , "" , $inputs['price']);
        $property->purpose            = $inputs['purpose'];
        $property->address            = $inputs['address'];
        $property->plot_no            = $inputs['plot_no'];
        $property->legal              = $inputs['legal'];
        $property->apartment_no       = $inputs['apartment_no'];
        $property->description        = $inputs['description'];
        $property->unit_type          = $inputs['unit_type'];
        $property->city               = $inputs['city'];
        $property->neighbour          = $inputs['neighbour'];
        $property->is_compound        = $inputs['is_compound'];
        $property->is_published       = $inputs['is_published'];
        $property->finishing          = $inputs['finishing'];
        $property->payment_style      = $inputs['payment_style'];
        $property->floor              = $inputs['floor'];
        $property->area               = str_replace("," , "" , $inputs['area']);;
        $property->rooms_no           = $inputs['rooms_no'];
        $property->toilets_no         = $inputs['toilets_no'];
        $property->distance_facilites = $inputs['distance_facilites'];
        $property->objective_id       = $inputs['objective'];
        $property->latitude           = $inputs['latitude'];
        $property->longitude          = $inputs['longitude'];
        $property->heard              = $inputs['heard'];
        $property->show_map           = $inputs['show_map'];
        $property->update();


        // delete the property features if exsist
        $property_features = PropertyFeature::whereProperty_id($property->id)->delete();

        // add new property features if exsist
        if(Input::has('features')){
            foreach ($inputs['features'] as $feature) {
                $property_feature = new PropertyFeature;
                $property_feature->property_id = $property->id;
                $property_feature->feature_id  = $feature;
                $property_feature->save();
            }
        }

        $old_images = Image::whereProperty_id($property->id)->get();
        foreach ($old_images as $image) {
            $image->delete();
        }


        if(Input::has('images')){
            $offset = 0;
            foreach (Input::get('images') as $image) {
                $i              = new Image;
                $i->url         = $image;
                $i->property_id = $property->id;
                $i->main        = (Input::get('google_image') == $image) ? 1 : 0;
                $i->published   = $published[$offset];
                $i->save();
                $offset++;
            }
        }

        $data['properties'] = Property::latest()->get();
        return Response::json(array(
            'grid'    => (String)View::make('properties.grid' , $data),
        ));
    }

    /**
     * Author : Sherif and Kamel
     *
     * @param void
     * @return Array of objects
     * Filters properties by values
     */
    public function postFilter(){
        $property         = new Property();  // Exact Match
        $suggestions      = new Property();  // Suggested Properties
        $price_properties = new Property();  // Properties with the same price

        // check if value exists or not
        // If exists filter by value for both
        if(Input::has('purpose')){
            $property         = $property->wherePurpose(Input::get('purpose'));
            $suggestions      = $suggestions->wherePurpose(Input::get('purpose'));
            $price_properties = $price_properties->wherePurpose(Input::get('purpose'));
        }

        if(Input::has('unit_type')){
            $property = $property->where(function($query){
                $query->whereIn('unit_type' , Input::get('unit_type'));
            });

            $suggestions = $suggestions->where(function($query){
                $query->whereIn('unit_type' , Input::get('unit_type'));
            });

            $price_properties = $price_properties->where(function($query){
                $query->whereIn('unit_type' , Input::get('unit_type'));
            });
        }

        if(Input::has('city')){
            $property    = $property->whereCity(Input::get('city'));
            if (!Input::has('neighbour')) {
                $suggestions = $suggestions->whereCity(Input::get('city'));
            }
        }

        
        if(Input::has('neighbour')){
            $property = $property->where(function($query){
                $query->whereIn('neighbour' , Input::get('neighbour'));
            });

            // store all the groups of the neighbours sent by the user
            $group_array      = [];
            $neighbours_array = [];
            foreach (Input::get('neighbour') as $neighbour) {
                $group = NeighbourGroup::whereNeighbour_id($neighbour)->get();
                foreach ($group as $g) {
                    array_push($group_array , $g->group_id);
                }
            }

            $neighbours = NeighbourGroup::whereIn('group_id' , array_unique($group_array))->get();
            foreach ($neighbours as $n) {
                array_push($neighbours_array , $n->neighbour_id);
            }

            $neighbours_array = array_unique($neighbours_array);

            $suggestions = $suggestions->where(function($query) use ($neighbours_array){
                $query->whereIn('neighbour' , $neighbours_array);
            });
        }

        

        if(Input::has('floor')){
            $property = $property->where(function($query){
                $query->whereIn('floor' , Input::get('floor'));
            });

            // store all the groups of the neighbours sent by the user
            $group_array  = [];
            $floors_array = [];
            foreach (Input::get('floor') as $floor) {
                $floor = FloorGroup::whereFloor_id($floor)->get();
                foreach ($floor as $f) {
                    array_push($group_array , $f->group_id);
                }
            }

            $floors = FloorGroup::whereIn('group_id' , array_unique($group_array))->get();
            foreach ($floors as $f) {
                array_push($floors_array , $f->floor_id);
            }

            $floors_array = array_unique($floors_array);

            $suggestions = $suggestions->where(function($query) use ($floors_array){
                $query->whereIn('floor' , $floors_array);
            });
        }


        // return $suggestions->get();

        if(Input::has('finishing')){
            $property = $property->where(function($query){
                $query->whereIn('finishing' , Input::get('finishing'));
            });

            $suggestions = $suggestions->where(function($query){
                $query->whereIn('finishing' , Input::get('finishing'));
            });
        }

        if(Input::has('payment_style')){
            $property = $property->where(function($query){
                $query->whereIn('payment_style' , Input::get('payment_style'));
            });

            $suggestions = $suggestions->where(function($query){
                $query->whereIn('payment_style' , Input::get('payment_style'));
            });
        }

        if(Input::has('objective')){
            $property    = $property->whereObjective_id(Input::get('objective'));
            $suggestions = $suggestions->whereObjective_id(Input::get('objective'));
        }
        // dd(Input::all());
        if(Input::has('price_from')){
            $property         = $property->where('price' , '>=' , str_replace("$" , "" , str_replace("," , "" , Input::get('price_from'))));
            $suggestions      = $suggestions->where('price' , '>=' , str_replace("$" , "" , str_replace("," , "" , Input::get('price_from'))));
            $price_properties = $price_properties->where('price' , '>=' , str_replace("$" , "" , str_replace("," , "" , Input::get('price_from'))));
        }

        if(Input::has('price_to')){
            $property         = $property->where('price' , '<=' , str_replace("$" , "" , str_replace("," , "" , Input::get('price_to'))));
            $suggestions      = $suggestions->where('price' , '<=' , str_replace("$" , "" , str_replace("," , "" , Input::get('price_to'))));
            $price_properties = $price_properties->where('price' , '<=' , str_replace("$" , "" , str_replace("," , "" , Input::get('price_to'))));
        }



        // Fetch Margin values before matching
        if(Input::has('area')){
            $margin = Margin::whereField_name('area')->pluck('value_exact');
            if (!$margin){
                $margin = 10;
            }
            $margin      = Input::get('area') * ($margin/100);
            $property    = $property->whereBetween('area' , [ (int)str_replace("," , "" , Input::get('area')) - $margin  , (int)str_replace("," , "" , Input::get('area')) + $margin ]);

            $margin_s = Margin::whereField_name('area')->pluck('value_suggest');
            if (!$margin_s){
                $margin_s = 20;
            }
            $margin_s    = Input::get('area') * ($margin_s/100);
            $suggestions = $suggestions->whereBetween('area' , [ (int)str_replace("," , "" , Input::get('area')) - $margin_s  , (int)str_replace("," , "" , Input::get('area')) + $margin_s  ]);
        }

        if(Input::has('rooms_no')){
            $margin      = Margin::whereField_name('rooms_no')->pluck('value_exact');
            if (!$margin){
                $margin = 1;
            }
            $property    = $property->whereBetween('rooms_no' , [ Input::get('rooms_no') - $margin , Input::get('rooms_no') + $margin ]);
            $margin      = Margin::whereField_name('rooms_no')->pluck('value_suggest');
            if (!$margin){
                $margin = 2;
            }
            $suggestions = $suggestions->whereBetween('rooms_no' , [ Input::get('rooms_no') - $margin , Input::get('rooms_no') + $margin ]);
        }

        if(Input::has('toilets_no')){
            $margin      = Margin::whereField_name('toilets_no')->pluck('value_exact');
            if (!$margin){
                $margin = 1;
            }
            $property    = $property->whereBetween('toilets_no' , [ Input::get('toilets_no') - $margin , Input::get('toilets_no') + $margin ]);
            $margin      = Margin::whereField_name('toilets_no')->pluck('value_suggest');
            if (!$margin){
                $margin = 2;
            }
            $suggestions = $suggestions->whereBetween('toilets_no' , [ Input::get('toilets_no') - $margin , Input::get('toilets_no') + $margin ]);
        }

        // if(Input::has('distance_facilites')){
        //     $margin      = Input::get('distance_facilites') * ( Margin::whereField_name('distance_facilites')->pluck('value_exact') /100);
        //     $property    = $property->whereBetween('distance_facilites' , [ Input::get('distance_facilites') - $margin , Input::get('distance_facilites') + $margin ]);
        //     $margin      = Input::get('distance_facilites') * ( Margin::whereField_name('distance_facilites')->pluck('value_suggest') /100);
        //     $suggestions = $suggestions->whereBetween('distance_facilites' , [ Input::get('distance_facilites') - $margin , Input::get('distance_facilites') + $margin ]);
        // }


        if(Input::has('is_compound')){
            $property    = $property->whereIs_compound(1);
            $suggestions = $suggestions->whereIs_compound(1);
        }else{
            $property    = $property->whereIs_compound(0);
            $suggestions = $suggestions->whereIs_compound(0);
        }
        // dd(Input::get('sort_by'));
        if(Input::get('sort_by') == "highest"){
            $property->orderBy('price' , 'desc');
            $suggestions->orderBy('price' , 'desc');
            $price_properties->orderBy('price' , 'desc');
        }elseif(Input::get('sort_by') == "lowest"){
            $property->orderBy('price' , 'asc');
            $suggestions->orderBy('price' , 'asc');
            $price_properties->orderBy('price' , 'asc');
        }else{
            $property->latest();
            $suggestions->latest();
            $price_properties->latest();
        }
        $property         = $property->get();
        $suggestions      = $suggestions->get();
        $price_properties = $price_properties->get();
        $suggestions      = $suggestions->diff($property);
        $price_properties = $price_properties->diff($property);
        $price_properties = $price_properties->diff($suggestions);


        // if the price isn't set return empty for the [price properties] \\
        $price_set = 1;
        if (!Input::has('price_from') && !Input::has('price_to')) {
            $price_properties = [];
            $price_set = 0;
        }

        // return [$property , $suggestions];
        $data['properties']       = $property;
        $data['suggestions']      = $suggestions;
        $data['price_properties'] = $price_properties;
        $data['price_set']        = $price_set;

        return Response::json(array(
            'filter_result'    =>  (String)View::make('properties.filter_result' , $data),
        ));

    }

    public function postDelete($id){
        $property = Property::withTrashed()->find($id);
        if($property->trashed()){
            $property->forceDelete();
        }else{
            Property::destroy($id);
        }


        $data['properties'] = Property::latest()->get();

        return Response::json(array(
            'grid'    =>  (String)View::make('properties.grid' , $data),
            'state'   => 'success'
        ));
    }

    public function postUnarchive($id){
        $property = Property::withTrashed()->find($id);

        $property->restore();

        $data['properties'] = Property::orderBy('created_at' , 'desc')->get();
        return View::make('properties.grid' , $data)->render();
    }

    public function publicCustomer($property_id){
        // purpose - unit type - neigbour - price from - price to - area - objective - payment type - finishing - is compound
        $property = Property::withTrashed()->find($property_id);

        $exactMatch     = new PublicUser();


        $exactMatch     = $exactMatch->where('purpose' , 'LIKE' , $property->purpose);
        $exactMatch     = $exactMatch->where('unit_type' , 'LIKE' , "%|$property->unit_type|%");
        $exactMatch     = $exactMatch->where('neighbour' , 'LIKE' , "%|$property->neighbour|%");
        $exactMatch     = $exactMatch->where('price_from' , '<=' , $property->price);
        $exactMatch     = $exactMatch->where('price_to' , '>=' , $property->price);
        $exactMatch     = $exactMatch->where('area' , '>=' , $property->area);
        $exactMatch     = $exactMatch->where('objective' , 'LIKE' , $property->objective);
        $exactMatch     = $exactMatch->where('payment_type' , 'LIKE' , "%|$property->payment_style|%");
        $exactMatch     = $exactMatch->where('finishing' , 'LIKE' , "%|$property->finishing|%");
        $exactMatch     = $exactMatch->where('is_compound' , 'LIKE' , $property->is_compound);

        return [
            'property' => $property,
            'exact'    => $exactMatch->get(),
        ];


        /**
         **  ============================================  NEIGHBOURS  =======================================================
         **/
        $exactmatch = $exactMatch->whereIn('id' , RequestNeighbour::whereNeighbour_id($property->neighbour)->lists('request_id'));

        $group_array      = [];
        $neighbours_array = [];

        // get all the groups that property belongs to
        $groups = NeighbourGroup::whereNeighbour_id($property->neighbour)->get();
        foreach ($groups as $g) {
            array_push($group_array , $g->group_id);
        }

        // get all the neigbours that belongs to that groups
        $neighbours = NeighbourGroup::whereIn('group_id' , array_unique($group_array))->get();
        foreach ($neighbours as $n) {
            array_push($neighbours_array , $n->neighbour_id);
        }

        $neighbours_array = array_unique($neighbours_array);

        $suggestedMatch = $suggestedMatch->whereIn('id' , RequestNeighbour::whereIn('neighbour_id' , $neighbours_array)->lists('request_id'));
        /**
         **  ============================================  NEIGHBOURS  =======================================================
         **/

         /**
          **  ============================================  FLOORS  =======================================================
          **/

         $exactMatch = $exactMatch->whereIn('id' , RequestFloor::whereFloor_id($property->floor)->lists('request_id'));

         $group_array  = [];
         $floors_array = [];


         // get all the groups that property belongs to
         $groups = FloorGroup::whereFloor_id($property->floor)->get();
         foreach ($groups as $g) {
             array_push($group_array , $g->group_id);
         }

         // get all the floors that belongs to that groups
         $floors = FloorGroup::whereIn('group_id' , array_unique($group_array))->get();
         foreach ($floors as $f){
             array_push($floors_array , $f->floor_id);
         }

         $floors_array = array_unique($floors_array);

         $suggestedMatch = $suggestedMatch->whereIn('id' , RequestFloor::whereIn('floor_id' , $floors_array)->lists('request_id'));


         /**
          **  ============================================  FLOORS  =======================================================
          **/


        $exactMatch     = $exactMatch->wherePurpose($property->purpose);
        $suggestedMatch = $suggestedMatch->wherePurpose($property->purpose);
        $exactMatch     = $exactMatch->where('price_from' , '<=' , $property->price);
        $suggestedMatch = $suggestedMatch->where('price_from' , '<=' , $property->price);
        $exactMatch     = $exactMatch->where('price_to' , '>=' , $property->price);
        $suggestedMatch = $suggestedMatch->where('price_to' , '>=' , $property->price);
        $exactMatch     = $exactMatch->whereIn('id' , RequestPayment::wherePayment_id($property->payment_style)->lists('request_id'));
        $suggestedMatch = $suggestedMatch->whereIn('id' , RequestPayment::wherePayment_id($property->payment_style)->lists('request_id'));
        $features = PropertyFeature::whereProperty_id($property->id)->lists('feature_id');
        $ids = array();
        foreach (CustomerRequest::all() as $requests) {
            $temp = array_diff(explode("|" , $requests->features), $features);
            if(empty($temp)){
                array_push($ids, $requests->id);
            }
        }

        // $exactMatch     = $exactMatch->whereIn('id' , $ids);
        $exactMatch     = $exactMatch->whereIn('id' , RequestFinishing::whereFinishing_id($property->finishing)->lists('request_id'));
        // return RequestFinishing::whereFinishing_id($property->finishing)->lists('request_id');
        $suggestedMatch = $suggestedMatch->whereIn('id' , RequestFinishing::whereFinishing_id($property->finishing)->lists('request_id'));
        $exactMatch     = $exactMatch->whereIn('id' , RequestUnit::whereUnit_type_id($property->unit_type)->lists('request_id'));
        $suggestedMatch = $suggestedMatch->whereIn('id' , RequestUnit::whereUnit_type_id($property->unit_type)->lists('request_id'));

        $exactCustomer     = Customer::whereIn('id' , $exactMatch->lists('customer_id'))->get();
        $suggestedCustomer = Customer::whereIn('id' , $suggestedMatch->lists('customer_id'))->get();

        return [
            'property' => $property,
            'exact'    => $exactCustomer,
            'suggest'  => $suggestedCustomer
        ];
    }

    public function customer($property_id){
        $property = Property::withTrashed()->find($property_id);

        $exactMatch     = new CustomerRequest();
        $suggestedMatch = new CustomerRequest();
        $priceMatch     = new CustomerRequest();
        /**
         **  ============================================  NEIGHBOURS  =======================================================
         **/

        $exactMatch = $exactMatch->whereIn('id' , RequestNeighbour::whereNeighbour_id($property->neighbour)->lists('request_id'));

        $group_array      = [];
        $neighbours_array = [];

        // get all the groups that property belongs to
        $groups = NeighbourGroup::whereNeighbour_id($property->neighbour)->get();
        foreach ($groups as $g) {
            array_push($group_array , $g->group_id);
        }

        // get all the neigbours that belongs to that groups
        $neighbours = NeighbourGroup::whereIn('group_id' , array_unique($group_array))->get();
        foreach ($neighbours as $n) {
            array_push($neighbours_array , $n->neighbour_id);
        }

        $neighbours_array = array_unique($neighbours_array);

        $suggestedMatch = $suggestedMatch->whereIn('id' , RequestNeighbour::whereIn('neighbour_id' , $neighbours_array)->lists('request_id'));
        /**
         **  ============================================  NEIGHBOURS  =======================================================
         **/

         /**
          **  ============================================  FLOORS  =======================================================
          **/

         $exactMatch = $exactMatch->whereIn('id' , RequestFloor::whereFloor_id($property->floor)->lists('request_id'));

         $group_array  = [];
         $floors_array = [];


         // get all the groups that property belongs to
         $groups = FloorGroup::whereFloor_id($property->floor)->get();
         foreach ($groups as $g) {
             array_push($group_array , $g->group_id);
         }

         // get all the floors that belongs to that groups
         $floors = FloorGroup::whereIn('group_id' , array_unique($group_array))->get();
         foreach ($floors as $f){
             array_push($floors_array , $f->floor_id);
         }

         $floors_array = array_unique($floors_array);

         $suggestedMatch = $suggestedMatch->whereIn('id' , RequestFloor::whereIn('floor_id' , $floors_array)->lists('request_id'));


         /**
          **  ============================================  FLOORS  =======================================================
          **/


        $exactMatch     = $exactMatch->wherePurpose($property->purpose);
        $suggestedMatch = $suggestedMatch->wherePurpose($property->purpose);
        $priceMatch     = $priceMatch->wherePurpose($property->purpose);

        $exactMatch     = $exactMatch->where('price_from' , '<=' , $property->price);
        $suggestedMatch = $suggestedMatch->where('price_from' , '<=' , $property->price);
        $priceMatch     = $priceMatch->where('price_from' , '<=' , $property->price);

        $exactMatch     = $exactMatch->where('price_to' , '>=' , $property->price);
        $suggestedMatch = $suggestedMatch->where('price_to' , '>=' , $property->price);
        $priceMatch     = $priceMatch->where('price_to' , '>=' , $property->price);

        $exactMatch     = $exactMatch->whereIn('id' , RequestPayment::wherePayment_id($property->payment_style)->lists('request_id'));
        $suggestedMatch = $suggestedMatch->whereIn('id' , RequestPayment::wherePayment_id($property->payment_style)->lists('request_id'));
        $features = PropertyFeature::whereProperty_id($property->id)->lists('feature_id');
        $ids = array();
        foreach (CustomerRequest::all() as $requests) {
            $temp = array_diff(explode("|" , $requests->features), $features);
            if(empty($temp)){
                array_push($ids, $requests->id);
            }
        }

        // $exactMatch     = $exactMatch->whereIn('id' , $ids);
        $exactMatch     = $exactMatch->whereIn('id' , RequestFinishing::whereFinishing_id($property->finishing)->lists('request_id'));
        // return RequestFinishing::whereFinishing_id($property->finishing)->lists('request_id');
        $suggestedMatch = $suggestedMatch->whereIn('id' , RequestFinishing::whereFinishing_id($property->finishing)->lists('request_id'));
        $exactMatch     = $exactMatch->whereIn('id' , RequestUnit::whereUnit_type_id($property->unit_type)->lists('request_id'));
        $suggestedMatch = $suggestedMatch->whereIn('id' , RequestUnit::whereUnit_type_id($property->unit_type)->lists('request_id'));

        $exactCustomer     = Customer::whereIn('id' , $exactMatch->lists('customer_id'))->get();
        $suggestedCustomer = Customer::whereIn('id' , $suggestedMatch->lists('customer_id'))->get();
        $priceCustomer     = Customer::whereIn('id' , $priceMatch->lists('customer_id'))->get();


        return [
            'property' => $property,
            'exact'    => $exactCustomer,
            'suggest'  => $suggestedCustomer,
            'price'    => $priceCustomer
        ];
    }

    public function getProperty(){
        $id = Input::get('id');
        $info = $this->customer($id);
        $data['messages'] = PropertyTimeline::whereProperty_id($id)->get();
        $data['exactCustomers']     = $info['exact'];
        $data['suggestedCustomers'] = $info['suggest']->diff($info['exact']);
        $data['property']           = $info['property'];
        $data['priceCustomers']     = $info['price'];
        // $data['notifications']      = AdminNotification::whereType(2)->whereProperty_id($id)->get();
        $ids                   = AssignedUser::whereUser_id(Auth::user()->id())->lists('notice_id');
        $data['notifications']      = AdminNotification::whereType(2)->whereProperty_id($id)->where(function($query) use ($ids) {
            $query->whereIn('id' , $ids)->orWhere('from' , '=' , Auth::user()->id());
        })->paginate(100);
        $data['images']             = Image::whereProperty_id($id)->get();
        // dd(addslashes($data['messages'][0]->message));
        return Response::json(array(
            'property'        => (String)View::make('properties.property' , $data),
        ));
    }

    public function getNeighbour($city_id){
        $data['neighbours'] = Neighbour::whereCity_id($city_id)->get();

        if (Input::get('target') == 'search') {
            return Response::json(array(
                'select'  => (String)View::make('general.neighbour_select' , $data)
            ));
        }

        return Response::json(array(
            'select'  => (String)View::make('general.neighbour_add' , $data)
        ));
    }

    public function postAddMessage(){
        $id  = Input::get('id');
        $msg = Input::get('msg');

        $ct = new PropertyTimeline();
        $ct->property_id = $id;
        $ct->message = $msg;

        $ct->save();

        $data['ct'] = $ct;

        return View::make('properties.timeline' , $data);

        return 1;
    }

    public function postEditMessage($id){
        $msg = Input::get('msg');
        $ct  = PropertyTimeline::find($id);
        $ct->message = $msg;
        $ct->save();

        return 1;
    }

    public function postDeleteMessage($id){
        $ct = PropertyTimeline::destroy($id);

        return 1;
    }

    public function getDeletedProperties(){
        $properties = Property::onlyTrashed()->get();
        $data['properties'] = $properties;
        return View::make('properties.grid' , $data)->render();
    }

    public function getCustomerAdd(){
        if (Input::has('is_compound')) {
            $data['is_compound'] = Input::get('is_compound');
        }

        $data['area_selected']               = Input::get('area');
        $data['cities_selected']             = Input::get('city');
        $data['finishing_selected']          = Input::get('finishing');
        $data['floor_selected']              = Input::get('floor');
        $data['compound_selected']           = Input::get('is_compound');
        $data['neighbours_selected']         = Input::get('neighbour');
        $data['objective_selected']          = Input::get('objective');
        $data['payment_style_selected']      = Input::get('payment_style');
        $data['price_from_selected']         = Input::get('price_from');
        $data['price_to_selected']           = Input::get('price_to');
        $data['purpose_selected']            = Input::get('purpose');
        $data['unit_type_selected']          = Input::get('unit_type');
        $data['users'] = User::all();
        // return Input::all();
        // return View::make('customers.add' , $data);
        return Response::json(array(
            'customer_add'  => (String)View::make('customers.add' , $data)
        ));
    }

    // public function getPropertyCustomer($id){
    //     $property = Property::withTrashed()->find($id);
    //     $data['images']   = Image::whereProperty_id($id)->get();

    //     $exactMatch     = new CustomerRequest();
    //     $suggestedMatch = new CustomerRequest();

    //     /**
    //      **  ============================================  NEIGHBOURS  =======================================================
    //      **/

    //     $exactMatch = $exactMatch->whereIn('id' , RequestNeighbour::whereNeighbour_id($property->neighbour)->lists('request_id'));

    //     $group_array      = [];
    //     $neighbours_array = [];

    //     // get all the groups that property belongs to
    //     $groups = NeighbourGroup::whereNeighbour_id($property->neighbour)->get();
    //     foreach ($groups as $g) {
    //         array_push($group_array , $g->group_id);
    //     }

    //     // get all the neigbours that belongs to that groups
    //     $neighbours = NeighbourGroup::whereIn('group_id' , array_unique($group_array))->get();
    //     foreach ($neighbours as $n) {
    //         array_push($neighbours_array , $n->neighbour_id);
    //     }

    //     $neighbours_array = array_unique($neighbours_array);

    //     $suggestedMatch = $suggestedMatch->whereIn('id' , RequestNeighbour::whereIn('neighbour_id' , $neighbours_array)->lists('request_id'));

    //     /**
    //      **  ============================================  NEIGHBOURS  =======================================================
    //      **/

    //      /**
    //       **  ============================================  FLOORS  =======================================================
    //       **/

    //      $exactMatch = $exactMatch->whereIn('id' , RequestFloor::whereFloor_id($property->floor)->lists('request_id'));

    //      $group_array  = [];
    //      $floors_array = [];

    //      // get all the groups that property belongs to
    //      $groups = FloorGroup::whereFloor_id($property->floor)->get();
    //      foreach ($groups as $g) {
    //          array_push($group_array , $g->group_id);
    //      }

    //      // get all the floors that belongs to that groups
    //      $floors = FloorGroup::whereIn('group_id' , array_unique($group_array))->get();
    //      foreach ($floors as $f){
    //          array_push($floors_array , $f->floor_id);
    //      }

    //      $floors_array = array_unique($floors_array);

    //      $suggestedMatch = $suggestedMatch->whereIn('id' , RequestFloor::whereIn('floor_id' , $floors_array)->lists('request_id'));


    //      /**
    //       **  ============================================  FLOORS  =======================================================
    //       **/

    //     $exactMatch     = $exactMatch->wherePurpose($property->purpose);
    //     $suggestedMatch = $suggestedMatch->wherePurpose($property->purpose);
    //     $exactMatch     = $exactMatch->where('price_from' , '<=' , $property->price);
    //     $suggestedMatch = $suggestedMatch->where('price_from' , '<=' , $property->price);


    //     $exactMatch     = $exactMatch->where('price_to' , '>=' , $property->price);
    //     $suggestedMatch = $suggestedMatch->where('price_to' , '>=' , $property->price);
    //     $exactMatch     = $exactMatch->whereIn('id' , RequestPayment::wherePayment_id($property->payment_style)->lists('request_id'));
    //     $suggestedMatch = $suggestedMatch->whereIn('id' , RequestPayment::wherePayment_id($property->payment_style)->lists('request_id'));
    //     // return RequestFeature::whereIn('feature_id' , PropertyFeature::whereProperty_id($property->id)->lists('feature_id'))->lists('request_id');
    //     // $exactMatch     = $exactMatch->whereIn('id' , RequestFeature::whereIn('feature_id' , PropertyFeature::whereProperty_id($property->id)->lists('feature_id'))->lists('request_id'));
    //     // $suggestedMatch = $suggestedMatch->whereIn('id' , RequestFeature::whereIn('feature_id' , PropertyFeature::whereProperty_id($property->id)->lists('feature_id'))->lists('request_id'));
    //     // $exactMatch     = $exactMatch->whereIn('id' , RequestFeature::whereIn('feature_id' , PropertyFeature::whereId($property->id)->lists('feature_id'))->lists('request_id'));
    //     // $suggestedMatch = $suggestedMatch->whereIn('id' , RequestFeature::whereIn('feature_id' , PropertyFeature::whereId($property->id)->lists('feature_id'))->lists('request_id'));
    //     $features = PropertyFeature::whereProperty_id($property->id)->lists('feature_id');
    //     $ids = array();
    //     foreach (CustomerRequest::all() as $requests) {
    //         $temp = array_diff(explode("|" , $requests->features), $features);
    //         if(empty($temp)){
    //             array_push($ids, $requests->id);
    //         }
    //     }

    //     $exactMatch     = $exactMatch->whereIn('id' , $ids);
    //     $exactMatch     = $exactMatch->whereIn('id' , RequestFinishing::whereFinishing_id($property->finishing)->lists('request_id'));
    //     $suggestedMatch = $suggestedMatch->whereIn('id' , RequestFinishing::whereFinishing_id($property->finishing)->lists('request_id'));
    //     $exactMatch     = $exactMatch->whereIn('id' , RequestUnit::whereUnit_type_id($property->unit_type)->lists('request_id'));
    //     $suggestedMatch = $suggestedMatch->whereIn('id' , RequestUnit::whereUnit_type_id($property->unit_type)->lists('request_id'));

    //     // return CustomerRequest::whereIn('id' , [])->get();
    //     //
    //     // return [
    //     //     'exactMatch'     => $exactMatch->get() ,
    //     //     'suggestedMatch' => $suggestedMatch->get()
    //     // ];

    //     $exactCustomer     = Customer::whereIn('id' , $exactMatch->lists('customer_id'))->get();
    //     $suggestedCustomer = Customer::whereIn('id' , $suggestedMatch->lists('customer_id'))->get();
    //     $suggestedCustomer = $suggestedCustomer->diff($exactCustomer);


    //     $data['exactCustomers']     = $exactCustomer;
    //     $data['suggestedCustomers'] = $suggestedCustomer;
    //     $data['property']           = $property;
    //     $data['messages']           = PropertyTimeline::whereProperty_id($id)->get();
    //     $data['notifications']      = AdminNotification::whereType(2)->whereProperty_id($id)->get();
    //     return View::make('customers.property' , $data);
    // }

    public function getPropertyCustomer($id){
        // $id = Input::get('id');
        $info = $this->customer($id);

        $data['exactCustomers']     = $info['exact'];
        $data['suggestedCustomers'] = $info['suggest']->diff($info['exact']);
        $data['property']           = $info['property'];
        $data['priceCustomers']     = $info['price'];
        $data['notifications']      = AdminNotification::whereType(2)->whereProperty_id($id)->get();
        $data['images']             = Image::whereProperty_id($id)->get();
        $data['page']               = 'single_property';
        $data['messages'] = PropertyTimeline::whereProperty_id($id)->get();
        $data['admins']             = User::where('id' , '!=' , Auth::user()->id())->get();
        return View::make('_layouts.core' , $data)->nest('content' , 'customers.property' , $data);
    }
    public function getFetchNeighbour($city_id){
        $data['neighbours'] = Neighbour::whereCity_id($city_id)->get();


        return Response::json(array(
            'select'  => (String)View::make('customers.neighbour_select' , $data)
        ));
    }

    public function postAddNotice(){
        $validator = Validator::make(Input::all() , ['body' => 'required']);
        if($validator->fails()){
            return $validator->messages()->first();
        }
        
        $task = new AdminNotification();
        $task->title       = '';
        $task->description = Input::get('body');
        $task->from        = Auth::user()->id();
        // $task->to          = Input::get('assignee');
        $task->type        = 2;
        $task->priority    = 0;
        $task->property_id = Input::get('property_id');
        $task->key         = $task->generate_random_string(7);
        $task->save();

        foreach (Input::get('assignee') as $assignee) {
            $assigned = new AssignedUser();
            $assigned->notice_id = $task->id;
            $assigned->user_id = $assignee;
            $assigned->save();

            if($assignee != Auth::user()->id()){
                $notification = new AdminNotifiy();
                $notification->notifier  = Auth::user()->id();
                $notification->notice_id = $task->id;
                $notification->user_id   = $assignee;
                $notification->message   = User::whereId(Auth::user()->id())->pluck("name") . trans('main.comment_task')  ."-" . mb_strimwidth($task->description, 0, 30, "...");
                $notification->read      = 0;
                $notification->save();
                $this->pusher->trigger('channel_' . $assignee, 'event_notify', array('message' => $notification->message) );
            }
        }

        $ids                   = AssignedUser::whereUser_id(Auth::user()->id())->lists('notice_id');
        $data['notifications']      = AdminNotification::whereType(2)->whereProperty_id(Input::get('property_id'))->where(function($query) use ($ids) {
            $query->whereIn('id' , $ids)->orWhere('from' , '=' , Auth::user()->id());
        })->paginate(100);


        // foreach (Input::get('assignee') as $assignee) {
        //     $assigned = new AssignedUser();
        //     $assigned->notice_id = $task->id;
        //     $assigned->user_id = $assignee;
        //     $assigned->save();

        //     $notification = new AdminNotifiy();
        //     $notification->notice_id = $task->id;
        //     $notification->user_id   = $assignee;
        //     $notification->message   = "You have a new Notification";
        //     $notification->read      = 0;
        //     $notification->save();
        // }

        return Response::json(array(
            'table'    => (String)View::make('notifications.notification_filter' , $data),
            'code'     => 1
        ));
    }

    private function generate_random_string($name_length = 4){
        $alpha_numeric = '0123456789';
        return substr(str_shuffle($alpha_numeric), 0 , $name_length);
    }

    public function getGetPrices(){
        if(Input::get('purpose') == '0'){
            return (String)View::make('general.sale_prices');
        }else{
            return (String)View::make('general.rent_prices');
        }
    }

    public function getPricesTo(){
        $sale_prices = [100000
                        ,125000
                        ,150000
                        ,175000
                        ,200000
                        ,250000
                        ,300000
                        ,350000
                        ,400000
                        ,450000
                        ,500000
                        ,600000
                        ,700000
                        ,800000
                        ,900000
                        ,1000000
                        ,1200000
                        ,1400000
                        ,1600000
                        ,1800000
                        ,2000000
                        ,2300000
                        ,2600000
                        ,3000000
                        ,3500000
                        ,4000000
                        ,5000000
                        ,6000000
                        ,7500000
                        ,10000000
                        ,15000000
                        ,20000000];
        $rent_prices = [1000
                        ,1500
                        ,2000
                        ,3000
                        ,5000
                        ,7000
                        ,15000
                        ,20000];
        
        if(Input::get('purpose') == '0'){
            $price_start = array_search(Input::get('price') , $sale_prices);
            $data['prices'] = array_slice($sale_prices , $price_start +1);
            return View::make('general.prices_to' , $data);
        }else{
            $price_start = array_search(Input::get('price') , $rent_prices);
            $data['prices'] = array_slice($rent_prices , $price_start +1);
            return View::make('general.prices_to' , $data);
        }
    }

}

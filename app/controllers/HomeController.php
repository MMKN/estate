<?php

class HomeController extends BaseController {

	// get login page
	public function getLogin()
	{
		if(Auth::user()->check()){
			return Redirect::route('properties.index');
		}
		return View::make('login');
	}

	// handle login page submit
	public function postLogin()
	{
		$email    = Input::get('email');
		$password = Input::get('password');

		if(Auth::user()->attempt(array('email' => $email , 'password' => $password , 'deleted_at' => null))){
			$user = Auth::user()->user();
			if($user->image == ""){
				Session::put('image'     , 'user.png');
			}else{
				Session::put('image'     , $user->image);
			}

			$data['message'] = "You Are Logged In Successfully.";
			return Response::json(array(
				'url'      => URL::route('properties.index'),
				'message'  => (String)View::make('success_message' , $data),
				'state'    => 'success'
			));
		}

		$data['message'] = "Wrong Email/Password.";
		return Response::json(array(
			'message' => (String)View::make('danger_message' , $data),
			'state'   => 'fail'
		));
	}

	public function postLogout()
	{
		Auth::user()->logout();
		return Redirect::to('/');
	}

	public function getCustomerLogin(){
		return View::make('customers_login');
	}

	public function postCustomerLogin()
	{
		$email    = Input::get('email');
		$password = Input::get('password');
		if(Auth::customer()->attempt(array('username' => $email , 'password' => $password))){
			$customer = Auth::customer()->user();
			Session::put('id' , $customer->id);
			$data['message'] = "You Are Logged In Successfully.";
			return Response::json(array(
				'url'      => URL::route('account'),
				'message'  => (String)View::make('success_message' , $data),
				'state'    => 'success'
			));
		}

		$data['message'] = "Wrong Email/Password.";
		return Response::json(array(
			'message' => (String)View::make('danger_message' , $data),
			'state'   => 'fail'
		));
	}

	public function postCustomerLogout()
	{
		Auth::customer()->logout();
		return Redirect::to('/login');
	}
}

<?php

class NotificationsController extends BaseController {

    public $pusher;
    public function __construct(){
        parent::__construct();
        $this->pusher = new Pusher('740b3e5ad1236ca3a8d8' ,'bf5a68796761c46e6b02' ,'180891');
    }

    public function getIndex(){

        if(User::find(Auth::user()->id())->is_admin){
            $data['notifications'] = AdminNotification::latest()->paginate(10);
        }else{
            $ids                   = AssignedUser::whereUser_id(Auth::user()->id())->lists('notice_id');
            $data['notifications'] = AdminNotification::latest()->whereIn('id' , $ids)->orWhere('from' , '=' , Auth::user()->id())->paginate(10);
        }

        $data['admins']        = User::where('id' , '!=' , Auth::user()->id())->get();
        $data['page']          = 'notifications';
        return View::make('notifications.index' , $data);
    }

    public function getPaginated(){
        // $notifications = AdminNotification::latest()->whereIn('to' , [Auth::user()->id() , 0])->paginate(10);
        if(User::find(Auth::user()->id())->is_admin){
            $notifications = AdminNotification::latest()->paginate(10);
        }else{
            $ids           = AssignedUser::whereUser_id(Auth::user()->id())->lists('notice_id');
            $notifications = AdminNotification::latest()->whereIn('id' , $ids)->orWhere('from' , '=' , Auth::user()->id())->paginate(10);
        }
        $data['notifications'] = $notifications;
        return Response::json(array(
            'notifications'    => (String)View::make('notifications.notification_filter' , $data),
            'links'        =>  (String)$notifications->links()
        ));
    }

    public function getNotificationFilter(){
        // $notifications = AdminNotification::latest()->whereIn('to' , [Auth::user()->id() , 0]);
        if(User::find(Auth::user()->id())->is_admin){
            $notifications = AdminNotification::latest();
        }else{
            $ids = AssignedUser::whereUser_id(Auth::user()->id())->lists('notice_id');
            $notifications = AdminNotification::latest()->whereIn('id' , $ids)->orWhere('from' , '=' , Auth::user()->id());
        }
        if(Input::has('type') && Input::get('type') != 5){
            $notifications = $notifications->whereType(Input::get('type'));
        }
        if(Input::has('assignee') && Input::get('assignee') != 0){
            $ids = AssignedUser::whereUser_id(Input::get('assignee'))->lists('notice_id');
            $notifications = $notifications->whereIn('id' , $ids);
        }
        if(Input::has('status') && Input::get('status') != 4){
            $notifications = $notifications->whereStatus(Input::get('status'));
        }
        if(Input::has('start_date')){
            $notifications = $notifications->where('created_at' , '>=' , date('Y-m-d' , strtotime(Input::get('start_date'))));
        }
        if(Input::has('end_date')){
            $notifications = $notifications->where('created_at' , '<=' , date('Y-m-d' , strtotime(Input::get('end_date'))));
        }
        if(Input::has('key')){
            $notifications = $notifications->where('key' , 'LIKE' , '%'.Input::get('key').'%');
        }
        // dd(Input::all());
        if(Input::get('type') != "5" || Input::get('assignee') != "0" || Input::get('status') != "4" || Input::get('start_date') != "" || Input::has('end_date') != "" || Input::has('key') != ""){
            $data['notifications'] = $notifications->paginate(1000);
        }else{
            $data['notifications'] = $notifications->paginate(10);
        }

        return View::make('notifications.notification_filter' , $data);
    }

    public function postDone(){
        $user_id     = Input::get('user_id');
        $property_id = Input::get('property_id');

        $request = RequestsToCall::whereUser_id($user_id)->whereProperty_id($property_id)->whereDone(0)->get()->first();
        if(!$request){return;}

        $request->done = 1;
        $request->update();
        return 'Success';
    }

    public function postUndone(){
        $user_id     = Input::get('user_id');
        $property_id = Input::get('property_id');

        $request = RequestsToCall::whereUser_id($user_id)->whereProperty_id($property_id)->whereDone(1)->get()->first();
        if(!$request){return;}

        $request->done = 0;
        $request->update();
        return 'Success';
    }

    public function postNotes(){
        $validator = Validator::make(Input::all() , ['body' => 'required']);
        if($validator->fails()){
            return $validator->messages()->first();
        }

        $note                  = new NotificationNote();
        $note->notification_id = Input::get('id');
        $note->body            = Input::get('body');
        $note->admin_id        = Auth::user()->id();
        $note->save();

        $assignees = AssignedUser::whereNotice_id($note->notification_id)->get();
        foreach ($assignees as $assignee) {
            if($assignee->user_id != Auth::user()->id()){
                $notification = new AdminNotifiy();
                $notification->notifier  = Auth::user()->id();
                $notification->notice_id = $note->notification_id;
                $notification->user_id   = $assignee->user_id;
                $notification->message   = User::whereId(Auth::user()->id())->pluck("name") . trans('main.comment_add')  ."-" . mb_strimwidth(AdminNotification::whereId($note->notification_id)->pluck('description') , 0, 20, "...")."': " . mb_strimwidth($note->body, 0, 20, "...");
                $notification->read      = 0;
                $notification->save();
                // $request = Request::create('localhost:3011/message/notify/'.$assignee->user_id , 'POST' , ['body' =>  "User addd you to the notice"]);
                // $url = 'localhost:3011/message/notify/'.$assignee->user_id;
                // $data = array('body' => 'value1');

                // $curl = curl_init($url);
                // curl_setopt($curl, CURLOPT_POST, true);
                // curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
                // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                // $response = curl_exec($curl);
                // curl_close($curl);
                // return $response;
            }
        }
        $notice = AdminNotification::find(Input::get('id'));
        if($notice->from != Auth::user()->id()){
            $notification = new AdminNotifiy();
            $notification->notifier  = Auth::user()->id();
            $notification->notice_id = $note->notification_id;
            $notification->user_id   = $notice->from;
            $notification->message   = User::whereId(Auth::user()->id())->pluck("name") . trans('main.comment_add')  ."-" . mb_strimwidth(AdminNotification::whereId($note->notification_id)->pluck('description') , 0, 20, "...")."': " . mb_strimwidth($note->body, 0, 20, "...");
            $notification->read      = 0;
            $notification->save();
        }
        return Response::json(array(
            'result'    =>  1,
            'data'      =>  (String)View::make('notifications.comment' , compact('note'))
        ));
    }

    public function postAddNotice(){
        $validator = Validator::make(Input::all() , ['body' => 'required']);
        if($validator->fails()){
            return $validator->messages()->first();
        }
        $task = new AdminNotification();

        $task->title       = ''; //Input::get('title');
        $task->description = Input::get('body');
        $task->from        = Auth::user()->id();
        // $task->to          = Input::get('assignee');
        $task->type        = 0;
        $task->priority    = 0; //Input::get('priority');
        $task->key         = $task->generate_random_string(7);
        $task->save();

        foreach (Input::get('assignee') as $assignee) {
            $assigned = new AssignedUser();
            $assigned->notice_id = $task->id;
            $assigned->user_id = $assignee;
            $assigned->save();

            if($assignee != Auth::user()->id()){
                $notification = new AdminNotifiy();
                $notification->notifier  = Auth::user()->id();
                $notification->notice_id = $task->id;
                $notification->user_id   = $assignee;
                $notification->message   = User::whereId(Auth::user()->id())->pluck("name") . trans('main.comment_task')  ."-" . mb_strimwidth($task->description, 0, 30, "...");
                $notification->read      = 0;
                $notification->save();
                $this->pusher->trigger('channel_' . $assignee, 'event_notify', array('message' => $notification->message) );
            }
        }
        return 1; 
    }

    public function getDetailsPage($n_id = 0){
        $data['notification'] = AdminNotification::find($n_id);
        if (count($data['notification']) == 0) {
            return Redirect::to('/');
        }
        $data['notes']        = NotificationNote::latest()->whereNotification_id($n_id)->get();
        $data['assignees']    = AssignedUser::whereNotice_id($n_id)->get();
        $data['is_admin']     = User::find(Auth::user()->id())->is_admin;

        $ns = AdminNotifiy::whereNotice_id($n_id)->whereUser_id(Auth::user()->id())->get();
        foreach ($ns as $n) {
            $n->read = 1;
            $n->save();
        }

        if($data['notification']->type == 1) {
            $data['user'] = PublicUser::find($data['notification']->user_id);
        }else if($data['notification']->type == 3){
            $data['customer']     = Customer::find($data['notification']->user_id);
        }

        
        $data['selected']     = User::whereIn('id' , AssignedUser::whereNotice_id(Input::get('id'))->lists('user_id'))->lists('id');
        $data['admins']       = User::where('id' , '!=' , Auth::user()->id())->get();
        $data['page']         = 'details_page';

        return View::make('notifications.details_page' , $data);
    }

    public function getDetails(){
        $data['notification']  = AdminNotification::find(Input::get('id'));
        $data['notes']         = NotificationNote::latest()->whereNotification_id(Input::get('id'))->get();
        $data['is_admin']      = User::find(Auth::user()->id())->is_admin;
        $data['users']     = AssignedUser::whereNotice_id(Input::get('id'));

        $ns = AdminNotifiy::whereNotice_id(Input::get('id'))->whereUser_id(Auth::user()->id())->get();
        foreach ($ns as $n) {
            $n->read = 1;
            $n->save();
        }
        $data['selected']     = User::whereIn('id' , $data['users']->lists('user_id'))->lists('id');

        if($data['notification']->type == 1) {
            $data['user'] = PublicUser::find($data['notification']->user_id);
        }else if($data['notification']->type == 3){
            $data['customer']     = Customer::find($data['notification']->user_id);
        }
        $data['assignees']     = AssignedUser::whereNotice_id(Input::get('id'))->get();
        return View::make('notifications.notification_details' , $data);
    }

    public function postEditNotification($id){
        $notification = AdminNotification::find($id);
        $input        = Input::all();

        $notification->title       = '';
        $notification->description = $input['description'];
        // $notification->to          = $input['assignee'];
        // $notification->priority    = $input['priority'];
        // 
        
        $notification->update();
        $assignees = AssignedUser::whereNotice_id($id)->lists('user_id');
        $assigned = AssignedUser::whereNotice_id($id)->delete();

        $description = $notification->description;
        foreach (Input::get('assignee') as $assignee) {
            $assigned = new AssignedUser();
            $assigned->notice_id = $id;
            $assigned->user_id = $assignee;
            $assigned->save();

            if(!in_array($assignee, $assignees)){
                $notification = new AdminNotifiy();
                $notification->notice_id = $id;
                $notification->user_id   = $assignee;
                $notification->message   = User::whereId(Auth::user()->id())->pluck("name") . trans('main.comment_task')  ."-" . mb_strimwidth($description, 0, 30, "...");
                $notification->read      = 0;
                $notification->save();
                $this->pusher->trigger('channel_' . $assignee, 'event_notify', array('message' => $notification->message) );
            }
        }



        $data['notification'] = AdminNotification::find($id);
        if($data['notification']->type == 1) {
            $data['user'] = PublicUser::find($data['notification']->user_id);
        }else if($data['notification']->type == 3){
            $data['customer']     = Customer::find($data['notification']->user_id);
        }
        $data['selected']     = AssignedUser::whereNotice_id(Input::get('id'))->lists('user_id');
        $data['notes']        = NotificationNote::latest()->whereNotification_id($id)->get();
        $data['is_admin']      = User::find(Auth::user()->id())->is_admin;
        $data['customer']      = Customer::find($data['notification']->user_id);
        $data['assignees']     = AssignedUser::whereNotice_id(Input::get('id'))->get();
        return (String)View::make('notifications.notification_details' , $data);

    }

    public function postToggleIssue(){
        $notice = AdminNotification::find(Input::get('id'));
        // dd($notification->status);
        // 
        $text = "";
        $assignees = AssignedUser::whereNotice_id($notice->id)->get();
        if($notice->status == 0 || $notice->status == 2){
            $notice->status = 1;
            foreach ($assignees as $assignee) {
                if($assignee->user_id != Auth::user()->id()){
                    $notification = new AdminNotifiy();
                    $notification->notifier  = Auth::user()->id();
                    $notification->notice_id = $notice->id;
                    $notification->user_id   = $assignee->user_id;
                    $notification->message   = User::whereId(Auth::user()->id())->pluck("name")  . trans('main.task_close')  ."'" . mb_strimwidth($notice->description , 0, 30, "...")."'";
                    $notification->read      = 0;
                    $notification->save();
                    $this->pusher->trigger('channel_' . $assignee->user_id, 'event_notify', array('message' => $notification->message) );
                }
            }
            $text = "Re-Open";
        }else{
            $notice->status = 2;
            foreach ($assignees as $assignee) {
                if($assignee->user_id != Auth::user()->id()){
                    $notification = new AdminNotifiy();
                    $notification->notifier  = Auth::user()->id();
                    $notification->notice_id = $notice->id;
                    $notification->user_id   = $assignee->user_id;
                    $notification->message   = User::whereId(Auth::user()->id())->pluck("name") . trans('main.task_reopen')  ."-" . mb_strimwidth($notice->description , 0, 30, "...");
                    $notification->read      = 0;
                    $notification->save();
                    $this->pusher->trigger('channel_' . $assignee->user_id, 'event_notify', array('message' => $notification->message) );
                }
            }
            $text = "Close_Issue";
        }
        $notice->save();
        return Response::json(
            [
                'status' => $notice->status,
                'element'=> $notice->getStatus($notice->status),
                'text'   => trans('main.'.$text)
            ]
        );
    }

    public function postDeleteNotification(){
        $id = Input::get('id');

        $notification = AdminNotification::find($id);
        $notification->delete();
        AdminNotifiy::whereNotice_id($id)->delete();
    }

    public function getMoreNotifications(){
        $skip = Input::get('offset') * 5;
        $notifications = AdminNotifiy::latest()->whereUser_id(Auth::user()->id())->skip($skip)->take(5)->get();
        if(count($notifications)){
            $data['notifications'] = $notifications;
            return View::make('general.notification_item', $data);
        }else{
            return 0;
        }
    }

    public function getUpdateNotifications(){
        $notifications = AdminNotifiy::latest()->whereUser_id(Auth::user()->id());
        $data['notifications'] = $notifications->get();
        $data['count'] = $notifications->whereRead(0)->count();
        return Response::json(
            [
                'notifications' => (String)View::make('general.notification_item', $data),
                'count'         => $data['count']
            ]
        );
    }

    public function getNotificationCount(){
        return AdminNotifiy::latest()->whereUser_id(Auth::user()->id())->whereRead(0)->get()->count();
    }
}

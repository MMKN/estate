<?php

class IndividualsController extends \APIController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$individuals = Individual::paginate(500);
		return $this->respond([
			'data' => $individuals->getCollection(),
			'code' => 200
			]);
	}



	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all() , Individual::$rules);
		if($validator->fails()){
			return $this->respondNotValid($validator->errors());
		}
		$individual = new Individual();
		$individual->name = Input::get('name');
		$individual->email = Input::get('email');
		$individual->address = Input::get('address');
		$individual->phone = Input::get('phone');
		$individual->description = Input::get('description');
		if(Input::has('company_id')){
			$individual->company_id = Input::get('company_id');
		}
		$individual->save();
		return $this->respond([
			'data' => $individual,
			'code' => 201
			]);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$individual = Individual::withTrashed()->find($id);

		if(!$individual){
			return $this->respondNotFound('individual Not Found');
		}

		return $this->respond([
			'data' => $individual,
			'code' => '200'
			]);
	}



	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$individual = Individual::withTrashed()->find($id);
		if(!$individual){
			return $this->respondNotFound('Individual Not Found');
		}
		$validator = Validator::make(Input::all() , Individual::$rules);
		if($validator->fails()){
			return $this->respondNotValid($validator->errors());
		}
		$individual->name = Input::get('name');
		$individual->email = Input::get('email');
		$individual->address = Input::get('address');
		$individual->phone = Input::get('phone');
		$individual->description = Input::get('description');
		if(Input::has('company_id')){
			$individual->company_id = Input::get('company_id');
		}
		$individual->update();
		return $this->respond([
			'data' => $individual,
			'code' => 200
			]);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$individual = Individual::withTrashed()->find($id);

		if(!$individual){
			return $this->respondNotFound('Individual Not Found');
		}

		if($individual->trashed()){
			$individual->forceDelete();
		}else{
			$individual->delete();
		}

		return $this->respond([
			'message' => 'Individual deleted successfully',
			'code' => 200
			]);
	}

	public function search()
	{
		$individuals = Individual::withTrashed()->where('name' , 'LIKE' , Input::get('query') . "%")
						->orWhere('phone' , 'LIKE' , Input::get('query') . "%")
						->orWhere('email' , 'LIKE' , Input::get('query') . "%")
						->get();

		return $this->respond([
			'data' => $individuals,
			'code' => 200
			]);
	}

	public function getArchived()
	{
		$individuals = Individual::onlyTrashed()->get();
		return $this->respond([
			'data' => $individuals,
			'code' => 200
			]);
	}

	public function globalSearch()
	{
		$individuals = Individual::withTrashed()->where('name' , 'LIKE' , Input::get('query') . "%")
						->orWhere('phone' , 'LIKE' , Input::get('query') . "%")
						->orWhere('email' , 'LIKE' , Input::get('query') . "%")
						->get();

		$companies = Company::withTrashed()->where('name' , 'LIKE' , Input::get('query') . "%")
						->orWhere('phone' , 'LIKE' , Input::get('query') . "%")
						->orWhere('email' , 'LIKE' , Input::get('query') . "%")
						->get();

		$users = User::withTrashed()->where('name' , 'LIKE' , Input::get('query') . "%")
						->orWhere('mobile' , 'LIKE' , Input::get('query') . "%")
						->orWhere('email' , 'LIKE' , Input::get('query') . "%")
						->get();


		return $this->respond([
			'data' => [
				'companies' => $companies,
				'individuals' => $individuals,
				'employees' => $users
			],
			'code' => 200
			]);
	}

	public function restore($id)
	{
		$individual = Individual::withTrashed()->find($id);
		$individual->restore();
		return $this->respond([
			'message' => 'Individual restored successfully',
			'code' => 200
			]);
	}

}

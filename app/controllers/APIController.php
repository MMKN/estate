<?php

class APIController extends \BaseController {

	public function getIndex(){

	}

	public function getCustomer($id){
		$customer = Customer::find($id);
		if(count($customer)){
			return Response::json($customer->toArray());
		}else{
			return Response::json([] , '404');
		}
	}

	/**
	 * Status code for response.
	 * 	
	 * @var integer
	 */
	protected $statusCode = 200;


	/**
	 * Set status code.
	 * 
	 * @param integer $statusCode 
	 */
	public function setStatusCode($statusCode)
	{
		$this->statusCode = $statusCode;
		return $this;
	}


	/**
	 * Return current status code.
	 * 
	 * @return integer
	 */
	public function getStatusCode()
	{
		return $this->statusCode;
	}


	/**
	 * Generic method to generate respond.
	 * 
	 * @param  array $data    
	 * @param  array  $headers 
	 * 
	 * @return mixed
	 */
	public function respond($data , $headers = [])
	{
		return Response::json($data , $this->getStatusCode() , $headers);	
	}


	/**
	 * Generic method to generate error respond.
	 * 
	 * @param  string $message 
	 * 
	 * @return mixed
	 */
	public function respondWithError($message)	
	{
		return $this->respond([
			'error' => [
				'message' => $message,
				'code'    => $this->getStatusCode()
			]
		]);
	}

	/**
	 * Return response not found with code 404.
	 * 
	 * @param  string $message 
	 * 
	 * @return mixed
	 */
	public function respondNotFound($message)
	{
		return $this->setStatusCode(404)->respondWithError($message);
	}


	/**
	 * Return response not valid with code 422.
	 * 
	 * @param  string $message 
	 */
	public function respondNotValid($message)
	{
		return $this->setStatusCode(422)->respondWithError($message);
	}

}

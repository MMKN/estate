<?php

class LeadUpdatesController extends \APIController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($lead_id)
	{
		$lead = Lead::find($lead_id);
		if(!$lead){
			return $this->respondNotFound('Lead Not Found');
		}

		return $this->respond([
			'data' => $lead->updates,
			'code' => '200'
			]);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($lead_id)
	{
		$lead = Lead::find($lead_id);
		$update = new LeadUpdate();
		$update->description = Input::get('description');

		$lead->updates()->save($update);
		return $this->respond([
			'data' => $update,
			'code' => 200
			]);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($lead_id , $update_id)
	{
		$update = LeadUpdate::find($update_id);
		if(!$update){
			return $this->respondNotFound('Update Not Found');
		}

		return $this->respond([
			'data' => $update,
			'code' => 200
			]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($lead_id , $update_id)
	{
		$update = LeadUpdate::find($update_id);
		if(!$update){
			return $this->respondNotFound('Update Not Found');
		}

		$update->description = Input::get('description');
		$update->update();

		return $this->respond([
			'data' => $update,
			'code' => '200'
			]);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($lead_id , $update_id)
	{
		$update = LeadUpdate::withTrashed()->find($update_id);

		if(!$update){
			return $this->respondNotFound('Update Not Found');
		}

		if($update->trashed()){
			$update->forceDelete();
		}else{
			$update->delete();
		}

		return $this->respond([
			'message' => 'Update deleted successfully',
			'code' => 200
			]);
	}


}

<?php

class CompaniesController extends \APIController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$companies = Company::paginate(500);

		return $this->respond([
			'data' => $companies->getCollection(),
			'code' => 200
			]);
	}



	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all() , Company::$rules);
		if($validator->fails()){
			return $this->respondNotValid($validator->errors());
		}
		$company = new Company();
		$company->name = Input::get('name');
		$company->email = Input::get('email');
		$company->address = Input::get('address');
		$company->phone = Input::get('phone');
		$company->description = Input::get('description');
		$company->save();
		return $this->respond([
			'data' => $company,
			'code' => '200'
			]);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$company = Company::find($id);

		if(!$company){
			return $this->respondNotFound('company Not Found');
		}

		return $this->respond([
			'data' => $company,
			'code' => '200'
			]);
	}



	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$company = Company::find($id);
		if(!$company){
			return $this->respondNotFound('Company Not Found');
		}
		$validator = Validator::make(Input::all() , Company::$rules);
		if($validator->fails()){
			return $this->respondNotValid($validator->errors());
		}
		$company->name = Input::get('name');
		$company->email = Input::get('email');
		$company->address = Input::get('address');
		$company->phone = Input::get('phone');
		$company->description = Input::get('description');
		$company->update();
		return $this->respond([
			'data' => $company,
			'code' => 200
			]);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$company = Company::withTrashed()->find($id);

		if(!$company){
			return $this->respondNotFound('Company Not Found');
		}

		if($company->trashed()){
			$company->forceDelete();
		}else{
			$company->delete();
		}

		return $this->respond([
			'message' => 'Company deleted successfully',
			'code' => 200
			]);
	}

	public function getArchived()
	{
		$companies = Company::onlyTrashed()->get();
		return $this->respond([
			'data' => $companies,
			'code' => 200
			]);
	}

	public function search()
	{
		$companies = Company::withTrashed()->where('name' , 'LIKE' , Input::get('query') . "%")
						->orWhere('phone' , 'LIKE' , Input::get('query') . "%")
						->orWhere('email' , 'LIKE' , Input::get('query') . "%")
						->get();

		return $this->respond([
			'data' => $companies,
			'code' => 200
			]);
	}

	public function restore($id)
	{
		$company = Company::withTrashed()->find($id);
		$company->restore();
		return $this->respond([
			'message' => 'Company restored successfully',
			'code' => 200
			]);
	}


}

<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */


	public function __construct(){

		/*
		*  Global Variable Declaration
		*  Author: Sherif Muhammad
		*  Date: 17/10/2015
		*/

   		App::setLocale(Language::whereSelected(1)->get()->first()->value);

		$data['unit_types']       = UnitType::all();
		$data['cities']           = City::all();
		$data['neighbours']       = Neighbour::all();
		$data['payment_types']    = PaymentType::all();
		$data['finishings']       = Finishing::all();
		$data['features']         = Feature::all();
		$data['floors']           = Floor::all();
		$data['objectives']       = Objective::all();
		$data['groups']           = Group::all();
		$data['languages']        = Language::all();
		// $data['admins']           = User::where('id' , '!=' , Auth::user()->id())->get();
		
		$notifications         	  = AdminNotifiy::latest()->whereUser_id(Auth::user()->id());

		// if(User::find(Auth::user()->id())->is_admin){
		//     $data['sb_notifications'] = AdminNotification::latest()->paginate(10);
		// }else{
		//     $data['sb_notifications'] = AdminNotification::latest()->where('to' , Auth::user()->id())->paginate(10);
		// }

        $data['sb_notifications'] = $notifications->get()->take(5);
        $data['count']            = count($notifications->whereRead(0)->get());
        
        $data['legals']     	  = [
        		'توكيل بيع وعقد نهائي ',
        		'توكيل بيع شهر عقاري فقط', 
        		'عقد إبتدائي فقط', 
        		'عقد نهائي فقط', 
        		'مالك أصل للوحدة', 
        		'أول توكيل من المالك الأصيل',
        		'ثاني توكيل من المالك الأصيل',
        		'ثالث توكيل من المالك الأصيل',
        		'رابع توكيل من المالك الأصيل',
        		'خامس توكيل من المالك الأصيل',
        ];

		View::share('unit_types' , $data['unit_types']);
		View::share('cities' , $data['cities']);
		View::share('neighbours' , $data['neighbours']);
		View::share('payment_types' , $data['payment_types']);
		View::share('finishings' , $data['finishings']);
		View::share('features' , $data['features']);
		View::share('floors' , $data['floors']);
		View::share('objectives' , $data['objectives']);
		View::share('groups' , $data['groups']);
		View::share('languages' , $data['languages']);
		View::share('sb_notifications' , $data['sb_notifications']);
		View::share('count' , $data['count']);
		View::share('legals' , $data['legals']);
	}

	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
